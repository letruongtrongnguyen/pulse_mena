import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/core/utils/assets_path.dart';

class AvatarWithFrameWidget extends StatelessWidget {
  final String avatarUrl;
  final String frameUrl;
  final double width;
  final double height;
  final Function onTap;
  final BoxFit fit;
  final bool isEmpty;

  const AvatarWithFrameWidget({
    Key key,
    this.avatarUrl,
    this.frameUrl,
    this.width,
    this.height,
    this.onTap,
    this.fit,
    this.isEmpty = false,
  })  : assert(width != null || height != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final double sizeRatio = 1;
    final double realWidth = width == null ? height * sizeRatio : width;
    final double realHeight = height == null ? width / sizeRatio : height;

    return GestureDetector(
      onTap: onTap != null && !isEmpty ? onTap : null,
      child: Container(
        width: realWidth,
        height: realHeight,
        // color: Colors.amber,
        child: Stack(
          children: [
            Positioned.fill(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  vertical: realHeight * 14 / 70,
                  horizontal: realWidth * 11 / 64,
                ),
                child: CachedNetworkImage(
                  imageUrl: avatarUrl,
                  // imageUrl: "",
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: imageProvider,
                        fit: fit ?? BoxFit.cover,
                      ),
                    ),
                  ),
                  placeholder: (context, url) => Container(
                    decoration: BoxDecoration(
                      color: Color(0xff424242),
                      shape: BoxShape.circle,
                    ),
                  ),
                  errorWidget: (context, url, error) => Container(
                    decoration: BoxDecoration(
                      color: Color(0xff424242),
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: isEmpty ? Colors.white : Colors.transparent,
                        width: isEmpty ? realWidth / 50 : 0,
                      ),
                    ),
                    // padding: EdgeInsets.all(realWidth / 2 * 0.1),
                    child: isEmpty
                        ? SvgPicture.asset(
                            AssetsPath.fromImagesSvg("unknown_user"),
                          )
                        : Image.asset(
                            AssetsPath.fromImagesCommon("default_user_avatar"),
                            fit: BoxFit.contain,
                          ),
                  ),
                ),
              ),
            ),
            if (frameUrl != null && frameUrl.isNotEmpty)
              SizedBox(
                width: width == null ? height * sizeRatio : width,
                height: height == null ? width / sizeRatio : height,
                child: CachedNetworkImage(
                  imageUrl: frameUrl,
                ),
              ),
          ],
        ),
      ),
    );
  }
}
