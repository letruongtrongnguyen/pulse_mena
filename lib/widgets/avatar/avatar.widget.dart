import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/core/utils/assets_path.dart';

class AvatarWidget extends StatelessWidget {
  final String avatarUrl;
  final String avatarDefault;
  final double radius;
  final double borderWidth;
  final Color borderColor;
  final Gradient borderGradient;
  final bool isOnline;
  final Function onTap;
  final BoxFit fit;

  const AvatarWidget({
    Key key,
    this.avatarUrl = "",
    this.radius = 25,
    this.borderWidth = 3,
    this.borderColor,
    this.borderGradient,
    this.isOnline = false,
    this.onTap,
    this.avatarDefault,
    this.fit,
  })  : assert(borderColor == null || borderGradient == null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.all(borderWidth),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              gradient: borderGradient == null
                  ? LinearGradient(
                      colors: [
                        borderColor ?? Colors.white,
                        borderColor ?? Colors.white
                      ],
                    )
                  : borderGradient,
            ),
            width: radius * 2,
            height: radius * 2,
            child: avatarUrl != ""
                ? CachedNetworkImage(
                    imageUrl: avatarUrl,
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.black,
                        image: DecorationImage(
                          image: imageProvider,
                          fit: fit ?? BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => Container(
                      decoration: BoxDecoration(
                        color: Color(0xff424242),
                        shape: BoxShape.circle,
                      ),
                    ),
                    errorWidget: (context, url, error) => Container(
                      decoration: BoxDecoration(
                        color: Color(0xff191919),
                        shape: BoxShape.circle,
                      ),
                      // padding: EdgeInsets.all(
                      //     avatarDefault != null ? radius * 0.5 : radius * 0.35),
                      child: avatarDefault != null
                          ? SvgPicture.asset(
                              avatarDefault,
                              color: Colors.white.withOpacity(0.5),
                            )
                          : Image.asset(
                              AssetsPath.fromImagesCommon("default_user_avatar"),
                              fit: BoxFit.contain,
                            ),
                    ),
                  )
                : Container(
                    decoration: BoxDecoration(
                      color: Color(0xff191919),
                      shape: BoxShape.circle,
                    ),
                    // padding: EdgeInsets.all(
                    //     avatarDefault != null ? radius * 0.5 : radius * 0.35),
                    child: avatarDefault != null
                        ? SvgPicture.asset(
                            avatarDefault,
                            color: Colors.white.withOpacity(0.5),
                          )
                        : Image.asset(
                      AssetsPath.fromImagesCommon("default_user_avatar"),
                      fit: BoxFit.contain,
                    ),
                  ),
          ),
          if (isOnline)
            PositionedDirectional(
              bottom: 0,
              end: 0,
              child: Container(
                width: radius / 3.5 * 2,
                height: radius / 3.5 * 2,
                decoration: BoxDecoration(
                  color: Color(0xff23ED4F),
                  shape: BoxShape.circle,
                  border: Border.all(width: radius / 15, color: Colors.white),
                ),
              ),
            )
        ],
      ),
    );
  }
}
