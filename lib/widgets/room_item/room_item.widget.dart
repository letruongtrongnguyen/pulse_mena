import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/general.config.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/room.model.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/country/country.widget.dart';

import 'audience_stat.dart';

class RoomItemWidget extends StatelessWidget {
  final Room roomData;

  RoomItemWidget({
    Key key,
    this.roomData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        getIt<CommunicateService>()
            .navigate(route: "/audio-room-screen", args: {
          "user-id": roomData.hostId,
          "room-id": roomData.id,
        });
      },
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Theme.of(context).cardTheme.color,
              borderRadius: BorderRadius.circular(12),
              boxShadow: [
                BoxShadow(
                  color: Theme.of(context).cardTheme.shadowColor,
                  offset: Offset(0, 1),
                  blurRadius: 5,
                )
              ],
            ),
            height: SizeConfig.safeBlockHorizontal * 25,
            width: double.infinity,
            child: Row(
              children: [
                Stack(
                  children: [
                    CachedNetworkImage(
                      width: SizeConfig.safeBlockHorizontal * 23,
                      imageUrl: roomData.thumbnail,
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadiusDirectional.only(
                            topStart: Radius.circular(12),
                            bottomStart: Radius.circular(12),
                          ),
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      placeholder: (context, url) => Container(
                        decoration: BoxDecoration(
                          color: Color(0xff424242),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12),
                            bottomLeft: Radius.circular(12),
                          ),
                        ),
                      ),
                      errorWidget: (context, url, error) => Container(
                        width: double.infinity,
                        height: double.infinity,
                        alignment: Alignment.center,
                        padding:
                            EdgeInsets.all(SizeConfig.safeBlockHorizontal * 5),
                        decoration: BoxDecoration(
                          color: Color(0xff424242),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12),
                            bottomLeft: Radius.circular(12),
                          ),
                        ),
                        child: Container(
                          width: SizeConfig.safeBlockHorizontal * 8,
                          height: SizeConfig.safeBlockHorizontal * 8,
                          child: SvgPicture.asset(
                            AssetsPath.fromImagesSvg("room_default_ic"),
                            color: Colors.white.withOpacity(0.3),
                          ),
                        ),
                      ),
                    ),
                    if (getIt<MeProvider>().data.value?.id == roomData.hostId ||
                        roomData.isOfficial)
                      PositionedDirectional(
                        bottom: 0,
                        start: 0,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [Color(0xffE2C046), Color(0xffF2AD00)],
                            ),
                            borderRadius: BorderRadiusDirectional.only(
                              topEnd: Radius.circular(4),
                              bottomEnd: Radius.circular(4),
                              bottomStart: Radius.circular(12),
                            ),
                          ),
                          padding: EdgeInsets.symmetric(
                            vertical: SizeConfig.safeBlockHorizontal * 1,
                            horizontal: SizeConfig.safeBlockHorizontal * 1.5,
                          ),
                          child: CustomText(
                            roomData.isOfficial
                                ? "common.official"
                                : "room.tag.mine",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                              fontSize: 12,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
                SizedBox(width: SizeConfig.safeBlockHorizontal * 3),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        // CountryWidget(
                        //   code: roomData.countryCode,
                        //   widthFlag: SizeConfig.safeBlockHorizontal * 7,
                        //   showCountryName: false,
                        //   textStyle: TextStyle(
                        //     fontSize: 11,
                        //     color: Color(0xffd4d4d4),
                        //   ),
                        // ),
                        // SizedBox(
                        //   width: SizeConfig.safeBlockHorizontal * 2,
                        // ),
                        Container(
                          constraints: BoxConstraints(
                            maxWidth: SizeConfig.safeBlockHorizontal * 55,
                          ),
                          child: CustomText(
                            Formatter.clearZeroWidthSpace(
                              Formatter.capitalize("${roomData.name}"),
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            softWrap: false,
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: SizeConfig.safeBlockHorizontal * 4),
                    CountryWidget(
                      code: roomData.countryCode,
                      widthFlag: SizeConfig.safeBlockHorizontal * 6,
                      textStyle: TextStyle(
                        fontSize: 11,
                        color: Color(0xffd4d4d4),
                      ),
                    ),
                    SizedBox(height: SizeConfig.safeBlockHorizontal * 2),
                    SizedBox(
                      width: SizeConfig.safeBlockHorizontal * 40,
                      child: CustomText(
                        Formatter.clearZeroWidthSpace(
                          Formatter.capitalize(roomData.description),
                        ),
                        // roomData.description,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        textAlign: TextAlign.start,
                        softWrap: false,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          PositionedDirectional(
            bottom: 10,
            end: 10,
            child: AudienceStat(
              roomId: roomData.uid,
            ),
          ),
        ],
      ),
    );
  }
}
