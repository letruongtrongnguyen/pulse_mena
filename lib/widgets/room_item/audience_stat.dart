import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/widgets/basic/circle_ic.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:lottie/lottie.dart';

class AudienceStat extends StatelessWidget {
  final String roomId;

  AudienceStat({
    Key key,
    this.roomId,
  }) : super(key: key);

  final double _avatarRadius = SizeConfig.safeBlockHorizontal * 2.5;

  int getNumberOfAudience(Map<String, dynamic> data) {
    if (data.containsKey("$roomId")) {
      return data["$roomId"]["users_count"] ?? 0;
    }

    return 0;
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: getIt<RoomProvider>().audienceData,
        builder: (context, audienceData, child) {
          final int audienceNumber = getNumberOfAudience(audienceData);

          return Container(
            child: Row(
              children: [
                Lottie.asset(
                  AssetsPath.fromImagesJson("sound_animation"),
                  width: _avatarRadius * 2,
                  height: _avatarRadius * 2,
                  fit: BoxFit.fill,
                ),
                SizedBox(width: 5),
                CircleIconWidget(
                  icon: SvgPicture.asset(
                    AssetsPath.fromImagesSvg("avatar_default_ic"),
                  ),
                  backgroundColor: Color(0xff576B8B),
                  radius: _avatarRadius,
                ),
                Container(
                  alignment: AlignmentDirectional.centerStart,
                  margin: EdgeInsetsDirectional.only(start: 5),
                  width: 26,
                  child: CustomText(
                    Formatter.clearZeroWidthSpace("$audienceNumber"),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    softWrap: false,
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 12,
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
