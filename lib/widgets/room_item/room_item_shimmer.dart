import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:shimmer/shimmer.dart';

class RoomItemShimmer extends StatelessWidget {
  final _height = SizeConfig.safeBlockHorizontal * 22;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).cardTheme.color,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            color: Theme.of(context).cardTheme.shadowColor,
            offset: Offset(0, 1),
            blurRadius: 5,
          )
        ],
      ),
      child: Shimmer.fromColors(
        baseColor: Theme.of(context).accentColor.withOpacity(0.1),
        highlightColor:
            Theme.of(context).accentColor.withOpacity(0.05),
        child: Row(
          children: [
            Container(
              width: _height,
              height: _height,
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadiusDirectional.only(
                  topStart: Radius.circular(12),
                  bottomStart: Radius.circular(12),
                ),
              ),
            ),
            SizedBox(width: SizeConfig.safeBlockHorizontal * 4),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: SizeConfig.safeBlockHorizontal * 30,
                  height: 14,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(3),
                  ),
                ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 2),
                Container(
                  width: SizeConfig.safeBlockHorizontal * 20,
                  height: 14,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(3),
                  ),
                ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 2),
                Container(
                  width: SizeConfig.safeBlockHorizontal * 50,
                  height: 14,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(3),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
