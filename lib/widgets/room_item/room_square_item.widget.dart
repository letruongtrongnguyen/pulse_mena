import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/room.model.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/country/country.widget.dart';

import 'audience_stat.dart';

class RoomSquareItem extends StatelessWidget {
  final double width;
  final double height;
  final Room roomData;

  const RoomSquareItem({
    Key key,
    this.width,
    this.height,
    this.roomData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double widthDefault = SizeConfig.safeBlockHorizontal * 10;
    final double heightDefault = SizeConfig.safeBlockHorizontal * 10;

    return GestureDetector(
      onTap: () {
        getIt<CommunicateService>().navigate(
            route: "/audio-room-screen", args: {"user-id": roomData?.hostId});
      },
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: Container(
              width: width ?? widthDefault,
              height: height ?? heightDefault,
              // decoration: BoxDecoration(
              //   color: Colors.amber,
              // ),
              child: CachedNetworkImage(
                width: width ?? widthDefault,
                height: height ?? heightDefault,
                imageUrl: roomData?.thumbnail ?? "",
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) => Container(
                  decoration: BoxDecoration(
                    color: Color(0xff424242),
                  ),
                ),
                errorWidget: (context, url, error) => Container(
                  width: double.infinity,
                  height: double.infinity,
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 5),
                  decoration: BoxDecoration(
                    color: Color(0xff424242),
                  ),
                  child: Container(
                    width: SizeConfig.safeBlockHorizontal * 8,
                    height: SizeConfig.safeBlockHorizontal * 8,
                    child: SvgPicture.asset(
                      AssetsPath.fromImagesSvg("room_default_ic"),
                      color: Colors.white.withOpacity(0.3),
                    ),
                  ),
                ),
              ),
              // child: Image.network(
              //   "https://s3-alpha-sig.figma.com/img/e07b/254d/dbf597124d44eaed6cd6ff02de9171ee?Expires=1616371200&Signature=Vm8QrqvDD21g6Y5AhbfvPyOB94JLE1rh9yNLWeYCFdhoSbLMPww8xY2Q1jQCHw~w1Q7Bzgza-Eb70dNZIKfCR6rWqXfkOn3fMLVrARi6iKOqUDYaM5-KE7hwuq9RNCCU1xMcGhP17c3W4So2PsXdiBAS8hxG293QXE5IgA7oBq6Zyp6FhoLM7Dc15qcHbjv4ed9SCb3ae5nWAOFMe6eJJSImNG0j24Iy7JYsDeliwgtOfH04cCiAFHHtIzbshVb6UnZXqDEbXpe97-6dvGLNVbo4vAZEz0dZOxlrzBRU~5X74uAanZbwIisPyIYzGrHgS~OUr4ovwapL0b~4fmwwGQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
              //   width: width ?? widthDefault,
              //   height: height ?? heightDefault,
              //   fit: BoxFit.cover,
              // ),
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: Container(
              width: width ?? widthDefault,
              height: height ?? heightDefault,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.black.withOpacity(0.3),
                    Colors.black.withOpacity(0),
                    Colors.black.withOpacity(0.5),
                  ],
                ),
              ),
            ),
          ),
          if (roomData.isOfficial)
            PositionedDirectional(
              start: 0,
              top: 10,
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 8,
                  vertical: 6,
                ),
                decoration: BoxDecoration(
                  color: Color(0xff0C2424).withOpacity(0.6),
                  borderRadius: BorderRadiusDirectional.only(
                    topEnd: Radius.circular(8),
                    bottomEnd: Radius.circular(8),
                  ),
                ),
                child: CustomText(
                  "common.official",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.italic,
                    color: Color(0xffD5F55A),
                  ),
                ),
              ),
            ),
          PositionedDirectional(
            bottom: 10,
            start: 10,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                CountryWidget(
                  code: roomData?.countryCode ?? "vn",
                  widthFlag: SizeConfig.safeBlockHorizontal * 7,
                  showCountryName: false,
                ),
                SizedBox(height: 2),
                Container(
                  constraints: BoxConstraints(
                    maxWidth: (width ?? widthDefault) - 20,
                  ),
                  child: CustomText(
                    Formatter.clearZeroWidthSpace(
                      Formatter.capitalize("${roomData?.name ?? ""}"),
                    ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    softWrap: false,
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                        color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
          PositionedDirectional(
            top: 10,
            end: 10,
            child: AudienceStat(
              roomId: roomData.uid,
            ),
          ),
        ],
      ),
    );
  }
}
