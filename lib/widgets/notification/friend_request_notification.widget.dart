import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/notification.model.dart';
import 'package:flutterbase/core/@services/services/relationship.service.dart';
import 'package:flutterbase/core/@services/utils/friend.status.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/basic/popup/center_popup.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class FriendRequestNotification extends StatelessWidget {
  final Notif notificationData;

  const FriendRequestNotification({
    Key key,
    this.notificationData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextStyle descriptionStyle = TextStyle(
      fontWeight: notificationData.isRead ? FontWeight.normal : FontWeight.bold,
      color: notificationData.isRead
          ? Colors.white.withOpacity(0.6)
          : Colors.white,
      fontSize: 12,
    );

    return GestureDetector(
      onTap: () {
        notificationData.read();
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.safeBlockHorizontal * 4,
        ),
        color: Colors.transparent,
        child: Column(
          children: [
            Stack(
              children: [
                Row(
                  children: [
                    AvatarWidget(
                      avatarUrl: notificationData.user.avatarUrl,
                      radius: SizeConfig.safeBlockHorizontal * 6,
                      borderWidth: 0,
                    ),
                    SizedBox(width: SizeConfig.safeBlockHorizontal * 4),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 3,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              constraints: BoxConstraints(
                                maxWidth: SizeConfig.safeBlockHorizontal * 50,
                              ),
                              child: CustomText(
                                Formatter.clearZeroWidthSpace(
                                  notificationData.user.fullName,
                                ),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                softWrap: false,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                            SizedBox(
                                height: SizeConfig.safeBlockHorizontal * 1),
                            Row(
                              children: [
                                Container(
                                  constraints: BoxConstraints(
                                    maxWidth:
                                        SizeConfig.safeBlockHorizontal * 40,
                                  ),
                                  child: CustomText(
                                    Formatter.clearZeroWidthSpace(
                                      notificationData.user.fullName,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    softWrap: false,
                                    style: descriptionStyle,
                                  ),
                                ),
                                CustomText(
                                  " ",
                                  style: descriptionStyle,
                                ),
                                CustomText(
                                  "notification.friendRequest.content",
                                  style: descriptionStyle,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Positioned(
                  right: 0,
                  top: 10,
                  child: CustomText(
                    "${Formatter.formatDate(DateTime.fromMillisecondsSinceEpoch(notificationData.time * 1000))}",
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.white.withOpacity(0.6),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(
                left: SizeConfig.safeBlockHorizontal * 16,
              ),
              padding:
                  EdgeInsets.only(bottom: SizeConfig.safeBlockHorizontal * 3),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Colors.white.withOpacity(0.1),
                    width: 1,
                  ),
                ),
              ),
              child: Column(
                children: [
                  BuildButton(notificationData: notificationData),
                  SizedBox(height: 10),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BuildButton extends StatefulWidget {
  final Notif notificationData;

  const BuildButton({
    Key key,
    this.notificationData,
  }) : super(key: key);

  @override
  _BuildButtonState createState() => _BuildButtonState();
}

class _BuildButtonState extends State<BuildButton> {
  bool loadingAccept = false;
  bool loadingCancel = false;

  void acceptFriendRequest() async {
    setState(() {
      loadingAccept = true;
    });

    final res = await RelationshipService.accept(
      senderId: widget.notificationData.user.id,
    );

    if (res)
      widget.notificationData.handleFriendRequest(type: FriendStatus.ACCEPTED);
    else {
      _openCenterPopup(context);
      widget.notificationData.read();
    }

    setState(() {
      loadingAccept = false;
    });
  }

  void cancelFriendRequest() async {
    setState(() {
      loadingCancel = true;
    });

    final res = await RelationshipService.decline(
      senderId: widget.notificationData.user.id,
    );

    if (res)
      widget.notificationData.handleFriendRequest(type: FriendStatus.DECLINE);
    else {
      _openCenterPopup(context);
      widget.notificationData.read();
    }

    setState(() {
      loadingCancel = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.notificationData.action == FriendStatus.ACCEPTED)
      return Container(
        alignment: Alignment.centerLeft,
        child: CustomText(
          "common.accepted",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            fontSize: 12,
            color: Color(0xff00FFE0),
          ),
        ),
      );

    if (widget.notificationData.action == FriendStatus.DECLINE)
      return Container(
        alignment: Alignment.centerLeft,
        child: CustomText(
          "common.canceled",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            fontSize: 12,
            color: Color(0xff7f7f7f).withOpacity(0.8),
          ),
        ),
      );

    return Row(
      children: [
        Expanded(
          child: GestureDetector(
            onTap: loadingCancel || loadingAccept ? () {} : acceptFriendRequest,
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(
                vertical: SizeConfig.safeBlockHorizontal * 2,
              ),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Color(0xff81FFCA), Color(0xff2CB7FF)],
                ),
                borderRadius: BorderRadius.circular(50),
              ),
              child: CustomText(
                loadingAccept ? "common.loading" : "common.accept",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: SizeConfig.safeBlockHorizontal * 5,
        ),
        Expanded(
          child: GestureDetector(
            onTap: loadingCancel || loadingAccept ? () {} : cancelFriendRequest,
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(
                vertical: SizeConfig.safeBlockHorizontal * 2,
              ),
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.2),
                borderRadius: BorderRadius.circular(50),
              ),
              child: CustomText(
                loadingCancel ? "common.loading" : "common.cancel",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _openCenterPopup(BuildContext context) {
    showCenterPopup(
      context: context,
      width: SizeConfig.safeBlockHorizontal * 92,
      height: 160,
      backgroundColor: Color(0xff2f2f2f),
      isDismissible: true,
      borderRadius: BorderRadius.circular(20),
      content: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            CustomText(
              "Something Error",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
            SizedBox(height: 30),
            GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Container(
                width: SizeConfig.safeBlockHorizontal * 45,
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(
                  vertical: SizeConfig.safeBlockHorizontal * 3,
                ),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Color(0xff81FFCA), Color(0xff2CB7FF)],
                  ),
                  borderRadius: BorderRadius.circular(50),
                ),
                child: CustomText(
                  "common.ok",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      // actions: [
      //   {
      //     "title": "OK",
      //     "action": () => print("OK"),
      //   },
      // ],
    );
  }
}
