import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:shimmer/shimmer.dart';

class GiftNotificationShimmerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.safeBlockHorizontal * 4,
      ),
      child: Row(
        children: [
          Shimmer(
            gradient: LinearGradient(
              colors: [
                Colors.yellow,
                Colors.lightGreen,
                Colors.red,
                Colors.yellow,
                Colors.lightGreen,

                // Colors.lightGreen,
                // Colors.yellow,
              ],
            ),
            child: Text("aasdasfafafsf"),
          )
        ],
      ),
    );
  }
}
