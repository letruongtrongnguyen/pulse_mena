import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class GiftNotificationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.safeBlockHorizontal * 4,
      ),
      child: Stack(
        children: [
          Row(
            children: [
              AvatarWidget(
                avatarUrl:
                    "https://s3-alpha-sig.figma.com/img/d33b/c770/59c17c13e14fc514ecb5ece753182fba?Expires=1603065600&Signature=HRSZcAU5ZeILNr1H8B1-G5nlj1aHDtijoYs0fSlD5gxAk6jrAt7Bpm-azKMDpIuZfw3bvbUfUdlnTtgjTD~VaAT9YOn2GtFrO0H7NQWtN1z3vzBrrG1U7~A1LsRnmcpqDltgwLuJmt0x19YURa4dg4jzwY-esNLPUYBt4kAd7ICCKvaCnfPbCc-CwKcNblQVsykmn7xm9gw79qF7Q2WsnG50qLDthCYawYWbNdl9Jw9s1h8LMGUyZmawZz1Zjbad9M2-CCPiwaXv2BiNEoMPkkZdtZ~7lpEuEZE2vWmjMwm4Zl-VDSN5Y3OtTn0~tFD5akhqT4JLmBuMcfNHeMc~Dg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
                radius: SizeConfig.safeBlockHorizontal * 6,
                borderWidth: 0,
              ),
              SizedBox(width: SizeConfig.safeBlockHorizontal * 4),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(
                    vertical: SizeConfig.safeBlockHorizontal * 3,
                  ),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Colors.white.withOpacity(0.1),
                        width: 1,
                      ),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText(
                        "Robertson",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                        ),
                      ),
                      SizedBox(height: SizeConfig.safeBlockHorizontal * 1),
                      CustomText(
                        "Robertson sent you x1 Roses",
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white.withOpacity(0.6),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            right: 0,
            top: 10,
            child: CustomText(
              "06/08/2020",
              style: TextStyle(
                fontSize: 12,
                color: Colors.white.withOpacity(0.6),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
