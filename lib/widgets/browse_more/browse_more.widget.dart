import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class BrowseMoreWidget extends StatefulWidget {
  final Function onTap;

  const BrowseMoreWidget({
    Key key,
    this.onTap,
  }) : super(key: key);

  @override
  _BrowseMoreWidgetState createState() => _BrowseMoreWidgetState();
}

class _BrowseMoreWidgetState extends State<BrowseMoreWidget>
    with TickerProviderStateMixin {
  Animation _arrowAnimation;
  AnimationController _arrowAnimationController;

  @override
  void initState() {
    super.initState();

    // _arrowAnimationController = AnimationController(
    //   vsync: this,
    //   duration: Duration(milliseconds: 500),
    // );
    // _arrowAnimation =
    //     Tween(begin: -2.0, end: 2.0).animate(_arrowAnimationController);
    //
    // _arrowAnimationController.addStatusListener((AnimationStatus status) {
    //   if (status == AnimationStatus.completed) {
    //     _arrowAnimationController.repeat(reverse: true);
    //   }
    // });

    // _arrowAnimationController.forward();
  }

  @override
  void dispose() {
    super.dispose();
    // _arrowAnimationController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 8,
          vertical: 5,
        ),
        decoration: BoxDecoration(
            border: Border.all(
              color: Color(0xff717171),
              width: 1,
            ),
            borderRadius: BorderRadius.circular(8)),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            CustomText(
              "common.browseMoreRooms",
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w300,
              ),
            ),
            SizedBox(width: 15),
            // AnimatedBuilder(
            //   animation: _arrowAnimationController,
            //   builder: (context, child) {
            //     return Transform.translate(
            //       offset: Offset(0.0, _arrowAnimation.value),
            //       child: SvgPicture.asset(
            //         AssetsPath.fromImagesSvg("arrow_down_square_ic"),
            //       ),
            //     );
            //   },
            // ),
            SvgPicture.asset(
              AssetsPath.fromImagesSvg("arrow_down_square_ic"),
              color: Theme.of(context).iconTheme.color,
            )
          ],
        ),
      ),
    );
  }
}
