import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class ConversationItemWidget extends StatelessWidget {
  final String title;
  final String avatarUrl;
  final bool isNew;

  const ConversationItemWidget({
    Key key,
    this.title,
    this.avatarUrl,
    this.isNew = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.safeBlockHorizontal * 92,
      color: Colors.transparent,
      child: Row(
        children: [
          AvatarWidget(
            avatarUrl: avatarUrl,
            radius: SizeConfig.safeBlockHorizontal * 8,
            borderWidth: 0,
          ),
          SizedBox(width: SizeConfig.safeBlockHorizontal * 4),
          Column(
            children: [
              SizedBox(
                width: SizeConfig.safeBlockHorizontal * 72,
                child: Column(
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        CustomText(
                          title,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          softWrap: false,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                          ),
                        ),
                        Spacer(),
                        CustomText(
                          "02:00 PM",
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.white.withOpacity(0.6),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 8),
              SizedBox(
                width: SizeConfig.safeBlockHorizontal * 72,
                child: Column(
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                          constraints: BoxConstraints(
                            maxWidth: SizeConfig.safeBlockHorizontal * 50,
                          ),
                          child: CustomText(
                            Formatter.clearZeroWidthSpace(
                              Formatter.capitalize(
                                  "natoque penatibus et magnis dis parturient montes nascetur ridiculus mus"),
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            softWrap: false,
                            style: TextStyle(
                              fontWeight:
                                  isNew ? FontWeight.bold : FontWeight.normal,
                              fontSize: 12,
                            ),
                          ),
                        ),
                        Spacer(),
                        if (isNew)
                          Container(
                            width: SizeConfig.safeBlockHorizontal * 2.5,
                            height: SizeConfig.safeBlockHorizontal * 2.5,
                            decoration: BoxDecoration(
                                color: Color(0xffff0000),
                                shape: BoxShape.circle),
                          )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
