import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/i18n/i18n.dart';
import 'package:flutterbase/core/utils/formatter.dart';

class CustomText extends StatelessWidget {
  final String data;
  final Key key;
  final TextStyle style;
  final StrutStyle strutStyle;
  final TextAlign textAlign;
  final TextDirection textDirection;
  final Locale locale;
  final bool softWrap;
  final TextOverflow overflow;
  final double textScaleFactor;
  final int maxLines;
  final String semanticsLabel;
  final TextWidthBasis textWidthBasis;
  final TextHeightBehavior textHeightBehavior;
  final bool uppercase;
  final bool capitalize;
  final bool clearZeroSpace;

  CustomText(
    this.data, {
    this.key,
    this.style,
    this.strutStyle,
    this.textAlign,
    this.textDirection,
    this.locale,
    this.softWrap,
    this.overflow,
    this.textScaleFactor,
    this.maxLines,
    this.semanticsLabel,
    this.textWidthBasis,
    this.textHeightBehavior,
    this.capitalize = false,
    this.uppercase = false,
    this.clearZeroSpace = false,
  });

  Widget build(BuildContext context) {
    var _style = TextStyle(fontSize: 14 * SizeConfig.screenWidth / 375);

    if (this.style != null && this.style.fontSize != null) {
      _style = style.merge(TextStyle(
        fontSize: this.style.fontSize * SizeConfig.screenWidth / 375,
      ));
    } else if (this.style != null)
      _style = style.merge(
        TextStyle(fontSize: 14 * SizeConfig.screenWidth / 375),
      );

    String value = I18n.get(this.data);

    if (this.capitalize) value = Formatter.formatCapitalizeString(value);

    if (this.uppercase) value = value.toUpperCase();

    if (this.clearZeroSpace) value = Formatter.clearZeroWidthSpace(value);

    return Text(
      value,
      key: this.key,
      style: _style,
      strutStyle: this.strutStyle,
      textAlign: this.textAlign,
      textDirection: this.textDirection,
      locale: this.locale,
      softWrap: this.softWrap,
      overflow: this.overflow,
      textScaleFactor: this.textScaleFactor,
      maxLines: this.maxLines,
      semanticsLabel: this.semanticsLabel,
      textWidthBasis: this.textWidthBasis,
      textHeightBehavior: this.textHeightBehavior,

    );
  }
}
