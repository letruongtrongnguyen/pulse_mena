import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';

import '../text.widget.dart';

void showCenterPopup({
  BuildContext context,
  Color barrierColor,
  Color backgroundColor,
  double width,
  double height,
  bool isDismissible = true,
  BorderRadius borderRadius,
  Widget content,
  List<Map<String, dynamic>> actions,
}) {
  assert(context != null);

  showModal(
    context: context,
    configuration: FadeScaleTransitionConfiguration(
      barrierDismissible: isDismissible,
      barrierColor: barrierColor ?? Colors.black.withOpacity(0.3),
    ),
    builder: (ctx) => _CenterPopup(
      context,
      width: width ?? 100,
      height: height ?? 200,
      actions: actions,
      borderRadius: borderRadius,
      backgroundColor: backgroundColor,
      content: content,
    ),
  );
}

class _CenterPopup extends StatelessWidget {
  final BuildContext popupContext;
  final double width;
  final double height;
  final BorderRadius borderRadius;
  final Color backgroundColor;
  final Widget content;
  final List<Map<String, dynamic>> actions;

  _CenterPopup(
    this.popupContext, {
    this.width,
    this.height,
    this.actions,
    this.borderRadius,
    this.content,
    this.backgroundColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: (this.width != null && SizeConfig.screenWidth > this.width)
            ? (SizeConfig.screenWidth -
                    SizeConfig.paddingLeft -
                    SizeConfig.paddingRight -
                    this.width) /
                2
            : 0,
        vertical: (SizeConfig.screenHeight > this.height)
            ? (SizeConfig.screenHeight -
                    SizeConfig.paddingTop -
                    SizeConfig.paddingBottom -
                    this.height) /
                2
            : 0,
      ),
      child: ClipRRect(
        borderRadius: borderRadius ?? BorderRadius.circular(15),
        child: Material(
          color: backgroundColor ?? Colors.white,
          child: Column(
            children: [
              Container(
                height: actions != null ? this.height - 60 : this.height,
                width: this.width,
                child: content ?? SizedBox.shrink(),
              ),
              if (actions != null) Spacer(),
              if (actions != null)
                Row(
                  children: [
                    ...actions
                        .asMap()
                        .map(
                          (i, e) => MapEntry(
                            i,
                            GestureDetector(
                              onTap: () {
                                if (e["action"] != null) e["action"]();
                                Navigator.pop(context);
                              },
                              child: Container(
                                width: this.width / actions.length,
                                height: 60,
                                child: Center(
                                  child: CustomText(
                                    e["title"],
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  border: BorderDirectional(
                                    top: BorderSide(
                                      width: 1,
                                      color: Color(0xffebecec),
                                    ),
                                    end: BorderSide(
                                      width: i == actions.length - 1 ? 0 : 1,
                                      color: i == actions.length - 1
                                          ? Colors.transparent
                                          : Color(0xffebecec),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                        .values
                        .toList(),
                  ],
                ),
            ],
          ),
        ),
      ),
    );
  }
}
