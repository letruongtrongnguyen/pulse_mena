import 'package:flutter/material.dart';

class CircleIconWidget extends StatelessWidget {
  final Widget icon;
  final double radius;
  final EdgeInsetsGeometry padding;
  final Color backgroundColor;
  final Gradient backgroundGradient;

  const CircleIconWidget({
    Key key,
    @required this.icon,
    this.radius = 20,
    this.backgroundColor,
    this.backgroundGradient,
    this.padding,
  })  : assert(backgroundColor == null || backgroundGradient == null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding ?? EdgeInsets.all(radius * 2.5 / 5),
      width: radius * 2,
      height: radius * 2,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        gradient: backgroundGradient == null
            ? LinearGradient(
                colors: [
                  backgroundColor ?? Colors.white,
                  backgroundColor ?? Colors.white
                ],
              )
            : backgroundGradient,
      ),
      child: icon,
    );
  }
}
