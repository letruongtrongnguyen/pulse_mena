import 'package:flutter/material.dart';

class ImagePlaceholderWidget extends StatelessWidget {
  final ImageProvider image;
  final Widget placeholder;
  final double width;
  final double height;
  final BoxFit fit;
  final bool matchTextDirection;

  const ImagePlaceholderWidget({
    Key key,
    this.image,
    this.placeholder,
    this.width,
    this.height,
    this.fit,
    this.matchTextDirection = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image(
      image: image,
      width: width,
      height: height,
      fit: fit,
      matchTextDirection: matchTextDirection,
      frameBuilder: (context, child, frame, wasSynchronouslyLoaded) {
        if (wasSynchronouslyLoaded) {
          return child;
        } else {
          return AnimatedSwitcher(
            duration: const Duration(milliseconds: 400),
            child: frame != null ? child : placeholder,
          );
        }
      },
    );
  }
}
