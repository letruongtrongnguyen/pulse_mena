import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class ProgressBarWidget extends StatelessWidget {
  final double width;
  final double height;
  final double ratio;
  final double percent;
  final Color color;
  final Color backgroundColor;
  final List<Color> gradientColors;
  final List<Color> gradientBackgroundColors;

  const ProgressBarWidget({
    Key key,
    @required this.width,
    this.ratio,
    this.percent = 20,
    this.height = 15.0,
    this.color,
    this.backgroundColor,
    this.gradientColors,
    this.gradientBackgroundColors,
  })  : assert(color != null || gradientColors != null),
        assert(backgroundColor != null || gradientBackgroundColors != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final realHeight = ratio != null ? width * ratio : height;

    return Stack(
      children: [
        Container(
          width: width,
          height: realHeight,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: backgroundColor != null
                  ? [backgroundColor, backgroundColor]
                  : gradientBackgroundColors,
            ),
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        Positioned(
          left: 0,
          child: Container(
            width: width * percent / 100 < realHeight
                ? realHeight
                : width * percent / 100,
            height: realHeight,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: color != null ? [color, color] : gradientColors,
              ),
              borderRadius: BorderRadius.circular(100),
            ),
          ),
        ),
        Positioned.fill(
          child: Container(
            alignment: Alignment.center,
            child: CustomText(
              "$percent%",
              style: TextStyle(
                fontSize: realHeight * 10 / 17,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        )
      ],
    );
  }
}
