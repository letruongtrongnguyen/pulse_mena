import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/user.model.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/@services/utils/gender.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/avatar/avatar_with_frame.dart';
import 'package:flutterbase/widgets/basic/circle_ic.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/country/country.widget.dart';
import 'package:flutterbase/widgets/user/user_shimmer.widget.dart';

class UserWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final me = getIt<MeProvider>().data;
    final loading = getIt<MeProvider>().loading;

    return AnimatedBuilder(
      animation: Listenable.merge([me, loading]),
      builder: (ctx, child) {
        if (loading.value) return UserShimmerWidget();

        return GestureDetector(
          onTap: () {
            getIt<CommunicateService>().navigate(
              route: "/user-profile",
              args: {
                "user-id": me.value.id,
              },
            );
          },
          child: Container(
            color: Colors.transparent,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                AvatarWithFrameWidget(
                  avatarUrl: me.value.avatarUrl,
                  width: SizeConfig.safeBlockHorizontal * 20,
                  frameUrl: me.value.pendantUrl,
                  // borderWidth: SizeConfig.safeBlockHorizontal * 0.5,
                  // radius: SizeConfig.safeBlockHorizontal * 8,
                ),
                SizedBox(
                  width: SizeConfig.safeBlockHorizontal * 2,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      constraints: BoxConstraints(
                        maxWidth: SizeConfig.safeBlockHorizontal * 40,
                      ),
                      child: CustomText(
                        Formatter.clearZeroWidthSpace(me.value.fullName),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        softWrap: false,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    IdentificationAndCountry(),
                    SizedBox(height: 5),
                    GenderAndAge(),

                    // Row(
                    //   children: [
                    //     GenderAndAge(),
                    //     SizedBox(width: 10),
                    //     Container(
                    //       padding: EdgeInsets.symmetric(
                    //         horizontal: 15,
                    //         vertical: 2,
                    //       ),
                    //       decoration: BoxDecoration(
                    //         border: Border.all(
                    //           color: me.value.level.borderColor,
                    //           width: 1,
                    //         ),
                    //         gradient: LinearGradient(
                    //           begin: me.value.level.gradientBegin,
                    //           end: me.value.level.gradientEnd,
                    //           colors: me.value.level.colors,
                    //         ),
                    //         borderRadius: BorderRadius.circular(50),
                    //       ),
                    //       child: CustomText(
                    //         "Lv.${me.value.level.value}",
                    //         style: TextStyle(
                    //           fontSize: 12,
                    //           fontWeight: FontWeight.bold,
                    //           color: Colors.white,
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                  ],
                ),
                Spacer(),
                Container(
                  color: Colors.transparent,
                  padding: EdgeInsetsDirectional.fromSTEB(
                    SizeConfig.safeBlockVertical * 2,
                    SizeConfig.safeBlockVertical * 2,
                    0,
                    SizeConfig.safeBlockVertical * 2,
                  ),
                  child: SvgPicture.asset(
                    AssetsPath.fromImagesSvg("arrow_forward_ios_ic"),
                    color: Colors.white,
                    height: 20,
                    matchTextDirection: true,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

// class UpgradePlatinum extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Row(
//       crossAxisAlignment: CrossAxisAlignment.center,
//       children: [
//         Image.asset(
//           AssetsPath.fromImagesCommon("gold_diamond_ic"),
//         ),
//         SizedBox(
//           width: SizeConfig.safeBlockHorizontal * 2,
//         ),
//         Text(
//           "Upgrade Platinum",
//           style: TextStyle(
//             color: Color(0xffFFD487),
//             fontSize: 14,
//             fontWeight: FontWeight.bold,
//             decoration: TextDecoration.underline,
//           ),
//         ),
//       ],
//     );
//   }
// }

// class UserInfo extends StatelessWidget {
//   final User me = getIt<MeProvider>().data.value;
//
//   @override
//   Widget build(BuildContext context) {
//     return Row(
//       crossAxisAlignment: CrossAxisAlignment.center,
//       children: [
//         AvatarWidget(
//           avatarUrl: me.avatarUrl,
//           borderWidth: SizeConfig.safeBlockHorizontal * 0.5,
//           radius: SizeConfig.safeBlockHorizontal * 8,
//         ),
//         SizedBox(
//           width: SizeConfig.safeBlockHorizontal * 2,
//         ),
//         // AvatarWithFrameWidget(
//         //   onTap: () {
//         //     getIt<CommunicateService>().navigate(route: "/my-profile");
//         //   },
//         //   avatarUrl: me.avatarUrl,
//         //   width: SizeConfig.safeBlockHorizontal * 18,
//         //   frameUrl: (me.pendantUrl != "" && me.pendantUrl != null)
//         //       ? me.pendantUrl
//         //       : me.level.frameUrl,
//         // ),
//         SizedBox(width: SizeConfig.safeBlockHorizontal * 2),
//         Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Container(
//               constraints: BoxConstraints(
//                 maxWidth: SizeConfig.safeBlockHorizontal * 40,
//               ),
//               child: CustomText(
//                 Formatter.clearZeroWidthSpace(
//                     getIt<MeProvider>().data.value.fullName),
//                 overflow: TextOverflow.ellipsis,
//                 maxLines: 1,
//                 softWrap: false,
//                 style: TextStyle(
//                   fontWeight: FontWeight.bold,
//                   fontSize: 15,
//                 ),
//               ),
//             ),
//             SizedBox(height: 5),
//             IdentificationAndCountry(),
//             SizedBox(height: 5),
//             Row(
//               children: [
//                 GenderAndAge(),
//                 SizedBox(width: 10),
//                 Container(
//                   padding: EdgeInsets.symmetric(
//                     horizontal: 15,
//                     vertical: 2,
//                   ),
//                   decoration: BoxDecoration(
//                     border: Border.all(
//                       color: me.level.borderColor,
//                       width: 1,
//                     ),
//                     gradient: LinearGradient(
//                       begin: me.level.gradientBegin,
//                       end: me.level.gradientEnd,
//                       colors: me.level.colors,
//                     ),
//                     borderRadius: BorderRadius.circular(50),
//                   ),
//                   child: CustomText(
//                     "Lv.${me.level.value}",
//                     style: TextStyle(
//                       fontSize: 12,
//                       fontWeight: FontWeight.bold,
//                       color: Colors.white,
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ],
//         ),
//         Spacer(),
//         GestureDetector(
//           onTap: () {
//             getIt<CommunicateService>().navigate(route: "/edit-profile");
//           },
//           child: SvgPicture.asset(
//             AssetsPath.fromImagesSvg("arrow_forward_ios_ic"),
//             color: Colors.white,
//             height: 20,
//           ),
//         ),
//       ],
//     );
//   }
// }

class GenderAndAge extends StatelessWidget {
  final User me = getIt<MeProvider>().data.value;

  @override
  Widget build(BuildContext context) {
    String iconPath = me.gender == Gender.FEMALE
        ? AssetsPath.fromImagesSvg('female_gender_ic')
        : AssetsPath.fromImagesSvg('male_gender_ic');

    return Container(
      padding: EdgeInsetsDirectional.fromSTEB(7, 3, 12, 3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: Colors.white.withOpacity(0.1),
      ),
      child: Row(
        children: [
          ShaderMask(
            shaderCallback: (bounds) {
              return LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [Color(0xffDF0C8B), Color(0xffF486C8)],
              ).createShader(bounds);
            },
            child: SvgPicture.asset(
              iconPath,
              height: SizeConfig.safeBlockHorizontal * 5,
            ),
          ),
          SizedBox(width: 5),
          CustomText(
            "${me.age == null ? "--" : me.age}",
            style: TextStyle(
              fontSize: 14,
            ),
          ),
        ],
      ),
    );
  }
}

class IdentificationAndCountry extends StatelessWidget {
  final User me = getIt<MeProvider>().data.value;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // ShaderMask(
          //   shaderCallback: (bounds) {
          //     return LinearGradient(
          //       begin: Alignment.bottomCenter,
          //       end: Alignment.topCenter,
          //       colors: [Color(0xffDF0C8B), Color(0xffF486C8)],
          //     ).createShader(bounds);
          //   },
          //   child: SvgPicture.asset(
          //     AssetsPath.fromImagesSvg('id_ic'),
          //     width: SizeConfig.safeBlockHorizontal * 4,
          //   ),
          // ),
          // SizedBox(width: SizeConfig.safeBlockHorizontal * 2),
          // if (me.getUserTypeEnum() != UserTypeEnum.ADMIN)
          Padding(
            padding: EdgeInsets.only(right: SizeConfig.safeBlockHorizontal * 4),
            child: CustomText(
              "ID: ${me.id}",
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w700,
                color: Color(0xffD4D4D4),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.white.withOpacity(0.1),
            ),
            child: CountryWidget(
              code: "${me.country.code}",
              // code: "us",
              widthFlag: SizeConfig.safeBlockHorizontal * 6,
              textStyle: TextStyle(
                fontSize: 14,
                color: Color(0xffD4D4D4),
              ),
              maxWidthText: SizeConfig.safeBlockHorizontal * 22,
            ),
          ),
        ],
      ),
    );
  }
}
