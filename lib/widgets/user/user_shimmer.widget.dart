import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:shimmer/shimmer.dart';

class UserShimmerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Theme.of(context).accentColor.withOpacity(0.2),
      highlightColor: Theme.of(context).accentColor.withOpacity(0.1),
      child: Row(
        children: [
          Container(
            width: SizeConfig.safeBlockHorizontal * 20,
            height: SizeConfig.safeBlockHorizontal * 20,
            decoration: BoxDecoration(
              color: Colors.black,
              shape: BoxShape.circle,
            ),
          ),
          SizedBox(
            width: SizeConfig.safeBlockHorizontal * 2,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: SizeConfig.safeBlockHorizontal * 40,
                height: 15,
                margin: EdgeInsets.only(
                  bottom: 15,
                ),
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(60),
                ),
              ),
              Container(
                width: SizeConfig.safeBlockHorizontal * 60,
                height: 15,
                margin: EdgeInsets.only(
                  bottom: 15,
                ),
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(60),
                ),
              ),
              Container(
                width: SizeConfig.safeBlockHorizontal * 30,
                height: 15,
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(60),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
