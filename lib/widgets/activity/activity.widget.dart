import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class ActivityWidget extends StatelessWidget {
  final String bannerUrl;
  final String title;
  final String content;
  final Function onTap;
  final double width;

  const ActivityWidget({
    Key key,
    @required this.bannerUrl,
    @required this.title,
    @required this.content,
    this.onTap,
    this.width,
  })  : assert(bannerUrl != null),
        assert(title != null),
        assert(content != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Theme.of(context).cardTheme.shadowColor,
            offset: Offset(0, 1),
            blurRadius: 5,
          )
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Container(
          width: width ?? SizeConfig.safeBlockHorizontal * 92,
          color: Theme.of(context).cardTheme.color,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                bannerUrl,
                width: width ?? SizeConfig.safeBlockHorizontal * 92,
                fit: BoxFit.fitWidth,
              ),
              Padding(
                padding: EdgeInsets.all(
                  SizeConfig.safeBlockHorizontal * 4,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(bottom: 8),
                      child: CustomText(
                        title,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    CustomText(
                      content,
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
