import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';

class MyScaffold extends StatelessWidget {
  final Widget child;
  final Widget floatingActionButton;
  final PreferredSizeWidget appBar;
  final String backgroundImagePath;
  final bool extendBodyBehindAppBar;
  final Color backgroundColor;

  const MyScaffold({
    Key key,
    @required this.child,
    this.backgroundImagePath,
    this.appBar,
    this.floatingActionButton,
    this.extendBodyBehindAppBar,
    this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    Widget finalWidget = Scaffold(
      appBar: appBar,
      body: child,
      floatingActionButton: floatingActionButton,
      extendBodyBehindAppBar: extendBodyBehindAppBar ?? false,
    );

    if (backgroundImagePath != null) {
      finalWidget = Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(backgroundImagePath),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          appBar: appBar,
          body: child,
          extendBodyBehindAppBar: extendBodyBehindAppBar ?? true,
          backgroundColor: backgroundColor ?? Colors.transparent,
          floatingActionButton: floatingActionButton,
        ),
      );
    }

    return finalWidget;
  }
}
