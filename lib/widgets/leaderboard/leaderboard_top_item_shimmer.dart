import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LeaderboardTopItemShimmer extends StatelessWidget {
  final double width;
  final double height;

  const LeaderboardTopItemShimmer({
    Key key,
    this.width,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double sizeRatio = 68 / 110;
    final double realWidth = width == null ? height * sizeRatio : width;
    final double realHeight = height == null ? width / sizeRatio : height;

    return Container(
      width: realWidth,
      height: realHeight,
      child: Shimmer.fromColors(
        baseColor: Colors.black.withOpacity(0.3),
        highlightColor: Colors.black.withOpacity(0.2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: realWidth * 0.7 / 48 * 25,
            ),
            Container(
              width: realWidth * 0.7,
              height: realWidth * 0.7,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.black,
              ),
            ),
            SizedBox(height: realWidth * 0.7 / 48),
            Container(
              width: realWidth,
              height: 15,
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(50),
              ),
            ),
            SizedBox(height: realWidth * 0.7 / 48 * 3),
            Container(
              width: realWidth * 0.8,
              height: 15,
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(50),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
