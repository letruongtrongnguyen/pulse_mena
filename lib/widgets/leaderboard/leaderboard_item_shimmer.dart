import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:shimmer/shimmer.dart';

class LeaderboardItemShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.safeBlockHorizontal * 92,
      height: SizeConfig.safeBlockHorizontal * 18,
      padding: EdgeInsets.all(
        SizeConfig.safeBlockHorizontal * 3,
      ),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.1),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Shimmer.fromColors(
        baseColor: Colors.white.withOpacity(0.2),
        highlightColor: Colors.white.withOpacity(0.1),
        child: Row(
          children: [
            SizedBox(width: SizeConfig.safeBlockHorizontal * 8),
            Container(
              height: SizeConfig.safeBlockHorizontal * 12,
              width: SizeConfig.safeBlockHorizontal * 12,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.black,
              ),
            ),
            SizedBox(width: SizeConfig.safeBlockHorizontal * 2),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: SizeConfig.safeBlockHorizontal * 20,
                  height: SizeConfig.safeBlockHorizontal * 4,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.black,
                  ),
                ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
                Container(
                  width: SizeConfig.safeBlockHorizontal * 10,
                  height: SizeConfig.safeBlockHorizontal * 4,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
