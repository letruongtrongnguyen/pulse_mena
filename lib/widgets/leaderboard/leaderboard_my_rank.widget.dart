import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

import 'leaderboard_item_value_module.dart';

class LeaderboardMyRankWidget extends StatelessWidget {
  final LeaderboardType leaderboardType;
  final double height;

  const LeaderboardMyRankWidget({
    Key key,
    this.leaderboardType,
    this.height = 75,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final myRankData = getIt<LeaderboardProvider>().myRankData;
    final leaderboardDataLoading =
        getIt<LeaderboardProvider>().leaderboardDataLoading;

    return Container(
      padding: EdgeInsets.only(
        bottom: SizeConfig.paddingBottom,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8),
          topRight: Radius.circular(8),
        ),
        color: Color(0xff424242),
      ),
      child: Container(
        padding: EdgeInsetsDirectional.fromSTEB(
          SizeConfig.safeBlockHorizontal * 4,
          0,
          SizeConfig.safeBlockHorizontal * 4,
          0,
        ),
        height: height,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8),
            topRight: Radius.circular(8),
          ),
          color: Color(0xff424242),
        ),
        child: Container(
          width: SizeConfig.safeBlockHorizontal * 92,
          padding: EdgeInsets.all(
            SizeConfig.safeBlockHorizontal * 3,
          ),
          color: Colors.transparent,
          child: AnimatedBuilder(
              animation: Listenable.merge([myRankData, leaderboardDataLoading]),
              builder: (context, child) {
                if (leaderboardDataLoading.value) return SizedBox();

                if (myRankData.value == null) return _buildCreateButton();

                return Row(
                  children: [
                    SizedBox(
                      width: SizeConfig.safeBlockHorizontal * 6,
                      child: CustomText(
                        "${myRankData.value.rank < 0 ? "--" : myRankData.value.rank}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                    ),
                    SizedBox(width: SizeConfig.safeBlockHorizontal * 2),
                    AvatarWidget(
                      avatarUrl: "${myRankData.value.avatarUrl}",
                      borderWidth: 0,
                      // avatarDefault:
                      //     leaderboardType == LeaderboardType.TopRoomGift
                      //         ? AssetsPath.fromImagesSvg("room_default_ic")
                      //         : null,
                    ),
                    SizedBox(width: SizeConfig.safeBlockHorizontal * 2),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          constraints: BoxConstraints(
                            maxWidth: myRankData.value.rank > 0
                                ? SizeConfig.safeBlockHorizontal * 40
                                : SizeConfig.safeBlockHorizontal * 20,
                          ),
                          child: CustomText(
                            Formatter.clearZeroWidthSpace(
                                Formatter.capitalize(myRankData.value.name)),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            softWrap: false,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        // if (leaderboardType == LeaderboardType.TopRoomGift)
                        //   SizedBox(height: 5),
                        // if (leaderboardType == LeaderboardType.TopRoomGift)
                        //   Container(
                        //     constraints: BoxConstraints(
                        //       maxWidth: myRankData.value.rank > 0
                        //           ? SizeConfig.safeBlockHorizontal * 40
                        //           : SizeConfig.safeBlockHorizontal * 25,
                        //     ),
                        //     child: CustomText(
                        //       Formatter.clearZeroWidthSpace(
                        //           Formatter.capitalize(
                        //               myRankData.value.description)),
                        //       overflow: TextOverflow.ellipsis,
                        //       maxLines: 1,
                        //       softWrap: false,
                        //       style: TextStyle(
                        //         fontSize: 12,
                        //       ),
                        //     ),
                        //   ),
                      ],
                    ),
                    Spacer(),
                    if (myRankData.value.rank > 0)
                      LeaderboardItemValueModule(
                        value: myRankData.value.point,
                        type: leaderboardType,
                      ),
                    if (myRankData.value.rank <= 0)
                      CustomText(
                        "leaderboard.outRank",
                        style: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                  ],
                );
              }),
        ),
      ),
    );
  }

  Widget _buildCreateButton() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        GestureDetector(
          onTap: () {
            getIt<CommunicateService>().navigate(route: "/create-room");
          },
          child: Container(
            // margin: EdgeInsets.symmetric(vertical: SizeConfig.safeBlockHorizontal * 5),
            alignment: Alignment.center,
            width: SizeConfig.safeBlockHorizontal * 60,
            height: SizeConfig.safeBlockHorizontal * 12,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(60),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color(0xff81FFCA), Color(0xff2CB7FF)],
              ),
            ),
            child: CustomText(
              "room.btn.create",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
