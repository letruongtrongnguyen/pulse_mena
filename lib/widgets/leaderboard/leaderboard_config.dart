import 'package:flutter/material.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';

class LeaderboardConfig {
  static double leaderboardStandSizeRatio = 268 / 94;

  static Map<LeaderboardType, Map<String, dynamic>> itemConfig = {
    // LeaderboardType.TopRoomLevel: {
    //   "weeklyTitle": "leaderboard.weeklyTitle.topRoomLevel",
    //   "tabTitle": "leaderboard.tabTitle.topRoomLevel",
    //   "icon": AssetImage(AssetsPath.fromImagesLeaderboardIcon("")),
    //   "gradient": LinearGradient(
    //     begin: Alignment.centerLeft,
    //     end: Alignment.centerRight,
    //     colors: [
    //       Color(0xffF2DC46),
    //       Color(0xffF73427),
    //     ],
    //   ),
    //   "colors": [
    //     Color(0xffF2DC46),
    //     Color(0xffF73427),
    //   ]
    // },
    // LeaderboardType.TopRoomGift: {
    //   "weeklyTitle": "leaderboard.weeklyTitle.topRoomGift",
    //   "tabTitle": "leaderboard.tabTitle.topRoomGift",
    //   "icon": AssetImage(AssetsPath.fromImagesLeaderboardIcon("gift_ic")),
    //   "gradient": LinearGradient(
    //     begin: Alignment.centerLeft,
    //     end: Alignment.centerRight,
    //     colors: [
    //       Color(0xffF5C5A3),
    //       Color(0xffF52165),
    //     ],
    //   ),
    //   "colors": [
    //     Color(0xffF5C5A3),
    //     Color(0xffF52165),
    //   ]
    // },
    LeaderboardType.TopBuyer: {
      "weeklyTitle": "leaderboard.weeklyTitle.topBuyer",
      "monthlyTitle": "leaderboard.monthlyTitle.topBuyer",
      "tabTitle": "leaderboard.tabTitle.topBuyer",
      "icon": AssetImage(AssetsPath.fromImagesCommon("coin_ic")),
      "gradient": LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          Color(0xffCB47F6),
          Color(0xff7505E7),
        ],
      ),
      "colors": [
        Color(0xffCB47F6),
        Color(0xff7505E7),
      ]
    },
    // LeaderboardType.TopHotIdol: {
    //   "weeklyTitle": "leaderboard.weeklyTitle.topHotIdol",
    //   "tabTitle": "leaderboard.tabTitle.topHotIdol",
    //   "icon": AssetImage(AssetsPath.fromImagesLeaderboardIcon("star_ic")),
    //   "gradient": LinearGradient(
    //     begin: Alignment.centerLeft,
    //     end: Alignment.centerRight,
    //     colors: [
    //       Color(0xff22D5F6),
    //       Color(0xff5175F5),
    //     ],
    //   ),
    //   "colors": [
    //     Color(0xff22D5F6),
    //     Color(0xff5175F5),
    //   ]
    // },
    // LeaderboardType.TopGiftSender: {
    //   "weeklyTitle": "leaderboard.weeklyTitle.topGiftSender",
    //   "tabTitle": "leaderboard.tabTitle.topGiftSender",
    //   "icon": AssetImage(AssetsPath.fromImagesLeaderboardIcon("gift_send_ic")),
    //   "gradient": LinearGradient(
    //     begin: Alignment.centerLeft,
    //     end: Alignment.centerRight,
    //     colors: [
    //       Color(0xffD6F22B),
    //       Color(0xff0DCCF5),
    //     ],
    //   ),
    //   "colors": [
    //     Color(0xffD6F22B),
    //     Color(0xff0DCCF5),
    //   ]
    // },
    LeaderboardType.TopGiftReceiver: {
      "weeklyTitle": "leaderboard.weeklyTitle.topGiftReceiver",
      "monthlyTitle": "leaderboard.monthlyTitle.topGiftReceiver",
      "tabTitle": "leaderboard.tabTitle.topGiftReceiver",
      "icon":
          AssetImage(AssetsPath.fromImagesLeaderboardIcon("gift_receive_ic")),
      "gradient": LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          Color(0xffF00C69),
          Color(0xff7F28ED),
        ],
      ),
      "colors": [
        Color(0xffF00C69),
        Color(0xff7F28ED),
      ]
    },
    LeaderboardType.TopWinnerCoin: {
      "weeklyTitle": "leaderboard.weeklyTitle.topWinnerCoin",
      "monthlyTitle": "leaderboard.monthlyTitle.topWinnerCoin",
      "tabTitle": "leaderboard.tabTitle.topWinnerCoin",
      "icon":
      AssetImage(AssetsPath.fromImagesLeaderboardIcon("star_ic")),
      "gradient": LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          Color(0xffFFCB02),
          Color(0xffFF9A02),
        ],
      ),
      "colors": [
        Color(0xffFFCB02),
        Color(0xffFF9A02),
      ]
    },
  };
}
