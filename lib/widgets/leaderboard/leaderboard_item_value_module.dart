import 'package:flutter/material.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_config.dart';

class LeaderboardItemValueModule extends StatelessWidget {
  final LeaderboardType type;
  final int value;
  final bool showValue;

  const LeaderboardItemValueModule({
    Key key,
    this.type = LeaderboardType.TopBuyer,
    this.value = 0,
    this.showValue = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String _value =
        "${value > 99999 ? "${Formatter.formatCompactNumber(value)}" : Formatter.formatDecimalNumber(value)}";

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // if (type != LeaderboardType.TopRoomGift)
        Image(
          image: LeaderboardConfig.itemConfig[type]["icon"],
          height: 16,
          fit: BoxFit.fitHeight,
        ),
        if (showValue) SizedBox(width: 5),
        if (showValue)
          CustomText(
            _value,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 13,
            ),
          ),
        // if (type == LeaderboardType.TopRoomGift)
        //   CustomText(
        //     " pt",
        //     style: TextStyle(
        //       fontWeight: FontWeight.bold,
        //       fontSize: 13,
        //     ),
        //   ),
      ],
    );
  }
}
