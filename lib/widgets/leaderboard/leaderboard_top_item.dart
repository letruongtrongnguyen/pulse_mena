import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/avatar/avatar_with_frame.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/user_bottom_sheet/user_bottom_sheet.widget.dart';

import 'leaderboard_config.dart';

class LeaderboardTopItemWidget extends StatelessWidget {
  final double width;
  final double height;
  final LeaderboardType type;
  final int value;
  final String name;
  final String avatarUrl;
  final String pendantUrl;
  final int index;
  final int userId;

  const LeaderboardTopItemWidget({
    Key key,
    this.width,
    this.height,
    this.type = LeaderboardType.TopBuyer,
    this.value,
    this.name,
    this.avatarUrl,
    this.index,
    this.userId,
    this.pendantUrl,
  })  : assert(width == null || height == null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final double sizeRatio = 68 / 95;
    final double realWidth = width == null ? height * sizeRatio : width;
    final double realHeight = height == null ? width / sizeRatio : height;

    String crownPath = AssetsPath.fromImagesCommon("1st_crown");

    switch (index) {
      case 2:
        crownPath = AssetsPath.fromImagesCommon("2nd_crown");
        break;
      case 3:
        crownPath = AssetsPath.fromImagesCommon("3rd_crown");
        break;
    }

    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: realWidth,
          height: realHeight,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // SizedBox(
              //   height: realWidth * 0.5 / 48 * 25,
              // ),
              AvatarWithFrameWidget(
                onTap: () {
                  // if (type == LeaderboardType.TopRoomGift) return;

                  getIt<GeneralProvider>().getUserBottomSheetData(id: userId);

                  showUserBottomSheet(context: context, userId: userId);
                },
                avatarUrl: avatarUrl ?? "",
                width: realWidth * 0.75,
                frameUrl: pendantUrl ?? "",
                isEmpty: userId == null,
                // radius: realWidth * 0.7 / 2,
                // borderWidth: realWidth * 0.7 / 48,
                // avatarDefault: type == LeaderboardType.TopRoomGift
                //     ? AssetsPath.fromImagesSvg("room_default_ic")
                //     : null,
              ),
              SizedBox(height: realWidth * 0.7 / 48),
              Container(
                constraints: BoxConstraints(
                  maxWidth: realWidth,
                ),
                child: CustomText(
                  Formatter.clearZeroWidthSpace(
                    Formatter.capitalize(name ?? ""),
                  ),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  softWrap: false,
                  style: TextStyle(
                    fontSize: 11,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    shadows: <Shadow>[
                      Shadow(
                        offset: Offset(0, 2),
                        blurRadius: 2.0,
                        color: Colors.black.withOpacity(0.25),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: realWidth * 0.7 / 48 * 3),
              // if (type != LeaderboardType.TopRoomLevel)
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: realWidth * 0.7 / 48 * 5,
                  vertical: realWidth * 0.7 / 48 * 2,
                ),
                decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.4),
                  borderRadius: BorderRadius.circular(50),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    // if (type != LeaderboardType.TopRoomGift)
                    Image(
                      width: realWidth * 0.7 / 48 * 9,
                      fit: BoxFit.fitWidth,
                      image: LeaderboardConfig.itemConfig[type]["icon"],
                    ),
                    // if (type != LeaderboardType.TopRoomGift)
                    SizedBox(width: realWidth * 0.7 / 48 * 3),
                    CustomText(
                      value == null
                          ? "???"
                          : "${Formatter.formatCompactNumber(value)}",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 10,
                        color: Colors.white
                      ),
                    ),
                    // if (type == LeaderboardType.TopRoomGift)
                    //   CustomText(
                    //     " pt",
                    //     style: TextStyle(
                    //       fontWeight: FontWeight.bold,
                    //       fontSize: 10,
                    //     ),
                    //   ),
                  ],
                ),
              ),
            ],
          ),
        ),
        // Positioned(
        //   top: realWidth * 0.7 / 48 * 5,
        //   child: Image(
        //     height: realWidth * 0.7 / 48 * 25,
        //     fit: BoxFit.fitHeight,
        //     image: AssetImage(crownPath),
        //   ),
        // ),
      ],
    );
  }
}
