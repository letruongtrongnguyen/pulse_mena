import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/leaderboard.item.model.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/avatar/avatar_with_frame.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_item_value_module.dart';
import 'package:flutterbase/widgets/user_bottom_sheet/user_bottom_sheet.widget.dart';

class LeaderboardUserItemWidget extends StatelessWidget {
  final LeaderboardType leaderboardType;
  final Leader leaderboardData;

  const LeaderboardUserItemWidget({
    Key key,
    this.leaderboardType,
    this.leaderboardData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.safeBlockHorizontal * 92,
      height: SizeConfig.safeBlockHorizontal * 18,
      padding: EdgeInsets.all(
        SizeConfig.safeBlockHorizontal * 3,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white.withOpacity(0.1),
      ),
      child: Row(
        children: [
          SizedBox(
            width: SizeConfig.safeBlockHorizontal * 6,
            child: CustomText(
              "${leaderboardData?.rank ?? "--"}",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          ),
          SizedBox(width: SizeConfig.safeBlockHorizontal * 2),
          AvatarWithFrameWidget(
            onTap: () {
              getIt<GeneralProvider>()
                  .getUserBottomSheetData(id: leaderboardData.userId);

              showUserBottomSheet(
                  context: context, userId: leaderboardData.userId);
            },
            avatarUrl: leaderboardData?.avatarUrl ?? "",
            frameUrl: leaderboardData?.pendantUrl ?? "",
            width: SizeConfig.safeBlockHorizontal * 11,
          ),
          SizedBox(width: SizeConfig.safeBlockHorizontal * 2),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                constraints: BoxConstraints(
                  maxWidth: SizeConfig.safeBlockHorizontal * 40,
                ),
                child: CustomText(
                  Formatter.clearZeroWidthSpace(
                      Formatter.capitalize(leaderboardData?.name ?? "--")),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  softWrap: false,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: 5),
              LeaderboardItemValueModule(
                value: leaderboardData?.point ?? 0,
                type: leaderboardType,
                showValue: leaderboardData == null ? false : true,
              )
            ],
          ),
          // Spacer(),
          // ActionModule(),
        ],
      ),
    );
  }
}

class ActionModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(width: 1, color: Colors.white),
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 5,
      ),
      child: CustomText(
        "common.addFriend",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 12,
        ),
      ),
    );

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        SvgPicture.asset(
          AssetsPath.fromImagesSvg("check_ic"),
          color: Color(0xff26D57D),
        ),
        SizedBox(
          width: 5,
        ),
        CustomText(
          "common.friend",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 12,
          ),
        ),
      ],
    );

    return CustomText(
      "common.pendingAccept",
      style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 12,
        fontStyle: FontStyle.italic,
      ),
    );
  }
}
