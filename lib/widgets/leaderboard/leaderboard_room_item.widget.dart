import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/leaderboard.item.model.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_item_value_module.dart';
import 'package:flutterbase/widgets/user_bottom_sheet/user_bottom_sheet.widget.dart';

class LeaderboardRoomItem extends StatelessWidget {
  final LeaderboardType leaderboardType;
  final Leader roomData;

  const LeaderboardRoomItem({
    Key key,
    this.leaderboardType,
    this.roomData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.safeBlockHorizontal * 92,
      height: SizeConfig.safeBlockHorizontal * 18,
      padding: EdgeInsets.all(
        SizeConfig.safeBlockHorizontal * 3,
      ),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.1),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        children: [
          SizedBox(
            width: SizeConfig.safeBlockHorizontal * 6,
            child: CustomText(
              "${roomData.rank}",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          ),
          SizedBox(width: SizeConfig.safeBlockHorizontal * 2),
          AvatarWidget(
            avatarUrl: roomData.avatarUrl,
            borderWidth: 0,
            borderColor: Colors.transparent,
            avatarDefault: AssetsPath.fromImagesSvg("room_default_ic"),
          ),
          SizedBox(width: SizeConfig.safeBlockHorizontal * 2),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                constraints: BoxConstraints(
                  maxWidth: SizeConfig.safeBlockHorizontal * 40,
                ),
                child: CustomText(
                  Formatter.clearZeroWidthSpace(
                    Formatter.capitalize(roomData.name),
                  ),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  softWrap: false,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: 5),
              Container(
                constraints: BoxConstraints(
                  maxWidth: SizeConfig.safeBlockHorizontal * 40,
                ),
                child: CustomText(
                  Formatter.clearZeroWidthSpace(
                    Formatter.capitalize(roomData.description),
                  ),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  softWrap: false,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              ),
            ],
          ),
          Spacer(),
          LeaderboardItemValueModule(
            value: roomData.point,
            type: leaderboardType,
          ),
        ],
      ),
    );
  }
}
