import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class CoinWidget extends StatelessWidget {
  final double sizeRatio;

  const CoinWidget({
    Key key,
    this.sizeRatio = 1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ValueNotifier me = getIt<MeProvider>().data;

    return Container(
      // color: Colors.blueGrey,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Stack(
            children: [
              // Positioned.fill(
              //   child: ShaderMask(
              //     shaderCallback: (Rect bounds) {
              //       return LinearGradient(
              //         begin: Alignment.bottomCenter,
              //         end: Alignment.topCenter,
              //         colors: <Color>[Color(0xffB1B1B1), Colors.white],
              //       ).createShader(bounds);
              //     },
              //     child: Stack(
              //       alignment: Alignment.center,
              //       children: [
              //         Container(
              //           decoration: BoxDecoration(
              //             color: Colors.white,
              //             borderRadius: BorderRadius.circular(9),
              //           ),
              //           margin: EdgeInsets.only(right: 5.5),
              //         ),
              //         Positioned(
              //           right: 0,
              //           child: CustomPaint(
              //             painter: TrianglePainter(
              //               strokeColor: Colors.white,
              //               strokeWidth: 10,
              //               paintingStyle: PaintingStyle.fill,
              //             ),
              //             child: Container(
              //               height: 10,
              //               width: 6,
              //             ),
              //           ),
              //         )
              //       ],
              //     ),
              //   ),
              // ),
              ValueListenableBuilder(
                  valueListenable: me,
                  builder: (context, _, child) {
                    String _value =
                        "${me.value.coins > 99999 ? "≈${Formatter.formatCompactNumber(me.value.coins)}" : Formatter.formatDecimalNumber(me.value.coins)}";
                    return Container(
                      // padding: EdgeInsetsDirectional.fromSTEB(8, 5, 14, 5),
                      child: CustomText(
                        _value,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14 * sizeRatio,
                        ),
                      ),
                    );
                  }),
            ],
          ),
          SizedBox(width: SizeConfig.safeBlockHorizontal * 1.5 * sizeRatio),
          Image(
            height: 20 * sizeRatio,
            width: 20 * sizeRatio,
            fit: BoxFit.fitHeight,
            image: AssetImage(
              AssetsPath.fromImagesCommon("coin_ic"),
            ),
          ),
        ],
      ),
    );
  }
}

class TrianglePainter extends CustomPainter {
  final Color strokeColor;
  final PaintingStyle paintingStyle;
  final double strokeWidth;

  TrianglePainter(
      {this.strokeColor = Colors.black,
      this.strokeWidth = 3,
      this.paintingStyle = PaintingStyle.stroke});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = strokeColor
      ..strokeWidth = strokeWidth
      ..style = paintingStyle;

    canvas.drawPath(getTrianglePath(size.width, size.height), paint);
  }

  Path getTrianglePath(double x, double y) {
    return Path()
      ..moveTo(0, 0)
      ..lineTo(x, y / 2)
      ..lineTo(0, y)
      ..lineTo(0, 0);
  }

  @override
  bool shouldRepaint(TrianglePainter oldDelegate) {
    return oldDelegate.strokeColor != strokeColor ||
        oldDelegate.paintingStyle != paintingStyle ||
        oldDelegate.strokeWidth != strokeWidth;
  }
}
