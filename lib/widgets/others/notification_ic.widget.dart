import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class NotificationIconWidget extends StatelessWidget {
  final EdgeInsetsGeometry padding;

  const NotificationIconWidget({
    Key key,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        getIt<CommunicateService>().navigate(route: "/notification-screen");
      },
      child: Padding(
        padding: padding ?? EdgeInsets.zero,
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Container(
              color: Colors.transparent,
              alignment: AlignmentDirectional.centerEnd,
              // color: Colors.blueGrey,
              child: SvgPicture.asset(
                AssetsPath.fromImagesSvg("notification_ic"),
                color: Theme.of(context).appBarTheme.iconTheme.color,
              ),
            ),
            ValueListenableBuilder(
                valueListenable: getIt<MeProvider>().data,
                builder: (context, data, snapshot) {
                  if (!data.hasNewNotification) return SizedBox();

                  return PositionedDirectional(
                    end: 0,
                    top: -4,
                    child: Container(
                      width: 12,
                      height: 12,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xffFF002E),
                      ),
                      // child: CustomText(
                      //   "2",
                      //   style: TextStyle(
                      //     fontSize: 10,
                      //   ),
                      // ),
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }
}
