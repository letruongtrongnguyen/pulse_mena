import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/widgets/basic/circle_ic.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class SessionTitle extends StatelessWidget {
  final String title;
  final String iconPath;
  final Function onTap;
  final Widget suffix;

  const SessionTitle({
    Key key,
    this.title,
    this.iconPath,
    this.onTap,
    this.suffix,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap ?? () {},
      child: Container(
        color: Colors.transparent,
        child: Row(
          children: [
            if (iconPath != null)
              CircleIconWidget(
                icon: SvgPicture.asset(
                  iconPath,
                  color: Colors.black,
                ),
                radius: SizeConfig.safeBlockHorizontal * 3.5,
                padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 1.5),
              ),
            if (iconPath != null) SizedBox(width: 10),
            CustomText(
              title,
              style: Theme.of(context).textTheme.headline1,
            ),
            Spacer(),
            suffix ?? SizedBox(),
          ],
        ),
      ),
    );
  }
}
