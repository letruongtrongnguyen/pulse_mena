import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class GemWidget extends StatelessWidget {
  final int value;
  final double sizeRatio;

  const GemWidget({
    Key key,
    this.value = 1000,
    this.sizeRatio = 1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String _value =
        "${value > 99999 ? "≈${Formatter.formatCompactNumber(value)}" : Formatter.formatDecimalNumber(value)}";

    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          ValueListenableBuilder(
              valueListenable: getIt<MeProvider>().data,
              builder: (context, me, child) {
                String _value =
                    "${me.gems > 99999 ? "≈${Formatter.formatCompactNumber(me.gems)}" : Formatter.formatDecimalNumber(me.gems)}";

                return CustomText(
                  _value,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14 * sizeRatio,
                  ),
                );
              }),
          SizedBox(width: SizeConfig.safeBlockHorizontal * 1 * sizeRatio),
          Image(
            height: 22 * sizeRatio,
            width: 22 * sizeRatio,
            fit: BoxFit.fitHeight,
            image: AssetImage(
              AssetsPath.fromImagesCommon("gem_ic"),
            ),
          ),
        ],
      ),
    );
  }
}
