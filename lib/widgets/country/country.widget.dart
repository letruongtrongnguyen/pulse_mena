import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/country.model.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

enum CountryNameType {
  ISO3,
  ABBREVIATION,
}

class CountryWidget extends StatelessWidget {
  final String code;
  final TextStyle textStyle;
  final double widthFlag;
  final double maxWidthText;
  final bool showCountryName;
  final CountryNameType countryNameType;

  const CountryWidget({
    Key key,
    @required this.code,
    this.textStyle,
    this.widthFlag,
    this.showCountryName = true,
    this.maxWidthText = 150,
    this.countryNameType,
  }) : super(key: key);

  String getFlagSvgPath({String code}) {
    if (Country(code: code).name == "(None)") return '';
    return AssetsPath.fromImagesFlags("${code.toLowerCase().trim()}");
  }

  Widget build(BuildContext context) {
    TextStyle realTextStyle = TextStyle(
      fontSize: 14 * SizeConfig.screenWidth / 375,
    );

    Size _textSize(String text, TextStyle style) {
      final span = TextSpan(text: text, style: style);

      final TextPainter textPainter = TextPainter(
        text: span,
        maxLines: 1,
        textScaleFactor: MediaQuery.of(context).textScaleFactor,
        textDirection: getIt<GeneralProvider>().textDirection.value,
        textAlign: TextAlign.start,
      )..layout(minWidth: 0, maxWidth: double.infinity);
      return textPainter.size;
    }

    if (this.textStyle != null) {
      realTextStyle = realTextStyle.merge(this.textStyle);

      if (this.textStyle.fontSize != null) {
        realTextStyle = realTextStyle.merge(
          TextStyle(
            fontSize: this.textStyle.fontSize * SizeConfig.screenWidth / 375,
          ),
        );
      }
    }

    final Size txtSize = _textSize(
      Country(code: this.code.trim()).name,
      realTextStyle,
    );

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(widthFlag != null
              ? widthFlag * 0.1
              : SizeConfig.safeBlockHorizontal * 10 * 0.1),
          child: (getFlagSvgPath(code: this.code.trim()).isEmpty)
              ? Icon(Icons.public,
                  color: Colors.white54,
                  size: widthFlag ?? SizeConfig.safeBlockHorizontal * 10 * 0.1)
              : SvgPicture.asset(
                  getFlagSvgPath(code: this.code.trim()),
                  width: widthFlag ?? SizeConfig.safeBlockHorizontal * 10,
                ),
        ),
        if (showCountryName) SizedBox(width: 5),
        if (showCountryName)
          Container(
            constraints: BoxConstraints(
              maxWidth: maxWidthText,
            ),
            child: CustomText(
              txtSize.width < maxWidthText
                  ? Formatter.clearZeroWidthSpace(
                      Country(code: this.code.trim()).name)
                  : Country(code: this.code.trim()).abbreviation,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              // softWrap: false,
              style: TextStyle(
                fontSize: 14,
              ).merge(this.textStyle),
            ),
          ),
      ],
    );
  }
}
