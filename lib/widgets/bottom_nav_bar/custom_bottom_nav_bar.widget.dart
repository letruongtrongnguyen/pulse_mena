import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class CustomBottomNavBar extends StatelessWidget {
  final PageController pageController;

  const CustomBottomNavBar({
    Key key,
    this.pageController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Container(
        color: Colors.black,
        padding: EdgeInsets.only(
          bottom: SizeConfig.paddingBottom,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            NavBarItem(
              type: MainScreenTabType.feedScreen,
              pageController: pageController,
            ),
            NavBarItem(
              type: MainScreenTabType.roomScreen,
              pageController: pageController,
            ),
            NavBarItem(
              type: MainScreenTabType.discoverScreen,
              pageController: pageController,
            ),
            NavBarItem(
              type: MainScreenTabType.notificationScreen,
              pageController: pageController,
            ),
            NavBarItem(
              type: MainScreenTabType.moreScreen,
              pageController: pageController,
            ),
          ],
        ),
      ),
    );
  }
}

class NavBarItem extends StatelessWidget {
  final Map<MainScreenTabType, Map<String, dynamic>> navItemList = {
    MainScreenTabType.feedScreen: {
      "iconPath": AssetsPath.fromImagesNavBarIcon("feed_ic"),
      "title": "Feed",
      "pageIndex": 0,
    },
    MainScreenTabType.roomScreen: {
      "iconPath": AssetsPath.fromImagesNavBarIcon("room_ic"),
      "title": "Rooms",
      "pageIndex": 1,
    },
    MainScreenTabType.discoverScreen: {
      "iconPath": AssetsPath.fromImagesNavBarIcon("discover_ic"),
      "title": "Discover",
      "pageIndex": 2,
    },
    MainScreenTabType.notificationScreen: {
      "iconPath": AssetsPath.fromImagesNavBarIcon("notification_ic"),
      "title": "Notifications",
      "pageIndex": 3,
    },
    MainScreenTabType.moreScreen: {
      "iconPath": AssetsPath.fromImagesNavBarIcon("more_ic"),
      "title": "More",
      "pageIndex": 4,
    },
  };

  final MainScreenTabType type;
  final PageController pageController;

  NavBarItem({
    Key key,
    this.type,
    this.pageController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {

        pageController.animateToPage(
          navItemList[type]["pageIndex"],
          curve: Curves.easeIn,
          duration: Duration(milliseconds: 100),
        );
        getIt<GeneralProvider>().onActiveTabChange(type);
      },
      child: Container(
        height: 64,
        width: SizeConfig.safeBlockHorizontal * 19,
        color: Colors.transparent,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              navItemList[type]["iconPath"],
              width: SizeConfig.safeBlockHorizontal * 6,
            ),
            SizedBox(height: 5),
            CustomText(
              navItemList[type]["title"],
              style: TextStyle(
                fontSize: 11,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
