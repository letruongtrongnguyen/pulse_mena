import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/country.model.dart';
import 'package:flutterbase/core/@services/models/item.model.dart';
import 'package:flutterbase/core/@services/models/level.model.dart';
import 'package:flutterbase/core/@services/models/user.model.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/@services/utils/gender.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/widgets/avatar/avatar_with_frame.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/country/country.widget.dart';
import 'package:shimmer/shimmer.dart';

void showUserBottomSheet({
  BuildContext context,
  int userId,
}) {
  assert(context != null);

  showModalBottomSheet(
    context: context,
    backgroundColor: Colors.transparent,
    enableDrag: false,
    builder: (ctx) {
      return ValueListenableBuilder(
          valueListenable: getIt<GeneralProvider>().userBottomSheetLoading,
          builder: (context, loading, snapshot) {
            final User data =
                getIt<GeneralProvider>().userBottomSheetData.value;
            final User me = getIt<MeProvider>().data.value;
            // final List<Item> receivedGift =
            //     getIt<GeneralProvider>().userBottomSheetReceivedGift.value;

            return Stack(
              children: [
                Container(
                  height: SizeConfig.safeBlockVertical * 45,
                  width: SizeConfig.safeBlockHorizontal * 100,
                  padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.safeBlockVertical * 2,
                  ),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Color(0xff2f2f2f),
                    borderRadius: BorderRadius.only(
                      topLeft:
                          Radius.circular(SizeConfig.safeBlockVertical * 2),
                      topRight:
                          Radius.circular(SizeConfig.safeBlockVertical * 2),
                    ),
                  ),
                  child: loading
                      ? Shimmer.fromColors(
                          baseColor: Colors.white.withOpacity(0.2),
                          highlightColor: Colors.white.withOpacity(0.1),
                          child: ListView(
                            children: [
                              Align(
                                child: Container(
                                  margin: EdgeInsets.all(
                                    SizeConfig.safeBlockHorizontal * 5,
                                  ),
                                  width: SizeConfig.safeBlockHorizontal * 20,
                                  height: SizeConfig.safeBlockHorizontal * 20,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              Align(
                                child: Container(
                                  width: SizeConfig.safeBlockHorizontal * 25,
                                  height: SizeConfig.safeBlockHorizontal * 5,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(60),
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              Align(
                                child: Container(
                                  margin: EdgeInsets.only(top: 10),
                                  width: SizeConfig.safeBlockHorizontal * 35,
                                  height: SizeConfig.safeBlockHorizontal * 4,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(60),
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsetsDirectional.only(end: 10),
                                      width:
                                          SizeConfig.safeBlockHorizontal * 35,
                                      height:
                                          SizeConfig.safeBlockHorizontal * 5,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(60),
                                        color: Colors.black,
                                      ),
                                    ),
                                    Container(
                                      margin:
                                          EdgeInsetsDirectional.only(end: 10),
                                      width:
                                          SizeConfig.safeBlockHorizontal * 15,
                                      height:
                                          SizeConfig.safeBlockHorizontal * 5,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(60),
                                        color: Colors.black,
                                      ),
                                    ),
                                    // Container(
                                    //   width:
                                    //       SizeConfig.safeBlockHorizontal * 15,
                                    //   height:
                                    //       SizeConfig.safeBlockHorizontal * 5,
                                    //   decoration: BoxDecoration(
                                    //     borderRadius: BorderRadius.circular(60),
                                    //     color: Colors.black,
                                    //   ),
                                    // )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )
                      : ListView(
                          padding: EdgeInsets.symmetric(
                            vertical: 10,
                          ),
                          children: [
                            Align(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                  vertical: 10,
                                ),
                                child: AvatarWithFrameWidget(
                                  avatarUrl: data.avatarUrl,
                                  width: SizeConfig.safeBlockHorizontal * 18,
                                  frameUrl: data.pendantUrl,
                                ),
                              ),
                            ),
                            Align(
                              child: Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: CustomText(
                                  data.fullName,
                                  clearZeroSpace: true,
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            // if (data.getUserTypeEnum() != UserTypeEnum.ADMIN)
                            Align(
                              child: Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: CustomText(
                                  "ID: ${data.id}",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: _detailInformation(
                                data: data,
                              ),
                            ),
                            if (me.id != null && me.id != userId)
                            _buildAction(userId),
                            CustomText(
                              "common.receivedGift",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 12,
                              ),
                            ),
                            GridView.builder(
                              padding: EdgeInsets.only(
                                top: 15,
                                bottom: kBottomNavigationBarHeight,
                              ),
                              shrinkWrap: true,
                              itemCount: data.gifts.length,
                              physics: NeverScrollableScrollPhysics(),
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 4,
                                childAspectRatio: 0.8,
                                mainAxisSpacing:
                                    SizeConfig.safeBlockHorizontal * 2.5,
                                crossAxisSpacing:
                                    SizeConfig.safeBlockHorizontal * 2.5,
                              ),
                              itemBuilder: (context, index) {
                                return Container(
                                  decoration: BoxDecoration(
                                    color: Colors.black.withOpacity(0.2),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 10,
                                    vertical: 5,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      CachedNetworkImage(
                                        imageUrl: data.gifts[index].thumbnail,
                                        // imageUrl: "",
                                        width: double.infinity,
                                        placeholder: (context, url) =>
                                            Container(
                                          height: 20,
                                          decoration: BoxDecoration(
                                            color: Colors.transparent,
                                          ),
                                          // child: SvgPicture.asset(
                                          //   AssetsPath.fromImagesSvg(
                                          //       "avatar_default_ic"),
                                          //   color: Color(0xff717171),
                                          // ),
                                        ),
                                        errorWidget: (context, url, error) =>
                                            Container(
                                          decoration: BoxDecoration(
                                            color: Colors.transparent,
                                          ),
                                          child: SvgPicture.asset(
                                            AssetsPath.fromImagesSvg(
                                                "image_ic"),
                                            color: Color(0xff717171),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 5),
                                        child: CustomText(
                                          "x${data.gifts[index].amount}",
                                          style: TextStyle(
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                ),
                if (!loading)
                  PositionedDirectional(
                    top: SizeConfig.safeBlockVertical,
                    end: SizeConfig.safeBlockVertical,
                    child: GestureDetector(
                      onTap: () {
                        getIt<CommunicateService>().navigate(
                          route: "/user-profile",
                          args: {
                            "user-id": userId,
                          },
                        );
                      },
                      child: SvgPicture.asset(
                        AssetsPath.fromImagesSvg("arrow_forward_ios_circle_ic"),
                        width: SizeConfig.safeBlockVertical * 3,
                        matchTextDirection: true,
                      ),
                    ),
                  )
              ],
            );
          });
    },
  );
}

Widget _buildAction(int userId) {
  return Padding(
    padding: EdgeInsets.only(
      top: 5,
      bottom: 20,
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        GestureDetector(
          onTap: () {
            getIt<CommunicateService>().navigate(route: "/private-chat", args: {
              "id": userId,
              "isSendGift": false,
            });
          },
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal * 2),
                decoration: BoxDecoration(
                  color: Color(0xFF424242),
                  shape: BoxShape.circle,
                ),
                child: SvgPicture.asset(
                  AssetsPath.fromImagesSvg("message_ic"),
                  width: SizeConfig.blockSizeHorizontal * 5,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              CustomText(
                "common.chat",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
        ),
        GestureDetector(
          onTap: () {
            getIt<CommunicateService>().navigate(route: "/private-chat", args: {
              "id": userId,
              "isSendGift": true,
            });
          },
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                AssetsPath.fromImagesCommon("gift_ic"),
                height: SizeConfig.blockSizeHorizontal * 9,
              ),
              SizedBox(
                height: 5,
              ),
              CustomText(
                "common.sendGift",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
        )
      ],
    ),
  );
}

Widget _detailInformation({
  User data,
}) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.1),
          borderRadius: BorderRadius.circular(60),
        ),
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.safeBlockHorizontal * 2,
          vertical: SizeConfig.safeBlockHorizontal,
        ),
        child: CountryWidget(
          code: data.country.code ?? "",
          widthFlag: SizeConfig.safeBlockHorizontal * 5,
          maxWidthText: 150,
          textStyle: TextStyle(
            fontSize: 12,
          ),
        ),
      ),
      SizedBox(width: 10),
      GenderAndAge(
        gender: data.gender,
        age: data.age,
      ),
      // SizedBox(width: 10),
      // Container(
      //   padding: EdgeInsets.symmetric(
      //     horizontal: SizeConfig.safeBlockHorizontal * 3,
      //     vertical: SizeConfig.safeBlockHorizontal,
      //   ),
      //   decoration: BoxDecoration(
      //     border: Border.all(
      //       color: data.level.borderColor,
      //       width: 1,
      //     ),
      //     gradient: LinearGradient(
      //       begin: data.level.gradientBegin,
      //       end: data.level.gradientEnd,
      //       colors: data.level.colors,
      //     ),
      //     borderRadius: BorderRadius.circular(50),
      //   ),
      //   child: CustomText(
      //     "Lv.${data.level.value}",
      //     style: TextStyle(
      //       fontSize: 12,
      //       fontWeight: FontWeight.bold,
      //       color: Colors.white,
      //     ),
      //   ),
      // ),
    ],
  );
}

class GenderAndAge extends StatelessWidget {
  final Gender gender;
  final int age;

  const GenderAndAge({
    Key key,
    this.gender,
    this.age,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String iconPath = gender == Gender.FEMALE
        ? AssetsPath.fromImagesSvg('female_gender_ic')
        : AssetsPath.fromImagesSvg('male_gender_ic');

    return Container(
      padding: EdgeInsetsDirectional.fromSTEB(7, 3, 12, 3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: Colors.white.withOpacity(0.1),
      ),
      child: Row(
        children: [
          ShaderMask(
            shaderCallback: (bounds) {
              return LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [Color(0xffDF0C8B), Color(0xffF486C8)],
              ).createShader(bounds);
            },
            child: SvgPicture.asset(
              iconPath,
              height: SizeConfig.safeBlockHorizontal * 5,
            ),
          ),
          SizedBox(width: 5),
          CustomText(
            "${age > 0 ? age : "-"}",
            style: TextStyle(
              fontSize: 12,
            ),
          ),
        ],
      ),
    );
  }
}
