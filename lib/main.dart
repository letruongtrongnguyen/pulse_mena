import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutterbase/configs/environment.config.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/configs/theme.config.dart';
import 'package:flutterbase/core/i18n/i18n.dart';
import 'package:flutterbase/core/routes/routes.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/room.provider.dart';

import 'core/dependencies/dependenciesInjection.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await injection();
  getIt<Environment>().set("staging");
  runApp(App());
}

class App extends StatelessWidget {
  Widget build(BuildContext context) {
    ValueNotifier lang = getIt<GeneralProvider>().lang;
    ValueNotifier textDirection = getIt<GeneralProvider>().textDirection;
    ValueNotifier isDarkTheme = getIt<GeneralProvider>().isDarkTheme;

    return AnimatedBuilder(
      animation: Listenable.merge([lang, textDirection, isDarkTheme]),
      builder: (ctx, child) {
        I18n.set(getIt<GeneralProvider>().lang.value);

        return MaterialApp(
          debugShowCheckedModeBanner: true,
          routes: getIt<MyRouter>().routes,
          initialRoute: "/home-screen",
          theme: AppTheme.lightTheme,
          darkTheme: AppTheme.darkTheme,
          themeMode: isDarkTheme.value ? ThemeMode.dark : ThemeMode.light,
          builder: (context, child) {
            SizeConfig().init(context);
            return Directionality(
              textDirection: textDirection.value,
              child: child,
            );
          },
        );

        // return AdaptiveTheme(
        //   light: AppTheme.lightTheme,
        //   dark: AppTheme.darkTheme,
        //   builder: (theme, darkTheme) => MaterialApp(
        //     debugShowCheckedModeBanner: true,
        //     routes: getIt<MyRouter>().routes,
        //     initialRoute: "/more-screen",
        //     theme: theme,
        //     darkTheme: darkTheme,
        //     themeMode: ThemeMode.dark,
        //     builder: (context, child) {
        //       SizeConfig().init(context);
        //       return Directionality(
        //         textDirection: textDirection.value,
        //         child: child,
        //       );
        //     },
        //   ),
        // );
      },
    );
  }
}
