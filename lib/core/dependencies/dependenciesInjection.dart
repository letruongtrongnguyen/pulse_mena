import 'package:flutterbase/configs/environment.config.dart';
import 'package:flutterbase/configs/general.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/routes/routes.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/providers/level.provider.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/providers/relationship.provider.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/providers/search.provider.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

Future<void> injection() async {
  //Router
  getIt.registerSingleton<MyRouter>(MyRouter());

  //Configs
  getIt.registerSingleton<GeneralConfig>(GeneralConfig());

  //Environment
  getIt.registerSingleton<Environment>(Environment());

  //CommunicateService
  getIt.registerSingleton<CommunicateService>(CommunicateService());

  //Providers
  getIt.registerSingleton<GeneralProvider>(GeneralProvider());
  getIt.registerSingleton<MeProvider>(MeProvider());
  getIt.registerSingleton<RelationshipProvider>(RelationshipProvider());
  getIt.registerSingleton<RoomProvider>(RoomProvider());
  getIt.registerSingleton<LeaderboardProvider>(LeaderboardProvider());
  getIt.registerLazySingleton<SearchProvider>(() => SearchProvider());
  getIt.registerLazySingleton<LevelProvider>(() => LevelProvider());
}
