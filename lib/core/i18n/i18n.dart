import 'package:flutterbase/configs/general.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/i18n/languages/en.dart';
import 'package:flutterbase/core/i18n/languages/vi.dart';

import 'languages/ar.dart';

class I18n {
  static Map<String, String> _data;

  static String get(String key) {;
    return _data[key] ?? key;
  }

  static void set(String lang) {
    _data = SupportedLanguages.data[lang] ?? en;
  }
}

class SupportedLanguages {
  static Map<String, Map<String, String>> data = {
    "ar": ar,
    "en": en,
    "vi": vi,
  };

  SupportedLanguages();
}
