final Map<String, String> ar = {
  "home.name": "This is Home",

//Common
  "common.leaderboard": "المتصدرين",
  "common.daily": "يوميا",
  "common.weekly": "أسبوعيا",
  "common.monthly": "شهريا",
  "common.recently": "مؤخرًا",
  "common.store": "متجر",
  "common.topupStore": "متجر الشحن",
  "common.itemStore": "متجر العناصر",
  "common.wallet": "محفظة",
  "common.myBag": "حقيبتي",
  "common.history": "تاريخ",
  "common.friendList": "قائمة الاصدقاء",
  "common.level": "مستوى",
  "common.helpCenter": "مساعدة",
  "common.generalSetting": "إعدادات",
  "common.activityCenter": "مركز النشاطات",
  "common.dailyLogin": "تسجيل الدخول اليومي",
  "common.taskCenter": "مركز المهام",
  "common.task": "مهمة",
  "common.vipBadges": "شارات كبار الشخصيات",
  "common.hakiVip": "هاكي VIP",
  "common.feedback": "تعليق",
  "common.following": "تابع",
  "common.accept": "إقبل",
  "common.accepted": "تم القبول",
  "common.cancel": "إلغي",
  "common.canceled": "تم الإلغاء",
  "common.ok": "موافق",
  "common.loading": "جار التحميل...",
  "common.emptyRoom": "لا غرفة",
  "common.noResult": "لا نتيجة",
  "common.addFriend": "أضف صديق",
  "common.friend": "صديق",
  "common.pendingAccept": "في انتظار القبول",
  "common.noNotification": "لا يوجد إشعارات",
  "common.viewAll": "مشاهدة الكل",
  "common.send": "إرسل",
  "common.sending": "إرسال...",
  "common.receivedGift": "استلمت هدية",
  "common.official": "رسمي",
  "common.browseMoreRooms": "تصفح المزيد من الغرف",
  "common.noMoreRoom": "لا مزيد من الغرف",
  "common.soon": "قريبًا",
  "common.more": "أكثر",
  "common.chat": "راسل",
  "common.sendGift": "أرسل هدية",
//// New
////

//Retry module
  "retry.btn.title": "حاول مرة أخرى",
  "retry.title": "أيقظ اتصالك ",
  "retry.subtitle": "يبدو إن الإنترنت بطيئًا للغاية \n لتحميل هذه الصفحة",
////New
////

//More screen
  "more.title": "أكثر",
  "more.leaderboardModule.hotIdol": "أفضل 1 محبوب جماهير",
  "more.leaderboardModule.hotBuyer": "أفضل 1 مشترٍ تحمس",
  "more.hotAudioRoom": "غرفة صوتية مثيرة",
  "more.friend.currency": "صديق (أصدقاء)",
  "more.request.currency": "طلب (طلبات)",
//// New
////

//Profile screen
  "profile.title": "الملف الشخصي",
  "profile.visitor.currency": "زائر (زائرين)",
  "profile.friend.currency": "صديق (أصدقاء)",
  "profile.day.currency": "يوم (أيام)",
  "profile.menu.badge": "شارة",
  "profile.menu.interest": "اهتمام",
  "profile.menu.joined": "انضم",
  "profile.btn.addFriend": "أضف صديق",
  "profile.btn.unFriend": "الغاء الصداقه",
//// New
////

//Room screen
  "room.title": "مركز الغرف",
  "room.menuTitle.countries": "بلدان",
  "room.menuTitle.myRoom": "غرفتي",
  "room.menuTitle.roomList": "قائمة الغرف",
  "room.tab.hotRoom": "غرفة مثيرة",
  "room.tab.newRoom": "غرفة جديدة",
  "room.tag.mine": "الخاص بي",
  "room.btn.create": "قم بإنشاء غرفة",
  "room.create.description": "أبجد هوز",
//// New
////

//Home screen
  "home.title": "الصفحة الرئيسية",
  "home.tabbar.title.forYou": "لك",
  "home.tabbar.title.all": "الكل",
  "home.session.title.myRoom": "غرفتي",
  "home.session.title.recently": "انضم مؤخرا",
  "home.session.title.newRooms": "غرف جديدة",
  "home.session.title.mostPopular": '''إليك أكثر الغرف شهرة في
  مجتمع هاكي حاليًا''',
  "home.welcomeBack": "مرحبا بعودتك،",
  "home.createRoom": "قم بإنشاء غرفة الصوت الخاصة بك",
  "home.calendar": "تقاويم البث الخاصة بنا",
  "home.calendar.empty": "ترقبوا أكثر المذيعين إثارة لليوم",
  "home.topTalents": "أفضل المواهب",
//// New
////

//Discover screen
  "discover.title": "أكتشف",
  "discover.menuTitle.countries": "بلدان",
  "discover.menuTitle.activities": "أنشطة",
  "discover.menuTitle.recommendedRoom": "موصى به لك",
//// New
////

//Notification screen
  "notification.title": "مركز الإشعارات",
  "notification.session.system": "رسائل النظام",
  "notification.session.friendRequests": "طلبات صداقة",
  "notification.session.receivedGifts": "الهدايا المستلمة",
  "notification.session.luckyHaki": "لاكي هاكي",
  "notification.friendRequest.content": "أرسل طلب صداقة",
  "notification.giftSend.content": "ارسل لك",
//// New
////

//Leaderboard screen
  "leaderboard.tabTitle.topRoomLevel": "أفضل مستوى للغرفة",
  "leaderboard.tabTitle.topRoomGift": "أفضل غرفة",
  "leaderboard.tabTitle.topBuyer": "الأثرياء",
  "leaderboard.tabTitle.topHotIdol": "أفضل محبوب جماهير",
  "leaderboard.tabTitle.topGiftSender": "أعلى مرسل للهدايا",
  "leaderboard.tabTitle.topGiftReceiver": "أعلى متلقي للهدايا",
  "leaderboard.tabTitle.topWinnerCoin": "أفضل الفائزين في مسابقة Lucky Haki",
  "leaderboard.weeklyTitle.topRoomLevel": "أفضل مستوى للغرفة هذا الأسبوع",
  "leaderboard.weeklyTitle.topRoomGift": "أفضل غرفة هذا الأسبوع",
  "leaderboard.weeklyTitle.topBuyer": "أثرياء هذا الأسبوع",
  "leaderboard.weeklyTitle.topHotIdol": "أفضل محبوب جماهير هذا الأسبوع",
  "leaderboard.weeklyTitle.topGiftSender": "أعلى مرسلي الهدايا هذا الأسبوع",
  "leaderboard.weeklyTitle.topGiftReceiver": "اعلى متلقي هدايا هذا الأسبوع",
  "leaderboard.weeklyTitle.topWinnerCoin": "أفضل الفائزين في Lucky Haki هذا الأسبوع",
  "leaderboard.monthlyTitle.topRoomLevel": "أعلى مستوى للغرفة هذا الشهر",
  "leaderboard.monthlyTitle.topRoomGift": "أفضل غرفة هذا الشهر",
  "leaderboard.monthlyTitle.topBuyer": "أثرياء هذا الشهر",
  "leaderboard.monthlyTitle.topHotIdol": "أفضل المشاهير هذا الشهر",
  "leaderboard.monthlyTitle.topGiftSender": "أفضل مرسلي الهدايا هذا الشهر",
  "leaderboard.monthlyTitle.topGiftReceiver": "أفضل متلقي الهدايا هذا الشهر",
  "leaderboard.monthlyTitle.topWinnerCoin": "أفضل الفائزين في Lucky Haki هذا الشهر",
  "leaderboard.outRank": "أنت لست في قائمة أفضل 100",
  "leaderboard.emptyMessage": "تفاعل في غرفتك أكثر\nللوصول إلى لوحة المتصدرين",
////New
////

//Level screen
////New
  "level.levelUp.title": "How to level up?",
  "level.label.login": "Login",
  "level.description.login": "Login daily to earn 1 Xp per day",
  "level.label.sendGifts": "Send Gifts",
  "level.description.sendGifts": "Every coin spent will grant you 5 Xp",
  "level.label.recharge": "Recharge",
  "level.description.recharge": "Every coin recharged will grant you 5 Xp",
  "level.label.takeMic": "Take mic",
  "level.description.takeMic":
      "Every 10 min of taking mic will grant you 10 Xp",
  "level.label.earnGems": "Earn Gems",
  "level.description.earnGems": "Every 2 gems earned will grant you  1 Xp",
  "level.label.sendMessage": "Send Message",
  "level.description.sendMessage": "Every message sent will grant you 3 Xp",
  "level.pendantRewards.title": "Pendant Rewards",
////

//Search screen
  "search.input": "ابحث عن اسم الغرف أو وصف الغرف",
  "search.room": "غرفة(غرف)",
////New
////

//Task center screen
  "taskCenter.title": "مركز المهام",
  "taskCenter.session.TasksCenter": "مركز المهام",
  "taskCenter.session.dailyTasks": "مهمات يوميه",
////New
////

//Activity center screen
  "activityCenter.title": "مركز النشاطات",
  "activityCenter.sessionTitle.recentlyJoined": "انضم مؤخرا",
  "activityCenter.sessionTitle.allActivities": "كل الأنشطة",
  "activityCenter.action.play": "إلعب",
////New
////

//Help center screen
  "helpCenter.title": "مركز مساعدة هاكي",
  "helpCenter.sessionTitle.helping": "مرحبا كيف يمكننا مساعدتك؟",
  "helpCenter.sessionTitle.talkToAdmin": "ألم تجد ما تريده؟ تحدث إلى المسؤول",
  "helpCenter.action.reportAnIssue": "بلغ عن خطأ",
  "helpCenter.action.reviewApplication": "مراجعة التطبيق",
  "helpCenter.action.forgotPassword": "هل نسيت كلمة السر",
  "helpCenter.action.communityRegulation": "قواعد المجتمع",
  "helpCenter.action.termPolicy": "الشروط والسياسة",
////New
////

//LeaderboardScreen
////New
  "leaderboard.comingSoon":
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
////

//HakiMailBox
////New
  "hakiMailBox.title": "Haki Mailbox",
  "hakiMailBox.comingSoon":
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
////

//Feedback screen
////New
  "feedback.subtitle": "Give us feedback about a Haki feature!",
  "feedback.inputTitle.opinion": "Your feedback",
  "feedback.inputPlaceholder.opinion": "Please tell us you’re opinion.",
  "feedback.inputTitle.contact": "How shall we contact you",
  "feedback.inputPlaceholder.contact": "Leave your contacts here.",
  "feedback.thankYouPopup.title": "Thank you",
  "feedback.thankYouPopup.content":
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vulputate.",
  "feedback.content": '''Thanks for taking the time to give us feedback.
Though we can’t review and respond to every submission, we do use feedback like yours to improve the Haki experience for everyone.

We may use feedback or suggestions submitted by the community without any restriction or obligation to provide compensation for them or keep them confidential.''',
////

//ComingSoonPopup
  "comingSoonPopup.title": "زرنا مرة أخرى قريبًا",
  "comingSoonPopup.content": "من فضلك ترقب ، هاكي يجهز شيئًا مميزًا لك.",
  "comingSoonPopup.button": "أرى",
////New
////

//Rate app dialog
////New
  "rateAppDialog.title": "قيم هذا التطبيق",
  "rateAppDialog.content":
      "هل تحب هذا التطبيق؟ خذ بعض الوقت لتترك تقييمًا:",
////

//Activitis
  "specialOffer.title": "عرض خاص",
  "specialOffer.content1": "احصل على مكافأة x2 عند شحن العملات المعدنية لأول مرة!",
  "specialOffer.content2": "احصل على خصم 25٪ عند عملية الشراء الثانية!",
  "luckyHaki.content":
  "اختر طعامك المفضل واربح جوائز كبيرة. مجموع جوائز يصل إلى 1،000،000 قطعة نقدية",

//Term & Policy screen
////New
  "termPolicy.title": "Term & Policy",
  "termPolicy.sessionTitle.introduction": "Introduction",
  "termPolicy.content.introduction":
      "This Privacy Policy applies across all websites that we own and operate and all services we provide, including all customer support websites, applications, web services. For the purpose of this notice, we’ll just call them our ‘services’. When we say ‘personal data’ we mean identifiable information about you, like your name, email, address, telephone number, bank account details, payment information, support queries, community comments and so on. If you can’t be identified (for example, when personal data has been aggregated and anonymised) then this notice doesn’t apply. Check out our terms and conditions for more information on how we treat your other data for each individual service. We will keep this privacy policy updated and will keep a date issued reference at the bottom of this document.",
  "termPolicy.sessionTitle.about": "Who are ‘we’?",
  "termPolicy.content.about":
      "When we refer to ‘we’ (or ‘our’ or ‘us’), that means Pulse Pte LTD Our headquarters are in Singapore. We provide services for the enjoyment of people and to maximise the potential of the mobile phone in all parts of the world.",
  "termPolicy.sessionTitle.dataProtection": "Our principles of data protection",
  "termPolicy.content.dataProtection":
      '''Our approach to data protection is built around three key principles and in accordance to the General Data Protection Regulation set by the European Union. These principles are:
Transparency:
We ensure that your data is accessible and can be retrieved at your discretion.
Privacy:
We will not share your data with any third party that isn’t acting in your best interest. We may share your data with partners who will act with your best interests at heart e.g. aggregators working with your mobile operator, regulatory bodies etc.
Security:
We ensure your information is kept secure on our servers with our expert team keeping tabs on and eliminating any potential threats.
Stewardship:
We accept the responsibility that comes with processing and storing personal data.''',
  "termPolicy.sessionTitle.collectYourData": "How we collect your data",
  "termPolicy.content.collectYourData":
      '''When you visit our websites or use our services, we collect personal data. The ways we collect it can be generally categorised below:
Information you provide to us directly:
When you visit or use some parts of our services we might ask you to provide personal data to us. For example, we ask for your contact information when you sign up for a service whilst using WiFi, or when creating your own account on one of our services. If you don’t want to provide us with personal data, you don’t have to, but it might mean you can’t use some parts of our services.
Information we collect automatically:
We collect some information about you automatically when you visit our websites or use our services, like your IP address and device type. We also collect information when you navigate through our websites and services, including what pages you looked at and what links you clicked on. This information is useful for us as it helps us get a better understanding of how you’re using our websites and services so that we can continue to provide the best experience possible (eg. by improving our services continuously to give you a better experience.). Some of this information is collected using cookies and similar tracking technologies.
Information we get from third parties:
Depending on the region you are in and the service being interacted with, we will collect information from third parties. These third parties will usually be an aggregator working on the behalf of your operator to communicate to us who you are (usually an encryption of your mobile phone number), process a transaction if requested and then provide us the result of said transaction. Where we collect personal data, we’ll only process it:
• to perform a contract with you, or
• where we have legitimate interests to process the personal data and they’re not overridden by your rights, or
• in accordance with a legal obligation, or
• where we have your consent.
If we don’t collect your personal data, we may be unable to provide you with all our services. If you think we have your information but you have never entered it, you’ll need to contact our customer service team, whereby contact information will be available in the service in question, or can be found here: http://Pulse-Web.com''',
  "termPolicy.sessionTitle.useYourData": "How we use your data",
  "termPolicy.content.useYourData":
      '''Primarily, we use your personal data to provide you with any services you’ve requested, and to manage our relationship with you whilst staying compliant with any regulations set by the territories regulator. We also use your personal data for other purposes, which may include the following: To communicate with you.
This may include:
• providing you with information you’ve requested from us (like PIN’s to access the service) or information we are required to send to you
• asking you for feedback or to take part in any research we are conducting (which we may engage a third party to assist with).
To support you:
This may include assisting with the resolution of customer queries via e-mail or social media sites, or any other way you have communicated with us.
To enhance our websites and services and develop new ones:
For example, by tracking and monitoring your use of websites and services so we can keep improving, or by carrying out technical analysis of our services so that we can optimise your user experience and provide you with the optimum experience.
To protect:
So that we can detect and prevent any fraudulent or malicious activity, and make sure that everyone is using our services fairly and in accordance with our terms of use. To analyse, aggregate and report:  We may use the personal data we collect about you and other users of our websites and services (whether obtained directly or from third parties) to produce aggregated and anonymised analytics and reports, which we may share publicly or with third parties.''',
  "termPolicy.sessionTitle.shareYourData": "How we can share your data",
  "termPolicy.content.shareYourData":
      '''There will be times when we need to share your personal data with third parties. We will only disclose your personal data to:
• third party aggregators and partners who assist and enable us to use the personal data to, for example, support delivery of or provide functionality of our services, or
to market or promote our goods and services to you.
• regulators, law enforcement bodies, government agencies, courts or other third parties where we think it’s necessary to comply with applicable laws or regulations, or
to exercise, establish or defend our legal rights. Where possible and appropriate, we will notify you of this type of disclosure.
• an actual or potential buyer or investor (and its agents and advisers) in connection with an actual or proposed purchase or investment, merger or acquisition of any
part of our business.
• other people where we have your consent.''',
  "termPolicy.sessionTitle.internationalDataTransfers":
      "International Data Transfers",
  "termPolicy.content.internationalDataTransfers":
      '''When we share data, it may be tran sferred to, and processed in, countries other than the country you live in. These countries may have laws different to what you’re used to. Rest assured, where we disclose personal data to a third party in another country, we put safeguards in place to ensure your personal data remains protected. This also is the case for where we store your data outside of the country you live in. For individuals in the European Economic Area (EEA), this means that your data may be transferred outside of the EEA. Where your personal data is transferred outside the EEA, it will only be transferred to countries that have been identified as providing adequate protection for EEA data, or to a third party where we have approved transfer mechanisms in place to protect your personal data. For more information on where your data is being held, please refer to the services’ terms and/or get in touch with us.''',
  "termPolicy.sessionTitle.security": "Security",
  "termPolicy.content.security":
      '''Security is paramount for us when it comes to your personal data. We’re committed to protecting your personal data and have appropriate technical and organisational measures in place to make sure that happens, with an expert team of IT Security specialists in place to protect your data from any malicious activities.''',
  "termPolicy.sessionTitle.retention": "Retention",
  "termPolicy.content.retention":
      '''The length of time we keep your personal data depends on what it is and whether we have an ongoing business need to retain it (for example, to provide you with a service you’ve requested or to comply with applicable legal requirements). We’ll retain your personal data for as long as we have a relationship with you and for a period of time afterwards where we have an ongoing business need to retain it, in accordance with our data retention policies and practices. Following that period, we’ll make sure it’s deleted or anonymised.''',
  "termPolicy.sessionTitle.yourRights": "Your rights",
  "termPolicy.content.yourRights":
      '''It’s your personal data and you have certain rights relating to it. When it comes to our ongoing communications with you e.g. reminder messages for a service you enjoy provided by us, you can just opt out by multiple avenues detailed in the terms of that service and in the messages themselves.
You also have rights to:
• know what personal data we hold about you, and to make sure it’s correct and up to date
• request a copy of your personal data, or ask us to restrict processing your personal data or delete it
• object to our continued processing of your personal data
You can exercise these rights at any time by sending an email to cs@Pulse-Web.com If you’re not happy with how we are processing your personal data, please let us know by sending an email to cs@Pulse-Web.com .We will review and investigate your complaint, and try to get back to you within a reasonable time frame.''',
  "termPolicy.sessionTitle.contactUs": "How to contact us",
  "termPolicy.content.contactUs":
      '''We’re always keen to hear from you. If you’re curious about what personal data we hold about you or you have a question or feedback for us on this notice, our websites or services, please get in touch. As a technology company, we prefer to communicate with you by email – this ensures that you’re put in contact with the right person, in the right location, and in accordance with any regulatory time frames. Our email is cs@Pulse-Web.com''',
////
};
