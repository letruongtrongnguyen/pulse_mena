final Map<String, String> en = {
  "home.name": "This is Home",

//Common
  "common.leaderboard": "Leaderboard",
  "common.daily": "Daily",
  "common.weekly": "Weekly",
  "common.monthly": "Monthly",
  "common.recently": "Recently",
  "common.store": "Store",
  "common.topupStore": "Topup Store",
  "common.itemStore": "Item Store",
  "common.wallet": "Wallet",
  "common.myBag": "My Bag",
  "common.history": "History",
  "common.friendList": "Friend List",
  "common.level": "Level",
  "common.helpCenter": "Help",
  "common.generalSetting": "Settings",
  "common.activityCenter": "Activity Center",
  "common.dailyLogin": "Daily Login",
  "common.taskCenter": "Task Center",
  "common.task": "Task",
  "common.vipBadges": "Vip Badges",
  "common.hakiVip": "Haki VIP",
  "common.feedback": "Feedback",
  "common.following": "Follow",
  "common.accept": "Accept",
  "common.accepted": "Accepted",
  "common.cancel": "Cancel",
  "common.canceled": "Canceled",
  "common.ok": "OK",
  "common.loading": "Loading...",
  "common.emptyRoom": "No Room",
  "common.noResult": "No Result",
  "common.addFriend": "Add friend",
  "common.friend": "Friend",
  "common.pendingAccept": "Pending Accept",
  "common.noNotification": "No Notification",
  "common.viewAll": "View all",
  "common.send": "Send",
  "common.sending": "Sending...",
  "common.receivedGift": "Received Gift",
  "common.official": "Official",
  "common.browseMoreRooms": "Browse more rooms",
  "common.noMoreRoom": "No more room",
  "common.soon": "SOON",
  "common.more": "More",
  "common.chat": "Chat",
  "common.sendGift": "Send Gift",
//// New
////

//Retry module
  "retry.btn.title": "Try Again",
  "retry.title": "Wake up your connection",
  "retry.subtitle": "Your internet seems too slow\nto load this page",
////New
////

//More screen
  "more.title": "More",
  "more.leaderboardModule.hotIdol": "Top 1 Hot Idol",
  "more.leaderboardModule.hotBuyer": "Top 1 Hot Buyer",
  "more.hotAudioRoom": "Hot Audio Room",
  "more.friend.currency": "Friend(s)",
  "more.request.currency": "request(s)",
//// New
////

//Profile screen
  "profile.title": "Profile",
  "profile.visitor.currency": "Visitor(s)",
  "profile.friend.currency": "Friend(s)",
  "profile.day.currency": "day(s)",
  "profile.menu.badge": "Badge",
  "profile.menu.interest": "Interest",
  "profile.menu.joined": "Joined",
  "profile.btn.addFriend": "Add Friend",
  "profile.btn.unFriend": "Unfriend",
//// New
////

//Room screen
  "room.title": "Room Center",
  "room.menuTitle.countries": "Countries",
  "room.menuTitle.myRoom": "My Room",
  "room.menuTitle.roomList": "Room List",
  "room.tab.hotRoom": "Hot Room",
  "room.tab.newRoom": "New Room",
  "room.tag.mine": "Mine",
  "room.btn.create": "Create your room",
  "room.create.description": "Lorem ipsum dolor sit amet",
//// New
////

//Home screen
  "home.title": "Home",
  "home.tabbar.title.forYou": "FOR YOU",
  "home.tabbar.title.all": "ALL",
  "home.session.title.myRoom": "My Room",
  "home.session.title.recently": "Recently joined",
  "home.session.title.newRooms": "New Rooms",
  "home.session.title.mostPopular": '''Here are most popular rooms on
  Haki community right now''',
  "home.welcomeBack": "Welcome Back,",
  "home.createRoom": "Create Your Audio Room",
  "home.calendar": "Broadcasting calendars of our",
  "home.calendar.empty": "Stay tuned for hottest BC of the day...",
  "home.topTalents": "Top Talents",
//// New
////

//Discover screen
  "discover.title": "Discover",
  "discover.menuTitle.countries": "Countries",
  "discover.menuTitle.activities": "Activities",
  "discover.menuTitle.recommendedRoom": "Recommended for you",
//// New
////

//Notification screen
  "notification.title": "Notifications Center",
  "notification.session.system": "System Messages",
  "notification.session.friendRequests": "Friend Requests",
  "notification.session.receivedGifts": "Received Gifts",
  "notification.session.luckyHaki": "Lucky Haki",
  "notification.friendRequest.content": "sent a friend request",
  "notification.giftSend.content": "sent you",
//// New
////

//Leaderboard screen
  "leaderboard.tabTitle.topRoomLevel": "Top Room Level",
  "leaderboard.tabTitle.topRoomGift": "Top Room",
  "leaderboard.tabTitle.topBuyer": "Top Wealthy",
  "leaderboard.tabTitle.topHotIdol": "Top Hot Idol",
  "leaderboard.tabTitle.topGiftSender": "Top Gift Sender",
  "leaderboard.tabTitle.topGiftReceiver": "Top Gift Receivers",
  "leaderboard.tabTitle.topWinnerCoin": "Top Lucky Haki Winners",
  "leaderboard.weeklyTitle.topRoomLevel": "Top Room Level this week",
  "leaderboard.weeklyTitle.topRoomGift": "Top Room this week",
  "leaderboard.weeklyTitle.topBuyer": "Top Wealthy this week",
  "leaderboard.weeklyTitle.topHotIdol": "Top Hot Idol this week",
  "leaderboard.weeklyTitle.topGiftSender": "Top Gift Sender this week",
  "leaderboard.weeklyTitle.topGiftReceiver": "Top Gift Receivers this week",
  "leaderboard.weeklyTitle.topWinnerCoin": "Top Lucky Haki Winners this week",
  "leaderboard.monthlyTitle.topRoomLevel": "Top Room Level this month",
  "leaderboard.monthlyTitle.topRoomGift": "Top Room this month",
  "leaderboard.monthlyTitle.topBuyer": "Top Wealthy this month",
  "leaderboard.monthlyTitle.topHotIdol": "Top Hot Idol this month",
  "leaderboard.monthlyTitle.topGiftSender": "Top Gift Sender this month",
  "leaderboard.monthlyTitle.topGiftReceiver": "Top Gift Receivers this month",
  "leaderboard.monthlyTitle.topWinnerCoin": "Top Lucky Haki Winners this month",
  "leaderboard.outRank": "You are not in the top 100",
  "leaderboard.emptyMessage":
      "Interact in your room more to\nget to the Leaderboard",
////New
////

//Level screen
////New
  "level.levelUp.title": "How to level up?",
  "level.label.login": "Login",
  "level.description.login": "Login daily to earn 1 Xp per day",
  "level.label.sendGifts": "Send Gifts",
  "level.description.sendGifts": "Every coin spent will grant you 5 Xp",
  "level.label.recharge": "Recharge",
  "level.description.recharge": "Every coin recharged will grant you 5 Xp",
  "level.label.takeMic": "Take mic",
  "level.description.takeMic":
      "Every 10 min of taking mic will grant you 10 Xp",
  "level.label.earnGems": "Earn Gems",
  "level.description.earnGems": "Every 2 gems earned will grant you  1 Xp",
  "level.label.sendMessage": "Send Message",
  "level.description.sendMessage": "Every message sent will grant you 3 Xp",
  "level.pendantRewards.title": "Pendant Rewards",
////

//Search screen
  "search.input": "Search for rooms\' name or rooms\' description",
  "search.room": "Room(s)",
////New
////

//Task center screen
  "taskCenter.title": "Task Center",
  "taskCenter.session.TasksCenter": "Tasks Center",
  "taskCenter.session.dailyTasks": "Daily Tasks",
////New
////

//Activity center screen
  "activityCenter.title": "Activity Center",
  "activityCenter.sessionTitle.recentlyJoined": "Recently Joined",
  "activityCenter.sessionTitle.allActivities": "All Activities",
  "activityCenter.action.play": "Play",
////New
////

//Help center screen
  "helpCenter.title": "Haki Help Center",
  "helpCenter.sessionTitle.helping": "Hello, how can we help you?",
  "helpCenter.sessionTitle.talkToAdmin":
      "Not finding what you need? Talk to an Admin",
  "helpCenter.action.reportAnIssue": "Report An Issue",
  "helpCenter.action.reviewApplication": "Review Application",
  "helpCenter.action.forgotPassword": "Forgot Password",
  "helpCenter.action.communityRegulation": "Community Regulation",
  "helpCenter.action.termPolicy": "Terms & Policy",
////New
////

//LeaderboardScreen
////New
  "leaderboard.comingSoon":
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
////

//HakiMailBox
////New
  "hakiMailBox.title": "Haki Mailbox",
  "hakiMailBox.comingSoon":
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
////

//Feedback screen
////New
  "feedback.subtitle": "Give us feedback about a Haki feature!",
  "feedback.inputTitle.opinion": "Your feedback",
  "feedback.inputPlaceholder.opinion": "Please tell us you’re opinion.",
  "feedback.inputTitle.contact": "How shall we contact you",
  "feedback.inputPlaceholder.contact": "Leave your contacts here.",
  "feedback.thankYouPopup.title": "Thank you",
  "feedback.thankYouPopup.content":
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vulputate.",
  "feedback.content": '''Thanks for taking the time to give us feedback.
Though we can’t review and respond to every submission, we do use feedback like yours to improve the Haki experience for everyone.

We may use feedback or suggestions submitted by the community without any restriction or obligation to provide compensation for them or keep them confidential.''',
////

//ComingSoonPopup
  "comingSoonPopup.title": "Drop by again soon",
  "comingSoonPopup.content":
      "Please stay tuned, Haki is  building something special for you.",
  "comingSoonPopup.button": "I See",
////New
////

//Rate app dialog
////New
  "rateAppDialog.title": "Rate this app",
  "rateAppDialog.content":
      "You like this app ? Then take a little bit of your time to leave a rating:",
////

//Activitis
  "specialOffer.title": "Special Offer",
  "specialOffer.content1": "Get x2 bonus when top-up Coins for the first time!",
  "specialOffer.content2": "Get 25% discount at the second purchase!",
  "luckyHaki.content":
      "Choose your favorite food and win big prizes. Prize pool up to 1,000,000 Coins",

//Term & Policy screen
////New
  "termPolicy.title": "Terms & Policy",
  "termPolicy.sessionTitle.introduction": "Introduction",
  "termPolicy.content.introduction":
      "This Privacy Policy applies across all websites that we own and operate and all services we provide, including all customer support websites, applications, web services. For the purpose of this notice, we’ll just call them our ‘services’. When we say ‘personal data’ we mean identifiable information about you, like your name, email, address, telephone number, bank account details, payment information, support queries, community comments and so on. If you can’t be identified (for example, when personal data has been aggregated and anonymised) then this notice doesn’t apply. Check out our terms and conditions for more information on how we treat your other data for each individual service. We will keep this privacy policy updated and will keep a date issued reference at the bottom of this document.",
  "termPolicy.sessionTitle.about": "Who are ‘we’?",
  "termPolicy.content.about":
      "When we refer to ‘we’ (or ‘our’ or ‘us’), that means Pulse Pte LTD Our headquarters are in Singapore. We provide services for the enjoyment of people and to maximise the potential of the mobile phone in all parts of the world.",
  "termPolicy.sessionTitle.dataProtection": "Our principles of data protection",
  "termPolicy.content.dataProtection":
      '''Our approach to data protection is built around three key principles and in accordance to the General Data Protection Regulation set by the European Union. These principles are:
Transparency:
We ensure that your data is accessible and can be retrieved at your discretion.
Privacy:
We will not share your data with any third party that isn’t acting in your best interest. We may share your data with partners who will act with your best interests at heart e.g. aggregators working with your mobile operator, regulatory bodies etc.
Security:
We ensure your information is kept secure on our servers with our expert team keeping tabs on and eliminating any potential threats.
Stewardship:
We accept the responsibility that comes with processing and storing personal data.''',
  "termPolicy.sessionTitle.collectYourData": "How we collect your data",
  "termPolicy.content.collectYourData":
      '''When you visit our websites or use our services, we collect personal data. The ways we collect it can be generally categorised below:
Information you provide to us directly:
When you visit or use some parts of our services we might ask you to provide personal data to us. For example, we ask for your contact information when you sign up for a service whilst using WiFi, or when creating your own account on one of our services. If you don’t want to provide us with personal data, you don’t have to, but it might mean you can’t use some parts of our services.
Information we collect automatically:
We collect some information about you automatically when you visit our websites or use our services, like your IP address and device type. We also collect information when you navigate through our websites and services, including what pages you looked at and what links you clicked on. This information is useful for us as it helps us get a better understanding of how you’re using our websites and services so that we can continue to provide the best experience possible (eg. by improving our services continuously to give you a better experience.). Some of this information is collected using cookies and similar tracking technologies.
Information we get from third parties:
Depending on the region you are in and the service being interacted with, we will collect information from third parties. These third parties will usually be an aggregator working on the behalf of your operator to communicate to us who you are (usually an encryption of your mobile phone number), process a transaction if requested and then provide us the result of said transaction. Where we collect personal data, we’ll only process it:
• to perform a contract with you, or
• where we have legitimate interests to process the personal data and they’re not overridden by your rights, or
• in accordance with a legal obligation, or
• where we have your consent.
If we don’t collect your personal data, we may be unable to provide you with all our services. If you think we have your information but you have never entered it, you’ll need to contact our customer service team, whereby contact information will be available in the service in question, or can be found here: http://Pulse-Web.com''',
  "termPolicy.sessionTitle.useYourData": "How we use your data",
  "termPolicy.content.useYourData":
      '''Primarily, we use your personal data to provide you with any services you’ve requested, and to manage our relationship with you whilst staying compliant with any regulations set by the territories regulator. We also use your personal data for other purposes, which may include the following: To communicate with you.
This may include:
• providing you with information you’ve requested from us (like PIN’s to access the service) or information we are required to send to you
• asking you for feedback or to take part in any research we are conducting (which we may engage a third party to assist with).
To support you:
This may include assisting with the resolution of customer queries via e-mail or social media sites, or any other way you have communicated with us.
To enhance our websites and services and develop new ones:
For example, by tracking and monitoring your use of websites and services so we can keep improving, or by carrying out technical analysis of our services so that we can optimise your user experience and provide you with the optimum experience.
To protect:
So that we can detect and prevent any fraudulent or malicious activity, and make sure that everyone is using our services fairly and in accordance with our terms of use. To analyse, aggregate and report:  We may use the personal data we collect about you and other users of our websites and services (whether obtained directly or from third parties) to produce aggregated and anonymised analytics and reports, which we may share publicly or with third parties.''',
  "termPolicy.sessionTitle.shareYourData": "How we can share your data",
  "termPolicy.content.shareYourData":
      '''There will be times when we need to share your personal data with third parties. We will only disclose your personal data to:
• third party aggregators and partners who assist and enable us to use the personal data to, for example, support delivery of or provide functionality of our services, or
to market or promote our goods and services to you.
• regulators, law enforcement bodies, government agencies, courts or other third parties where we think it’s necessary to comply with applicable laws or regulations, or
to exercise, establish or defend our legal rights. Where possible and appropriate, we will notify you of this type of disclosure.
• an actual or potential buyer or investor (and its agents and advisers) in connection with an actual or proposed purchase or investment, merger or acquisition of any
part of our business.
• other people where we have your consent.''',
  "termPolicy.sessionTitle.internationalDataTransfers":
      "International Data Transfers",
  "termPolicy.content.internationalDataTransfers":
      '''When we share data, it may be tran sferred to, and processed in, countries other than the country you live in. These countries may have laws different to what you’re used to. Rest assured, where we disclose personal data to a third party in another country, we put safeguards in place to ensure your personal data remains protected. This also is the case for where we store your data outside of the country you live in. For individuals in the European Economic Area (EEA), this means that your data may be transferred outside of the EEA. Where your personal data is transferred outside the EEA, it will only be transferred to countries that have been identified as providing adequate protection for EEA data, or to a third party where we have approved transfer mechanisms in place to protect your personal data. For more information on where your data is being held, please refer to the services’ terms and/or get in touch with us.''',
  "termPolicy.sessionTitle.security": "Security",
  "termPolicy.content.security":
      '''Security is paramount for us when it comes to your personal data. We’re committed to protecting your personal data and have appropriate technical and organisational measures in place to make sure that happens, with an expert team of IT Security specialists in place to protect your data from any malicious activities.''',
  "termPolicy.sessionTitle.retention": "Retention",
  "termPolicy.content.retention":
      '''The length of time we keep your personal data depends on what it is and whether we have an ongoing business need to retain it (for example, to provide you with a service you’ve requested or to comply with applicable legal requirements). We’ll retain your personal data for as long as we have a relationship with you and for a period of time afterwards where we have an ongoing business need to retain it, in accordance with our data retention policies and practices. Following that period, we’ll make sure it’s deleted or anonymised.''',
  "termPolicy.sessionTitle.yourRights": "Your rights",
  "termPolicy.content.yourRights":
      '''It’s your personal data and you have certain rights relating to it. When it comes to our ongoing communications with you e.g. reminder messages for a service you enjoy provided by us, you can just opt out by multiple avenues detailed in the terms of that service and in the messages themselves.
You also have rights to:
• know what personal data we hold about you, and to make sure it’s correct and up to date
• request a copy of your personal data, or ask us to restrict processing your personal data or delete it
• object to our continued processing of your personal data
You can exercise these rights at any time by sending an email to cs@Pulse-Web.com If you’re not happy with how we are processing your personal data, please let us know by sending an email to cs@Pulse-Web.com .We will review and investigate your complaint, and try to get back to you within a reasonable time frame.''',
  "termPolicy.sessionTitle.contactUs": "How to contact us",
  "termPolicy.content.contactUs":
      '''We’re always keen to hear from you. If you’re curious about what personal data we hold about you or you have a question or feedback for us on this notice, our websites or services, please get in touch. As a technology company, we prefer to communicate with you by email – this ensures that you’re put in contact with the right person, in the right location, and in accordance with any regulatory time frames. Our email is cs@Pulse-Web.com''',
////
};
