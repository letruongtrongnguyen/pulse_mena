import 'package:flutter/cupertino.dart';
import 'package:flutterbase/screens/activity_center/activity_center.screen.dart';
import 'package:flutterbase/screens/discover/discover.screen.dart';
import 'package:flutterbase/screens/discover/sub_screens/country_filter.screen.dart';
import 'package:flutterbase/screens/discover/sub_screens/country_full_list.screen.dart';
import 'package:flutterbase/screens/feedback/feedback.screen.dart';
import 'package:flutterbase/screens/haki_mailbox/haki_mailbox.screen.dart';
import 'package:flutterbase/screens/help_center/help_center.screen.dart';
import 'package:flutterbase/screens/home/home.screen.dart';
import 'package:flutterbase/screens/home_bk.dart';
import 'package:flutterbase/screens/leaderboard/leaderboard.screen.dart';
import 'package:flutterbase/screens/level/level.screen.dart';
import 'package:flutterbase/screens/me/me.screen.dart';
import 'package:flutterbase/screens/more/more.screen.dart';
import 'package:flutterbase/screens/notification/notification.screen.dart';
import 'package:flutterbase/screens/profile/profile.screen.dart';
import 'package:flutterbase/screens/room/room.screen.dart';
import 'package:flutterbase/screens/search/search.screen.dart';
import 'package:flutterbase/screens/system_messages/system_screen.screen.dart';
import 'package:flutterbase/screens/task_center/task_center.screen.dart';
import 'package:flutterbase/screens/term_policy/term_policy.screen.dart';

class MyRouter {
  final Map<String, Widget Function(BuildContext)> routes = {
    "/": (_) => HomeScreenBK(),
    "/home-screen": (_) => HomeScreen(),
    "/more-screen": (_) => MoreScreen(),
    "/me-screen": (_) => MeScreen(),
    "/profile-screen": (_) => ProfileScreen(),
    "/room-screen": (_) => RoomScreen(),
    "/notification-screen": (_) => NotificationScreen(),
    "/search-screen": (_) => SearchScreen(),
    "/leaderboard-screen": (_) => LeaderboardScreen(),
    "/level-screen": (_) => LevelScreen(),
    "/help-center-screen": (_) => HelpCenterScreen(),
    "/task-center-screen": (_) => TaskCenterScreen(),
    "/activity-center-screen": (_) => ActivityCenterScreen(),
    "/term-policy-screen": (_) => TermPolicyScreen(),
    "/haki-mailbox-screen": (_) => HakiMailBox(),
    "/feedback-screen": (_) => FeedbackScreen(),
    "/system-messages-screen": (_) => SystemMessagesScreen(),
    "/discover-screen": (_) => DiscoverScreen(),
    "/country-filter-screen": (_) => CountryFilterScreen(),
    "/countries-full-list-screen": (_) => CountryFullListScreen(),
  };

  MyRouter();
}
