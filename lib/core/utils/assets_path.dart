class AssetsPath {
  static String fromImagesCommon(String name, [String extension]) =>
      'assets/images/common/$name.${extension != null ? extension : 'png'}';

  static String fromImagesFlags(String name, [String extension]) =>
      'assets/images/flags/$name.${extension != null ? extension : 'svg'}';

  static String fromImagesNavBarIcon(String name, [String extension]) =>
      'assets/images/nav_bar_ic/$name.${extension != null ? extension : 'svg'}';

  static String fromImagesSvg(String name, [String extension]) =>
      'assets/images/svg/$name.${extension != null ? extension : 'svg'}';

  static String fromImagesJson(String name, [String extension]) =>
      'assets/images/json/$name.${extension != null ? extension : 'json'}';

  static String fromImagesGifs(String name, [String extension]) =>
      'assets/images/gifs/$name.${extension != null ? extension : 'gif'}';

  static String fromImagesLeaderboardIcon(String name, [String extension]) =>
      'assets/images/leaderboard_ic/$name.${extension != null ? extension : 'png'}';

  static String fromImagesActivities(String name, [String extension]) =>
      'assets/images/activities/$name.${extension != null ? extension : 'png'}';

  static String fromImagesCharacterAction(String name, [String extension]) =>
      'assets/images/character_action/$name.${extension != null ? extension : 'png'}';

// static String fromVideos(String name, [String extension]) =>
//     'assets/videos/$name.${extension != null ? extension : 'mp4'}';
}
