import 'dart:convert';

import 'package:flutter_emoji/flutter_emoji.dart';
import 'package:intl/intl.dart';

class Formatter {
  static NumberFormat numberDecimalFormatter = NumberFormat.decimalPattern();
  static NumberFormat numberCompactFormatter = NumberFormat.compact()
    ..significantDigits = 3;

  static DateFormat dateFormat = DateFormat('dd/MM/yyyy');

  static String formatDecimalNumber(int number) =>
      numberDecimalFormatter.format(number);

  static String formatCompactNumber(int number) =>
      numberCompactFormatter.format(number);

  static String formatDate(DateTime date) => dateFormat.format(date);

  static String formatCapitalizeString(String str) {
    if (str.isEmpty) return str;

    List<String> splited = str.split(" ");
    List<String> res = [];
    splited.forEach((o) {
      res.add("${o[0].toUpperCase()}${o.substring(1)}");
    });

    return res.join(" ");
  }

  static String clearZeroWidthSpace(String str) {
    // final emojiParser = EmojiParser();

    String clearZeroWidthSpaceString = "";
    str.runes.toList().forEach((element) {
      clearZeroWidthSpaceString += String.fromCharCode(element);
      clearZeroWidthSpaceString += "\u200B";
    });

    return clearZeroWidthSpaceString;
    // try {
    //   if (emojiParser.hasEmoji(str)) {
    //     String resultsStr = emojiParser
    //         .unemojify(str.split("\u200B").join(""))
    //         .replaceAll("", "\u200B");
    //
    //     final List<String> emojis =
    //         findEmoji(emojiParser.unemojify(resultsStr));
    //
    //     for (var i = 0; i < emojis.length; i++) {
    //       String clearZeroWidthSpaceEmoji = emojis[i].replaceAll("", "\u200B");
    //
    //       if (clearZeroWidthSpaceEmoji[0] == "\u200B") {
    //         clearZeroWidthSpaceEmoji = clearZeroWidthSpaceEmoji.substring(1);
    //       }
    //       if (clearZeroWidthSpaceEmoji[clearZeroWidthSpaceEmoji.length - 1] ==
    //           "\u200B") {
    //         clearZeroWidthSpaceEmoji = clearZeroWidthSpaceEmoji.substring(
    //             0, clearZeroWidthSpaceEmoji.length - 1);
    //       }
    //
    //       List<String> splited = [];
    //
    //       if (resultsStr.indexOf(clearZeroWidthSpaceEmoji) != -1)
    //         splited = resultsStr.split(clearZeroWidthSpaceEmoji);
    //       else
    //         splited = resultsStr.split(emojis[i]);
    //
    //       resultsStr = splited.join(emojis[i]);
    //     }
    //
    //     // print("Clear zero width space: ${emojiParser.emojify(resultsStr)}");
    //
    //     return "âcsdcsdfasfasdfasdfaf ${emojiParser.emojify(resultsStr)}";
    //   }
    //   return str;
    // } catch (e) {
    //   print(e);
    //
    //   String resultsStr = emojiParser
    //       .unemojify(str.split("\u200B").join(""))
    //       .replaceAll("", "\u200B");
    //
    //   final List<String> emojis =
    //       findEmoji(emojiParser.unemojify(str.split("\u200B").join("")));
    //
    //   for (var i = 0; i < emojis.length; i++) {
    //     String clearZeroWidthSpaceEmoji = emojis[i].replaceAll("", "\u200B");
    //
    //     if (clearZeroWidthSpaceEmoji[0] == "\u200B") {
    //       clearZeroWidthSpaceEmoji = clearZeroWidthSpaceEmoji.substring(1);
    //     }
    //     if (clearZeroWidthSpaceEmoji[clearZeroWidthSpaceEmoji.length - 1] ==
    //         "\u200B") {
    //       clearZeroWidthSpaceEmoji = clearZeroWidthSpaceEmoji.substring(
    //           0, clearZeroWidthSpaceEmoji.length - 1);
    //     }
    //
    //     List<String> splited = [];
    //
    //     if (resultsStr.indexOf(clearZeroWidthSpaceEmoji) != -1)
    //       splited = resultsStr.split(clearZeroWidthSpaceEmoji);
    //     else
    //       splited = resultsStr.split(emojis[i]);
    //
    //     resultsStr = splited.join(emojis[i]);
    //   }
    //
    //   // print("Clear zero width space: ${emojiParser.emojify(resultsStr)}");
    //
    //   return resultsStr;
    //   return emojiParser.emojify(resultsStr);
    // }
  }

  static List<String> findEmoji(String str) {
    final List<String> result = [];

    final splited = str.split(":");

    for (var i = 1; i < splited.length - 1; i++) {
      if (EmojiParser().hasName(splited[i])) {
        // final emojiClearZeroWidthSpace =
        //     ":${splited[i]}:".replaceAll("", "\u200B");
        result.add(":${splited[i]}:");
      }
    }

    return result;
  }

  static String capitalize(String str) {
    if (str.isEmpty) return str;
    return str[0].toUpperCase() + str.substring(1);
  }

  static String utf8Decode(String str) {
    try {
      List<int> bytes = str.toString().codeUnits;
      return utf8.decode(bytes);
    } catch (e) {
      print("Error when handle utf8 with string: $str");
      return str;
    }
  }
}
