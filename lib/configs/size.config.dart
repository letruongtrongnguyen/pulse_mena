import 'package:flutter/material.dart';

class SizeConfig {
  static MediaQueryData _mediaQueryData;

  static double screenWidth;
  static double screenHeight;

  static double paddingTop;
  static double paddingBottom;
  static double paddingLeft;
  static double paddingRight;

  static double viewInsetTop;
  static double viewInsetBottom;
  static double viewInsetLeft;
  static double viewInsetRight;

  static double blockSizeHorizontal;
  static double blockSizeVertical;

  static double blockSizeVerticalNoToolBar;
  static double safeBlockSizeVerticalNoToolBar;

  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;

  static bool isMobile = false;
  static bool isTablet = false;
  static bool isDesktop = false;
  static bool isWatch = false;

  static Orientation orientation;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);

    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;

    paddingTop = _mediaQueryData.padding.top;
    paddingBottom = _mediaQueryData.padding.bottom;
    paddingLeft = _mediaQueryData.padding.left;
    paddingRight = _mediaQueryData.padding.right;

    viewInsetTop = _mediaQueryData.viewInsets.top;
    viewInsetBottom = _mediaQueryData.viewInsets.bottom;
    viewInsetLeft = _mediaQueryData.viewInsets.left;
    viewInsetRight = _mediaQueryData.viewInsets.right;

    if (screenWidth > 950)
      isDesktop = true;
    else if (screenWidth > 600)
      isTablet = true;
    else if (screenWidth < 300)
      isWatch = true;
    else
      isMobile = true;

    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;

    blockSizeVerticalNoToolBar = (screenHeight - kToolbarHeight) / 100;

    _safeAreaHorizontal = paddingLeft + paddingRight;
    _safeAreaVertical = paddingTop + paddingBottom;

    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;

    safeBlockSizeVerticalNoToolBar =
        (screenHeight - kToolbarHeight - _safeAreaVertical) / 100;

    orientation = _mediaQueryData.orientation;
  }
}
