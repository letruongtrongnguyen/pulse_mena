import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData get darkTheme {
    return ThemeData(
      brightness: Brightness.dark,
      scaffoldBackgroundColor: Colors.transparent,
      appBarTheme: AppBarTheme(
        brightness: Brightness.dark,
        backgroundColor: Color(0xff1c1c1c).withOpacity(0.96),
        shadowColor: Colors.white.withOpacity(0.05),
        textTheme: TextTheme(
          headline1: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
            color: Colors.white,
            fontFamily: 'Euclid Circular A',
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      fontFamily: 'Euclid Circular A',
      primaryColor: Colors.black,
      accentColor: Colors.white,
      cardColor: Colors.white.withOpacity(0.1),
      cardTheme: CardTheme(
        color: Colors.white.withOpacity(0.1),
        shadowColor: Colors.transparent,
      ),
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
      textTheme: TextTheme(
        bodyText1: TextStyle(
          color: Colors.white,
          fontFamily: 'Euclid Circular A',
        ),
        bodyText2: TextStyle(
          color: Colors.white,
          fontFamily: 'Euclid Circular A',
        ),
        headline1: TextStyle(
          color: Colors.white,
          fontFamily: 'Euclid Circular A',
          fontSize: 16,
          fontWeight: FontWeight.w700,
        ),
        headline2: TextStyle(
          color: Color(0xffD5F55A),
          fontFamily: 'Euclid Circular A',
          fontSize: 16,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }

  static ThemeData get lightTheme {
    return ThemeData(
      brightness: Brightness.light,
      scaffoldBackgroundColor: Color(0xfff7faff),
      appBarTheme: AppBarTheme(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        shadowColor: Colors.black.withOpacity(0.5),
        textTheme: TextTheme(
          headline1: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 18,
            color: Colors.black,
            fontFamily: 'Euclid Circular A',
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      fontFamily: 'Euclid Circular A',
      primaryColor: Colors.white,
      accentColor: Colors.black,
      cardColor: Color(0xffb4b4b4).withOpacity(0.1),
      cardTheme: CardTheme(
        color: Colors.white,
        shadowColor: Colors.black.withOpacity(0.1),
      ),
      iconTheme: IconThemeData(
        color: Colors.black,
      ),
      textTheme: TextTheme(
        bodyText1: TextStyle(
          color: Colors.black,
          fontFamily: 'Euclid Circular A',
        ),
        bodyText2: TextStyle(
          color: Colors.black,
          fontFamily: 'Euclid Circular A',
        ),
        headline1: TextStyle(
          color: Color(0xff00cdc1),
          fontFamily: 'Euclid Circular A',
          fontSize: 16,
          fontWeight: FontWeight.w700,
        ),
        headline2: TextStyle(
          color: Colors.black,
          fontFamily: 'Euclid Circular A',
          fontSize: 16,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }
}
