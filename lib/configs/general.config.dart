class GeneralConfig {
  String language = "en";
  int myUserId;

  void setLanguage(String lang) {
    this.language = lang ?? "en";
  }

  void setMyUserId(int newUserId) {
    this.myUserId = newUserId;
  }

  GeneralConfig();
}
