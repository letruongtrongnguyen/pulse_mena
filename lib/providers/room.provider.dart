import 'dart:convert';

import 'package:flutter/material.dart';
import "package:multi_sort/multi_sort.dart";
import 'package:flutterbase/core/@services/models/broadcast.model.dart';
import 'package:flutterbase/core/@services/request/request.dart';
import 'package:flutterbase/core/@services/services/broadcast.service.dart';
import 'package:flutterbase/configs/general.config.dart';
import 'package:flutterbase/core/@services/models/room.model.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/@services/services/room.service.dart';
import 'package:flutterbase/core/@services/utils/map.dart';
import 'package:flutterbase/core/@services/utils/utils.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/providers/me.provider.dart';

enum RoomTabItemType {
  hotRoom,
  newRoom,
}

class RoomProvider {
  RoomProvider() {
    // getIt<CommunicateService>().reloadMyRoomListener.listen((event) {
    //   fetchMyRoomData();
    //   // fetchRoomData();
    // });
  }

  //// Stats: start
  ValueNotifier _countryFilter = ValueNotifier<String>("");
  ValueNotifier _currentActiveTab =
      ValueNotifier<RoomTabItemType>(RoomTabItemType.hotRoom);
  ValueNotifier _previousActiveTab =
      ValueNotifier<RoomTabItemType>(RoomTabItemType.hotRoom);

  ValueNotifier _data = ValueNotifier<List<Room>>([]);

  ValueNotifier _loading = ValueNotifier<bool>(false);

  ValueNotifier _recentlyRoomData = ValueNotifier<List<Room>>([]);

  ValueNotifier _recentlyRoomLoading = ValueNotifier<bool>(false);

  ValueNotifier _popularRoomData = ValueNotifier<List<Room>>([]);

  ValueNotifier _popularRoomLoading = ValueNotifier<bool>(false);

  ValueNotifier _newRoomData = ValueNotifier<List<Room>>([]);

  ValueNotifier _newRoomLoading = ValueNotifier<bool>(false);

  ValueNotifier _audienceData = ValueNotifier<Map<String, dynamic>>({});

  ValueNotifier _broadcastLoading = ValueNotifier<bool>(false);

  ValueNotifier _broadcastData = ValueNotifier<List<BroadcastModel>>([]);

  String _sortField = "point";

  int _roomDataCurrentPage = 1;

  int _popularRoomDataCurrentPage = 1;

  int _newRoomDataCurrentPage = 1;

  int _recentlyRoomDataCurrentPage = 1;

  ValueNotifier _myRoomData = ValueNotifier<Room>(null);
  ValueNotifier _myRoomLoading = ValueNotifier<bool>(false);

  // Stats: end

  //// Getters: start
  ValueNotifier get countryFilter => _countryFilter;

  ValueNotifier get currentActiveTab => _currentActiveTab;

  ValueNotifier get previousActiveTab => _previousActiveTab;

  ValueNotifier get data => _data;

  ValueNotifier get loading => _loading;

  ValueNotifier get recentlyRoomData => _recentlyRoomData;

  ValueNotifier get recentlyRoomLoading => _recentlyRoomLoading;

  ValueNotifier get popularRoomData => _popularRoomData;

  ValueNotifier get popularRoomLoading => _popularRoomLoading;

  ValueNotifier get newRoomData => _newRoomData;

  ValueNotifier get newRoomLoading => _newRoomLoading;

  int get popularRoomDataCurrentPage => _popularRoomDataCurrentPage;

  int get roomDataCurrentPage => _roomDataCurrentPage;

  int get newRoomDataCurrentPage => _newRoomDataCurrentPage;

  int get recentlyRoomDataCurrentPage => _recentlyRoomDataCurrentPage;

  ValueNotifier get audienceData => _audienceData;

  ValueNotifier get myRoomData => _myRoomData;

  ValueNotifier get myRoomLoading => _myRoomLoading;

  ValueNotifier get broadcastLoading => _broadcastLoading;

  ValueNotifier get broadcastData => _broadcastData;

// Getters: end

//// Methods: start
  void setCountryFilter(String code) {
    this._countryFilter.value = code;
    fetchRoomData();
  }

  void onRoomActiveTabChange(RoomTabItemType newTab) {
    if (_currentActiveTab.value == newTab) return;
    _previousActiveTab = _currentActiveTab;
    _currentActiveTab.value = newTab;

    switch (newTab) {
      case RoomTabItemType.hotRoom:
        _sortField = "point";
        break;

      default:
        _sortField = "created_at";
        break;
    }

    Future.delayed(const Duration(milliseconds: 100), () {
      fetchRoomData();
    });
  }

  void fetchBroadcastData() async {
    this._broadcastLoading.value = true;
    this._broadcastData.value = await BroadcastService.schedules();
    this._broadcastLoading.value = false;
  }

  void fetchRoomData({
    int page = 0,
    int limit = 10,
    String countryCode,
    String sortField,
  }) async {
    this._loading.value = true;

    this._roomDataCurrentPage = page;

    if (page == 0) this._data.value.clear();

    final tempData = await RoomService.list(
        countryCode: countryCode, sortField: sortField, page: page);

    this._data.value = this._data.value + tempData;

    this._loading.value = false;
  }

  void fetchMyRoomData() async {
    final int myUserId = getIt<GeneralConfig>().myUserId;
    final me = getIt<MeProvider>().data.value;

    // if (_myRoomLoading.value) return;
    this._myRoomLoading.value = true;
    this._myRoomData.value = await RoomService.roomById(id: me.id);
    this._myRoomLoading.value = false;
  }

  void setMyRoom({String data}) async {
    this._myRoomLoading.value = true;
    fetchRecentlyRoomData();
    fetchPopularRoomData();
    fetchNewRoomData();

    if (data == null)
      this._myRoomData.value = null;
    else {
      this._myRoomData.value = new Room();
      final tempData = jsonDecode(data);

      try {
        Room room = new Room(
          thumbnail: tempData["cover"] == null
              ? ""
              : tempData["cover"]['item_info'] == null
                  ? ""
                  : tempData["cover"]["item_info"]["thumbnail"],
          name: Utils.utf8Decode(tempData["name"]),
          uid: "${tempData["id"]}",
          id: tempData["id"],
          description: Utils.utf8Decode(tempData["description"]),
          countryCode: tempData["country"],
          hostId: tempData["host_id"],
          point: tempData["point"],
        );

        this._myRoomData.value = room;
      } catch (e) {
        print(e);
      }
    }

    this._myRoomLoading.value = false;
  }

  Future<void> fetchPopularRoomData({
    int page = 1,
    int limit = 10,
  }) async {
    this._popularRoomLoading.value = true;

    this._popularRoomDataCurrentPage = page;

    List<Room> tempData =
        await RoomService.list(sortField: "points", page: page);

    tempData = updateAudienceNumInRoom(tempData);

    if (page == 1)
      this._popularRoomData.value = tempData;
    else
      this._popularRoomData.value = this._popularRoomData.value + tempData;

    this._popularRoomData.value =
        updateAudienceNumInRoom(this._popularRoomData.value);
    sortPopularRoom();

    this._popularRoomLoading.value = false;
  }

  Future<void> fetchNewRoomData({
    int page = 1,
    int limit = 10,
  }) async {
    this._newRoomLoading.value = true;

    this._newRoomDataCurrentPage = page;

    final tempData =
        await RoomService.list(sortField: "created_at", page: page);

    if (page == 1)
      this._newRoomData.value = tempData;
    else
      this._newRoomData.value = this._newRoomData.value + tempData;

    this._newRoomLoading.value = false;
  }

  Future<void> fetchRecentlyRoomData({
    int page = 1,
    int limit = 10,
  }) async {
    final me = getIt<MeProvider>().data.value;

    this._recentlyRoomLoading.value = true;

    this._recentlyRoomDataCurrentPage = page;

    List<Room> tempData = await RoomService.recentlyList(
      userId: me.id,
      page: page,
    );

    if (page == 1)
      this._recentlyRoomData.value = tempData;
    else
      this._recentlyRoomData.value = this._recentlyRoomData.value + tempData;

    this._recentlyRoomLoading.value = false;
  }

  List<Room> updateAudienceNumInRoom(List<Room> oldValue) {
    List<Room> newValue = new List<Room>.from(oldValue);

    for (Room roomItem in newValue) {
      if (this._audienceData.value.containsKey("${roomItem.id}")) {
        roomItem.audienceNum =
            this._audienceData.value["${roomItem.id}"]["users_count"] ?? 0;
      }
    }

    return newValue;
  }

  void sortPopularRoom() {
    List<Room> sortingList = new List<Room>.from(this._popularRoomData.value);

    if (sortingList.isEmpty) return;

    Room firstRoom = sortingList.removeAt(0);
    //Criteria List
    List<bool> criteria = [false, false];
    //Preference List
    List<String> preference = ['audienceNum', 'point'];
    sortingList.multisort(criteria, preference);
    sortingList = [firstRoom, ...sortingList];

    this._popularRoomData.value = sortingList;
  }

  void updateWholeAudienceData(Map<String, dynamic> data) {
    if (data != null) {
      Map<String, dynamic> newAudienceData =
          new Map<String, dynamic>.from(this._audienceData.value);

      data.forEach((key, value) {
        if (value is Map) {
          newAudienceData["$key"] = value;
        }
      });

      this._audienceData.value = newAudienceData;

      this._popularRoomData.value =
          updateAudienceNumInRoom(this._popularRoomData.value);
      sortPopularRoom();
    }
  }
// Methods: end

}
