import 'package:flutter/material.dart';
import 'package:flutterbase/core/@services/models/relationship.model.dart';
import 'package:flutterbase/core/@services/services/relationship.service.dart';

class RelationshipProvider {
  ValueNotifier _data = ValueNotifier<Relationship>(Relationship());
  ValueNotifier _loading = ValueNotifier<bool>(true);

  ValueNotifier get data => _data;

  ValueNotifier get loading => _loading;

  void fetchRelationshipData() async {
    this._loading.value = true;
    this._data.value = await RelationshipService.list();
    this._loading.value = false;
  }
}
