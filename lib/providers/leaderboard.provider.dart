import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:flutterbase/configs/general.config.dart';
import 'package:flutterbase/core/@services/models/leaderboard.item.model.dart';
import 'package:flutterbase/core/@services/services/leaderboard.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/providers/room.provider.dart';

import 'me.provider.dart';

enum LeaderboardType {
  // TopRoomLevel,
  // TopRoomGift,
  TopBuyer,
  // TopHotIdol,
  // TopGiftSender,
  TopGiftReceiver,
  TopWinnerCoin,
}

enum LeaderboardTimeFrameType { Monthly, Weekly, Daily }

class LeaderboardProvider {
  //// Stats: start

  ValueNotifier _leaderboardData = ValueNotifier<List<Leader>>([]);
  ValueNotifier _myRankData = ValueNotifier<Leader>(new Leader());
  ValueNotifier _leaderboardDataLoading = ValueNotifier<bool>(false);

  ValueNotifier _currentActiveTab =
      ValueNotifier<LeaderboardType>(LeaderboardType.TopBuyer);
  ValueNotifier _previousActiveTab =
      ValueNotifier<LeaderboardType>(LeaderboardType.TopBuyer);
  ValueNotifier _timeFrame =
      ValueNotifier<LeaderboardTimeFrameType>(LeaderboardTimeFrameType.Monthly);

  ValueNotifier _leaderboardTopThreeData =
      ValueNotifier<Map<LeaderboardType, List<Leader>>>({});
  ValueNotifier _leaderboardTopThreeDataLoading = ValueNotifier<bool>(false);

  int _currentIndex = 0;
  int _prevControllerIndex = 0;
  double _aniValue = 0.0;
  double _prevAniValue = 0.0;

  CancelableOperation<List<Leader>> cancellableLeaderboardData;

  // Stats: end

  //// Getters: start

  ValueNotifier get leaderboardData => _leaderboardData;

  ValueNotifier get myRankData => _myRankData;

  ValueNotifier get leaderboardDataLoading => _leaderboardDataLoading;

  ValueNotifier get currentActiveTab => _currentActiveTab;

  ValueNotifier get previousActiveTab => _previousActiveTab;

  ValueNotifier get timeFrame => _timeFrame;

  ValueNotifier get leaderboardTopThreeData => _leaderboardTopThreeData;

  ValueNotifier get leaderboardTopThreeDataLoading =>
      _leaderboardTopThreeDataLoading;

  int get currentIndex => _currentIndex;

  int get prevControllerIndex => _prevControllerIndex;

  double get aniValue => _aniValue;

  double get prevAniValue => _prevAniValue;

// Getters: end

//// Setter: start
  void setCurrentIndex(int i) {
    _currentIndex = i;
  }

  void setPrevControllerIndex(int i) {
    _prevControllerIndex = i;
  }

  void setAniValue(double i) {
    _aniValue = i;
  }

  void setPrevAniValue(double i) {
    _prevAniValue = i;
  }

  void setCurrentActiveTab(LeaderboardType newType) {
    if (_currentActiveTab.value == newType) return;
    _previousActiveTab = _currentActiveTab;
    _currentActiveTab.value = newType;
  }

// Setter: end

  //// Methods: start

  Future<void> fetchLeaderboardData({LeaderboardType type}) async {
    if (this._leaderboardDataLoading.value &&
        cancellableLeaderboardData != null) {
      cancellableLeaderboardData.cancel();
    }

    _leaderboardData.value = <Leader>[];
    _leaderboardDataLoading.value = true;

    LeaderboardFilterType _type = LeaderboardFilterType.BUYER;

    switch (_currentActiveTab.value) {
      // case LeaderboardType.TopRoomGift:
      //   _type = LeaderboardFilterType.ROOM;
      //   break;
      case LeaderboardType.TopBuyer:
        _type = LeaderboardFilterType.BUYER;
        break;
      // case LeaderboardType.TopHotIdol:
      //   _type = LeaderboardFilterType.GEM_RECEIVED;
      //   break;
      // case LeaderboardType.TopGiftSender:
      //   _type = LeaderboardFilterType.GIFT_SENT;
      //   break;
      case LeaderboardType.TopGiftReceiver:
        _type = LeaderboardFilterType.GIFT_RECEIVED;
        break;
      case LeaderboardType.TopWinnerCoin:
        _type = LeaderboardFilterType.WINNER_COIN;
        break;
    }

    // if (type == LeaderboardType.TopRoomGift) _type = LeaderboardFilterType.ROOM;

    LeaderboardTimeRange _realTimeFrame = LeaderboardTimeRange.WEEKLY;

    switch (_timeFrame.value) {
      case LeaderboardTimeFrameType.Monthly:
        _realTimeFrame = LeaderboardTimeRange.MONTHLY;
        break;
      case LeaderboardTimeFrameType.Daily:
        _realTimeFrame = LeaderboardTimeRange.DAILY;
        break;
      default:
        _realTimeFrame = LeaderboardTimeRange.WEEKLY;
    }

    cancellableLeaderboardData = CancelableOperation<List<Leader>>.fromFuture(
      LeaderboardService.list(type: _type, timeRange: _realTimeFrame),
    );

    await cancellableLeaderboardData.value
        .then((value) => this._leaderboardData.value = value);

    // For flutter test
    final me = getIt<MeProvider>().data.value;

    // final myRoom = getIt<RoomProvider>().myRoomData.value;

    final _tempList = _leaderboardData.value
        .where((object) => object.userId == me.id)
        .toList();

    if (_tempList.length > 0)
      _myRankData.value = _tempList[0];
    else {
      // if (_currentActiveTab.value == LeaderboardType.TopRoomGift) {
      //   if (myRoom == null)
      //     _myRankData.value = null;
      //   else
      //     _myRankData.value = new Leader(
      //       avatarUrl: myRoom?.thumbnail?.url,
      //       name: myRoom?.name,
      //       description: myRoom?.description,
      //     );
      // } else
      _myRankData.value = new Leader(
        avatarUrl: me.avatarUrl,
        name: me.fullName,
      );
    }

    _leaderboardDataLoading.value = false;
  }

  Future<void> fetchLeaderboardTopThreeData() async {
    _leaderboardTopThreeDataLoading.value = true;

    // _leaderboardTopThreeData.value.clear();

    for (final element in LeaderboardType.values) {
      String _type;

      switch (element) {
        case LeaderboardType.TopBuyer:
          _type = "wealthy";
          break;
        // case LeaderboardType.TopGiftSender:
        //   _type = "gift_sender";
        //   break;
        case LeaderboardType.TopGiftReceiver:
          _type = "topreceivers";
          break;
        case LeaderboardType.TopWinnerCoin:
          _type = "winners";
          break;
      }

      final tempData = await LeaderboardService.list(
        type: leaderboardFilterTypeFromString(_type),
        timeRange: LeaderboardTimeRange.MONTHLY,
      );
      if (tempData.isNotEmpty) {
        if (tempData.length == 1)
          _leaderboardTopThreeData.value[element] = tempData.sublist(0, 1);
        else if (tempData.length == 2)
          _leaderboardTopThreeData.value[element] = tempData.sublist(0, 2);
        else if (tempData.length >= 3)
          _leaderboardTopThreeData.value[element] = tempData.sublist(0, 3);
        else
          _leaderboardTopThreeData.value[element] = <Leader>[];
      }
    }


    _leaderboardTopThreeDataLoading.value = false;
  }

  void onActiveTabChange(LeaderboardType newTab) {
    if (_currentActiveTab.value == newTab) return;
    _previousActiveTab = _currentActiveTab;
    _currentActiveTab.value = newTab;

    fetchLeaderboardData();
  }

  void onTimeFrameChange(LeaderboardTimeFrameType newTimeFrame) {
    if (_timeFrame.value == newTimeFrame) return;
    _timeFrame.value = newTimeFrame;

    fetchLeaderboardData();
  }
// Methods: end

}
