import 'package:flutter/cupertino.dart';
import 'package:flutterbase/core/@services/models/level.range.model.dart';
import 'package:flutterbase/core/@services/services/level.service.dart';

class LevelProvider {
  ValueNotifier _data = ValueNotifier<List<LevelRange>>([]);

  ValueNotifier get data => _data;

  void fetchLevelRangeData() async {
    _data.value = await LevelService.ranges();
  }
}
