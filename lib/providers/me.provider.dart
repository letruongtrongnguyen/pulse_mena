import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutterbase/core/@services/models/country.model.dart';
import 'package:flutterbase/core/@services/models/level.model.dart';
import 'package:flutterbase/core/@services/models/user.model.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/@services/services/user.service.dart';
import 'package:flutterbase/core/@services/utils/gender.dart';
import 'package:flutterbase/core/@services/utils/utils.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';

class MeProvider {
  MeProvider() {
    // getIt<CommunicateService>().reloadMyProfileListener.listen((event) {
    //   fetchUserData();
    // });
  }

  ValueNotifier _data = ValueNotifier<User>(new User());
  ValueNotifier _isError = ValueNotifier<bool>(false);
  ValueNotifier _loading = ValueNotifier<bool>(true);

  ValueNotifier get data {
    return _data;
  }

  ValueNotifier get loading {
    return _loading;
  }

  ValueNotifier get isError {
    return _isError;
  }

  void clearUserData() {
    this._data.value = new User();
  }

  Future<void> fetchUserData() async {
    try {
      this._loading.value = true;
      this._data.value = new User();
      this._data.value = await UserService.getDetailsById(id: "me");
      this._loading.value = false;
    } catch (e) {
      this._loading.value = false;
      this._isError.value = true;
      print(e);
    }
  }

  void setUserData({String data}) {
    this._loading.value = true;
    this._data.value = new User();
    final tempData = jsonDecode(data);

    try {
      int age = 0;

      // int birthYearFromResponse = 0;

      // if (tempData["birth_date"] >= 0 && tempData["birth_date"] != null)
      //   age = DateTime.now().year -
      //       DateTime.fromMillisecondsSinceEpoch((tempData["birth_date"])).year;
      //   birthYearFromResponse =
      //       DateTime.fromMillisecondsSinceEpoch((tempData["birth_day"]) * 1000)
      //           .year;
      // int currentYear = DateTime.now().year;

      User user = new User(
        avatarUrl: tempData["avatar_url"],
        fullName: Utils.utf8Decode(tempData["full_name"]),
        firstName: Utils.utf8Decode(tempData["first_name"]),
        lastName: Utils.utf8Decode(tempData["last_name"]),
        // email: tempData["email"],
        id: tempData["user_id"],
        country: new Country(code: tempData["country"]),
        gender: genderFromInt(tempData["gender"]),
        age: tempData["age"] ?? 0,
        coins: tempData["coins_amount"] ?? 0,
        gems: tempData["gems_amount"] ?? 0,
        // pendantUrl: tempData["chosen_pendant_url"],
        pendantUrl: tempData["equipped_items"] != null &&
                tempData["equipped_items"]["pendant"] != null &&
                tempData["equipped_items"]["pendant"]["item_info"] != null &&
                tempData["equipped_items"]["pendant"]["item_info"]["id"] != null
            ? tempData["equipped_items"]["pendant"]["item_info"]["thumbnail"]
            : "",
        hasNewNotification: tempData["has_new_notifications"],
        level: new Level(
          value: tempData["level"],
        ),
        userType: tempData["user_type"],
      );

      this._data.value = user;
    } catch (e) {
      print(e);
    }

    this._loading.value = false;
  }
}
