import 'dart:async';

import 'package:async/async.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterbase/core/@services/models/room.model.dart';
import 'package:flutterbase/core/@services/services/room.service.dart';

class SearchProvider extends ChangeNotifier {
  SearchProvider() {
    _searchEditingController.addListener(_trackSearchValue);
  }

  @override
  void dispose() {
    _searchEditingController.removeListener(_trackSearchValue);
    searchDelay.cancel();
    _searchEditingController.dispose();

    super.dispose();
  }

  //// Stats: start

  ValueNotifier _prevSearchString = ValueNotifier<String>("");
  TextEditingController _searchEditingController = TextEditingController();
  ValueNotifier _isSearchValueEmpty = ValueNotifier<bool>(true);

  ValueNotifier _searchData = ValueNotifier<List<Room>>([]);

  ValueNotifier _loading = ValueNotifier<bool>(false);

  Timer searchDelay = Timer(Duration(milliseconds: 500), () {});

  CancelableOperation<List<Room>> cancellableOperation;

  // Stats: end

  //// Getters: start

  TextEditingController get searchEditingController => _searchEditingController;

  ValueNotifier get prevSearchString => _prevSearchString;

  ValueNotifier get isSearchValueEmpty => _isSearchValueEmpty;

  ValueNotifier get searchData => _searchData;

  ValueNotifier get loading => _loading;

// Getters: end

//// Methods: start
  void resetSearchTextValue() {
    _searchEditingController.text = '';
  }

  void _trackSearchValue() {
    final isEmpty = _searchEditingController.text.isEmpty;
    _isSearchValueEmpty.value = isEmpty;
    if (_searchEditingController.text == _prevSearchString.value) return;
    if (!isEmpty) this._loading.value = true;
    if (isEmpty) this._loading.value = false;

    searchDelay.cancel();

    if (!isEmpty) {
      searchDelay = Timer(Duration(milliseconds: 500), () {
        searchRoomData();
      });
    }
  }

  void searchRoomData() async {
    if (this._loading.value && cancellableOperation != null) {
      cancellableOperation.cancel();
    }

    this._loading.value = true;

    _prevSearchString.value = _searchEditingController.text;

    cancellableOperation = CancelableOperation<List<Room>>.fromFuture(
      RoomService.list(search: _searchEditingController.text),
    );

    await cancellableOperation.value
        .then((value) => this._searchData.value = value);

    // this._searchData.value =
    //     await RoomService.list(search: _searchEditingController.text);
    this._loading.value = false;
  }

// Methods: end

}
