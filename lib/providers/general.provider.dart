import 'dart:convert';

import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:flutterbase/configs/general.config.dart';
import 'package:flutterbase/core/@services/models/feedback.model.dart';
import 'package:flutterbase/core/@services/models/item.model.dart';
import 'package:flutterbase/core/@services/models/user.model.dart';
import 'package:flutterbase/core/@services/models/promo.model.dart';
import 'package:flutterbase/core/@services/services/feedback.service.dart';
import 'package:flutterbase/core/@services/services/user.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/i18n/i18n.dart';

enum MainScreenTabType {
  feedScreen,
  roomScreen,
  discoverScreen,
  notificationScreen,
  moreScreen,
}

class GeneralProvider {
  //// Stats: start
  ValueNotifier _isProduction = ValueNotifier<bool>(false);

  ValueNotifier _lang = ValueNotifier<String>(getIt<GeneralConfig>().language);

  ValueNotifier _currentMainScreen =
      ValueNotifier<MainScreenTabType>(MainScreenTabType.moreScreen);
  ValueNotifier _previousMainScreen =
      ValueNotifier<MainScreenTabType>(MainScreenTabType.moreScreen);

  ValueNotifier _textDirection =
      ValueNotifier<TextDirection>(TextDirection.ltr);

  ValueNotifier _isDarkTheme = ValueNotifier<bool>(true);

  ValueNotifier _userBottomSheetLoading = ValueNotifier<bool>(false);

  ValueNotifier _userBottomSheetData = ValueNotifier<User>(new User());

  ValueNotifier _customerServiceUserLoading = ValueNotifier<bool>(false);

  ValueNotifier _customerServiceUserData = ValueNotifier<User>(new User());

  ValueNotifier _userBottomSheetReceivedGift = ValueNotifier<List<Item>>([]);

  ValueNotifier _feedbackIsSending = ValueNotifier<bool>(false);

  CancelableOperation<User> cancellableUserBottomSheetData;

  CancelableOperation<List<Item>> cancellableUserBottomSheetReceivedGift;

  ValueNotifier _myPromo = ValueNotifier<PromoModel>(null);

  // Stats: end

  //// Getters: start
  ValueNotifier get lang => _lang;

  ValueNotifier get currentMainScreen => _currentMainScreen;

  ValueNotifier get previousMainScreen => _previousMainScreen;

  ValueNotifier get textDirection => _textDirection;

  ValueNotifier get isDarkTheme => _isDarkTheme;

  ValueNotifier get userBottomSheetLoading => _userBottomSheetLoading;

  ValueNotifier get userBottomSheetData => _userBottomSheetData;

  ValueNotifier get customerServiceUserLoading => _customerServiceUserLoading;

  ValueNotifier get customerServiceUserData => _customerServiceUserData;

  ValueNotifier get userBottomSheetReceivedGift => _userBottomSheetReceivedGift;

  ValueNotifier get feedbackIsSending => _feedbackIsSending;

  ValueNotifier get myPromo => _myPromo;

  ValueNotifier get isProduction => _isProduction;

  String get luckyHakiUrl {
    if (_isProduction.value) {
      return "https://luckyhaki.mocogateway.com";
    }

    return "https://staging-luckyhaki.mocogateway.com";
  }

  // Getters: end

  //// Methods: start
  void setLanguage(String lang) {
    String realLang = lang ?? "en";
    I18n.set(realLang);
    this._lang.value = realLang;

    if (realLang == "ar")
      this._textDirection.value = TextDirection.rtl;
    else
      this._textDirection.value = TextDirection.ltr;
  }

  void setMyPromo(String data) {
    if (data == null) {
      _myPromo.value = null;
    } else {
      final tempData = jsonDecode(data);
      PromoEnum promoType =
          tempData["in_store_id"] == "haki_2nd_purchase_offer_"
              ? PromoEnum.PROMO_2
              : PromoEnum.PROMO_1;

      _myPromo.value = new PromoModel(
        promoId: tempData["promo_id"],
        name: tempData["name"],
        price: tempData["price"],
        coins: tempData["coins"],
        valuePerUsd: tempData["value_per_usd"],
        endAt: tempData["ends_at"],
        inStoreId: tempData["in_store_id"],
        promoType: promoType,
      );
    }
  }

  void setProductionStatus(bool status) {
    _isProduction.value = status;
  }

  void toggleTheme() {
    this._isDarkTheme.value = !this._isDarkTheme.value;
  }

  void onActiveTabChange(MainScreenTabType newTab) {
    if (_currentMainScreen.value == newTab) return;
    _previousMainScreen = _currentMainScreen;
    _currentMainScreen.value = newTab;
  }

  void setTextDirection(TextDirection direction) {
    _textDirection.value = direction;
  }

  void getUserBottomSheetData({int id}) async {
    if (this._userBottomSheetLoading.value &&
        // cancellableUserBottomSheetReceivedGift != null &&
        cancellableUserBottomSheetData != null) {
      cancellableUserBottomSheetData.cancel();
      // cancellableUserBottomSheetReceivedGift.cancel();
    }

    this._userBottomSheetLoading.value = true;

    cancellableUserBottomSheetData = CancelableOperation<User>.fromFuture(
      UserService.getDetailsById(id: id),
    );

    // cancellableUserBottomSheetReceivedGift =
    //     CancelableOperation<List<Item>>.fromFuture(
    //   ItemService.receivedGiftListByUserId(id: id),
    // );

    await cancellableUserBottomSheetData.value
        .then((value) => this._userBottomSheetData.value = value);

    // await cancellableUserBottomSheetReceivedGift.value
    //     .then((value) => this._userBottomSheetReceivedGift.value = value);

    this._userBottomSheetLoading.value = false;
  }

  void fetchCustomerServiceUserData() async {
    this._customerServiceUserLoading.value = true;

    this._customerServiceUserData.value =
        await UserService.getCustomerServiceUser();

    this._customerServiceUserLoading.value = false;
  }

  Future<bool> sendFeedback({FeedbackModel data}) async {
    _feedbackIsSending.value = true;
    var response;

    await FeedbackService.sendFeedback(
      data: data,
    ).then((res) {
      response = res;
    });

    _feedbackIsSending.value = false;
    if (response) {
      return true;
    } else
      return false;
  }
// Methods: end

}
