import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item_shimmer.dart';
import 'package:flutterbase/widgets/session_title/session_title.dart';

class CreateButtonModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ValueNotifier myRoomData = getIt<RoomProvider>().myRoomData;
    ValueNotifier myRoomLoading = getIt<RoomProvider>().myRoomLoading;

    return AnimatedBuilder(
      animation: Listenable.merge([myRoomData, myRoomLoading]),
      builder: (context, child) {
        var _content;

        if (myRoomLoading.value)
          _content = RoomItemShimmer();
        else if (myRoomData.value == null)
          _content = _buildCreateButton();
        else if (myRoomData.value.name.isEmpty)
          return SizedBox();
        else
          _content = RoomItemWidget(
            roomData: myRoomData.value,
          );

        return Padding(
          padding: EdgeInsets.only(top: 20),
          child: Column(
            children: [
              SessionTitle(
                title: "room.menuTitle.myRoom",
                iconPath: AssetsPath.fromImagesSvg("home_ic"),
              ),
              AnimatedContainer(
                duration: Duration(milliseconds: 500),
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(
                  vertical: SizeConfig.safeBlockVertical * 3,
                ),
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  border: Border(
                    bottom: BorderSide(
                      color: Color(0xff424242),
                      width: 1,
                    ),
                  ),
                ),
                child: _content,
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _buildCreateButton() {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            getIt<CommunicateService>().navigate(route: "/create-room");
          },
          child: Container(
            alignment: Alignment.center,
            width: SizeConfig.safeBlockHorizontal * 92,
            height: SizeConfig.safeBlockHorizontal * 12,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color(0xff64E2AD), Color(0xff0B99E2)],
              ),
            ),
            child: CustomText(
              "room.btn.create",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
