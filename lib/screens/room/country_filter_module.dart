import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/country.model.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/widgets/basic/circle_ic.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/country/country.widget.dart';
import 'package:flutterbase/widgets/session_title/session_title.dart';

class CountryFilterModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<String> countryFilters = ['sa', 'ae', 'eg', 'kw', 'iq', 'qa'];
    return Container(
      width: SizeConfig.safeBlockHorizontal * 92,
      padding: EdgeInsets.only(
        top: 20,
        bottom: 10,
      ),
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border(
          bottom: BorderSide(
            color: Color(0xff424242),
            width: 1,
          ),
        ),
      ),
      child: Column(
        children: [
          SessionTitle(
            title: "room.menuTitle.countries",
            iconPath: AssetsPath.fromImagesSvg("location_ic"),
          ),
          SizedBox(height: 10),
          Container(
            width: SizeConfig.safeBlockHorizontal * 92,
            height: 100,
            child: GridView.builder(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              itemCount: countryFilters.length,
              physics: NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                childAspectRatio: 2.8,
                mainAxisSpacing: SizeConfig.safeBlockHorizontal * 2.5,
                crossAxisSpacing: SizeConfig.safeBlockHorizontal * 2.5,
              ),
              itemBuilder: (context, index) {
                return CountryFilterItem(countryCode: countryFilters[index]);
              },
            ),
          ),
        ],
      ),
    );
  }
}

class CountryFilterItem extends StatelessWidget {
  final String countryCode;

  const CountryFilterItem({
    Key key,
    @required this.countryCode,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ValueListenable _countryFilter = getIt<RoomProvider>().countryFilter;

    // Here it is!
    Size _textSize(String text, TextStyle style) {
      final TextPainter textPainter = TextPainter(
          text: TextSpan(text: text, style: style),
          maxLines: 1,
          textDirection: TextDirection.ltr)
        ..layout(minWidth: 0, maxWidth: double.infinity);
      return textPainter.size;
    }

    return ValueListenableBuilder(
        valueListenable: _countryFilter,
        builder: (context, _, child) {
          TextStyle customTextStyle = _countryFilter.value == countryCode
              ? TextStyle(
                  color: Color(0xff00FFE0),
                  fontWeight: FontWeight.bold,
                )
              : TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.normal,
                );

          Color backgroundColor = _countryFilter.value == ""
              ? Colors.white.withOpacity(0.2)
              : _countryFilter.value == countryCode
                  ? Colors.white.withOpacity(0.2)
                  : Colors.white.withOpacity(0.1);

          final Size txtSize = _textSize(
            Country(code: countryCode).name,
            TextStyle(
              fontSize: 12,
            ).merge(customTextStyle),
          );

          return ValueListenableBuilder(
            valueListenable: getIt<RoomProvider>().loading,
            builder: (ctx, loading, child) {
              return GestureDetector(
                onTap: () {
                  if (loading) return;

                  if (_countryFilter.value == countryCode)
                    getIt<RoomProvider>().setCountryFilter("");
                  else
                    getIt<RoomProvider>().setCountryFilter(countryCode);
                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: backgroundColor,
                  ),
                  padding: EdgeInsets.symmetric(
                    vertical: SizeConfig.safeBlockHorizontal * 2.5,
                    horizontal: SizeConfig.safeBlockHorizontal * 3,
                  ),
                  child: Row(
                    children: [
                      CountryWidget(
                        code: countryCode,
                        showCountryName: false,
                        widthFlag: SizeConfig.safeBlockHorizontal * 7,
                      ),
                      Spacer(),
                      Container(
                        width: SizeConfig.safeBlockHorizontal * 14,
                        alignment: Alignment.centerRight,
                        child: CustomText(
                          txtSize.width < SizeConfig.safeBlockHorizontal * 14
                              ? Country(code: countryCode).name
                              : Country(code: countryCode).abbreviation,
                          style: TextStyle(
                            fontSize: 12,
                          ).merge(customTextStyle),
                          maxLines: 1,
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        });
  }
}
