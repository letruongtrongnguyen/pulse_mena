import 'dart:math';
import 'dart:ui';

import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/providers/search.provider.dart';
import 'package:flutterbase/screens/search/search.screen.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item_shimmer.dart';

import 'country_filter_module.dart';
import 'create_button_module.dart';
import 'leaderboard_module.dart';
import 'tab_bar_module.dart';

class RoomScreen extends StatefulWidget {
  @override
  _RoomScreenState createState() => _RoomScreenState();
}

class _RoomScreenState extends State<RoomScreen> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getIt<RoomProvider>().fetchRoomData();
      // getIt<RoomProvider>().fetchMyRoomData();
      getIt<LeaderboardProvider>().fetchLeaderboardTopThreeData();
    });

    // getIt<RoomProvider>().listenSocket();

    super.initState();
  }

  // @override
  // void didChangeDependencies() {
  //   precacheImage(
  //       Image.asset(
  //         AssetsPath.fromImagesCommon("leaderboard_stand"),
  //       ).image,
  //       context);
  //
  //
  //   super.didChangeDependencies();
  // }

  @override
  Widget build(BuildContext context) {
    getIt<CommunicateService>().context = context;

    ValueNotifier roomData = getIt<RoomProvider>().data;
    ValueNotifier loading = getIt<RoomProvider>().loading;
    final double tabBarHeight = 50.0;

    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            CustomText(
              "room.title",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
            Spacer(),
            Hero(
              tag: "searchInputContainer",
              child: Material(
                type: MaterialType.transparency,
                child: Container(
                  color: Colors.black,
                ),
              ),
            )
          ],
        ),
        backgroundColor: Color(0xff1c1c1c).withOpacity(0.96),
        // flexibleSpace: ClipRect(
        //   child: BackdropFilter(
        //     filter: ImageFilter.blur(
        //       sigmaX: 8.0,
        //       sigmaY: 8.0,
        //     ),
        //     child: Container(
        //       color: Colors.black.withOpacity(0.6),
        //     ),
        //   ),
        // ),
        actions: [
          GestureDetector(
            onTap: () {
              // Navigator.of(context).pushNamed("/search-screen");
              getIt<SearchProvider>().searchEditingController.clear();

              Navigator.push(
                context,
                PageRouteBuilder(
                  pageBuilder: (c, a1, a2) => SearchScreen(),
                  // transitionsBuilder: (c, anim, a2, child) =>
                  //     FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 0),
                ),
              );
            },
            child: Container(
              color: Colors.transparent,
              alignment: Alignment.centerRight,
              padding: EdgeInsets.fromLTRB(
                SizeConfig.safeBlockHorizontal * 4,
                SizeConfig.safeBlockHorizontal * 4.2,
                SizeConfig.safeBlockHorizontal * 4,
                SizeConfig.safeBlockHorizontal * 4.2,
              ),
              // color: Colors.blueGrey,
              child: SvgPicture.asset(
                AssetsPath.fromImagesSvg("search_ic"),
              ),
            ),
          ),
          Container(
            color: Colors.transparent,
            alignment: Alignment.centerRight,
            padding: EdgeInsets.fromLTRB(
              SizeConfig.safeBlockHorizontal * 2,
              SizeConfig.safeBlockHorizontal * 4.2,
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4.2,
            ),
            // color: Colors.blueGrey,
            child: SvgPicture.asset(
              AssetsPath.fromImagesSvg("notification_ic"),
            ),
          ),
        ],
        automaticallyImplyLeading: false,
      ),
      extendBodyBehindAppBar: true,
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: SizedBox(
              height: SizeConfig.safeBlockHorizontal * 4 +
                  kToolbarHeight +
                  SizeConfig.paddingTop,
            ),
          ),
          // SliverToBoxAdapter(
          //   child: Padding(
          //     padding: EdgeInsets.symmetric(
          //       horizontal: SizeConfig.safeBlockHorizontal * 4,
          //     ),
          //     child: CreateButtonModule(),
          //   ),
          // ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.safeBlockHorizontal * 4,
              ),
              child: LeaderboardModule(),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.safeBlockHorizontal * 4,
              ),
              child: CountryFilterModule(),
            ),
          ),
          // SliverToBoxAdapter(
          //   child: Padding(
          //     padding: EdgeInsets.fromLTRB(
          //       SizeConfig.safeBlockHorizontal * 4,
          //       20,
          //       SizeConfig.safeBlockHorizontal * 4,
          //       10,
          //     ),
          //     child: SessionTitle(
          //       title: "room.menuTitle.roomList",
          //       iconPath: AssetsPath.fromImagesSvg("room_list_ic"),
          //     ),
          //   ),
          // ),
          // SliverPersistentHeader(
          //   pinned: true,
          //   delegate: _SliverAppBarDelegate(
          //     minHeight: tabBarHeight,
          //     maxHeight: tabBarHeight,
          //     child: Padding(
          //       padding: EdgeInsets.symmetric(
          //         horizontal: SizeConfig.safeBlockHorizontal * 4,
          //       ),
          //       child: TabBarModule(),
          //     ),
          //   ),
          // ),
          // AnimatedBuilder(
          //   animation: Listenable.merge([roomData, loading]),
          //   builder: (context, child) {
          //     return SliverList(
          //       delegate: SliverChildBuilderDelegate(
          //         (BuildContext context, int index) {
          //           /// To convert this infinite list to a list with "n" no of items,
          //           /// uncomment the following line:
          //           /// if (index > n) return null;
          //           ///
          //           // if (roomData.value.length == 0) return null;
          //           return AnimatedOpacity(
          //             duration: Duration(milliseconds: 200),
          //             opacity: loading.value ? 0.7 : 1.0,
          //             child: Padding(
          //               padding: EdgeInsets.symmetric(
          //                 vertical: SizeConfig.safeBlockHorizontal * 1.5,
          //                 horizontal: SizeConfig.safeBlockHorizontal * 4,
          //               ),
          //               child: RoomItemWidget(
          //                 roomData: roomData.value[index],
          //               ),
          //               // child: RoomItemShimmer(),
          //             ),
          //           );
          //         },
          //         childCount: roomData.value.length,
          //       ),
          //     );
          //   },
          // ),
          // AnimatedBuilder(
          //   animation: Listenable.merge([roomData, loading]),
          //   builder: (ctx, child) {
          //     final double remainSpace = SizeConfig.screenHeight -
          //         SizeConfig.paddingTop -
          //         SizeConfig.paddingBottom -
          //         kToolbarHeight -
          //         kBottomNavigationBarHeight -
          //         tabBarHeight -
          //         (roomData.value.length * SizeConfig.safeBlockHorizontal * 25);
          //
          //     if (loading.value && roomData.value.length == 0)
          //       return SliverFillRemaining(
          //         hasScrollBody: false,
          //         child: Container(
          //           // color: Colors.blue[100],
          //           height: remainSpace < 0 ? 0 : remainSpace,
          //           child: ListView.builder(
          //             physics: NeverScrollableScrollPhysics(),
          //             padding: EdgeInsets.only(
          //               top: SizeConfig.safeBlockHorizontal * 1.5,
          //             ),
          //             shrinkWrap: true,
          //             itemCount: 5,
          //             itemBuilder: (ctx, index) {
          //               return Padding(
          //                 padding: EdgeInsets.symmetric(
          //                     vertical: SizeConfig.safeBlockHorizontal),
          //                 child: Padding(
          //                   padding: EdgeInsets.only(
          //                     bottom: SizeConfig.safeBlockHorizontal * 3,
          //                     left: SizeConfig.safeBlockHorizontal * 4,
          //                     right: SizeConfig.safeBlockHorizontal * 4,
          //                   ),
          //                   child: RoomItemShimmer(),
          //                 ),
          //               );
          //             },
          //           ),
          //         ),
          //       );
          //
          //     return SliverFillRemaining(
          //       hasScrollBody: false,
          //       child: Container(
          //         // color: Colors.blue[100],
          //         height: remainSpace < 0 ? 0 : remainSpace,
          //         child: roomData.value.length != 0 ? SizedBox() : EmptyRoom(),
          //       ),
          //     );
          //   },
          // ),
          // SliverToBoxAdapter(
          //   child: SizedBox(
          //       height: SizeConfig.paddingBottom + kBottomNavigationBarHeight),
          // ),
        ],
      ),
      floatingActionButton: FlatButton(
        onPressed: () {
          Navigator.of(context).pushNamed("/notification-screen");
        },
        onLongPress: () {
          final textDirection = getIt<GeneralProvider>().textDirection.value;
          if (textDirection == TextDirection.ltr)
            getIt<GeneralProvider>().setTextDirection(TextDirection.rtl);
          else
            getIt<GeneralProvider>().setTextDirection(TextDirection.ltr);

          // AdaptiveTheme.of(context).toggleThemeMode();
        },
        child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.blueAccent,
          ),
          child: Icon(Icons.navigate_next),
        ),
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    @required this.minHeight,
    @required this.maxHeight,
    @required this.child,
  });

  final double minHeight;
  final double maxHeight;
  final Widget child;

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => max(maxHeight, minHeight);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}

class EmptyRoom extends StatelessWidget {
  final _height = SizeConfig.safeBlockHorizontal * 22;

  @override
  Widget build(BuildContext context) {
    return Align(
      child: Column(
        children: [
          Stack(
            children: [
              Positioned(
                left: -SizeConfig.safeBlockHorizontal * 5,
                child: BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: 8.0,
                    sigmaY: 8.0,
                  ),
                  child: Container(
                    width: SizeConfig.safeBlockHorizontal * 45,
                    height: SizeConfig.safeBlockHorizontal * 45,
                    decoration: BoxDecoration(
                      gradient: RadialGradient(
                        colors: [
                          Color(0xff3EE4FF).withOpacity(0.6),
                          Colors.transparent,
                        ],
                        stops: [0, 0.8],
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                width: SizeConfig.safeBlockHorizontal * 40,
                height: SizeConfig.safeBlockHorizontal * 40,
                alignment: Alignment.center,
                child: Image.asset(
                  AssetsPath.fromImagesCharacterAction("haki_lookup"),
                  height: SizeConfig.safeBlockHorizontal * 30,
                  fit: BoxFit.contain,
                ),
              ),
            ],
          ),
          CustomText(
            "common.emptyRoom",
          ),
        ],
      ),
    );
  }
}
