import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/leaderboard.item.model.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/widgets/basic/circle_ic.widget.dart';
import 'package:flutterbase/widgets/basic/image_placeholder.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_config.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_top_item.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_top_item_shimmer.dart';
import 'package:flutterbase/widgets/session_title/session_title.dart';
import 'package:shimmer/shimmer.dart';

class LeaderboardModule extends StatefulWidget {
  @override
  _LeaderboardModuleState createState() => _LeaderboardModuleState();
}

class _LeaderboardModuleState extends State<LeaderboardModule> {
  final CarouselController _controller = CarouselController();

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    ValueNotifier loading =
        getIt<LeaderboardProvider>().leaderboardTopThreeDataLoading;
    ValueNotifier leaderboardTopThreeData =
        getIt<LeaderboardProvider>().leaderboardTopThreeData;

    return Container(
      width: SizeConfig.safeBlockHorizontal * 92,
      padding: EdgeInsets.only(
        top: SizeConfig.safeBlockVertical * 3,
        bottom: SizeConfig.safeBlockVertical * 2,
      ),
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border(
          bottom: BorderSide(
            color: Color(0xff424242),
            width: 1,
          ),
        ),
      ),
      child: Column(
        children: [
          SessionTitle(
            title: "common.leaderboard",
            onTap: () {
              Navigator.of(context).pushNamed("/leaderboard-screen");
            },
            iconPath: AssetsPath.fromImagesSvg("leaderboard_ic"),
            suffix: SvgPicture.asset(
              AssetsPath.fromImagesSvg("arrow_forward_ios_ic"),
              height: SizeConfig.safeBlockHorizontal * 4,
            ),
          ),
          SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
          AnimatedBuilder(
            animation: Listenable.merge([loading, leaderboardTopThreeData]),
            builder: (context, child) {
              if (loading.value)
                return CarouselSlider.builder(
                  carouselController: _controller,
                  options: CarouselOptions(
                      enlargeCenterPage: true,
                      enableInfiniteScroll: true,
                      pageSnapping: true,
                      autoPlay: false,
                      viewportFraction: 1.0,
                      aspectRatio: 1.5,
                      pauseAutoPlayOnManualNavigate: true,
                      pauseAutoPlayOnTouch: true,
                      onPageChanged: (index, reason) {
                        setState(() {
                          _currentIndex = index;
                        });
                      }),
                  itemCount: 1,
                  itemBuilder: (BuildContext context, int itemIndex, int realIdx) {
                    return Shimmer.fromColors(
                      baseColor: Colors.white.withOpacity(0.3),
                      highlightColor: Colors.white.withOpacity(0.2),
                      child: Container(
                        width: 500,
                        height: 500,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                    );
                  },
                );

              return CarouselSlider.builder(
                carouselController: _controller,
                options: CarouselOptions(
                    enlargeCenterPage: true,
                    enableInfiniteScroll: true,
                    pageSnapping: true,
                    autoPlay: true,
                    viewportFraction: 1.0,
                    aspectRatio: 1.5,
                    pauseAutoPlayOnManualNavigate: true,
                    pauseAutoPlayOnTouch: true,
                    onPageChanged: (index, reason) {
                      setState(() {
                        _currentIndex = index;
                      });
                    }),
                itemCount: LeaderboardType.values.length,
                itemBuilder: (BuildContext context, int itemIndex, int realIdx) {
                  return _buildSliderItem(
                    context,
                    type: LeaderboardType.values[itemIndex],
                    data: leaderboardTopThreeData
                        .value[LeaderboardType.values[itemIndex]],
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildSliderItem(
    BuildContext context, {
    LeaderboardType type,
    List<Leader> data,
  }) {
    return GestureDetector(
      onTap: () {
        getIt<LeaderboardProvider>().setCurrentActiveTab(type);
        int index = LeaderboardType.values.indexOf(type);
        getIt<LeaderboardProvider>().setCurrentIndex(index);

        Navigator.of(context).pushNamed("/leaderboard-screen");
      },
      child: Container(
        width: SizeConfig.safeBlockHorizontal * 92,
        height: SizeConfig.safeBlockHorizontal * 65,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          gradient: LeaderboardConfig.itemConfig[type]["gradient"],
        ),
        alignment: Alignment.center,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Column(
              children: [
                SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
                CustomText(
                  LeaderboardConfig.itemConfig[type]["weeklyTitle"],
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Spacer(),
                ImagePlaceholderWidget(
                  height: SizeConfig.safeBlockHorizontal * 25,
                  image: AssetImage(
                    AssetsPath.fromImagesCommon("leaderboard_stand"),
                  ),
                  fit: BoxFit.fitHeight,
                  placeholder: Container(
                    color: Colors.transparent,
                    height: SizeConfig.safeBlockHorizontal * 25,
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: 0,
              child: _buildTopThree(type: type, data: data),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTopThree({
    LeaderboardType type,
    List<Leader> data,
  }) {
    final _width = SizeConfig.safeBlockHorizontal *
            27 *
            LeaderboardConfig.leaderboardStandSizeRatio /
            3 -
        20;

    return Container(
      width: SizeConfig.safeBlockHorizontal *
          25 *
          LeaderboardConfig.leaderboardStandSizeRatio,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          if (data != null && data.length > 1)
            Container(
              margin:
                  EdgeInsets.only(bottom: SizeConfig.safeBlockHorizontal * 14),
              child: LeaderboardTopItemWidget(
                pendantUrl: data[1].pendantUrl,
                type: type,
                userId: data[1].userId,
                name: data[1].name,
                value: data[1].point,
                avatarUrl: data[1].avatarUrl,
                width: _width,
                index: 2,
              ),
            ),
          if (data != null && data.length <= 1) Container(width: _width),
          if (data != null && data.length > 0)
            Container(
              margin:
                  EdgeInsets.only(bottom: SizeConfig.safeBlockHorizontal * 20),
              child: LeaderboardTopItemWidget(
                pendantUrl: data[0].pendantUrl,
                type: type,
                userId: data[0].userId,
                name: data[0].name,
                value: data[0].point,
                avatarUrl: data[0].avatarUrl,
                width: _width,
                index: 1,
              ),
            ),
          if (data != null && data.length <= 2) Container(width: _width),
          if (data != null && data.length > 2)
            Container(
              margin:
                  EdgeInsets.only(bottom: SizeConfig.safeBlockHorizontal * 13),
              child: LeaderboardTopItemWidget(
                pendantUrl: data[2].pendantUrl,
                type: type,
                userId: data[2].userId,
                name: data[2].name,
                value: data[2].point,
                avatarUrl: data[2].avatarUrl,
                width: _width,
                index: 3,
              ),
            ),
        ],
      ),
    );
  }
}
