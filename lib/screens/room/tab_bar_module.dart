import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class TabBarModule extends StatefulWidget {
  @override
  _TabBarModuleState createState() => _TabBarModuleState();
}

class _TabBarModuleState extends State<TabBarModule> {
  final GlobalKey _hotRoomTabKey = GlobalKey();
  final GlobalKey _newRoomTabKey = GlobalKey();
  bool isReady = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => getSize());
  }

  getSize() {
    new Future<String>.delayed(new Duration(seconds: 0), () {
      RenderBox _hotRoomTab = _hotRoomTabKey.currentContext.findRenderObject();
      int tabIndex = tabItemList.indexWhere(
          (tabItem) => tabItem.tabItemType == RoomTabItemType.hotRoom);
      tabItemList[tabIndex].setWidth(_hotRoomTab.size.width);

      RenderBox _newRoomTab = _newRoomTabKey.currentContext.findRenderObject();
      tabIndex = tabItemList.indexWhere(
          (tabItem) => tabItem.tabItemType == RoomTabItemType.newRoom);
      tabItemList[tabIndex].setWidth(_newRoomTab.size.width);
      return;
    }).then((String value) {
      setState(() {
        isReady = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    ValueNotifier _currentActiveTab = getIt<RoomProvider>().currentActiveTab;
    ValueNotifier _loading = getIt<RoomProvider>().loading;

    return Container(
      alignment: Alignment.centerLeft,
      color: Colors.black,
      width: SizeConfig.safeBlockHorizontal * 92,
      child: AnimatedBuilder(
        animation: Listenable.merge([_loading, _currentActiveTab]),
        builder: (ctx, child) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TabBarItem(
                    isLoading: _loading.value,
                    key: _hotRoomTabKey,
                    title: "room.tab.hotRoom",
                    tabValue: RoomTabItemType.hotRoom,
                    isActive:
                        _currentActiveTab.value == RoomTabItemType.hotRoom,
                    width: SizeConfig.safeBlockHorizontal * 5,
                  ),
                  SizedBox(width: SizeConfig.safeBlockHorizontal * 5),
                  TabBarItem(
                    isLoading: _loading.value,
                    key: _newRoomTabKey,
                    title: "room.tab.newRoom",
                    tabValue: RoomTabItemType.newRoom,
                    isActive:
                        _currentActiveTab.value == RoomTabItemType.newRoom,
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      if (_loading.value) return;
                      getIt<RoomProvider>().fetchRoomData();
                    },
                    child: SvgPicture.asset(
                      AssetsPath.fromImagesSvg("reload_ic"),
                      height: SizeConfig.safeBlockHorizontal * 5,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 7),
              if (isReady)
                ActiveTabIndicator(
                  roomActiveTabType: _currentActiveTab.value,
                ),
            ],
          );
        },
      ),
    );
  }
}

typedef CallbackFunction = void Function(double value);

class TabBarItem extends StatelessWidget {
  final bool isActive;
  final String title;
  final RoomTabItemType tabValue;
  final double width;
  final bool isLoading;

  const TabBarItem({
    Key key,
    this.isActive = false,
    this.title,
    this.tabValue,
    this.width,
    this.isLoading = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GlobalKey _tabBarItemKey = GlobalKey();
    Color _textColor = isActive ? Color(0xff00FFE0) : Theme.of(context).textTheme.bodyText2.color;

    return GestureDetector(
      onTap: () {
        if (isLoading) return;
        getIt<RoomProvider>().onRoomActiveTabChange(tabValue);
      },
      child: Container(
        color: Colors.transparent,
        key: _tabBarItemKey,
        child: Row(
          children: [
            if (tabValue == RoomTabItemType.hotRoom)
              Row(
                children: [
                  Image(
                    image: AssetImage(
                      AssetsPath.fromImagesCommon("fire_ic"),
                    ),
                    width: SizeConfig.safeBlockHorizontal * 2,
                  ),
                  SizedBox(width: 7),
                ],
              ),
            CustomText(
              title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: _textColor,
                fontSize: 14,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ActiveTabIndicator extends StatelessWidget {
  final RoomTabItemType roomActiveTabType;

  const ActiveTabIndicator({
    Key key,
    this.roomActiveTabType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final tabIndex = tabItemList.indexWhere((tabItem) =>
        tabItem.tabItemType == getIt<RoomProvider>().currentActiveTab.value);
    double leftPos = 0.0;
    double barWidth = 0.0;

    if (tabIndex > -1) {
      leftPos = tabItemList.sublist(0, tabIndex).fold(
            0.0,
            (previousValue, tabType) =>
                previousValue +
                tabType.widthTab / SizeConfig.safeBlockHorizontal +
                5,
          );
      barWidth = tabItemList[tabIndex].widthTab;
    }

    return Stack(
      children: <Widget>[
        Container(height: 2.0),
        AnimatedPositioned(
          duration: const Duration(milliseconds: 100),
          curve: Curves.fastOutSlowIn,
          top: 0.0,
          left: leftPos * SizeConfig.safeBlockHorizontal,
          width: barWidth,
          height: 3.0,
          child: Container(
            decoration: BoxDecoration(
              color: Color(0xff00FFE0),
              borderRadius: BorderRadius.circular(30),
            ),
          ),
        )
      ],
    );
  }
}

class RoomTabItem {
  RoomTabItemType tabItemType;
  double widthTab;

  RoomTabItem(this.tabItemType, this.widthTab);

  void setWidth(double newWidth) {
    widthTab = newWidth;
  }
}

List<RoomTabItem> tabItemList = <RoomTabItem>[
  RoomTabItem(
    RoomTabItemType.hotRoom,
    50,
  ),
  RoomTabItem(
    RoomTabItemType.newRoom,
    50,
  ),
];
