import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class MenuModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MenuItem(
          title: "profile.menu.interest",
          content: CustomText(
            "Game, Travel",
            style: TextStyle(
              color: Color(0xffd4d4d4),
              fontSize: 12,
            ),
          ),
          onTap: () {
            print("interest touched");
          },
        ),
        MenuItem(
          title: "profile.menu.joined",
          content: CustomText(
            "30 day(s)",
            style: TextStyle(
              color: Color(0xffd4d4d4),
              fontSize: 12,
            ),
          ),
        ),
      ],
    );
  }
}

class MenuItem extends StatelessWidget {
  final String title;
  final Widget content;
  final Function onTap;

  const MenuItem({
    Key key,
    @required this.title,
    this.content,
    this.onTap,
  })  : assert(title != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap ?? () {},
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.1),
          borderRadius: BorderRadius.circular(8),
        ),
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.safeBlockHorizontal * 3,
          vertical: SizeConfig.safeBlockHorizontal * 3,
        ),
        margin: EdgeInsets.symmetric(
          vertical: SizeConfig.safeBlockHorizontal * 1.5,
        ),
        child: Row(
          children: [
            CustomText(
              title,
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            Spacer(),
            if (content != null) content,
            if (onTap != null)
              Padding(
                padding: EdgeInsets.only(left: 20),
                child: SvgPicture.asset(
                  AssetsPath.fromImagesSvg("arrow_forward_ios_ic"),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
