import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/country/country.widget.dart';

class UserInfoModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String iconPath = true
        ? AssetsPath.fromImagesSvg('female_gender_ic')
        : AssetsPath.fromImagesSvg('male_gender_ic');

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        AvatarWidget(
          avatarUrl:
              "https://s3-alpha-sig.figma.com/img/6992/5846/ac37b276cae20a3fa4e4d6574e9cfdc7?Expires=1603065600&Signature=KkJTzLmySUrZI3GTKz2lPlLk9vX~YD6w708cs9HBuZfb6X26wjkZsJTeUGLMJp-t4MExYwN6ZX-2nMoPfOYzJeQkd3QZJy6gJ0AMFnO8PAhE2K4IYktBwIFCYLAfgabUYahD~l7CaeqGTuyavbxU8FGAyQlb64G8hnGALNRzcIgOHN8H4FnaZpAHipA3csbTC~AVkh46umdBfX5tu-Qdsm8ScjxL55~YEbTy10HI8Hr9i9NINGWKcn23OX5VhwZTfbC4VUDilsmLgtHJOTYu1uPZP8nBUjp9x5aOu-AgulQbA1jqMJ~Yxbbjg2We8Kw~c-Uw04iPVEADCRehLSHlQQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
          radius: SizeConfig.safeBlockHorizontal * 9,
          borderWidth: 1.5,
        ),
        SizedBox(height: 8),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            ShaderMask(
              shaderCallback: (bounds) {
                return LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [Color(0xffDF0C8B), Color(0xffF486C8)],
                ).createShader(bounds);
              },
              child: SvgPicture.asset(
                iconPath,
                height: SizeConfig.safeBlockHorizontal * 5,
              ),
            ),
            SizedBox(width: SizeConfig.safeBlockHorizontal * 2),
            Container(
              constraints: BoxConstraints(
                maxWidth: SizeConfig.safeBlockHorizontal * 70,
              ),
              child: CustomText(
                Formatter.clearZeroWidthSpace("Mickey"),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                softWrap: false,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                ),
              ),
            ),
            CustomText(
              ", 25",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
            ),
          ],
        ),
        SizedBox(height: SizeConfig.safeBlockVertical * 3),
        Container(
          padding: EdgeInsets.symmetric(
            vertical: 5,
            horizontal: 15,
          ),
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.1),
            borderRadius: BorderRadius.circular(20),
          ),
          child: CountryWidget(
            code: "vi",
            widthFlag: SizeConfig.safeBlockHorizontal * 5,
            maxWidthText: SizeConfig.safeBlockHorizontal * 50,
            textStyle: TextStyle(
              fontSize: 13,
              // color: Color(),
            ),
          ),
        ),
        SizedBox(height: SizeConfig.safeBlockVertical * 2),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            CustomText("ID: 68686868"),
            SizedBox(width: 20),
            CustomText(
              "Lv.10",
              style: TextStyle(
                color: Color(0xff23ED4F),
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        SizedBox(height: SizeConfig.safeBlockVertical * 5),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(
              children: [
                CustomText(
                  "100",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                CustomText(
                  "profile.visitor.currency",
                  style: TextStyle(
                    fontSize: 13,
                    color: Color(0xff7F7F7F),
                  ),
                )
              ],
            ),
            SizedBox(width: SizeConfig.safeBlockHorizontal * 15),
            Column(
              children: [
                CustomText(
                  "9",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                CustomText(
                  "profile.friend.currency",
                  style: TextStyle(
                    fontSize: 13,
                    color: Color(0xff7F7F7F),
                  ),
                )
              ],
            ),
          ],
        ),
      ],
    );
  }
}
