import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class ButtonModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.screenWidth,
      height: SizeConfig.safeBlockHorizontal * 12 +
          SizeConfig.paddingBottom +
          SizeConfig.safeBlockVertical * 4,
      alignment: Alignment.center,
      padding: EdgeInsets.fromLTRB(
        0,
        SizeConfig.safeBlockVertical * 2,
        0,
        SizeConfig.paddingBottom + SizeConfig.safeBlockVertical * 2,
      ),
      decoration: BoxDecoration(
        color: Colors.black,
        border: Border(
          top: BorderSide(
            color: Color(0xff424242),
            width: 1.5,
          ),
        ),
      ),
      child: GestureDetector(
        child: Container(
          alignment: Alignment.center,
          width: SizeConfig.safeBlockHorizontal * 60,
          height: SizeConfig.safeBlockHorizontal * 12,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(60),
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Color(0xff81FFCA), Color(0xff2CB7FF)],
            ),
          ),
          child: CustomText(
            "profile.btn.addFriend",
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
