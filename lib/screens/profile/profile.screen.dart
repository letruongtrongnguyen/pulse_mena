import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

import 'button_module.dart';
import 'menu_module.dart';
import 'user_info_module.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Container(
          // color: Colors.red,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              CustomText(
                "Mickey's ",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
              CustomText(
                "profile.title",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            ],
          ),
        ),
        leading: Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.fromLTRB(
            SizeConfig.safeBlockHorizontal * 4,
            SizeConfig.safeBlockHorizontal * 4.5,
            0,
            SizeConfig.safeBlockHorizontal * 4.5,
          ),
          // color: Colors.blueGrey,
          child: SvgPicture.asset(
            AssetsPath.fromImagesSvg("arrow_back_ic"),
          ),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xff1c1c1c),
      ),
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            padding: EdgeInsets.fromLTRB(
              SizeConfig.safeBlockHorizontal * 4,
              0,
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 12 +
                  SizeConfig.paddingBottom +
                  SizeConfig.safeBlockVertical * 4 +
                  10,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: SizeConfig.safeBlockHorizontal * 4,
                  ),
                  UserInfoModule(),
                  Divider(
                    color: Color(0xff7f7f7f),
                    height: 30,
                  ),
                  MenuModule(),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: ButtonModule(),
          ),
        ],
      ),
    );
  }
}
