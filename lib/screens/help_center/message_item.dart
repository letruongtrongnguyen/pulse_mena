import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class MessageItem extends StatelessWidget {
  final String name;
  final String avatarUrl;
  final int userId;

  const MessageItem({
    Key key,
    this.name,
    this.avatarUrl,
    this.userId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        getIt<CommunicateService>().navigate(route: "/private-chat", args: {
          "id": userId,
        });
      },
      child: Container(
        width: SizeConfig.safeBlockHorizontal * 92,
        color: Colors.transparent,
        child: Row(
          children: [
            AvatarWidget(
              avatarUrl: avatarUrl ?? '',
              radius: SizeConfig.safeBlockHorizontal * 8,
              borderWidth: 0,
            ),
            SizedBox(width: SizeConfig.safeBlockHorizontal * 4),
            Column(
              children: [
                SizedBox(
                  width: SizeConfig.safeBlockHorizontal * 72,
                  child: Column(
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          CustomText(
                            Formatter.clearZeroWidthSpace(
                              Formatter.capitalize(name ?? "Haki CS"),
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            softWrap: false,
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                            ),
                          ),
                          // Spacer(),
                          // CustomText(
                          //   "02:00 PM",
                          //   style: TextStyle(
                          //     fontSize: 12,
                          //     color: Colors.white.withOpacity(0.6),
                          //   ),
                          // )
                        ],
                      ),
                    ],
                  ),
                ),
                // SizedBox(height: 8),
                // SizedBox(
                //   width: SizeConfig.safeBlockHorizontal * 72,
                //   child: Column(
                //     children: [
                //       Row(
                //         mainAxisSize: MainAxisSize.max,
                //         children: [
                //           Container(
                //             constraints: BoxConstraints(
                //               maxWidth: SizeConfig.safeBlockHorizontal * 50,
                //             ),
                //             child: CustomText(
                //               Formatter.clearZeroWidthSpace(
                //                 Formatter.capitalize(
                //                     "natoque penatibus et magnis dis parturient montes nascetur ridiculus mus"),
                //               ),
                //               overflow: TextOverflow.ellipsis,
                //               maxLines: 1,
                //               softWrap: false,
                //               style: TextStyle(
                //                 fontWeight: FontWeight.w700,
                //                 fontSize: 12,
                //               ),
                //             ),
                //           ),
                //           Spacer(),
                //           Container(
                //             width: SizeConfig.safeBlockHorizontal * 2.5,
                //             height: SizeConfig.safeBlockHorizontal * 2.5,
                //             decoration: BoxDecoration(
                //                 color: Color(0xffff0000), shape: BoxShape.circle),
                //           )
                //         ],
                //       ),
                //     ],
                //   ),
                // ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
