import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/user.model.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/i18n/i18n.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/screens/help_center/action_config.dart';
import 'package:flutterbase/widgets/basic/circle_ic.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:rate_my_app/rate_my_app.dart';

class ActionModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsetsDirectional.only(
            start: SizeConfig.safeBlockHorizontal * 4,
            end: SizeConfig.safeBlockHorizontal * 4,
            top: SizeConfig.safeBlockHorizontal * 4,
          ),
          child: CustomText(
            "helpCenter.sessionTitle.helping",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
          ),
        ),
        GridView.count(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.safeBlockHorizontal * 4,
            vertical: SizeConfig.safeBlockHorizontal * 4,
          ),
          crossAxisCount: 2,
          crossAxisSpacing: SizeConfig.safeBlockHorizontal * 2,
          mainAxisSpacing: SizeConfig.safeBlockHorizontal * 2,
          childAspectRatio: 2,
          children: List.generate(
            HelpCenterActionConfig.actionConfig(context).length,
            (index) {
              return HelpCenterItem(
                index: index,
              );
            },
          ),
        ),
      ],
    );
  }
}

class HelpCenterItem extends StatelessWidget {
  final int index;

  const HelpCenterItem({
    Key key,
    this.index,
  }) : super(key: key);

  getType() {
    return HelpCenterActionType.values[index];
  }

  reviewAppAction(BuildContext context) async {
    RateMyApp rateMyApp = RateMyApp(
      preferencesPrefix: 'rateMyApp_',
      minDays: 7,
      minLaunches: 10,
      remindDays: 7,
      remindLaunches: 10,
      googlePlayIdentifier: 'com.facebook.katana',
      appStoreIdentifier: '284882215',
    );

    rateMyApp.init();

    // rateMyApp.isNativeReviewDialogSupported
    //     .then((value) => print("isNativeReviewDialogSupported $value"));

    rateMyApp.showStarRateDialog(
      context,
      // The dialog title.
      title: I18n.get("rateAppDialog.title"),
      // The dialog message.
      message: I18n.get("rateAppDialog.content"),
      // contentBuilder: (context, defaultContent) => content, // This one allows you to change the default dialog content.
      // Set to false if you want to show the Apple's native app rating dialog on iOS or Google's native app rating dialog (depends on the current Platform).
      ignoreNativeDialog: true,
      actionsBuilder: (context, stars) {
        // Triggered when the user updates the star rating.
        return [
          // Return a list of actions (that will be shown at the bottom of the dialog).
          ElevatedButton(
            child: Text('OK'),
            style: ButtonStyle(
              backgroundColor:
                  MaterialStateProperty.all<Color>(Colors.transparent),
              shadowColor: MaterialStateProperty.all<Color>(Colors.transparent),
            ),
            onPressed: () async {
              if (stars.round() <= 3) {
                navigateToCustomerServiceChat();
              } else if (stars.round() >= 4) {
                navigateToRatingInStore(stars.round());
              }
              // You can handle the result as you want (for instance if the user puts 1 star then open your contact page, if he puts more then open the store page, etc...).
              // This allows to mimic the behavior of the default "Rate" button. See "Advanced > Broadcasting events" for more information :
              await rateMyApp.callEvent(RateMyAppEventType.rateButtonPressed);
              Navigator.pop<RateMyAppDialogButton>(
                  context, RateMyAppDialogButton.rate);
            },
          ),
        ];
      },
      dialogStyle: DialogStyle(
        // Custom dialog styles.
        titleAlign: TextAlign.center,
        messageAlign: TextAlign.center,
        messagePadding: EdgeInsets.only(bottom: 20),
      ),
      starRatingOptions: StarRatingOptions(
        starsFillColor: Colors.amber,
        starsBorderColor: Colors.amber,
      ),
      onDismissed: () => rateMyApp.callEvent(RateMyAppEventType
          .laterButtonPressed), // Called when the user dismissed the dialog (either by taping outside or by pressing the "back" button).
    );
  }

  navigateToCustomerServiceChat() {
    User customerServiceUserData =
        getIt<GeneralProvider>().customerServiceUserData.value;

    getIt<CommunicateService>().navigate(route: "/private-chat", args: {
      "id": customerServiceUserData.id,
    });
  }

  navigateToRatingInStore(int stars) {
    getIt<CommunicateService>().navigate(route: "/review-app", args: {
      "stars": stars,
    });
  }

  @override
  Widget build(BuildContext context) {
    final Map<HelpCenterActionType, dynamic> functions = {
      HelpCenterActionType.ReportAnIssue: navigateToCustomerServiceChat,
      HelpCenterActionType.ReviewApplication: () => reviewAppAction(context),
      // HelpCenterActionType.ForgotPassword: () {},
      // HelpCenterActionType.CommunityRegulation: () {},
      HelpCenterActionType.TermPolicy: () {
        getIt<CommunicateService>().navigate(route: "/webview-screen", args: {
          "url": "http://www.haki.live/documents",
          "title": "Term & Policy",
        });
      },
    };

    return GestureDetector(
      onTap: functions[getType()],
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Theme.of(context).cardTheme.color,
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).cardTheme.shadowColor,
              offset: Offset(0, 1),
              blurRadius: 5,
            )
          ],
        ),
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.safeBlockHorizontal * 4,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleIconWidget(
              icon: HelpCenterActionConfig.actionConfig(context)[getType()]
                  ["icon"],
              radius: SizeConfig.safeBlockHorizontal * 4,
              padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 1.5),
              backgroundGradient:
                  HelpCenterActionConfig.actionConfig(context)[getType()]
                      ["gradientBackground"],
            ),
            SizedBox(height: 8),
            CustomText(
              HelpCenterActionConfig.actionConfig(context)[getType()]["text"],
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              softWrap: false,
              clearZeroSpace: true,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 12,
              ),
            )
          ],
        ),
      ),
    );
  }
}
