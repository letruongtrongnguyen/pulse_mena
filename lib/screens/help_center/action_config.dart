import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/core/utils/assets_path.dart';

enum HelpCenterActionType {
  ReportAnIssue,
  ReviewApplication,
  // ForgotPassword,
  // CommunityRegulation,
  TermPolicy,
}

class HelpCenterActionConfig {
  static Map<HelpCenterActionType, Map<String, dynamic>> actionConfig(
      BuildContext context) {
    Gradient gradientColor = LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: [
        Color(0xffD5F55A),
        Color(0xff47D2E9),
      ],
    );

    Color iconColor = Theme.of(context).primaryColor;

    return {
      HelpCenterActionType.ReportAnIssue: {
        "text": "helpCenter.action.reportAnIssue",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("report_ic"),
          color: iconColor,
        ),
        "gradientBackground": gradientColor,
      },
      HelpCenterActionType.ReviewApplication: {
        "text": "helpCenter.action.reviewApplication",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("review_app_ic"),
          color: iconColor,
        ),
        "gradientBackground": gradientColor,
      },
      // HelpCenterActionType.ForgotPassword: {
      //   "text": "helpCenter.action.forgotPassword",
      //   "icon": SvgPicture.asset(
      //     AssetsPath.fromImagesSvg("lock_ic"),
      //     color: iconColor,
      //   ),
      //   "gradientBackground": gradientColor,
      // },
      // HelpCenterActionType.CommunityRegulation: {
      //   "text": "helpCenter.action.communityRegulation",
      //   "icon": SvgPicture.asset(
      //     AssetsPath.fromImagesSvg("paper_ic"),
      //     color: iconColor,
      //   ),
      //   "gradientBackground": gradientColor,
      // },
      HelpCenterActionType.TermPolicy: {
        "text": "helpCenter.action.termPolicy",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("shield_done_ic"),
          color: iconColor,
        ),
        "gradientBackground": gradientColor,
      },
    };
  }
}
