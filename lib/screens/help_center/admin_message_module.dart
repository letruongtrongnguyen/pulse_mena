import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/user.model.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/screens/help_center/message_item.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class AdminMessageModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    User customerServiceUserData =
        getIt<GeneralProvider>().customerServiceUserData.value;

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.safeBlockHorizontal * 4,
        vertical: SizeConfig.safeBlockHorizontal * 4,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomText(
            "helpCenter.sessionTitle.talkToAdmin",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
          ),
          SizedBox(height: 20),
          MessageItem(
            avatarUrl: customerServiceUserData.avatarUrl,
            name: customerServiceUserData.fullName,
            userId: customerServiceUserData.id,
          ),
        ],
      ),
    );
  }
}
