import 'package:flutter/material.dart';
import 'package:flutterbase/configs/general.config.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/request/request.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/i18n/i18n.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/widgets/basic/popup/center_popup.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

import 'menu_item/menu_item.dart';
import 'menu_item/menu_item_config.dart';
import 'menu_item/menu_item_full.dart';

class MenuModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MenuItemFull(
          type: MenuItemType.ActivityCenter,
        ),
        SizedBox(height: 12),
        Container(
          decoration: BoxDecoration(
            color: Theme.of(context).cardTheme.color,
            boxShadow: [
              BoxShadow(
                color: Theme.of(context).cardTheme.shadowColor,
                offset: Offset(0, 1),
                blurRadius: 5,
              ),
            ],
            borderRadius: BorderRadius.circular(12),
          ),
          padding: EdgeInsets.all(
            SizeConfig.safeBlockHorizontal * 4,
          ),
          width: SizeConfig.safeBlockHorizontal * 92,
          height: 150,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MenuItem(
                type: MenuItemType.Wallet,
                onTap: () {
                  getIt<CommunicateService>().navigate(
                    route: "/store",
                    args: {
                      "selected": "coins",
                    },
                  );
                },
              ),
              MenuItem(
                type: MenuItemType.Store,
                onTap: () {
                  getIt<CommunicateService>().navigate(
                    route: "/store",
                    args: {
                      "selected": "items",
                    },
                  );
                },
              ),
              MenuItem(
                type: MenuItemType.HakiVIP,
                onTap: () {
                  // _openComingSoonPopup(context);

                  getIt<CommunicateService>()
                      .navigate(route: "/webview-screen", args: {
                    "url": "http://www.haki.live/vip",
                    "title": I18n.get("common.hakiVip"),
                  });
                },
              ),
            ],
          ),
        ),
        SizedBox(height: 12),
        Container(
          decoration: BoxDecoration(
            color: Theme.of(context).cardTheme.color,
            boxShadow: [
              BoxShadow(
                color: Theme.of(context).cardTheme.shadowColor,
                offset: Offset(0, 1),
                blurRadius: 5,
              ),
            ],
            borderRadius: BorderRadius.circular(12),
          ),
          padding: EdgeInsets.all(
            SizeConfig.safeBlockHorizontal * 4,
          ),
          width: SizeConfig.safeBlockHorizontal * 92,
          height: 150,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MenuItem(
                type: MenuItemType.Task,
                isComingSoon: true,
                onTap: () {
                  _openComingSoonPopup(context);
                },
              ),
              MenuItem(
                type: MenuItemType.Help,
                onTap: () {
                  Navigator.of(context).pushNamed("/help-center-screen");
                },
              ),
              MenuItem(
                type: MenuItemType.Settings,
                onTap: () {
                  getIt<CommunicateService>()
                      .navigate(route: "/general-setting");
                  // _openComingSoonPopup(context);
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  void _openComingSoonPopup(BuildContext context) {
    showCenterPopup(
      context: context,
      width: SizeConfig.safeBlockHorizontal * 92,
      height: SizeConfig.safeBlockVertical * 100,
      backgroundColor: Colors.transparent,
      isDismissible: false,
      borderRadius: BorderRadius.circular(20),
      barrierColor: Colors.black.withOpacity(0.9),
      content: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            CustomText(
              "comingSoonPopup.title",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            SizedBox(height: SizeConfig.safeBlockVertical * 3),
            CustomText(
              "comingSoonPopup.content",
              style: TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.safeBlockVertical * 3),
            Image.asset(AssetsPath.fromImagesCommon("coming_soon"),
                width: SizeConfig.safeBlockHorizontal * 92),
            SizedBox(height: SizeConfig.safeBlockVertical * 3),
            GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Container(
                width: SizeConfig.safeBlockHorizontal * 40,
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(
                  vertical: SizeConfig.safeBlockHorizontal * 2,
                ),
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(50),
                  border: Border.all(color: Colors.white),
                ),
                child: CustomText(
                  "comingSoonPopup.button",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      // actions: [
      //   {
      //     "title": "OK",
      //     "action": () => print("OK"),
      //   },
      // ],
    );
  }
}
