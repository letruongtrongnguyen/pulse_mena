import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/core/utils/assets_path.dart';

enum MenuItemType {
  Wallet,
  Store,
  HakiVIP,
  Task,
  Help,
  Settings,
  ActivityCenter,
}

class MenuItemConfig {
  static Map<MenuItemType, Map<String, dynamic>> itemConfig(
      BuildContext context) {
    Color iconColor = Theme.of(context).primaryColor;

    return {
      MenuItemType.Wallet: {
        "text": "common.wallet",
        "icon_path": AssetsPath.fromImagesSvg("wallet_circle_ic"),
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("_circle_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xffFFE296),
            Color(0xffE79617),
          ],
        ),
      },
      MenuItemType.Store: {
        "text": "common.store",
        "icon_path": AssetsPath.fromImagesSvg("store_circle_ic"),
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("store_circle_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xff00FFA0),
            Color(0xff00ECFF),
          ],
        ),
      },
      MenuItemType.HakiVIP: {
        "text": "common.hakiVip",
        "icon_path": AssetsPath.fromImagesSvg("haki_vip_circle_ic"),
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("haki_vip_circle_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xffFFE41A),
            Color(0xffF531A7),
          ],
        ),
      },
      MenuItemType.Task: {
        "text": "common.task",
        "icon_path": AssetsPath.fromImagesSvg("task_circle_ic"),
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("task_circle_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xff96EA0E),
            Color(0xff07CD9D),
          ],
        ),
      },
      MenuItemType.Help: {
        "text": "common.helpCenter",
        "icon_path": AssetsPath.fromImagesSvg("message_circle_ic"),
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("message_circle_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xff81FFCA),
            Color(0xff2CB7FF),
          ],
        ),
      },
      MenuItemType.Settings: {
        "text": "common.generalSetting",
        "icon_path": AssetsPath.fromImagesSvg("gear_circle_ic"),
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("gear_circle_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xffF4FEFF),
            Color(0xffA2B8F0),
          ],
        ),
      },
      MenuItemType.ActivityCenter: {
        "text": "common.activityCenter",
        "icon_path": AssetsPath.fromImagesSvg("speaker_circle_ic"),
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("speaker_circle_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xffFF8E3E),
            Color(0xffF56121),
          ],
        ),
      },
    };
  }
}
