import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

import 'menu_item_config.dart';

class MenuItem extends StatelessWidget {
  final MenuItemType type;
  final bool isComingSoon;
  final Function onTap;

  const MenuItem({
    Key key,
    this.type,
    this.isComingSoon = false,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: Colors.transparent,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(
              alignment: Alignment.topCenter,
              clipBehavior: Clip.none,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 0),
                  child: Container(
                    width: SizeConfig.safeBlockHorizontal * 18,
                    height: SizeConfig.safeBlockHorizontal * 18,
                    padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 4),
                    decoration: BoxDecoration(
                      color: Theme.of(context).cardColor,
                      borderRadius: BorderRadius.circular(18),
                    ),
                    child: ShaderMask(
                      shaderCallback: (bounds) {
                        return MenuItemConfig.itemConfig(context)[type]
                                ["gradient"]
                            .createShader(bounds);
                      },
                      child: SvgPicture.asset(
                          MenuItemConfig.itemConfig(context)[type]
                              ["icon_path"]),
                    ),
                  ),
                ),
                if (isComingSoon)
                  Positioned(
                    top: 0,
                    child: FractionalTranslation(
                      translation: Offset(0, -0.5),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Color(0xffEB9B00),
                          borderRadius: BorderRadius.circular(4),
                        ),
                        width: 50,
                        padding: EdgeInsets.symmetric(
                          horizontal: 5,
                          vertical: 2,
                        ),
                        alignment: Alignment.center,
                        child: CustomText(
                          "common.soon",
                          style: TextStyle(
                            fontSize: 8,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                            shadows: <Shadow>[
                              Shadow(
                                offset: Offset(0, 1),
                                blurRadius: 2.0,
                                color: Colors.black.withOpacity(0.4),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
              ],
            ),
            SizedBox(height: 10),
            CustomText(
              MenuItemConfig.itemConfig(context)[type]["text"],
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 12,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
