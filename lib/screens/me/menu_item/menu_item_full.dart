import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/general.config.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/request/request.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/widgets/basic/circle_ic.widget.dart';
import 'package:flutterbase/widgets/basic/image_placeholder.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

import 'menu_item_config.dart';

class MenuItemFull extends StatelessWidget {
  final MenuItemType type;

  const MenuItemFull({
    Key key,
    this.type,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.safeBlockHorizontal * 92,
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.safeBlockHorizontal * 4,
        vertical: SizeConfig.safeBlockHorizontal * 3,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Theme.of(context).cardTheme.color,
        boxShadow: [
          BoxShadow(
            color: Theme.of(context).cardTheme.shadowColor,
            offset: Offset(0, 1),
            blurRadius: 5,
          )
        ],
      ),
      child: Column(
        children: [
          Row(
            children: [
              ShaderMask(
                shaderCallback: (bounds) {
                  return MenuItemConfig.itemConfig(context)[type]["gradient"]
                      .createShader(bounds);
                },
                child: SvgPicture.asset(
                    MenuItemConfig.itemConfig(context)[type]["icon_path"]),
              ),
              // SvgPicture.asset(MenuItemConfig.itemConfig(context)[type]["icon"]),
              // CircleIconWidget(
              //   icon: MenuItemConfig.itemConfig(context)[type]["icon"],
              //   radius: SizeConfig.safeBlockHorizontal * 4,
              //   padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 1.5),
              //   backgroundGradient: MenuItemConfig.itemConfig(context)[type]
              //       ["gradient"],
              // ),
              SizedBox(width: SizeConfig.safeBlockHorizontal * 2),
              CustomText(
                MenuItemConfig.itemConfig(context)[type]["text"],
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 13,
                ),
              ),
              Spacer(),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).pushNamed("/activity-center-screen");
                },
                child: CustomText(
                  "common.viewAll",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    color: Color(0xff00D1FF),
                    fontSize: 11,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: SizeConfig.safeBlockHorizontal * 2),
          Banner(),
        ],
      ),
    );
  }
}

class Banner extends StatefulWidget {
  @override
  _BannerState createState() => _BannerState();
}

class _BannerState extends State<Banner> {
  final CarouselController _controller = CarouselController();

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: CarouselSlider.builder(
            carouselController: _controller,
            options: CarouselOptions(
                enlargeCenterPage: true,
                enableInfiniteScroll: false,
                pageSnapping: true,
                autoPlay: true,
                viewportFraction: 1.0,
                aspectRatio: 3.5,
                pauseAutoPlayOnManualNavigate: true,
                pauseAutoPlayOnTouch: true,
                onPageChanged: (index, reason) {
                  setState(() {
                    _currentIndex = index;
                  });
                }),
            itemCount: 1,
            itemBuilder: (BuildContext context, int itemIndex, int realIdx) {
              return _sliderItem();
              return Container(
                child: ImagePlaceholderWidget(
                  width: SizeConfig.safeBlockHorizontal * 94,
                  image: AssetImage(
                    AssetsPath.fromImagesActivities("lucky_haki"),
                  ),
                  fit: BoxFit.fitWidth,
                  placeholder: Container(
                    color: Colors.transparent,
                    width: SizeConfig.safeBlockHorizontal * 94,
                  ),
                ),
              );
            },
          ),
        ),
        // _buildIndicator(),
      ],
    );
  }

  Widget _sliderItem() {
    return GestureDetector(
      onTap: () {
        String token = Request.token;
        final me = getIt<MeProvider>().data.value;

        getIt<CommunicateService>().navigate(route: "/webview-screen", args: {
          "url":
              "${getIt<GeneralProvider>().luckyHakiUrl}?token=$token&user_id=${me.id}",
          "title": "Lucky Haki",
          "is-lucky-haki": true,
        });
      },
      child: Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: Stack(
          alignment: Alignment.bottomRight,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: ImagePlaceholderWidget(
                width: SizeConfig.safeBlockHorizontal * 94,
                matchTextDirection: false,
                image: AssetImage(
                  AssetsPath.fromImagesActivities("lucky_haki"),
                ),
                fit: BoxFit.fitWidth,
                placeholder: Container(
                  color: Colors.transparent,
                  width: SizeConfig.safeBlockHorizontal * 94,
                ),
              ),
            ),
            // Positioned(
            //   bottom: 8,
            //   right: 8,
            //   child: SvgPicture.asset(
            //     AssetsPath.fromImagesSvg("arrow_forward_ios_circle_ic"),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }

  Widget _buildIndicator() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List<Widget>.generate(2, (index) {
        return GestureDetector(
          onTap: () => _controller.animateToPage(index),
          child: AnimatedContainer(
            duration: Duration(milliseconds: 200),
            width: _currentIndex == index ? 12 : 6,
            height: 6,
            margin: EdgeInsetsDirectional.fromSTEB(4, 10, 4, 7),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: _currentIndex == index
                  ? Colors.white
                  : Colors.white.withOpacity(0.3),
            ),
          ),
        );
      }),
    );
  }
}
