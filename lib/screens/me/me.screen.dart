import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/providers/search.provider.dart';
import 'package:flutterbase/screens/search/search.screen.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/my_scaffold/my_scaffold.widget.dart';
import 'package:flutterbase/widgets/others/notification_ic.widget.dart';
import 'package:flutterbase/widgets/user/user.widget.dart';

import 'menu_module.dart';

class MeScreen extends StatefulWidget {
  @override
  _MeScreenState createState() => _MeScreenState();
}

class _MeScreenState extends State<MeScreen> {
  ValueListenable _loading;
  ValueListenable _me;

  ValueListenable _isError;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getIt<GeneralProvider>().fetchCustomerServiceUserData();
      // getIt<MeProvider>().fetchUserData();

      _loading = getIt<MeProvider>().loading;
      _me = getIt<MeProvider>().data;
      _isError = getIt<MeProvider>().isError;
    });

    DefaultCacheManager manager = new DefaultCacheManager();
    manager.emptyCache(); //clears all data in cache.
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getIt<CommunicateService>().context = context;

    return MyScaffold(
      appBar: AppBar(
        title: Row(
          children: [
            CustomText(
              "more.title",
              style: Theme.of(context).appBarTheme.textTheme.headline1,
            ),
            Spacer(),
            Hero(
              tag: "searchInputContainer",
              child: Material(
                type: MaterialType.transparency,
                child: Container(
                  color: Colors.black,
                ),
              ),
            )
          ],
        ),
        backgroundColor: Theme.of(context).appBarTheme.color,
        actions: [
          NotificationIconWidget(
            padding: EdgeInsetsDirectional.fromSTEB(
              SizeConfig.safeBlockHorizontal * 2,
              SizeConfig.safeBlockHorizontal * 4.2,
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4.2,
            ),
          ),
        ],
        automaticallyImplyLeading: false,
      ),
      extendBodyBehindAppBar: true,
      backgroundImagePath: AssetsPath.fromImagesCommon("background"),
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      // floatingActionButton: FlatButton(
      //   onPressed: () {
      //     Navigator.of(context).pushNamed("/home-screen");
      //   },
      //   onLongPress: () {
      //     final textDirection = getIt<GeneralProvider>().textDirection.value;
      //     final lang = getIt<GeneralProvider>().lang.value;
      //
      //     if (textDirection == TextDirection.ltr)
      //       getIt<GeneralProvider>().setTextDirection(TextDirection.rtl);
      //     else
      //       getIt<GeneralProvider>().setTextDirection(TextDirection.ltr);
      //
      //     if (lang == "en")
      //       getIt<GeneralProvider>().setLanguage("ar");
      //     else
      //       getIt<GeneralProvider>().setLanguage("en");
      //
      //     // AdaptiveTheme.of(context).toggleThemeMode();
      //   },
      //   child: Container(
      //     padding: EdgeInsets.all(10),
      //     decoration: BoxDecoration(
      //       shape: BoxShape.circle,
      //       color: Colors.blueAccent,
      //     ),
      //     child: Icon(Icons.navigate_next),
      //   ),
      // ),
      child: AnimatedBuilder(
        animation: Listenable.merge([_loading, _me]),
        builder: (ctx, child) {
          // if (_loading.value && _me.value.id == null)
          //   return Center(
          //     child: CircularProgressIndicator(
          //       valueColor:
          //           new AlwaysStoppedAnimation<Color>(Color(0xff00FFE0)),
          //     ),
          //   );
          // if (_isError.value) return RetryModule();
          return ListView(
            padding: EdgeInsetsDirectional.fromSTEB(
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.paddingBottom +
                  kBottomNavigationBarHeight +
                  SizeConfig.safeBlockHorizontal * 4,
            ),
            children: [
              SizedBox(
                height: kToolbarHeight + SizeConfig.paddingTop,
              ),
              UserWidget(),
              SizedBox(height: 30),
              MenuModule(),
            ],
          );
        },
      ),
    );
  }
}
