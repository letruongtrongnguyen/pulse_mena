import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/core/utils/assets_path.dart';

enum MenuItemType {
  Store,
  MyBag,
  History,
  FriendList,
  Level,
  GeneralSetting,
  Leaderboard,
  HelpCenter,
  VipBadges,
  ActivityCenter,
  DailyLogin,
  TaskCenter,
  Feedback,
}

class MenuItemConfig {
  static Map<MenuItemType, Map<String, dynamic>> itemConfig(
      BuildContext context) {
    Color iconColor = Theme.of(context).primaryColor;

    return {
      MenuItemType.Store: {
        "text": "common.store",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("store_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xff00FFA0),
            Color(0xff00ECFF),
          ],
        ),
      },
      MenuItemType.MyBag: {
        "text": "common.myBag",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("bag_ic"),
          color: Colors.white,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xffC609FD),
            Color(0xff9A00FF),
          ],
        ),
      },
      MenuItemType.History: {
        "text": "common.history",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("history_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xff0CF25C),
            Color(0xff0D8358),
          ],
        ),
      },
      MenuItemType.FriendList: {
        "text": "common.friendList",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("friend_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xffFF85CE),
            Color(0xffF60C99),
          ],
        ),
      },
      MenuItemType.Level: {
        "text": "common.level",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("level_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xffFFB45C),
            Color(0xffF628A4),
          ],
        ),
      },
      MenuItemType.GeneralSetting: {
        "text": "common.generalSetting",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("gear_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xffF4FEFF),
            Color(0xffA2B8F0),
          ],
        ),
      },
      MenuItemType.Leaderboard: {
        "text": "common.leaderboard",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("leaderboard_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xffFCED92),
            Color(0xffF9A528),
          ],
        ),
      },
      MenuItemType.HelpCenter: {
        "text": "common.helpCenter",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("message_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xff81FFCA),
            Color(0xff2CB7FF),
          ],
        ),
      },
      MenuItemType.ActivityCenter: {
        "text": "common.activityCenter",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("speaker_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xffFF8E3E),
            Color(0xffF56121),
          ],
        ),
      },
      MenuItemType.DailyLogin: {
        "text": "common.dailyLogin",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("calendar_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xff96EA0E),
            Color(0xff07CD9D),
          ],
        ),
      },
      MenuItemType.TaskCenter: {
        "text": "common.taskCenter",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("task_center_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xff96EA0E),
            Color(0xff07CD9D),
          ],
        ),
      },
      MenuItemType.VipBadges: {
        "text": "common.vipBadges",
        "image": AssetImage(AssetsPath.fromImagesCommon("badge_ic")),
      },
      MenuItemType.Feedback: {
        "text": "common.feedback",
        "icon": SvgPicture.asset(
          AssetsPath.fromImagesSvg("feedback_ic"),
          color: iconColor,
        ),
        "gradient": LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Color(0xff8C0B00),
            Color(0xffFFAD8E),
          ],
        ),
      },
    };
  }
}
