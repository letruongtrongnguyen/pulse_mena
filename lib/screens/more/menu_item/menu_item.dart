import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/widgets/basic/circle_ic.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

import 'menu_item_config.dart';

class MenuItem extends StatelessWidget {
  final MenuItemType type;
  final Widget mainContent;
  final Widget extraContent;
  final Divider divider;
  final Function onTap;

  const MenuItem({
    Key key,
    this.type = MenuItemType.Store,
    this.mainContent,
    this.extraContent,
    this.divider,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap == null ? () {} : onTap,
      child: Container(
        // height: 100,
        width: SizeConfig.safeBlockHorizontal * 44.5,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Theme.of(context).primaryColor,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.15),
              offset: Offset(0,2),
              blurRadius: 3,
            )
          ],
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(
                SizeConfig.safeBlockHorizontal * 4,
                SizeConfig.safeBlockHorizontal * 4,
                SizeConfig.safeBlockHorizontal * 4,
                (extraContent != null) ? 0 : SizeConfig.safeBlockHorizontal * 4,
              ),
              child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (MenuItemConfig.itemConfig(context)[type]["icon"] !=
                          null)
                        CircleIconWidget(
                          icon: MenuItemConfig.itemConfig(context)[type]
                              ["icon"],
                          radius: SizeConfig.safeBlockHorizontal * 4,
                          padding: EdgeInsets.all(
                              SizeConfig.safeBlockHorizontal * 1.5),
                          backgroundGradient:
                              MenuItemConfig.itemConfig(context)[type]
                                  ["gradient"],
                        )
                      else
                        Image(
                          image: MenuItemConfig.itemConfig(context)[type]
                              ["image"],
                          height: SizeConfig.safeBlockHorizontal * 8,
                          fit: BoxFit.fitHeight,
                        ),
                      SizedBox(height: 6),
                      CustomText(
                        MenuItemConfig.itemConfig(context)[type]["text"],
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 13,
                        ),
                      ),
                    ],
                  ),
                  if (mainContent != null) Spacer(),
                  if (mainContent != null) mainContent,
                ],
              ),
            ),
            if (divider != null && extraContent != null) divider,
            if (divider == null && extraContent != null) SizedBox(height: 6),
            if (extraContent != null)
              Padding(
                padding: EdgeInsets.fromLTRB(
                  SizeConfig.safeBlockHorizontal * 4,
                  0,
                  SizeConfig.safeBlockHorizontal * 4,
                  SizeConfig.safeBlockHorizontal * 4,
                ),
                child: extraContent,
              ),
          ],
        ),
      ),
    );
  }
}
