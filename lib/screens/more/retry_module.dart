import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/widgets/basic/image_placeholder.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class RetryModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            alignment: Alignment.center,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                ImagePlaceholderWidget(
                  height: SizeConfig.safeBlockHorizontal * 15,
                  image: AssetImage(
                    AssetsPath.fromImagesCommon("network_error_ic"),
                  ),
                  fit: BoxFit.fitHeight,
                  placeholder: Container(
                    color: Colors.black,
                    height: SizeConfig.safeBlockHorizontal * 15,
                  ),
                ),
                SizedBox(height: 15),
                ShowUp(
                  child: CustomText(
                    "retry.title",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  delay: 250,
                ),
                SizedBox(height: 10),
                ShowUp(
                  child: CustomText(
                    "retry.subtitle",
                    style: TextStyle(
                      fontSize: 13,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  delay: 300,
                ),
              ],
            ),
          ),
          Positioned(
            bottom: SizeConfig.paddingBottom + SizeConfig.safeBlockHorizontal * 5,
            child: Ink(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(60),
                color: Color(0xffF5F5F5),
              ),
              child: InkWell(
                onTap: () {
                  // getIt<MeProvider>().fetchUserData();
                },
                borderRadius: BorderRadius.circular(60),
                child: Container(
                  alignment: Alignment.center,
                  width: SizeConfig.safeBlockHorizontal * 60,
                  height: SizeConfig.safeBlockHorizontal * 12,
                  child: CustomText(
                    "retry.btn.title",
                    uppercase: true,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 13,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ShowUp extends StatefulWidget {
  final Widget child;
  final int delay;

  ShowUp({@required this.child, this.delay});

  @override
  _ShowUpState createState() => _ShowUpState();
}

class _ShowUpState extends State<ShowUp> with TickerProviderStateMixin {
  AnimationController _animController;
  Animation<Offset> _animOffset;

  @override
  void initState() {
    super.initState();

    _animController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    final curve =
        CurvedAnimation(curve: Curves.decelerate, parent: _animController);
    _animOffset =
        Tween<Offset>(begin: const Offset(0.0, 0.35), end: Offset.zero)
            .animate(curve);

    if (widget.delay == null) {
      _animController.forward();
    } else {
      Timer(Duration(milliseconds: widget.delay), () {
        _animController.forward();
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    _animController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      child: SlideTransition(
        position: _animOffset,
        child: widget.child,
      ),
      opacity: _animController,
    );
  }
}
