import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/general.config.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/request/request.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/providers/relationship.provider.dart';
import 'package:flutterbase/screens/more/menu_item/menu_item_config.dart';
import 'package:flutterbase/screens/more/menu_item/menu_item_full.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/basic/popup/center_popup.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/coin/coin.widget.dart';
import 'package:flutterbase/widgets/gem/gem.widget.dart';

import 'menu_item/menu_item.dart';

class MenuModule extends StatefulWidget {
  @override
  _MenuModuleState createState() => _MenuModuleState();
}

class _MenuModuleState extends State<MenuModule> {
  @override
  void didChangeDependencies() {
    precacheImage(
        Image.asset(
          AssetsPath.fromImagesCommon("coming_soon"),
        ).image,
        context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MenuItemFull(
          type: MenuItemType.ActivityCenter,
        ),
        SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // First column
            Column(
              children: [
                MenuItem(
                  type: MenuItemType.Store,
                  onTap: () {
                    getIt<CommunicateService>().navigate(route: "/store");
                  },
                  mainContent: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      CoinWidget(),
                      SizedBox(height: SizeConfig.safeBlockHorizontal * 2),
                      GemWidget(),
                    ],
                  ),
                  divider: Divider(
                    color: Colors.white.withOpacity(0.5),
                    height: SizeConfig.safeBlockHorizontal * 8,
                  ),
                  extraContent: storeExtraContent(),
                ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
                MenuItem(
                  type: MenuItemType.DailyLogin,
                  onTap: () {
                    // Navigator.of(context).pushNamed("/daily-login-screen");
                    _openComingSoonPopup(context);
                  },
                ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
                MenuItem(
                  type: MenuItemType.TaskCenter,
                  onTap: () {
                    // Navigator.of(context).pushNamed("/task-center-screen");
                    _openComingSoonPopup(context);
                  },
                ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
                MenuItem(
                  type: MenuItemType.Level,
                  onTap: () {
                    _openComingSoonPopup(context);

                    // Navigator.of(context).pushNamed("/level-screen");
                  },
                  // mainContent: ValueListenableBuilder(
                  //   valueListenable: getIt<MeProvider>().data,
                  //   builder: (context, me, child) {
                  //     return CustomText(
                  //       "${me.level.value}",
                  //       style: TextStyle(
                  //         fontWeight: FontWeight.bold,
                  //         fontSize: 16,
                  //       ),
                  //     );
                  //   },
                  // ),
                ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
                MenuItem(
                  type: MenuItemType.HelpCenter,
                  onTap: () {
                    Navigator.of(context).pushNamed("/help-center-screen");
                  },
                ),
              ],
            ),

            // Second column
            SizedBox(width: SizeConfig.safeBlockHorizontal * 3),
            Column(
              children: [
                MenuItem(
                  type: MenuItemType.Leaderboard,
                  onTap: () {
                    Navigator.of(context).pushNamed("/leaderboard-screen");
                    // _openComingSoonPopup(context);
                  },
                ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
                MenuItem(
                  type: MenuItemType.VipBadges,
                  onTap: () {
                    String token = Request.token;
                    final int myUserId = getIt<GeneralConfig>().myUserId;

                    getIt<CommunicateService>()
                        .navigate(route: "/webview-screen", args: {
                      "url":
                          "${getIt<GeneralProvider>().luckyHakiUrl}?token=$token&user_id=$myUserId",
                      "title": "Lucky Haki",
                      "is-lucky-haki": true,
                    });
                    // _openComingSoonPopup(context);
                  },
                ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
                MenuItem(
                  type: MenuItemType.FriendList,
                  onTap: () {
                    // getIt<CommunicateService>().navigate(route: "/friend-list");
                    _openComingSoonPopup(context);
                  },
                  // mainContent: friendListMainContent(),
                  // extraContent: friendListExtraContent(),
                ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
                MenuItem(
                  type: MenuItemType.MyBag,
                  onTap: () {
                    getIt<CommunicateService>().navigate(route: "/my-bag");
                  },
                ),
                // SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
                // MenuItem(
                //   type: MenuItemType.History,
                //   mainContent: historyMainContent(),
                // ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
                MenuItem(
                  type: MenuItemType.GeneralSetting,
                  onTap: () {
                    getIt<CommunicateService>()
                        .navigate(route: "/general-setting");
                    // _openComingSoonPopup(context);
                  },
                ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
                MenuItem(
                  type: MenuItemType.Feedback,
                  onTap: () {
                    Navigator.of(context).pushNamed("/feedback-screen");
                    // _openComingSoonPopup(context);
                  },
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }

  Widget friendListMainContent() {
    ValueNotifier data = getIt<RelationshipProvider>().data;
    ValueNotifier loading = getIt<RelationshipProvider>().loading;

    return AnimatedBuilder(
        animation: Listenable.merge([data, loading]),
        builder: (context, _) {
          if (loading.value) return SizedBox();
          return Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              CustomText(
                "${Formatter.formatDecimalNumber(data.value.friends.length)}",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              CustomText(
                "more.friend.currency",
                style: TextStyle(
                  fontSize: 11,
                ),
              ),
            ],
          );
        });
  }

  Widget friendListExtraContent() {
    ValueNotifier data = getIt<RelationshipProvider>().data;
    ValueNotifier loading = getIt<RelationshipProvider>().loading;

    TextStyle _style = TextStyle(
      color: Color(0xff7F7F7F),
      fontSize: 11,
    );
    return AnimatedBuilder(
        animation: Listenable.merge([data, loading]),
        builder: (context, _) {
          if (loading.value || data.value.requests.length <= 0)
            return SizedBox();
          return Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                  color: Color(0xffFF0000),
                  shape: BoxShape.circle,
                ),
              ),
              SizedBox(width: 5),
              CustomText(
                "${data.value.requests.length}",
                style: _style,
              ),
              SizedBox(width: 2),
              CustomText(
                "more.request.currency",
                style: _style,
              ),
            ],
          );
        });
  }

  Widget storeExtraContent() {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            getIt<CommunicateService>().navigate(route: "/store", args: {
              "selected": "coins",
            });
          },
          child: Container(
            width: double.infinity,
            padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 2.5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color(0xffED3CBB),
                    Color(0xffF58B74),
                    Color(0xffFFE41A),
                  ]),
            ),
            child: Row(
              children: [
                CustomText(
                  "common.topupStore",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                  ),
                ),
                Spacer(),
                SvgPicture.asset(
                    AssetsPath.fromImagesSvg("arrow_forward_ios_circle_ic"))
              ],
            ),
          ),
        ),
        SizedBox(height: SizeConfig.safeBlockHorizontal * 2),
        GestureDetector(
          onTap: () {
            getIt<CommunicateService>().navigate(route: "/store", args: {
              "selected": "items",
            });
          },
          child: Container(
            width: double.infinity,
            padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 2.5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color(0xff196B99),
                    Color(0xff5C48AC),
                    Color(0xff071C3C),
                  ]),
            ),
            child: Row(
              children: [
                CustomText(
                  "common.itemStore",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                  ),
                ),
                Spacer(),
                SvgPicture.asset(
                    AssetsPath.fromImagesSvg("arrow_forward_ios_circle_ic"))
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget historyMainContent() {
    return Column(
      children: [
        AvatarWidget(
          avatarUrl:
              "https://s3-alpha-sig.figma.com/img/9edb/27c0/39dfc14cf6e247bdbc1958c7c71a00de?Expires=1602460800&Signature=G58f1airMLWHgPGVqKIIwFRKZC-l1O0YY2ZDd7PU23sPOB6eOkyZSw89Ou7x74KDSeLpMXJP3Kd4kWifYxcklyJnHht7Gh~6XtWRdKwZpNeLGt31IpUVFOVY4i9OUTNxTyCaQCMCaszUsvWYWmgmdO2YSYXKAYO7CU04hei5-yqGGzUJXVmF37IppzucIxHluOkjwRHmEitcRHKw3SJzU9HD5fKBTOID2t1v0y1rTg7-beCWIzosUwb-NS3GXd2Ed-HvzB02NAcom6YlTL~3ENwu2ncXJnonRk0~S7uNZx2p3NYekIAW5R05chyqif8N5yLLu9QEYvplAqRNeQ7ScA__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
          borderColor: Colors.white,
          borderWidth: 1.5,
          radius: SizeConfig.safeBlockHorizontal * 5,
          isOnline: true,
        ),
        SizedBox(height: 4),
        CustomText(
          "Lind***",
          style: TextStyle(
            fontSize: 12,
          ),
        ),
      ],
    );
  }

  void _openComingSoonPopup(BuildContext context) {
    showCenterPopup(
      context: context,
      width: SizeConfig.safeBlockHorizontal * 92,
      height: SizeConfig.safeBlockVertical * 100,
      backgroundColor: Colors.transparent,
      isDismissible: false,
      borderRadius: BorderRadius.circular(20),
      barrierColor: Colors.black.withOpacity(0.9),
      content: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            CustomText(
              "comingSoonPopup.title",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            SizedBox(height: SizeConfig.safeBlockVertical * 3),
            CustomText(
              "comingSoonPopup.content",
              style: TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.safeBlockVertical * 3),
            Image.asset(AssetsPath.fromImagesCommon("coming_soon"),
                width: SizeConfig.safeBlockHorizontal * 92),
            SizedBox(height: SizeConfig.safeBlockVertical * 3),
            GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Container(
                width: SizeConfig.safeBlockHorizontal * 40,
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(
                  vertical: SizeConfig.safeBlockHorizontal * 2,
                ),
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(50),
                  border: Border.all(color: Colors.white),
                ),
                child: CustomText(
                  "comingSoonPopup.button",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      // actions: [
      //   {
      //     "title": "OK",
      //     "action": () => print("OK"),
      //   },
      // ],
    );
  }
}

// class HotAudioRoom extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: [
//         Stack(
//           children: [
//             CachedNetworkImage(
//               width: SizeConfig.safeBlockHorizontal * 44.5,
//               height: SizeConfig.safeBlockHorizontal * 70,
//               imageUrl:
//                   "https://s3-alpha-sig.figma.com/img/64fa/7bee/5359efba3ed72603101df1831b9c0bd0?Expires=1603065600&Signature=TJyjt2~NVO0awI7wzzt1KY1w1a5HsyUQ0LHKxjci7bp1xAtwC2KhtYdgzIfrfHC2dThixU7bjR6cqATZbgjVtYki8OkvHQXjNovYb2Dj~O-ymIXid~afOjOobD6q-N3VAIkuT7Tnc~Zy3hPQVwf6vrDcI~4fWgtgXxeCA74TOep4zj0-DtdeXztjomtrQZ09GrRZcWiJ2iXjpEUFpQx6w4Pce5U71sONB-OtS-nsJ6dzsRNKEhv~Dl5LX7n74sVF~NAX6GWQQJP2VJ~PMJ~MagqDsg9yxv8LdbghwUqeeZHqAWI7l1XU~Im4T3R70x6h-OJmrkemYK38p-CiVx44HA__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
//               imageBuilder: (context, imageProvider) => Container(
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.only(
//                     topLeft: Radius.circular(8),
//                     topRight: Radius.circular(8),
//                   ),
//                   image: DecorationImage(
//                     image: imageProvider,
//                     fit: BoxFit.cover,
//                   ),
//                 ),
//               ),
//               placeholder: (context, url) => Container(
//                 decoration: BoxDecoration(
//                   color: Colors.white.withOpacity(0.1),
//                   borderRadius: BorderRadius.only(
//                     topLeft: Radius.circular(8),
//                     topRight: Radius.circular(8),
//                   ),
//                 ),
//               ),
//             ),
//             Positioned(
//               top: SizeConfig.safeBlockHorizontal * 2,
//               left: SizeConfig.safeBlockHorizontal * 2,
//               child: CustomText(
//                 "Black Swan",
//                 style: TextStyle(
//                   fontSize: 16,
//                   fontWeight: FontWeight.bold,
//                 ),
//               ),
//             ),
//           ],
//         ),
//         Container(
//           width: SizeConfig.safeBlockHorizontal * 44.5,
//           padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 3),
//           decoration: BoxDecoration(
//             color: Colors.white.withOpacity(0.1),
//             borderRadius: BorderRadius.only(
//               bottomLeft: Radius.circular(8),
//               bottomRight: Radius.circular(8),
//             ),
//           ),
//           child: Row(
//             children: [
//               Image.asset(AssetsPath.fromImagesCommon("fire_ic")),
//               SizedBox(width: SizeConfig.safeBlockHorizontal * 1),
//               CustomText(
//                 "more.hotAudioRoom",
//                 style: TextStyle(
//                   fontWeight: FontWeight.bold,
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ],
//     );
//   }
// }
