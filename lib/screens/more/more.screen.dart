import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/providers/relationship.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/user/user.widget.dart';
import 'package:lottie/lottie.dart';

import 'menu_module.dart';
import 'retry_module.dart';

class MoreScreen extends StatefulWidget {
  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  ValueListenable _loading;
  ValueListenable _me;

  ValueListenable _isError;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getIt<RelationshipProvider>().fetchRelationshipData();
      // getIt<MeProvider>().fetchUserData();

      _loading = getIt<MeProvider>().loading;
      _me = getIt<MeProvider>().data;
      _isError = getIt<MeProvider>().isError;
    });

    DefaultCacheManager manager = new DefaultCacheManager();
    manager.emptyCache(); //clears all data in cache.
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getIt<CommunicateService>().context = context;

    // ValueListenable _loading = getIt<MeProvider>().loading;
    // ValueListenable _me = getIt<MeProvider>().data;
    // ValueListenable _isError = getIt<MeProvider>().isError;

    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            CustomText("more.title",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                )),
            Spacer(),
          ],
        ),
        automaticallyImplyLeading: false,
      ),
      body: AnimatedBuilder(
        animation: Listenable.merge([_loading, _me]),
        builder: (ctx, child) {
          // if (_loading.value && _me.value.id == null)
          //   return Center(
          //     child: CircularProgressIndicator(
          //       valueColor:
          //           new AlwaysStoppedAnimation<Color>(Color(0xff00FFE0)),
          //     ),
          //   );
          // if (_isError.value) return RetryModule();
          return ListView(
            padding: EdgeInsets.fromLTRB(
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.paddingBottom +
                  kBottomNavigationBarHeight +
                  SizeConfig.safeBlockHorizontal * 4,
            ),
            children: [
              // Lottie.asset(
              //   AssetsPath.fromImagesJson("sound_animation"),
              // ),
              UserWidget(),
              Divider(
                color: Color(0xff424242),
                thickness: 0.5,
                height: 30,
              ),
              // LeaderboardModule(),
              // SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
              MenuModule(),
            ],
          );
        },
      ),
      floatingActionButton: FlatButton(
        onPressed: () {
          Navigator.of(context).pushNamed("/home-screen");
        },
        // onLongPress: () {
        //   AdaptiveTheme.of(context).toggleThemeMode();
        // },
        child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.blueAccent,
          ),
          child: Icon(Icons.navigate_next),
        ),
      ),
    );
  }
}
