import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/screens/more/leaderboard_module/leaderboard_module_config.dart';
import 'package:flutterbase/widgets/basic/circle_ic.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class LeaderboardModule extends StatefulWidget {
  @override
  _LeaderboardModuleState createState() => _LeaderboardModuleState();
}

class _LeaderboardModuleState extends State<LeaderboardModule> {
  final CarouselController _controller = CarouselController();
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _title(),
        Container(
          child: CarouselSlider.builder(
            carouselController: _controller,
            options: CarouselOptions(
                enlargeCenterPage: true,
                enableInfiniteScroll: true,
                pageSnapping: true,
                autoPlay: true,
                viewportFraction: 1.0,
                aspectRatio: 2.0,
                pauseAutoPlayOnManualNavigate: true,
                pauseAutoPlayOnTouch: true,
                onPageChanged: (index, reason) {
                  setState(() {
                    _currentIndex = index;
                  });
                }),
            itemCount: 6,
            itemBuilder: (BuildContext context, int itemIndex, int realIdx) {
              return _buildSliderItem(
                  type: LeaderboarModuleItemType.values[itemIndex]);
            },
          ),
        ),
        _buildIndicator(),
      ],
    );
  }

  Widget _title() {
    return Padding(
      padding: EdgeInsetsDirectional.fromSTEB(
        SizeConfig.safeBlockHorizontal * 4,
        0,
        SizeConfig.safeBlockHorizontal * 4,
        20,
      ),
      child: Row(
        children: [
          CircleIconWidget(
            icon: SvgPicture.asset(
              AssetsPath.fromImagesSvg("leaderboard_ic"),
            ),
            radius: SizeConfig.safeBlockHorizontal * 4,
            backgroundGradient: LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              colors: [Color(0xffF9A528), Color(0xffFCED92)],
            ),
          ),
          SizedBox(width: 10),
          CustomText(
            "common.leaderboard",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
          Spacer(),
          SvgPicture.asset(
            AssetsPath.fromImagesSvg("arrow_forward_ios_ic"),
            height: SizeConfig.safeBlockHorizontal * 4,
          ),
        ],
      ),
    );
  }

  Widget _buildSliderItem({LeaderboarModuleItemType type}) {
    return Container(
      width: SizeConfig.safeBlockHorizontal * 92,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        gradient: LeaderboardModuleConfig.itemConfig[type]["gradient"],
      ),
      child: Column(
        children: [
          SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
          CustomText(
            LeaderboardModuleConfig.itemConfig[type]["title"],
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Spacer(),
          Image(
            height: SizeConfig.safeBlockHorizontal * 25,
            fit: BoxFit.fitHeight,
            image: AssetImage(
              AssetsPath.fromImagesCommon("leaderboard_stand"),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildIndicator() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List<Widget>.generate(LeaderboardModuleConfig.itemConfig.length,
          (index) {
        return GestureDetector(
          onTap: () => _controller.animateToPage(index),
          child: AnimatedContainer(
            duration: Duration(milliseconds: 200),
            width: _currentIndex == index ? 16 : 8,
            height: 8,
            margin: EdgeInsetsDirectional.fromSTEB(4, 10, 4, 7),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: _currentIndex == index
                  ? Colors.white
                  : Colors.white.withOpacity(0.3),
            ),
          ),
        );
      }),
    );
  }
}
