import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/core/utils/assets_path.dart';

enum LeaderboarModuleItemType {
  TopRoomLevel,
  TopRoomGift,
  TopBuyer,
  TopHotIdol,
  TopGiftSender,
  TopGiftReceiver,
}

class LeaderboardModuleConfig {
  static Map<LeaderboarModuleItemType, Map<String, dynamic>> itemConfig = {
    LeaderboarModuleItemType.TopRoomLevel: {
      "title": "Top Room Gifts this week",
      "icon": SvgPicture.asset(
        AssetsPath.fromImagesSvg("store_ic"),
        color: Colors.black,
      ),
      "gradient": LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          Color(0xffF2DC46),
          Color(0xffF73427),
        ],
      ),
    },
    LeaderboarModuleItemType.TopRoomGift: {
      "title": "Top Room Gifts this week",
      "icon": SvgPicture.asset(
        AssetsPath.fromImagesSvg("store_ic"),
        color: Colors.black,
      ),
      "gradient": LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          Color(0xffF5C5A3),
          Color(0xffF52165),
        ],
      ),
    },
    LeaderboarModuleItemType.TopBuyer: {
      "title": "Top Wealthy this week",
      "icon": SvgPicture.asset(
        AssetsPath.fromImagesSvg("store_ic"),
        color: Colors.black,
      ),
      "gradient": LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          Color(0xffCB47F6),
          Color(0xff7505E7),
        ],
      ),
    },
    LeaderboarModuleItemType.TopHotIdol: {
      "title": "Top Hot Idol this week",
      "icon": SvgPicture.asset(
        AssetsPath.fromImagesSvg("store_ic"),
        color: Colors.black,
      ),
      "gradient": LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          Color(0xff22D5F6),
          Color(0xff5175F5),
        ],
      ),
    },
    LeaderboarModuleItemType.TopGiftSender: {
      "title": "Top Gifts Sender this week",
      "icon": SvgPicture.asset(
        AssetsPath.fromImagesSvg("store_ic"),
        color: Colors.black,
      ),
      "gradient": LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          Color(0xffD6F22B),
          Color(0xff0DCCF5),
        ],
      ),
    },
    LeaderboarModuleItemType.TopGiftReceiver: {
      "title": "Top Gifts Receiver this week",
      "icon": SvgPicture.asset(
        AssetsPath.fromImagesSvg("store_ic"),
        color: Colors.black,
      ),
      "gradient": LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          Color(0xffF00C69),
          Color(0xff7F28ED),
        ],
      ),
    },
  };
}
