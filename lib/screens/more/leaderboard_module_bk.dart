import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/basic/circle_ic.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

enum LeaderboarBkItemType { HotIdol, HotBuyer }

class LeaderboardBkModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.1),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: [
          _title(),
          _content(),
        ],
      ),
    );
  }

  Widget _title() {
    return Padding(
      padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 4),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CircleIconWidget(
                icon: SvgPicture.asset(
                  AssetsPath.fromImagesSvg("leaderboard_ic"),
                ),
                radius: SizeConfig.safeBlockHorizontal * 5,
                backgroundGradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [Color(0xffF9A528), Color(0xffFCED92)],
                ),
              ),
              SizedBox(height: 10),
              CustomText(
                "common.leaderboard",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),

              ),
            ],
          ),
          Spacer(),
          CustomText(
            "common.weekly",
            style: TextStyle(
              fontWeight: FontWeight.w300,
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }

  Widget _content() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.1),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(8),
          bottomRight: Radius.circular(8),
        ),
      ),
      width: double.infinity,
      child: Column(
        children: [
          _leaderboardItem(
            name: "Mehazabien",
            avatarUrl:
                "https://s3-alpha-sig.figma.com/img/11a1/cb80/b9bb5e186b6026ee4292908f2dacdda1?Expires=1602460800&Signature=BsniBGnjwiUyAKO6gDQvUibj~~eI6pZf3PpY7TlEMmA-3qxpuuLGnY1MfRuQFlIj0rl-xKuJVj6QqpbYSF9iujFVxp4MMZjol3FWUerH-3KfmQ2X3LGkW8VwBJQ6oOsfRw5cSdGo8z6qjs7G-T-F3eEZMStSv4i9NtVkKKxEqnS0fG0Pb-~RCSOo2qO4kQPzk--mZ1tJxnHP0HujiPRU~Y7V4p9hc3nWehHC8zH6k9NLBs6mI9fmsZhT03UYgesg9ikoQ2rhThwN~gKe5d~2d-hNyvcid~ZPlgm3SKREvy8B57ATR8I2RkY2wEUuHO1LEwQUW6Rfx1Sbcw5NtJjJ-Q__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
            type: LeaderboarBkItemType.HotIdol,
            value: 842302,
          ),
          _leaderboardItem(
            name: "Ricky Star",
            avatarUrl:
                "https://s3-alpha-sig.figma.com/img/360f/2f0a/ea39d1314dd0498a2a6fc082a24ca083?Expires=1602460800&Signature=TzNXQYaLnJATy5L~TdNOmwMumi6eN9fVwf5zQooWpDYAljAacLNeooITDU8C09ofyX9UrRxSQWajrdw-~qzxNLJrz-xz3VAo7av9yTOXkvrOQN6RQnZD8fDhUIU28aP8c1eXjqBopgHu2pKCx8HtsENFx783z7JduNNl3zcgJJhcunMai6RYgP6fr29qZIj47vRrSOQt77-UzX73JdDyJ7qO1y00URW7zztM19JZSRZ8RP0t3z0JF6I9phnUfh1sYD4IcSPRsyQQT1pE90XKD8gR9l-R0q3TfhmyQgdvMVGigsU7U~n7UdXST-~VXIfmYkhm787V0ZY1NPrQ6lYDmw__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
            type: LeaderboarBkItemType.HotBuyer,
            value: 1242302,
          ),
        ],
      ),
    );
  }

  Widget _leaderboardItem({
    String name,
    String avatarUrl,
    LeaderboarBkItemType type,
    int value,
  }) {
    String _title;
    Widget _icon;

    switch (type) {
      case LeaderboarBkItemType.HotIdol:
        _title = "more.leaderboardModule.hotIdol";
        _icon = Image.asset(
          AssetsPath.fromImagesCommon("star_idol_ic"),
          width: SizeConfig.safeBlockHorizontal * 4.5,
          fit: BoxFit.fitWidth,
        );
        break;
      case LeaderboarBkItemType.HotBuyer:
        _title = "more.leaderboardModule.hotBuyer";
        _icon = Image.asset(
          AssetsPath.fromImagesCommon("coin_ic"),
          width: SizeConfig.safeBlockHorizontal * 4.5,
          fit: BoxFit.fitWidth,
        );
        break;
      default:
        _icon = Image.asset(
          AssetsPath.fromImagesCommon("star_idol_ic"),
          width: SizeConfig.safeBlockHorizontal * 4.5,
          fit: BoxFit.fitWidth,
        );
        _title = "";
    }

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.safeBlockHorizontal * 4,
        vertical: 13,
      ),
      child: Row(
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              SvgPicture.asset(
                AssetsPath.fromImagesSvg("first_prize_tag"),
                width: SizeConfig.safeBlockHorizontal * 7,
              ),
              Positioned(
                top: 2,
                child: Transform.translate(
                  offset: Offset(-1, 0),
                  child: CustomText(
                    "1",
                    style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.bold,
                      shadows: <Shadow>[
                        Shadow(
                          offset: Offset(0, 1),
                          blurRadius: 3.0,
                          color: Colors.black.withOpacity(0.25),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(width: SizeConfig.safeBlockHorizontal * 3),
          AvatarWidget(
            avatarUrl: avatarUrl,
            borderWidth: 1,
            radius: SizeConfig.safeBlockHorizontal * 6,
          ),
          SizedBox(width: SizeConfig.safeBlockHorizontal * 3),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                name,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                ),
              ),
              SizedBox(height: 5),
              Row(
                children: [
                  _icon,
                  SizedBox(width: SizeConfig.safeBlockHorizontal * 2),
                  CustomText("${Formatter.formatDecimalNumber(value)}"),
                ],
              ),
            ],
          ),
          Spacer(),
          CustomText(
            _title,
            style: TextStyle(
              color: Color(0xffF8EE0E),
              fontWeight: FontWeight.bold,
              fontSize: 13,
            ),
          ),
        ],
      ),
    );
  }
}
