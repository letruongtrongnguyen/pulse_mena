import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/country.model.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/widgets/basic/circle_ic.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/country/country.widget.dart';
import 'package:flutterbase/widgets/session_title/session_title.dart';

class CountryFilterModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<String> countryFilters = [
      'sa',
      'us',
      'qa',
      'ae',
      'kw',
      'om',
    ];
    return Container(
      width: SizeConfig.safeBlockHorizontal * 92,
      padding: EdgeInsets.only(
        bottom: 30,
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 12),
            child: SessionTitle(
              title: "discover.menuTitle.countries",
              onTap: () {
                Navigator.of(context).pushNamed("/countries-full-list-screen");
              },
              suffix: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CustomText("common.more"),
                  SizedBox(width: 10),
                  SvgPicture.asset(
                    AssetsPath.fromImagesSvg("arrow_forward_ios_ic"),
                    color: Theme.of(context).iconTheme.color,
                    height: SizeConfig.safeBlockHorizontal * 4,
                    matchTextDirection: true,
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: SizeConfig.safeBlockHorizontal * 92,
            height: 100,
            child: GridView.builder(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              itemCount: countryFilters.length,
              physics: NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                childAspectRatio: 2.8,
                mainAxisSpacing: SizeConfig.safeBlockHorizontal * 2.5,
                crossAxisSpacing: SizeConfig.safeBlockHorizontal * 2.5,
              ),
              itemBuilder: (context, index) {
                return CountryFilterItem(countryCode: countryFilters[index]);
              },
            ),
          ),
        ],
      ),
    );
  }
}

class CountryFilterItem extends StatelessWidget {
  final String countryCode;

  const CountryFilterItem({
    Key key,
    @required this.countryCode,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ValueListenable _countryFilter = getIt<RoomProvider>().countryFilter;

    // Here it is!
    Size _textSize(String text, TextStyle style) {
      final TextPainter textPainter = TextPainter(
        text: TextSpan(
          text: text,
          style: style,
        ),
        maxLines: 1,
        textScaleFactor: MediaQuery.of(context).textScaleFactor,
        textDirection: getIt<GeneralProvider>().textDirection.value,
      )..layout(minWidth: 0, maxWidth: double.infinity);
      return textPainter.size;
    }

    final Size txtSize = _textSize(
      Country(code: countryCode).name,
      TextStyle(
        fontSize: 12 * SizeConfig.screenWidth / 375,
      ),
    );

    return GestureDetector(
      onTap: () {
        // if (_countryFilter.value == countryCode)
        //   getIt<RoomProvider>().setCountryFilter("");
        // else
        //   getIt<RoomProvider>().setCountryFilter(countryCode);

        Navigator.pushNamed(
          context,
          "/country-filter-screen",
          arguments: {
            "title": txtSize.width < SizeConfig.safeBlockHorizontal * 70
                ? Country(code: countryCode).name
                : Country(code: countryCode).abbreviation,
            "country_code": countryCode,
          },
        );
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Theme.of(context).cardTheme.color,
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).cardTheme.shadowColor,
              offset: Offset(0, 1),
              blurRadius: 5,
            )
          ],
        ),
        padding: EdgeInsets.symmetric(
          vertical: SizeConfig.safeBlockHorizontal * 2.5,
          horizontal: SizeConfig.safeBlockHorizontal * 3,
        ),
        child: Row(
          children: [
            CountryWidget(
              code: countryCode,
              showCountryName: false,
              widthFlag: SizeConfig.safeBlockHorizontal * 7,
            ),
            Spacer(),
            Container(
              width: SizeConfig.safeBlockHorizontal * 14,
              alignment: AlignmentDirectional.centerEnd,
              child: CustomText(
                txtSize.width < SizeConfig.safeBlockHorizontal * 13
                    ? Country(code: countryCode).name
                    : Country(code: countryCode).abbreviation,
                style: TextStyle(
                  fontSize: 12,
                ),
                maxLines: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
