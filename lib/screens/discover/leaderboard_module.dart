import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/leaderboard.item.model.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/widgets/basic/image_placeholder.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_config.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_top_item.dart';
import 'package:flutterbase/widgets/session_title/session_title.dart';
import 'package:shimmer/shimmer.dart';

class LeaderboardModule extends StatefulWidget {
  @override
  _LeaderboardModuleState createState() => _LeaderboardModuleState();
}

class _LeaderboardModuleState extends State<LeaderboardModule> {
  final CarouselController _controller = CarouselController();

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    ValueNotifier loading =
        getIt<LeaderboardProvider>().leaderboardTopThreeDataLoading;
    ValueNotifier leaderboardTopThreeData =
        getIt<LeaderboardProvider>().leaderboardTopThreeData;

    return Container(
      width: SizeConfig.safeBlockHorizontal * 92,
      padding: EdgeInsets.only(
        bottom: 30,
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 12),
            child: SessionTitle(
              title: "common.leaderboard",
              onTap: () {
                Navigator.of(context).pushNamed("/leaderboard-screen");
              },
              suffix: CustomText("common.monthly"),
            ),
          ),
          AnimatedBuilder(
            animation: Listenable.merge([loading, leaderboardTopThreeData]),
            builder: (context, child) {
              if (loading.value && leaderboardTopThreeData.value.length == 0)
                return CarouselSlider.builder(
                  carouselController: _controller,
                  options: CarouselOptions(
                      enlargeCenterPage: true,
                      enableInfiniteScroll: true,
                      pageSnapping: true,
                      autoPlay: false,
                      viewportFraction: 1.0,
                      aspectRatio: 1.5,
                      pauseAutoPlayOnManualNavigate: true,
                      pauseAutoPlayOnTouch: true,
                      onPageChanged: (index, reason) {
                        setState(() {
                          _currentIndex = index;
                        });
                      }),
                  itemCount: 1,
                  itemBuilder:
                      (BuildContext context, int itemIndex, int realIdx) {
                    return Shimmer.fromColors(
                      baseColor: Theme.of(context).accentColor.withOpacity(0.2),
                      highlightColor:
                          Theme.of(context).accentColor.withOpacity(0.1),
                      child: Container(
                        width: 500,
                        height: 500,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                    );
                  },
                );

              return CarouselSlider.builder(
                carouselController: _controller,
                options: CarouselOptions(
                    enlargeCenterPage: true,
                    enableInfiniteScroll: true,
                    pageSnapping: true,
                    autoPlay: true,
                    viewportFraction: 1.0,
                    aspectRatio: 1.5,
                    pauseAutoPlayOnManualNavigate: true,
                    pauseAutoPlayOnTouch: true,
                    onPageChanged: (index, reason) {
                      setState(() {
                        _currentIndex = index;
                      });
                    }),
                itemCount: LeaderboardType.values.length,
                itemBuilder:
                    (BuildContext context, int itemIndex, int realIdx) {
                  return _buildSliderItem(
                    context,
                    type: LeaderboardType.values[itemIndex],
                    data: leaderboardTopThreeData
                        .value[LeaderboardType.values[itemIndex]],
                    isLoading: loading.value,
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildSliderItem(
    BuildContext context, {
    LeaderboardType type,
    List<Leader> data,
    bool isLoading,
  }) {
    return GestureDetector(
      onTap: () {
        getIt<LeaderboardProvider>().setCurrentActiveTab(type);
        int index = LeaderboardType.values.indexOf(type);
        getIt<LeaderboardProvider>().setCurrentIndex(index);

        Navigator.of(context).pushNamed("/leaderboard-screen");
      },
      child: Container(
        width: SizeConfig.safeBlockHorizontal * 92,
        height: SizeConfig.safeBlockHorizontal * 65,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          gradient: LeaderboardConfig.itemConfig[type]["gradient"],
        ),
        alignment: Alignment.center,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Column(
              children: [
                SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
                CustomText(
                  LeaderboardConfig.itemConfig[type]["monthlyTitle"],
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                Spacer(),
                ImagePlaceholderWidget(
                  height: SizeConfig.safeBlockHorizontal * 25,
                  matchTextDirection: false,
                  image: AssetImage(
                    AssetsPath.fromImagesCommon("leaderboard_stand"),
                  ),
                  fit: BoxFit.fitHeight,
                  placeholder: Container(
                    color: Colors.transparent,
                    height: SizeConfig.safeBlockHorizontal * 25,
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: 0,
              child: _buildTopThree(type: type, data: data),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTopThreeItem({
    LeaderboardType type,
    Leader data,
    int index,
  }) {
    final _width = SizeConfig.safeBlockHorizontal *
            27 *
            LeaderboardConfig.leaderboardStandSizeRatio /
            3 -
        20;

    double marginBot = 0;

    switch (index) {
      case 1:
        marginBot = SizeConfig.safeBlockHorizontal * 20;
        break;
      case 2:
        marginBot = SizeConfig.safeBlockHorizontal * 12;
        break;
      case 3:
        marginBot = SizeConfig.safeBlockHorizontal * 10;
        break;
      default:
        marginBot = SizeConfig.safeBlockHorizontal * 20;
    }

    return Container(
      margin: EdgeInsets.only(bottom: marginBot),
      child: LeaderboardTopItemWidget(
        pendantUrl: data?.pendantUrl,
        type: type,
        userId: data?.userId,
        name: data?.name,
        value: data?.point,
        avatarUrl: data?.avatarUrl,
        width: _width,
        index: index,
      ),
    );
  }

  Widget _buildTopThree({
    LeaderboardType type,
    List<Leader> data,
  }) {
    return Container(
      width: SizeConfig.safeBlockHorizontal *
          25 *
          LeaderboardConfig.leaderboardStandSizeRatio,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        textDirection: TextDirection.ltr,
        children: [
          // if (data != null && data.length > 1)
          _buildTopThreeItem(
            type: type,
            data: data != null && data.length > 1 ? data.elementAt(1) : null,
            index: 2,
          ),
          // if (data != null && data.length <= 1) Container(width: _width),
          // if (data != null && data.length > 0)
          _buildTopThreeItem(
            type: type,
            data: data != null && data.length > 0 ? data.elementAt(0) : null,
            index: 1,
          ),
          // Container(
          //   margin:
          //       EdgeInsets.only(bottom: SizeConfig.safeBlockHorizontal * 20),
          //   child: LeaderboardTopItemWidget(
          //     pendantUrl: data?.elementAt(0)?.pendantUrl,
          //     type: type,
          //     userId: data?.elementAt(0)?.userId,
          //     name: data?.elementAt(0)?.name,
          //     value: data?.elementAt(0)?.point,
          //     avatarUrl: data?.elementAt(0)?.avatarUrl,
          //     width: _width,
          //     index: 1,
          //   ),
          // ),
          // if (data != null && data.length <= 2) Container(width: _width),
          // if (data != null && data.length > 2)
          _buildTopThreeItem(
            type: type,
            data: data != null && data.length > 2 ? data.elementAt(2) : null,
            index: 3,
          ),
        ],
      ),
    );
  }
}
