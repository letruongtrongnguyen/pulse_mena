import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/browse_more/browse_more.widget.dart';
import 'package:flutterbase/widgets/my_scaffold/my_scaffold.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item_shimmer.dart';

class CountryFilterScreen extends StatefulWidget {
  @override
  _CountryFilterScreenState createState() => _CountryFilterScreenState();
}

class _CountryFilterScreenState extends State<CountryFilterScreen> {
  @override
  void initState() {
    getIt<CommunicateService>().requestHideNavigationBar();

    super.initState();
  }

  @override
  void dispose() {
    getIt<CommunicateService>().requestShowNavigationBar();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String title = "";
    String countryCode = "";
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    if (arguments != null) {
      title = arguments['title'];
      countryCode = arguments['country_code'];
    }

    getIt<RoomProvider>().fetchRoomData(countryCode: countryCode);

    ValueListenable data = getIt<RoomProvider>().data;
    ValueListenable loading = getIt<RoomProvider>().loading;

    return MyScaffold(
      appBar: AppBar(
        title: CustomText(
          title,
          style: Theme.of(context).appBarTheme.textTheme.headline1,
        ),
        centerTitle: true,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: Colors.transparent,
            alignment: AlignmentDirectional.centerStart,
            padding: EdgeInsetsDirectional.fromSTEB(
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4.5,
              0,
              SizeConfig.safeBlockHorizontal * 4.5,
            ),
            // color: Colors.blueGrey,
            child: SvgPicture.asset(
              AssetsPath.fromImagesSvg("arrow_back_ic"),
              color: Theme.of(context).iconTheme.color,
              matchTextDirection: true,
            ),
          ),
        ),
        backgroundColor: Theme.of(context).appBarTheme.color,
        automaticallyImplyLeading: false,
      ),
      extendBodyBehindAppBar: true,
      backgroundImagePath: AssetsPath.fromImagesCommon("background"),
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      child: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: SizedBox(
              height: SizeConfig.paddingTop +
                  kToolbarHeight +
                  SizeConfig.safeBlockHorizontal * 4,
            ),
          ),
          AnimatedBuilder(
            animation: Listenable.merge([data, loading]),
            builder: (ctx, child) {
              if (loading.value && data.value.length == 0) {
                return SliverToBoxAdapter(
                  child: SizedBox(),
                );
              }

              if (!loading.value && data.value.length == 0)
                return SliverToBoxAdapter(
                  child: EmptyRoom(),
                );

              return SliverList(
                delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                    return AnimatedOpacity(
                      duration: Duration(milliseconds: 200),
                      opacity: loading.value ? 0.7 : 1.0,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemWidget(
                          roomData: data.value[index],
                        ),
                        // child: RoomItemShimmer(),
                      ),
                    );
                  },
                  childCount: data.value.length,
                ),
              );
            },
          ),
          SliverToBoxAdapter(
            child: ValueListenableBuilder(
              valueListenable: loading,
              builder: (ctx, loading, child) {
                if (loading)
                  return Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemShimmer(),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemShimmer(),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemShimmer(),
                      ),
                    ],
                  );

                return SizedBox();
              },
            ),
          ),
          SliverToBoxAdapter(
            child: ValueListenableBuilder(
              valueListenable: loading,
              builder: (ctx, loading, child) {
                if (getIt<RoomProvider>().data.value.length > 0 &&
                    getIt<RoomProvider>().data.value.length /
                            ((getIt<RoomProvider>().roomDataCurrentPage + 1) *
                                10) <
                        1 &&
                    // getIt<RoomProvider>().roomDataCurrentPage != 0 &&
                    !loading)
                  return Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: 10),
                    child: CustomText(
                      "common.noMoreRoom",
                      style: TextStyle(
                        color: Color(0xffb4b4b4),
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  );

                if (!loading && getIt<RoomProvider>().data.value.length > 0)
                  return Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: SizeConfig.safeBlockHorizontal * 4,
                      horizontal: SizeConfig.safeBlockHorizontal * 4,
                    ),
                    child: Align(
                      alignment: AlignmentDirectional.centerEnd,
                      child: BrowseMoreWidget(
                        onTap: () {
                          getIt<RoomProvider>().fetchRoomData(
                            page: getIt<RoomProvider>().roomDataCurrentPage + 1,
                          );
                        },
                      ),
                    ),
                  );

                return SizedBox();
              },
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 50,
            ),
          ),
        ],
      ),
    );
  }
}

class EmptyRoom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      child: Column(
        children: [
          Stack(
            children: [
              Positioned(
                left: -SizeConfig.safeBlockHorizontal * 5,
                child: BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: 8.0,
                    sigmaY: 8.0,
                  ),
                  child: Container(
                    width: SizeConfig.safeBlockHorizontal * 45,
                    height: SizeConfig.safeBlockHorizontal * 45,
                    decoration: BoxDecoration(
                      gradient: RadialGradient(
                        colors: [
                          Color(0xff3EE4FF).withOpacity(0.6),
                          Colors.transparent,
                        ],
                        stops: [0, 0.8],
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                width: SizeConfig.safeBlockHorizontal * 40,
                height: SizeConfig.safeBlockHorizontal * 40,
                alignment: Alignment.center,
                child: Image.asset(
                  AssetsPath.fromImagesCharacterAction("haki_lookup"),
                  height: SizeConfig.safeBlockHorizontal * 30,
                  fit: BoxFit.contain,
                ),
              ),
            ],
          ),
          CustomText(
            "common.emptyRoom",
          ),
        ],
      ),
    );
  }
}
