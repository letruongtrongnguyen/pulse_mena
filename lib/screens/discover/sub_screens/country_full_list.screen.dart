import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/my_scaffold/my_scaffold.widget.dart';

import '../country_filter_module.dart';

class CountryFullListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<String> countryFilters = [
      //Asia
      'sa',
      'ae',
      'kw',
      'qa',
      'om',
      'bh',
      'iq',
      'ye',
      'jo',
      'lb',
      'tr',
      'sg',
      'hk',
      'my',
      'th',
      'vn',
      'jp',
      'kr',
      //Africa
      'eg',
      'dz',
      'ma',
      'ly',
      'za',
      'tn',
      //Europe
      'gb',
      'fr',
      'nl',
      'de',
      'es',
      'at',
      'se',
      'it',
      'ro',
      'ua',
      'be',
      'pl',
      'gr',
      'cz',
      'dk',
      'no',
      'fi',
      'pt',
      'sk',
      'hu',
      'ru',
      'by',
      //America
      'us',
      'br',
      'mx',
      'ar',
      'pe',
      'co',
      'ca',
      //Oceania
      'au',
      'nz',
    ];

    return MyScaffold(
      appBar: AppBar(
        title: CustomText(
          "discover.menuTitle.countries",
          style: Theme.of(context).appBarTheme.textTheme.headline1,
        ),
        centerTitle: true,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: Colors.transparent,
            alignment: AlignmentDirectional.centerStart,
            padding: EdgeInsetsDirectional.fromSTEB(
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4.5,
              0,
              SizeConfig.safeBlockHorizontal * 4.5,
            ),
            // color: Colors.blueGrey,
            child: SvgPicture.asset(
              AssetsPath.fromImagesSvg("arrow_back_ic"),
              color: Theme.of(context).iconTheme.color,
              matchTextDirection: true,
            ),
          ),
        ),
        backgroundColor: Theme.of(context).appBarTheme.color,
        automaticallyImplyLeading: false,
      ),
      extendBodyBehindAppBar: true,
      backgroundImagePath: AssetsPath.fromImagesCommon("background"),
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      child: GridView.builder(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.safeBlockHorizontal * 4,
          vertical: kToolbarHeight +
              SizeConfig.paddingTop +
              SizeConfig.safeBlockHorizontal * 4,
        ),
        shrinkWrap: true,
        itemCount: countryFilters.length,
        // physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          childAspectRatio: 2.5,
          mainAxisSpacing: SizeConfig.safeBlockHorizontal * 2.5,
          crossAxisSpacing: SizeConfig.safeBlockHorizontal * 2.5,
        ),
        itemBuilder: (context, index) {
          return CountryFilterItem(countryCode: countryFilters[index]);
        },
      ),
    );
  }
}
