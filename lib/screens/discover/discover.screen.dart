import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/providers/search.provider.dart';
import 'package:flutterbase/screens/search/search.screen.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/browse_more/browse_more.widget.dart';
import 'package:flutterbase/widgets/my_scaffold/my_scaffold.widget.dart';
import 'package:flutterbase/widgets/others/notification_ic.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item_shimmer.dart';
import 'package:flutterbase/widgets/room_item/room_square_item.widget.dart';
import 'package:flutterbase/widgets/session_title/session_title.dart';

import 'activities_module.dart';
import 'country_filter_module.dart';
import 'leaderboard_module.dart';

class DiscoverScreen extends StatefulWidget {
  @override
  _DiscoverScreenState createState() => _DiscoverScreenState();
}

class _DiscoverScreenState extends State<DiscoverScreen> {
  @override
  void initState() {
    getIt<LeaderboardProvider>().fetchLeaderboardTopThreeData();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      getIt<RoomProvider>().fetchPopularRoomData();
    });

    // getIt<RoomProvider>().listenSocket();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getIt<CommunicateService>().context = context;

    ValueNotifier popularRoomData = getIt<RoomProvider>().popularRoomData;
    ValueNotifier popularRoomLoading = getIt<RoomProvider>().popularRoomLoading;

    Future<void> _onRefresh() async {
      await getIt<LeaderboardProvider>().fetchLeaderboardTopThreeData();
      await getIt<RoomProvider>().fetchPopularRoomData();
    }

    return MyScaffold(
      // appBar: AppBar(
      //   title: Row(
      //     children: [
      //       CustomText(
      //         "discover.title",
      //         style: TextStyle(
      //           fontWeight: FontWeight.bold,
      //           fontSize: 18,
      //         ),
      //       ),
      //       Spacer(),
      //       Hero(
      //         tag: "searchInputContainer",
      //         child: Material(
      //           type: MaterialType.transparency,
      //           child: Container(
      //             color: Colors.black,
      //           ),
      //         ),
      //       )
      //     ],
      //   ),
      //   backgroundColor: Color(0xff1c1c1c).withOpacity(0.96),
      //   actions: [
      //     GestureDetector(
      //       onTap: () {
      //         // Navigator.of(context).pushNamed("/search-screen");
      //         getIt<SearchProvider>().searchEditingController.clear();
      //
      //         Navigator.push(
      //           context,
      //           PageRouteBuilder(
      //             pageBuilder: (c, a1, a2) => SearchScreen(),
      //             // transitionsBuilder: (c, anim, a2, child) =>
      //             //     FadeTransition(opacity: anim, child: child),
      //             transitionDuration: Duration(milliseconds: 0),
      //           ),
      //         );
      //       },
      //       child: Container(
      //         color: Colors.transparent,
      //         alignment: Alignment.centerRight,
      //         padding: EdgeInsets.fromLTRB(
      //           SizeConfig.safeBlockHorizontal * 4,
      //           SizeConfig.safeBlockHorizontal * 4.2,
      //           SizeConfig.safeBlockHorizontal * 2,
      //           SizeConfig.safeBlockHorizontal * 4.2,
      //         ),
      //         // color: Colors.blueGrey,
      //         child: SvgPicture.asset(
      //           AssetsPath.fromImagesSvg("search_ic"),
      //         ),
      //       ),
      //     ),
      //     NotificationIconWidget(
      //       padding: EdgeInsets.fromLTRB(
      //         SizeConfig.safeBlockHorizontal * 2,
      //         SizeConfig.safeBlockHorizontal * 4.2,
      //         SizeConfig.safeBlockHorizontal * 4,
      //         SizeConfig.safeBlockHorizontal * 4.2,
      //       ),
      //     ),
      //   ],
      //   automaticallyImplyLeading: false,
      // ),
      extendBodyBehindAppBar: true,
      backgroundImagePath: AssetsPath.fromImagesCommon("background"),
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      child: RefreshIndicator(
        onRefresh: _onRefresh,
        displacement: SizeConfig.paddingTop + kToolbarHeight + 20,
        color: Theme.of(context).iconTheme.color,
        child: CustomScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          slivers: [
            SliverAppBar(
              pinned: true,
              forceElevated: true,
              title: Row(
                children: [
                  CustomText(
                    "discover.title",
                    style: Theme.of(context).appBarTheme.textTheme.headline1,
                  ),
                  Spacer(),
                  Hero(
                    tag: "searchInputContainer",
                    child: Material(
                      type: MaterialType.transparency,
                      child: Container(
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  )
                ],
              ),
              backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
              shadowColor: Theme.of(context).appBarTheme.shadowColor,
              actions: [
                GestureDetector(
                  onTap: () {
                    // Navigator.of(context).pushNamed("/search-screen");
                    getIt<SearchProvider>().searchEditingController.clear();

                    Navigator.push(
                      context,
                      PageRouteBuilder(
                        pageBuilder: (c, a1, a2) => SearchScreen(),
                        // transitionsBuilder: (c, anim, a2, child) =>
                        //     FadeTransition(opacity: anim, child: child),
                        transitionDuration: Duration(milliseconds: 0),
                      ),
                    );
                  },
                  child: Container(
                    color: Colors.transparent,
                    alignment: AlignmentDirectional.centerEnd,
                    padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.safeBlockHorizontal * 2,
                      vertical: SizeConfig.safeBlockHorizontal * 4.2,
                    ),
                    // color: Colors.blueGrey,
                    child: SvgPicture.asset(
                      AssetsPath.fromImagesSvg("search_ic"),
                      color: Theme.of(context).appBarTheme.iconTheme.color,
                    ),
                  ),
                ),
                NotificationIconWidget(
                  padding: EdgeInsetsDirectional.fromSTEB(
                    SizeConfig.safeBlockHorizontal * 2,
                    SizeConfig.safeBlockHorizontal * 4.2,
                    SizeConfig.safeBlockHorizontal * 4,
                    SizeConfig.safeBlockHorizontal * 4.2,
                  ),
                ),
              ],
              automaticallyImplyLeading: false,
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height: SizeConfig.safeBlockHorizontal * 4,
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.safeBlockHorizontal * 4,
                ),
                child: LeaderboardModule(),
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.safeBlockHorizontal * 4,
                ),
                child: CountryFilterModule(),
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.safeBlockHorizontal * 4,
                ),
                child: ActivitiesModule(),
              ),
            ),
            SliverPersistentHeader(
              // pinned: true,
              delegate: _SliverAppBarDelegate(
                minHeight: 50,
                maxHeight: 50,
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.safeBlockHorizontal * 4,
                  ),
                  child: SessionTitle(
                    title: "discover.menuTitle.recommendedRoom",
                    // suffix: GestureDetector(
                    //   onTap: () {
                    //     getIt<RoomProvider>().fetchPopularRoomData();
                    //   },
                    //   child: SvgPicture.asset(
                    //     AssetsPath.fromImagesSvg("reload_ic"),
                    //     height: SizeConfig.safeBlockHorizontal * 5,
                    //   ),
                    // ),
                  ),
                ),
              ),
            ),
            AnimatedBuilder(
              animation:
                  Listenable.merge([popularRoomData, popularRoomLoading]),
              builder: (ctx, child) {
                if (popularRoomData.value.length < 3) {
                  return SliverToBoxAdapter(
                    child: SizedBox(),
                  );
                }

                return SliverToBoxAdapter(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(
                      SizeConfig.safeBlockHorizontal * 4,
                      0,
                      SizeConfig.safeBlockHorizontal * 4,
                      SizeConfig.safeBlockHorizontal,
                    ),
                    child: Row(
                      children: [
                        RoomSquareItem(
                          width: SizeConfig.safeBlockHorizontal * 55,
                          height: SizeConfig.safeBlockHorizontal * 55,
                          roomData: popularRoomData?.value[0],
                        ),
                        SizedBox(
                          width: SizeConfig.safeBlockHorizontal * 2,
                        ),
                        Column(
                          children: [
                            RoomSquareItem(
                              width: SizeConfig.safeBlockHorizontal * 35,
                              height: SizeConfig.safeBlockHorizontal * 26.5,
                              roomData: popularRoomData?.value[1],
                            ),
                            SizedBox(
                              height: SizeConfig.safeBlockHorizontal * 2,
                            ),
                            RoomSquareItem(
                              width: SizeConfig.safeBlockHorizontal * 35,
                              height: SizeConfig.safeBlockHorizontal * 26.5,
                              roomData: popularRoomData?.value[2],
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
            AnimatedBuilder(
              animation:
                  Listenable.merge([popularRoomData, popularRoomLoading]),
              builder: (ctx, child) {
                if (popularRoomLoading.value &&
                    popularRoomData.value.length == 0) {
                  return SliverToBoxAdapter(
                    child: SizedBox(),
                  );
                }

                return SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                      /// To convert this infinite list to a list with "n" no of items,
                      /// uncomment the following line:
                      /// if (index > n) return null;
                      ///

                      if (index < 3 && popularRoomData.value.length > 2)
                        return SizedBox();

                      return AnimatedOpacity(
                        duration: Duration(milliseconds: 200),
                        opacity: popularRoomLoading.value ? 0.7 : 1.0,
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: SizeConfig.safeBlockHorizontal * 1.5,
                            horizontal: SizeConfig.safeBlockHorizontal * 4,
                          ),
                          child: RoomItemWidget(
                            roomData: popularRoomData.value[index],
                          ),
                        ),
                      );

                      return SizedBox();
                    },
                    childCount: popularRoomData.value.length,
                  ),
                );
              },
            ),
            SliverToBoxAdapter(
              child: ValueListenableBuilder(
                valueListenable: popularRoomLoading,
                builder: (ctx, loading, child) {
                  if (loading)
                    return ListView.builder(
                      padding: EdgeInsets.zero,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: 3,
                      itemBuilder: (ctx, index) {
                        return Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: SizeConfig.safeBlockHorizontal * 1.5,
                            horizontal: SizeConfig.safeBlockHorizontal * 4,
                          ),
                          child: RoomItemShimmer(),
                        );
                      },
                    );

                  return SizedBox();
                },
              ),
            ),
            SliverToBoxAdapter(
              child: ValueListenableBuilder(
                valueListenable: popularRoomLoading,
                builder: (ctx, loading, child) {
                  if (getIt<RoomProvider>().popularRoomData.value.length /
                              (getIt<RoomProvider>()
                                      .popularRoomDataCurrentPage *
                                  10) <
                          1 &&
                      // getIt<RoomProvider>().popularRoomDataCurrentPage != 1 &&
                      !loading)
                    return Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(top: 10),
                      child: CustomText(
                        "common.noMoreRoom",
                        style: TextStyle(
                          color: Color(0xffb4b4b4),
                          fontSize: 12,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    );

                  if (!loading)
                    return Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: SizeConfig.safeBlockHorizontal * 4,
                        horizontal: SizeConfig.safeBlockHorizontal * 4,
                      ),
                      child: Align(
                        alignment: AlignmentDirectional.centerEnd,
                        child: BrowseMoreWidget(
                          onTap: () {
                            getIt<RoomProvider>().fetchPopularRoomData(
                              page: getIt<RoomProvider>()
                                      .popularRoomDataCurrentPage +
                                  1,
                            );
                          },
                        ),
                      ),
                    );

                  return SizedBox();
                },
              ),
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height:
                    SizeConfig.paddingBottom + kBottomNavigationBarHeight + 50,
              ),
            ),
          ],
        ),
      ),
      // floatingActionButton: FlatButton(
      //   onPressed: () {
      //     Navigator.of(context).pushNamed("/me-screen");
      //   },
      //   onLongPress: () {
      //     final textDirection = getIt<GeneralProvider>().textDirection.value;
      //     final lang = getIt<GeneralProvider>().lang.value;
      //
      //     getIt<GeneralProvider>().toggleTheme();
      //
      //     // if (textDirection == TextDirection.ltr)
      //     //   getIt<GeneralProvider>().setTextDirection(TextDirection.rtl);
      //     // else
      //     //   getIt<GeneralProvider>().setTextDirection(TextDirection.ltr);
      //     //
      //     // if (lang == "en")
      //     //   getIt<GeneralProvider>().setLanguage("ar");
      //     // else
      //     //   getIt<GeneralProvider>().setLanguage("en");
      //
      //     // AdaptiveTheme.of(context).toggleThemeMode();
      //   },
      //   child: Container(
      //     padding: EdgeInsets.all(10),
      //     decoration: BoxDecoration(
      //       shape: BoxShape.circle,
      //       color: Colors.blueAccent,
      //     ),
      //     child: Icon(Icons.navigate_next),
      //   ),
      // ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    @required this.minHeight,
    @required this.maxHeight,
    @required this.child,
  });

  final double minHeight;
  final double maxHeight;
  final Widget child;

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => max(maxHeight, minHeight);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return LayoutBuilder(builder: (context, constraints) {
      return Container(
        height: constraints.maxHeight,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(
              color: overlapsContent
                  ? Colors.white.withOpacity(0.05)
                  : Colors.transparent,
              width: overlapsContent ? 1 : 0,
            ),
          ),
          color: overlapsContent
              ? Theme.of(context).appBarTheme.color
              : Colors.transparent,
        ),
        child: child,
      );
    });

    return new SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}
