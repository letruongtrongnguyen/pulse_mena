import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/request/request.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/i18n/i18n.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/widgets/activity/activity.widget.dart';
import 'package:flutterbase/widgets/session_title/session_title.dart';
import 'package:flutterbase/core/@services/models/promo.model.dart';

class ActivitiesModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.safeBlockHorizontal * 92,
      padding: EdgeInsets.only(
        bottom: 30,
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 12),
            child: SessionTitle(
              title: "discover.menuTitle.activities",
            ),
          ),
          GestureDetector(
            onTap: () {
              String token = Request.token;

              getIt<CommunicateService>()
                  .navigate(route: "/webview-screen", args: {
                "url": "${getIt<GeneralProvider>().luckyHakiUrl}?token=$token",
                "title": "Lucky Haki",
                "is-lucky-haki": true,
              });
            },
            child: ActivityWidget(
              width: double.infinity,
              bannerUrl: AssetsPath.fromImagesActivities('lucky_haki'),
              title: "Lucky Haki",
              content: I18n.get("luckyHaki.content"),
            ),
          ),
          SizedBox(height: 8),
          ValueListenableBuilder(
              valueListenable: getIt<GeneralProvider>().myPromo,
              builder: (context, myPromo, _) {
                print("Rebuild banner: $myPromo");
                if (myPromo == null) return SizedBox.shrink();
                PromoModel _myPromo = myPromo as PromoModel;
                return GestureDetector(
                  onTap: () {
                    getIt<CommunicateService>()
                        .navigate(route: "/special-offer");
                  },
                  child: ActivityWidget(
                    width: double.infinity,
                    bannerUrl: _myPromo.getImageUrl(),
                    title: I18n.get("specialOffer.title"),
                    content: _myPromo.getPromoDescription(),
                  ),
                );
              }),
        ],
      ),
    );
  }
}
