import 'package:flutter/material.dart';
import 'package:flutterbase/configs/general.config.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/request/request.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/widgets/basic/image_placeholder.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class ActivityItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.safeBlockHorizontal * 4,
        vertical: SizeConfig.safeBlockHorizontal * 2,

      ),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: ImagePlaceholderWidget(
              width: SizeConfig.safeBlockHorizontal * 60,
              image: AssetImage(
                AssetsPath.fromImagesActivities("lucky_haki"),
              ),
              fit: BoxFit.fitWidth,
              placeholder: Container(
                color: Colors.transparent,
                width: SizeConfig.safeBlockHorizontal * 60,
              ),
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: () {
              String token = Request.token;
              final int myUserId = getIt<GeneralConfig>().myUserId;

              getIt<CommunicateService>().navigate(route: "/webview-screen", args: {
                "url":
                "${getIt<GeneralProvider>().luckyHakiUrl}?token=$token&user_id=$myUserId",
                "title": "Lucky Haki",
                "is-lucky-haki": true,
              });
            },
            child: Container(
              width: SizeConfig.safeBlockHorizontal * 20,
              height: SizeConfig.safeBlockHorizontal * 7,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0xff96EA0E),
                    Color(0xff07CD9D),
                  ],
                ),
                borderRadius: BorderRadius.circular(60),
              ),
              child: CustomText(
                "activityCenter.action.play",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
