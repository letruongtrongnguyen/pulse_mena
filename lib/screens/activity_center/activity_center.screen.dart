import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/screens/activity_center/activity_item.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/my_scaffold/my_scaffold.widget.dart';

class ActivityCenterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      appBar: AppBar(
        title: CustomText(
          "activityCenter.title",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
            color: Colors.white,
          ),
        ),
        brightness: Brightness.dark,
        automaticallyImplyLeading: false,
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: <Color>[
                Color(0xffF00C69),
                Color(0xff7F28ED),
              ],
            ),
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: Colors.transparent,
            alignment: AlignmentDirectional.centerStart,
            padding: EdgeInsetsDirectional.fromSTEB(
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4.5,
              0,
              SizeConfig.safeBlockHorizontal * 4.5,
            ),
            // color: Colors.blueGrey,
            child: SvgPicture.asset(
              AssetsPath.fromImagesSvg("arrow_back_ic"),
              matchTextDirection: true,
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      backgroundImagePath: AssetsPath.fromImagesCommon("background"),
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      child: ListView(
        children: [
          // Padding(
          //   padding: EdgeInsets.symmetric(
          //     vertical: SizeConfig.safeBlockHorizontal * 4,
          //     horizontal: SizeConfig.safeBlockHorizontal * 4,
          //   ),
          //   child: CustomText(
          //     "activityCenter.sessionTitle.recentlyJoined",
          //     style: TextStyle(
          //       fontWeight: FontWeight.bold,
          //       fontSize: 16,
          //     ),
          //   ),
          // ),
          // Column(
          //   children: [
          //     ActivityItem(),
          //   ],
          // ),
          // Divider(
          //   height: 30,
          //   color: Color(0xff303030).withOpacity(0.5),
          //   thickness: 5,
          // ),
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: SizeConfig.safeBlockHorizontal * 4,
              horizontal: SizeConfig.safeBlockHorizontal * 4,
            ),
            child: CustomText(
              "activityCenter.sessionTitle.allActivities",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          ),
          Column(
            children: [
              ActivityItem(),
            ],
          ),
        ],
      ),
      // body: Container(
      //   child: ListView(
      //     padding: EdgeInsets.fromLTRB(
      //       SizeConfig.safeBlockHorizontal * 4,
      //       SizeConfig.safeBlockHorizontal * 8,
      //       SizeConfig.safeBlockHorizontal * 4,
      //       SizeConfig.paddingBottom + 10,
      //     ),
      //     children: [
      //       Image(
      //         width: SizeConfig.safeBlockHorizontal * 92,
      //         fit: BoxFit.fitWidth,
      //         image: AssetImage(
      //           AssetsPath.fromImagesCommon("task_center_banner"),
      //         ),
      //       ),
      //       TaskCenterSession(
      //         title: "taskCenter.session.dailyTasks",
      //       ),
      //       TaskCenterItem(
      //         task: "Send gifts in room",
      //         reward: 10,
      //         isCompleted: false,
      //       ),
      //     ],
      //   ),
      // ),
    );
  }
}
