// import 'dart:io';
//
// import 'package:flutter/material.dart';
// import 'package:flutter_inappwebview/flutter_inappwebview.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:flutterbase/configs/size.config.dart';
// import 'package:flutterbase/core/utils/assets_path.dart';
// import 'package:flutterbase/widgets/basic/circle_ic.widget.dart';
// import 'package:flutterbase/widgets/basic/text.widget.dart';
// import 'package:url_launcher/url_launcher.dart';
//
// import 'help_center_session.dart';
//
// class ActionModule extends StatelessWidget {
//   final ChromeSafariBrowser browser =
//       new MyChromeSafariBrowser(new MyInAppBrowser());
//
//   final Uri _emailLaunchUri = Uri(
//       scheme: 'mailto',
//       path: 'letruongtrongnguyen@gmail.com',
//       queryParameters: {'subject': 'Example Subject & Symbols are allowed!'});
//
//   void _launchWhatsApp({
//     @required String phone,
//     @required String message,
//   }) async {
//     String url() {
//       if (Platform.isIOS) {
//         return "https://wa.me/$phone/?text=${Uri.parse(message)}";
//       } else {
//         return "https://wa.me/$phone?text=$message";
//       }
//     }
//
//     await launch("market://details?id=" + "whatsapp");
//
//     if (await canLaunch(url())) {
//       if (Platform.isIOS) {
//         await launch(
//           url(),
//         );
//       } else {
//         await browser.open(
//           url: url(),
//           // options: ChromeSafariBrowserClassOptions(
//           //   ios: IOSSafariOptions(
//           //     entersReaderIfAvailable: false,
//           //     presentationStyle: IOSUIModalPresentationStyle.OVER_FULL_SCREEN,
//           //   ),
//           // ),
//         );
//       }
//     } else {
//       throw 'Could not launch ${url()}';
//     }
//   }
//
//   void _sendEmail() async {
//     final String _emailSubject = 'Test Subject';
//     final String _emailLaunchString = Uri.encodeFull(
//         'mailto:letruongtrongnguyen@gmail.com?subject=$_emailSubject');
//     if (await canLaunch(_emailLaunchString.toString())) {
//       await launch(_emailLaunchString);
//     } else {
//       throw 'Could not launch';
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return HelpCenterSession(
//       title: "Chat with Haki Team",
//       content: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           _actionItem(
//             text: "Haki Message",
//             icon: ShaderMask(
//               shaderCallback: (bounds) {
//                 return LinearGradient(
//                   begin: Alignment.topCenter,
//                   end: Alignment.bottomCenter,
//                   colors: [Color(0xff81FFCA), Color(0xff2CB7FF)],
//                 ).createShader(bounds);
//               },
//               child: SvgPicture.asset(
//                 AssetsPath.fromImagesSvg("message_ic"),
//                 fit: BoxFit.fitHeight,
//                 width: SizeConfig.safeBlockHorizontal * 7,
//                 height: SizeConfig.safeBlockHorizontal * 7,
//               ),
//             ),
//           ),
//           GestureDetector(
//             onTap: () {
//               // openWhatsApp();
//               _launchWhatsApp(phone: "841689929827", message: "test");
//             },
//             child: _actionItem(
//               text: "Whatsapp",
//               icon: Transform.scale(
//                 scale: 1.1,
//                 child: Image(
//                   image: AssetImage(
//                     AssetsPath.fromImagesCommon("whatsapp_logo"),
//                   ),
//                   fit: BoxFit.fitHeight,
//                   width: SizeConfig.safeBlockHorizontal * 7,
//                   height: SizeConfig.safeBlockHorizontal * 7,
//                 ),
//               ),
//             ),
//           ),
//           GestureDetector(
//             onTap: () {
//               _sendEmail();
//             },
//             child: _actionItem(
//               text: "Send email",
//               icon: CircleIconWidget(
//                 icon: SvgPicture.asset(
//                   AssetsPath.fromImagesSvg("email_ic"),
//                   color: Color(0xff1c1c1c),
//                 ),
//                 radius: SizeConfig.safeBlockHorizontal * 3.5,
//                 padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 1.5),
//                 backgroundColor: Color(0xff8FFF00),
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget _actionItem({String text, Widget icon}) {
//     return Container(
//       width: SizeConfig.safeBlockHorizontal * 29,
//       decoration: BoxDecoration(
//         color: Colors.white.withOpacity(0.2),
//         borderRadius: BorderRadius.circular(8),
//       ),
//       padding: EdgeInsets.symmetric(
//         vertical: SizeConfig.safeBlockHorizontal * 4,
//       ),
//       alignment: Alignment.center,
//       child: Column(
//         mainAxisSize: MainAxisSize.min,
//         children: [
//           if (icon != null)
//             Padding(
//               padding: EdgeInsets.only(
//                 bottom: SizeConfig.safeBlockHorizontal * 2,
//               ),
//               child: icon,
//             ),
//           CustomText(
//             text,
//             style: TextStyle(
//               fontWeight: FontWeight.bold,
//               fontSize: 12,
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
//
// class MyInAppBrowser extends InAppBrowser {
//   @override
//   Future onLoadStart(String url) async {
//     print("\n\nStarted $url\n\n");
//   }
//
//   @override
//   Future onLoadStop(String url) async {
//     print("\n\nStopped $url\n\n");
//   }
//
//   @override
//   void onLoadError(String url, int code, String message) {
//     print("\n\nCan't load $url.. Error: $message\n\n");
//   }
//
//   @override
//   void onExit() {
//     print("\n\nBrowser closed!\n\n");
//   }
// }
//
// class MyChromeSafariBrowser extends ChromeSafariBrowser {
//   MyChromeSafariBrowser(browserFallback) : super(bFallback: browserFallback);
//
//   @override
//   void onOpened() {
//     print("ChromeSafari browser opened");
//   }
//
//   @override
//   void onCompletedInitialLoad() {
//     print("ChromeSafari browser initial load completed");
//   }
//
//   @override
//   void onClosed() {
//     print("ChromeSafari browser closed");
//   }
// }
