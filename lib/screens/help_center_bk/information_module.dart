import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

import 'help_center_session.dart';

class InformationModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return HelpCenterSession(
      title: "Information",
      content: Container(
        child: Column(
          children: [
            Row(
              children: [
                CustomText(
                  "Email: ",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SelectableText(
                  "helpcenter@haki.live",
                  style: TextStyle(
                    fontFamily: "Euclid Circular A",
                    fontSize: 14 * SizeConfig.screenWidth / 375,
                  ),
                  autofocus: true,
                  toolbarOptions: ToolbarOptions(
                    copy: true,
                    selectAll: false,
                    cut: false,
                    paste: false,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: SizeConfig.safeBlockHorizontal,
            ),
            Row(
              children: [
                CustomText(
                  "Whatsapp: ",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SelectableText(
                  "123454321",
                  showCursor: false,
                  autofocus: false,
                  style: TextStyle(
                    fontFamily: "Euclid Circular A",
                    fontSize: 14 * SizeConfig.screenWidth / 375,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
