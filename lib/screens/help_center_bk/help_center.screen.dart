// import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:flutterbase/configs/size.config.dart';
// import 'package:flutterbase/core/utils/assets_path.dart';
// import 'package:flutterbase/widgets/basic/text.widget.dart';
//
// import 'action_module.dart';
// import 'avatar_module.dart';
// import 'information_module.dart';
// import 'others_module.dart';
//
// class HelpCenterScreen extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: CustomText(
//           "common.helpCenter",
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//             fontSize: 18,
//           ),
//         ),
//         centerTitle: true,
//         leading: GestureDetector(
//           onTap: () {
//             Navigator.pop(context);
//           },
//           child: Container(
//             color: Colors.transparent,
//             alignment: Alignment.centerLeft,
//             padding: EdgeInsets.fromLTRB(
//               SizeConfig.safeBlockHorizontal * 4,
//               SizeConfig.safeBlockHorizontal * 4.5,
//               0,
//               SizeConfig.safeBlockHorizontal * 4.5,
//             ),
//             // color: Colors.blueGrey,
//             child: SvgPicture.asset(
//               AssetsPath.fromImagesSvg("arrow_back_ic"),
//             ),
//           ),
//         ),
//         automaticallyImplyLeading: false,
//         backgroundColor: Color(0xff1c1c1c),
//       ),
//       backgroundColor: Colors.black,
//       body: ListView(
//         children: [
//           AvatarHelpCenterModule(),
//           InformationModule(),
//           ActionModule(),
//           OthersModule(),
//         ],
//       ),
//     );
//   }
// }
