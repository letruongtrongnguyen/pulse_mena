import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class HelpCenterSession extends StatelessWidget {
  final String title;
  final Widget content;
  final bool haveDivider;

  HelpCenterSession({
    Key key,
    this.title,
    this.content,
    this.haveDivider = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.safeBlockHorizontal * 4,
        vertical: SizeConfig.safeBlockHorizontal * 6,
      ),
      decoration: BoxDecoration(
          border: Border(
        bottom: BorderSide(
          color:
              haveDivider ? Colors.white.withOpacity(0.1) : Colors.transparent,
          width: haveDivider ? 5 : 0,
        ),
      )),
      alignment: AlignmentDirectional.centerStart,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _title(),
          SizedBox(
            height: SizeConfig.safeBlockHorizontal * 4,
          ),
          content ?? SizedBox(),
        ],
      ),
    );
  }

  Widget _title() {
    return CustomText(
      title ?? "Test",
      style: TextStyle(
        color: Color(0xff7F7F7F),
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
