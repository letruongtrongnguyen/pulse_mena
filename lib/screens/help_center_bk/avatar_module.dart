import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/widgets/avatar/avatar_with_frame.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class AvatarHelpCenterModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: SizeConfig.safeBlockHorizontal * 5,
      ),
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          AvatarWithFrameWidget(
            // width: SizeConfig.safeBlockHorizontal * 20,
            height: SizeConfig.safeBlockHorizontal * 20,
            frameUrl:
                "https://staging-gapi.mocogateway.com/static/female-frame/pulse-mena-black-crown.png",
            avatarUrl:
                "1https://s3-alpha-sig.figma.com/img/6992/5846/ac37b276cae20a3fa4e4d6574e9cfdc7?Expires=1606089600&Signature=VzdyX8wUok~mbVwU~IB-ZBYK2QY9AMphffaBAlrYAW-el2z6yAVyjZC1P5LI7q~7r9d-Iajo9jCVy~Cq9omp9oUJd3jUIKb0EDPh648jCEczw2jMX7WwyYfrBD5vYyspSrX6V4Gt4utelcg3cQC~SA3nYhnlPuacdCm3JSlHQutLBG5D~xwOGVixntmpN3J3GWnCMkf2BJGnu0DGu~3uOkZY44ueNF3uFoisYETVr-l7dhBtPQBeUx3~mL2OXSi6A7C2Ea9~sv-YaRk5D92lsRKOyplFQNt9a2tn5wYLeknScgqdg7LFxzxKZ65Q70zL99lNbD3yiQpq2jr7rR6iMQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
          ),
          SizedBox(
            height: SizeConfig.safeBlockHorizontal * 2,
          ),
          CustomText(
            "Haki Team",
            style: TextStyle(
              color: Color(0xff00FFE0),
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }
}
