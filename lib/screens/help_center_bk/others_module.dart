import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

import 'help_center_session.dart';

class OthersModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return HelpCenterSession(
      title: "Others",
      haveDivider: false,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          _item(title: "FAQ"),
          SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
          _item(title: "Feedback"),
        ],
      ),
    );
  }

  Widget _item({String title, Function onTap}) {
    return GestureDetector(
      onTap: onTap ?? () {},
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.1),
          borderRadius: BorderRadius.circular(8),
        ),
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.safeBlockHorizontal * 4,
          vertical: SizeConfig.safeBlockHorizontal * 3,
        ),
        child: Row(
          children: [
            CustomText(
              title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Spacer(),
            SvgPicture.asset(
              AssetsPath.fromImagesSvg("arrow_forward_ios_ic"),
            ),
          ],
        ),
      ),
    );
  }
}
