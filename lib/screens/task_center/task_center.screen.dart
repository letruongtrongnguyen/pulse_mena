import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/screens/task_center/task_center_item.dart';
import 'package:flutterbase/screens/task_center/task_center_session.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class TaskCenterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText("taskCenter.title",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18,
            )),
        automaticallyImplyLeading: false,
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: <Color>[
                Color(0xffD6F22B),
                Color(0xff0DCCF5),
              ],
            ),
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: Colors.transparent,
            alignment: AlignmentDirectional.centerStart,
            padding: EdgeInsetsDirectional.fromSTEB(
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4.5,
              0,
              SizeConfig.safeBlockHorizontal * 4.5,
            ),
            // color: Colors.blueGrey,
            child: SvgPicture.asset(
              AssetsPath.fromImagesSvg("arrow_back_ic"),
              matchTextDirection: true,
            ),
          ),
        ),
      ),
      body: Container(
        child: ListView(
          padding: EdgeInsetsDirectional.fromSTEB(
            SizeConfig.safeBlockHorizontal * 4,
            SizeConfig.safeBlockHorizontal * 8,
            SizeConfig.safeBlockHorizontal * 4,
            SizeConfig.paddingBottom + 10,
          ),
          children: [
            Image(
              width: SizeConfig.safeBlockHorizontal * 92,
              fit: BoxFit.fitWidth,
              image: AssetImage(
                AssetsPath.fromImagesCommon("task_center_banner"),
              ),
            ),
            TaskCenterSession(
              title: "taskCenter.session.dailyTasks",
            ),
            TaskCenterItem(
              task: "Send gifts in room",
              reward: 10,
              isCompleted: false,
            ),
          ],
        ),
      ),
    );
  }
}
