import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/screens/task_center/task_center_item.dart';
import 'package:flutterbase/widgets/basic/circle_ic.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class TaskCenterSession extends StatelessWidget {
  final String title;

  TaskCenterSession({
    Key key,
    @required this.title,
  }) : super(key: key) {
    assert(title != null);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: 20,
      ),
      child: Row(
        children: [
          CircleIconWidget(
            icon: SvgPicture.asset(
              AssetsPath.fromImagesSvg("task_center_ic"),
              color: Colors.black,

            ),
            radius: SizeConfig.safeBlockHorizontal * 4,
            padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 1.5),
            backgroundColor: Colors.white,
          ),
          SizedBox(width: SizeConfig.safeBlockHorizontal * 3),
          CustomText(
            title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }
}
