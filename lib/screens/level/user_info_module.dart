import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/user.model.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/avatar/avatar_with_frame.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/progress_bar/progress_bar.widget.dart';

class UserInfoModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    User me = getIt<MeProvider>().data.value;

    return Container(
      margin: EdgeInsets.only(bottom: SizeConfig.safeBlockHorizontal * 2),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          // AvatarWithFrameWidget(
          //   width: SizeConfig.safeBlockHorizontal * 20,
          //   avatarUrl: me.avatarUrl,
          //   frameUrl: (me.pendantUrl != null && me.pendantUrl != "" )? me.pendantUrl : me.level.frameUrl,
          // ),
          AvatarWidget(
            avatarUrl: me.avatarUrl,
            borderWidth: SizeConfig.safeBlockHorizontal * 0.5,
            radius: SizeConfig.safeBlockHorizontal * 8,
          ),
          Container(
            margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.safeBlockHorizontal * 4,
              vertical: SizeConfig.safeBlockHorizontal / 2,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              border: Border.all(
                color: me.level.borderColor,
                width: 1,
              ),
              gradient: LinearGradient(
                begin: me.level.gradientBegin,
                end: me.level.gradientEnd,
                colors: me.level.colors,
              ),
            ),
            child: CustomText(
              "Lv.${me.level.value}",
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(height: 20),
          ProgressBarWidget(
            width: SizeConfig.safeBlockHorizontal * 92,
            height: 20,
            percent: me.level.progress.toDouble(),
            gradientColors: [Color(0xff277AF7), Color(0xffFF67F0)],
            backgroundColor: Color(0xff424242),
          ),
        ],
      ),
    );
  }
}
