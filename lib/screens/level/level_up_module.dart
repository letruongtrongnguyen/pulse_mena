import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class LevelUpModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: SizeConfig.safeBlockHorizontal * 4,
          vertical: SizeConfig.safeBlockHorizontal * 2),
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.safeBlockHorizontal * 4,
          vertical: SizeConfig.safeBlockHorizontal * 4),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white.withOpacity(0.1),
      ),
      child: Column(
        children: [
          CustomText(
            "level.levelUp.title",
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 15),
          _buildItem(
            label: "level.label.login",
            description: "level.description.login",
          ),
          SizedBox(height: 15),
          _buildItem(
            label: "level.label.sendGifts",
            description: "level.description.sendGifts",
          ),
          SizedBox(height: 15),
          _buildItem(
            label: "level.label.recharge",
            description: "level.description.recharge",
          ),
          SizedBox(height: 15),
          _buildItem(
            label: "level.label.takeMic",
            description: "level.description.takeMic",
          ),
          SizedBox(height: 15),
          _buildItem(
            label: "level.label.earnGems",
            description: "level.description.earnGems",
          ),
          SizedBox(height: 15),
          _buildItem(
            label: "level.label.sendMessage",
            description: "level.description.sendMessage",
          ),
        ],
      ),
    );
  }

  Widget _buildItem({
    String label,
    String description,
  }) {
    return Column(
      children: [
        Row(
          children: [
            SizedBox(
              width: SizeConfig.safeBlockHorizontal * 4,
              child: CustomText("-"),
            ),
            // Container(
            //   width: SizeConfig.safeBlockHorizontal * 4,
            //   alignment: Alignment.centerLeft,
            //   child: Icon(
            //     Icons.circle,
            //     color: Colors.white.withOpacity(0.8),
            //     size: 7,
            //   ),
            // ),
            CustomText(
              label,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12,
              ),
            ),
          ],
        ),
        SizedBox(height: 3),
        Row(
          children: [
            SizedBox(
              width: SizeConfig.safeBlockHorizontal * 4,
            ),
            Container(
              width: SizeConfig.safeBlockHorizontal * 80,
              child: CustomText(
                description,
                style: TextStyle(
                  fontSize: 12,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
