import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

import 'level_up_module.dart';
import 'pendant_module.dart';
import 'user_info_module.dart';

class LevelScreen extends StatefulWidget {
  @override
  _LevelScreenState createState() => _LevelScreenState();
}

class _LevelScreenState extends State<LevelScreen> {
  @override
  void initState() {
    getIt<CommunicateService>().requestHideNavigationBar();
    super.initState();
  }

  @override
  void dispose() {
    getIt<CommunicateService>().requestShowNavigationBar();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          "common.level",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        centerTitle: true,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: Colors.transparent,
            alignment: AlignmentDirectional.centerStart,
            padding: EdgeInsetsDirectional.fromSTEB(
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4.5,
              0,
              SizeConfig.safeBlockHorizontal * 4.5,
            ),
            // color: Colors.blueGrey,
            child: SvgPicture.asset(
              AssetsPath.fromImagesSvg("arrow_back_ic"),
            ),
          ),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xff1c1c1c),
      ),
      body: Container(
        color: Colors.black,
        child: ListView(
          padding: EdgeInsets.symmetric(
              vertical: SizeConfig.safeBlockHorizontal * 4),
          children: [
            UserInfoModule(),
            SizedBox(height: 10),
            LevelUpModule(),
            SizedBox(height: 10),
            PendantModule(),
          ],
        ),
      ),
    );
  }
}
