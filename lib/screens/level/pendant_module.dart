import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/level.range.model.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/providers/level.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class PendantModule extends StatefulWidget {
  @override
  _PendantModuleState createState() => _PendantModuleState();
}

class _PendantModuleState extends State<PendantModule> {
  @override
  void initState() {
    getIt<LevelProvider>().fetchLevelRangeData();
    // DefaultCacheManager manager = new DefaultCacheManager();
    // manager.emptyCache(); //clears all data in cache.
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: getIt<LevelProvider>().data,
        builder: (context, levelRanges, child) {
          if (levelRanges.length <= 0) return SizedBox.shrink();
          return Container(
            margin: EdgeInsets.symmetric(
                horizontal: SizeConfig.safeBlockHorizontal * 4,
                vertical: SizeConfig.safeBlockHorizontal * 2),
            padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.safeBlockHorizontal * 4,
                vertical: SizeConfig.safeBlockHorizontal * 4),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white.withOpacity(0.1),
            ),
            child: Column(
              children: [
                CustomText(
                  "level.pendantRewards.title",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 15),
                GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: levelRanges.length % 3 != 1
                      ? levelRanges.length
                      : levelRanges.length + 1,
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: SizeConfig.safeBlockHorizontal * 4,
                    mainAxisSpacing: SizeConfig.safeBlockHorizontal * 4,
                    childAspectRatio: 0.65,
                  ),
                  itemBuilder: (BuildContext context, int index) {
                    if (levelRanges.length % 3 == 1) {
                      if (index == levelRanges.length - 1) return SizedBox();
                      if (index == levelRanges.length)
                        return PendantItem(pendantData: levelRanges[index - 1]);
                    }
                    return PendantItem(pendantData: levelRanges[index]);
                  },
                ),
              ],
            ),
          );
        });
  }
}

class PendantItem extends StatelessWidget {
  final LevelRange pendantData;

  const PendantItem({
    Key key,
    @required this.pendantData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.blueGrey,
      child: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Column(
          children: [
            CachedNetworkImage(
              imageUrl: pendantData.frameUrl,
              placeholder: (context, url) => Container(
                height: SizeConfig.safeBlockHorizontal * 76 / 3 * 70 / 64,
                margin: EdgeInsets.symmetric(
                    horizontal:
                        SizeConfig.safeBlockHorizontal * 76 / 3 * 11 / 64),
                decoration: BoxDecoration(
                  color: Color(0xff424242),
                  shape: BoxShape.circle,
                ),
              ),
            ),
            Transform.translate(
              offset: Offset(0, SizeConfig.safeBlockHorizontal * -2),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      bottom: 7,
                    ),
                    padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.safeBlockHorizontal * 3,
                      vertical: SizeConfig.safeBlockHorizontal / 2,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      border: Border.all(
                        color: pendantData.borderColor,
                        width: 1,
                      ),
                      gradient: LinearGradient(
                        begin: pendantData.gradientBegin,
                        end: pendantData.gradientEnd,
                        colors: pendantData.colors,
                      ),
                    ),
                    child: CustomText(
                      "Lv. ?",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  CustomText(
                    "Lv.${pendantData.min}${pendantData.max == null ? "" : " - Lv." + pendantData.max.toString()}",
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
