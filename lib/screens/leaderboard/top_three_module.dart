import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/widgets/basic/image_placeholder.widget.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_config.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_top_item.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_top_item_shimmer.dart';

class TopThreeModule extends StatelessWidget {
  final LeaderboardType type;

  const TopThreeModule({
    Key key,
    this.type,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.safeBlockHorizontal * 92,
      height: SizeConfig.safeBlockHorizontal * 50,
      alignment: Alignment.center,
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Column(
            children: [
              SizedBox(height: SizeConfig.safeBlockHorizontal * 3),
              Spacer(),
              ImagePlaceholderWidget(
                height: SizeConfig.safeBlockHorizontal * 25,
                matchTextDirection: false,
                image: AssetImage(
                  AssetsPath.fromImagesCommon("leaderboard_stand"),
                ),
                fit: BoxFit.fitHeight,
                placeholder: Container(
                  color: Colors.transparent,
                  height: SizeConfig.safeBlockHorizontal * 20,
                ),
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            child: _buildTopThree(),
          ),
        ],
      ),
    );
  }

  Widget _buildTopThree() {
    final _width = SizeConfig.safeBlockHorizontal *
            25 *
            LeaderboardConfig.leaderboardStandSizeRatio /
            3 -
        20;

    ValueNotifier leaderboardDataLoading =
        getIt<LeaderboardProvider>().leaderboardDataLoading;
    ValueNotifier leaderboardData =
        getIt<LeaderboardProvider>().leaderboardData;
    ValueNotifier currentActiveTab =
        getIt<LeaderboardProvider>().currentActiveTab;

    return AnimatedBuilder(
      animation: Listenable.merge([
        leaderboardDataLoading,
        leaderboardData,
        currentActiveTab,
      ]),
      builder: (context, child) {
        return AnimatedOpacity(
          duration: Duration(milliseconds: 300),
          opacity: leaderboardDataLoading.value ? 0.5 : 1,
          child: Container(
            color: Colors.transparent,
            width: SizeConfig.safeBlockHorizontal *
                25 *
                LeaderboardConfig.leaderboardStandSizeRatio,
            child: leaderboardDataLoading.value
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    textDirection: TextDirection.ltr,
                    children: [
                      Transform.translate(
                        offset: Offset(0, -SizeConfig.safeBlockHorizontal * 15),
                        child: LeaderboardTopItemShimmer(
                          width: _width,
                        ),
                      ),
                      Transform.translate(
                        offset: Offset(0, -SizeConfig.safeBlockHorizontal * 20),
                        child: LeaderboardTopItemShimmer(
                          width: _width,
                        ),
                      ),
                      Transform.translate(
                        offset: Offset(0, -SizeConfig.safeBlockHorizontal * 14),
                        child: LeaderboardTopItemShimmer(
                          width: _width,
                        ),
                      ),
                    ],
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    textDirection: TextDirection.ltr,
                    children: [
                      if (leaderboardData.value.length > 1)
                        Container(
                          margin: EdgeInsets.only(
                              bottom: SizeConfig.safeBlockHorizontal * 15),
                          child: LeaderboardTopItemWidget(
                            pendantUrl: leaderboardData.value[1].pendantUrl,
                            userId: leaderboardData.value[1].userId,
                            type: currentActiveTab.value,
                            name: leaderboardData.value[1].name,
                            value: leaderboardData.value[1].point,
                            avatarUrl: leaderboardData.value[1].avatarUrl,
                            width: _width,
                            index: 2,
                          ),
                        ),
                      if (leaderboardData.value.length <= 1)
                        Container(width: _width),
                      if (leaderboardData.value.length > 0)
                        Container(
                          margin: EdgeInsets.only(
                              bottom: SizeConfig.safeBlockHorizontal * 20),
                          child: LeaderboardTopItemWidget(
                            pendantUrl: leaderboardData.value[0].pendantUrl,
                            userId: leaderboardData.value[0].userId,
                            type: currentActiveTab.value,
                            name: leaderboardData.value[0].name,
                            value: leaderboardData.value[0].point,
                            avatarUrl: leaderboardData.value[0].avatarUrl,
                            width: _width,
                            index: 1,
                          ),
                        ),
                      if (leaderboardData.value.length <= 2)
                        Container(width: _width),
                      if (leaderboardData.value.length > 2)
                        Container(
                          margin: EdgeInsets.only(
                              bottom: SizeConfig.safeBlockHorizontal * 14),
                          child: LeaderboardTopItemWidget(
                            pendantUrl: leaderboardData.value[2].pendantUrl,
                            userId: leaderboardData.value[2].userId,
                            type: currentActiveTab.value,
                            name: leaderboardData.value[2].name,
                            value: leaderboardData.value[2].point,
                            avatarUrl: leaderboardData.value[2].avatarUrl,
                            width: _width,
                            index: 3,
                          ),
                        ),
                    ],
                  ),
          ),
        );
      },
    );
  }
}
