import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/leaderboard.item.model.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_item_shimmer.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_room_item.widget.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_user_item.widget.dart';

class LeaderboardListModule extends StatelessWidget {
  final LeaderboardType type;
  final EdgeInsetsGeometry padding;
  final List<Leader> data;
  final bool isLoading;

  const LeaderboardListModule({
    Key key,
    this.padding,
    this.data,
    this.isLoading,
    this.type,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Leader> realData = new List<Leader>.from(data);

    if (realData.length > 3)
      realData.removeRange(0, 3);
    else
      realData = <Leader>[];

    if (isLoading)
      return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        padding: padding,
        shrinkWrap: true,
        itemCount: 6,
        itemBuilder: (ctx, index) {
          return Padding(
            padding:
                EdgeInsets.symmetric(vertical: SizeConfig.safeBlockHorizontal),
            child: LeaderboardItemShimmer(),
          );
        },
      );

    if (realData.length == 0)
      return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        padding: padding,
        shrinkWrap: true,
        itemCount: 10,
        itemBuilder: (ctx, index) {
          return Padding(
            padding:
            EdgeInsets.symmetric(vertical: SizeConfig.safeBlockHorizontal),
            child: LeaderboardUserItemWidget(
              leaderboardType: type,
              // leaderboardData: realData[index],

            ),
          );
        },
      );

    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      padding: padding,
      shrinkWrap: true,
      itemCount: realData.length,
      itemBuilder: (ctx, index) {
        return Padding(
          padding:
              EdgeInsets.symmetric(vertical: SizeConfig.safeBlockHorizontal),
          child: LeaderboardUserItemWidget(
            leaderboardType: type,
            leaderboardData: realData[index],
          ),
        );
      },
    );
  }
}
