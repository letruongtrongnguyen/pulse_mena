import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_config.dart';

const tabBarHeight = 35.0;
const timeRangeHeight = 50.0;

class LeaderboardAppBar extends StatelessWidget implements PreferredSizeWidget {
  final TabController tabController;
  final ScrollController scrollController;

  const LeaderboardAppBar({
    Key key,
    this.tabController,
    this.scrollController,
  }) : super(key: key);

  @override
  Size get preferredSize =>
      Size.fromHeight(kToolbarHeight + tabBarHeight + timeRangeHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: CustomText(
        "common.leaderboard",
        style: Theme.of(context).appBarTheme.textTheme.headline1.merge(
              TextStyle(
                color: Colors.white,
              ),
            ),
      ),
      centerTitle: true,
      leading: GestureDetector(
        onTap: () {
          getIt<LeaderboardProvider>().fetchLeaderboardTopThreeData();
          Navigator.pop(context);
        },
        child: Container(
          color: Colors.transparent,
          alignment: AlignmentDirectional.centerStart,
          padding: EdgeInsetsDirectional.fromSTEB(
            SizeConfig.safeBlockHorizontal * 4,
            SizeConfig.safeBlockHorizontal * 4.5,
            0,
            SizeConfig.safeBlockHorizontal * 4.5,
          ),
          // color: Colors.blueGrey,
          child: SvgPicture.asset(
            AssetsPath.fromImagesSvg("arrow_back_ic"),
            matchTextDirection: true,
          ),
        ),
      ),
      // actions: [
      //   GestureDetector(
      //     onTap: () {
      //       getIt<LeaderboardProvider>().fetchLeaderboardData();
      //     },
      //     child: Container(
      //       color: Colors.transparent,
      //       alignment: AlignmentDirectional.centerEnd,
      //       padding: EdgeInsetsDirectional.fromSTEB(
      //         SizeConfig.safeBlockHorizontal * 4,
      //         SizeConfig.safeBlockHorizontal * 4.2,
      //         SizeConfig.safeBlockHorizontal * 4,
      //         SizeConfig.safeBlockHorizontal * 4.2,
      //       ),
      //       // color: Colors.blueGrey,
      //       child: SvgPicture.asset(
      //         AssetsPath.fromImagesSvg("reload_ic"),
      //       ),
      //     ),
      //   ),
      // ],
      automaticallyImplyLeading: false,
      backgroundColor: Colors.transparent,
      shadowColor: Colors.transparent,
      bottom: LeaderboardAppBarBottom(
        tabController: tabController,
        listViewScroller: scrollController,
      ),
    );
  }
}

class LeaderboardAppBarBottom extends StatefulWidget
    implements PreferredSizeWidget {
  final TabController tabController;
  final ScrollController listViewScroller;

  const LeaderboardAppBarBottom({
    Key key,
    this.tabController,
    this.listViewScroller,
  }) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(tabBarHeight + timeRangeHeight);

  @override
  _LeaderboardAppBarBottomState createState() =>
      _LeaderboardAppBarBottomState();
}

class _LeaderboardAppBarBottomState extends State<LeaderboardAppBarBottom>
    with TickerProviderStateMixin {
  // TickerProviderStateMixin allows the fade out/fade in animation when changing the active button

  // this will control the button clicks and tab changing
  TabController _controller;

  // this will control the animation when a button changes from an off state to an on state
  AnimationController _animationControllerOn;

  // this will control the animation when a button changes from an on state to an off state
  AnimationController _animationControllerOff;

  // this will give the background color values of a button when it changes to an on state
  Animation _colorTweenBackgroundOn;
  Animation _colorTweenBackgroundOff;

  // this will give the foreground color values of a button when it changes to an on state
  Animation _colorTweenForegroundOn;
  Animation _colorTweenForegroundOff;

  // when swiping, the _controller.index value only changes after the animation, therefore, we need this to trigger the animations and save the current index
  int _currentIndex = getIt<LeaderboardProvider>().currentIndex;

  // saves the previous active tab
  int _prevControllerIndex = getIt<LeaderboardProvider>().prevControllerIndex;

  // saves the value of the tab animation. For example, if one is between the 1st and the 2nd tab, this value will be 0.5
  double _aniValue = getIt<LeaderboardProvider>().aniValue;

  // saves the previous value of the tab animation. It's used to figure the direction of the animation
  double _prevAniValue = getIt<LeaderboardProvider>().prevAniValue;

  // active button's foreground color
  Color _foregroundOn = Colors.black;
  Color _foregroundOff = Colors.white;

  // active button's background color
  Color _backgroundOn = Colors.white;
  Color _backgroundOff = Colors.white.withOpacity(0.4);

  // scroll controller for the TabBar
  ScrollController _scrollController = new ScrollController();

  // this will save the keys for each Tab in the Tab Bar, so we can retrieve their position and size for the scroll controller
  List _keys = [];

  // regist if the the button was tapped
  bool _buttonTap = false;

  @override
  void initState() {
    super.initState();

    for (int index = 0; index < LeaderboardType.values.length; index++) {
      // create a GlobalKey for each Tab
      _keys.add(new GlobalKey());
    }

    // this creates the controller with tabs
    _controller =
        TabController(vsync: this, length: LeaderboardType.values.length);
    // this will execute the function every time there's a swipe animation
    _controller.animation.addListener(_handleTabAnimation);
    // this will execute the function every time the _controller.index value changes
    _controller.addListener(_handleTabChange);

    _animationControllerOff =
        AnimationController(vsync: this, duration: Duration(milliseconds: 75));
    // so the inactive buttons start in their "final" state (color)
    _animationControllerOff.value = 1.0;
    _colorTweenBackgroundOff =
        ColorTween(begin: _backgroundOn, end: _backgroundOff)
            .animate(_animationControllerOff);
    _colorTweenForegroundOff =
        ColorTween(begin: _foregroundOn, end: _foregroundOff)
            .animate(_animationControllerOff);

    _animationControllerOn =
        AnimationController(vsync: this, duration: Duration(milliseconds: 150));
    // so the inactive buttons start in their "final" state (color)
    _animationControllerOn.value = 1.0;
    _colorTweenBackgroundOn =
        ColorTween(begin: _backgroundOff, end: _backgroundOn)
            .animate(_animationControllerOn);
    _colorTweenForegroundOn =
        ColorTween(begin: _foregroundOff, end: _foregroundOn)
            .animate(_animationControllerOn);
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _scrollTo(_currentIndex));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _buildTabBar(),
        Container(
          height: timeRangeHeight,
          child: ValueListenableBuilder(
              valueListenable: getIt<LeaderboardProvider>().currentActiveTab,
              builder: (context, currentTab, child) {
                return Visibility(
                  // visible:
                  //     currentTab == LeaderboardType.TopRoomGift ? false : true,
                  child: TimeFrameModule(
                    tabController: widget.tabController,
                    listViewScroller: widget.listViewScroller,
                  ),
                );
              }),
        ),
      ],
    );
  }

  Widget _buildTabBar() {
    return Container(
      height: tabBarHeight,
      // this generates our tabs buttons
      child: ListView.builder(
        // this gives the TabBar a bounce effect when scrolling farther than it's size
        physics: BouncingScrollPhysics(),
        reverse:
            getIt<GeneralProvider>().textDirection.value == TextDirection.rtl,
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.safeBlockHorizontal * 3,
        ),
        controller: _scrollController,
        // make the list horizontal
        scrollDirection: Axis.horizontal,
        // number of tabs
        itemCount: LeaderboardType.values.length,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            // each button's key
            key: _keys[index],
            // padding for the buttons
            padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.safeBlockHorizontal * 1),
            child: AnimatedBuilder(
              animation: _colorTweenBackgroundOn,
              builder: (context, child) {
                return GestureDetector(
                  onTap: () {
                    getIt<LeaderboardProvider>()
                        .onActiveTabChange(LeaderboardType.values[index]);
                    if (widget.listViewScroller.hasClients)
                      widget.listViewScroller.animateTo(
                        widget.listViewScroller.position.minScrollExtent,
                        duration: Duration(milliseconds: 400),
                        curve: Curves.easeOut,
                      );
                    setState(() {
                      _buttonTap = true;
                      // trigger the controller to change between Tab Views
                      // _controller.animateTo(index);
                      // set the current index
                      _setCurrentIndex(index);
                      // scroll to the tapped button (needed if we tap the active button and it's not on its position)
                      // _scrollTo(index);
                    });
                  },
                  child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    decoration: BoxDecoration(
                      color: _getBackgroundColor(index),
                      borderRadius: BorderRadius.circular(60),
                    ),
                    child: CustomText(
                      LeaderboardConfig
                              .itemConfig[LeaderboardType.values[index]]
                          ["tabTitle"],
                      style: TextStyle(
                        color: _getForegroundColor(index),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }

  // runs during the switching tabs animation
  _handleTabAnimation() {
    // gets the value of the animation. For example, if one is between the 1st and the 2nd tab, this value will be 0.5
    _aniValue = _controller.animation.value;
    getIt<LeaderboardProvider>().setAniValue(_controller.animation.value);

    // if the button wasn't pressed, which means the user is swiping, and the amount swipped is less than 1 (this means that we're swiping through neighbor Tab Views)
    if (!_buttonTap && ((_aniValue - _prevAniValue).abs() < 1)) {
      // set the current tab index
      _setCurrentIndex(_aniValue.round());
    }

    // save the previous Animation Value
    _prevAniValue = _aniValue;
    getIt<LeaderboardProvider>().setPrevAniValue(_aniValue);
  }

  // runs when the displayed tab changes
  _handleTabChange() {
    // if a button was tapped, change the current index
    if (_buttonTap) _setCurrentIndex(_controller.index);

    // this resets the button tap
    if ((_controller.index == _prevControllerIndex) ||
        (_controller.index == _aniValue.round())) _buttonTap = false;

    // save the previous controller index
    _prevControllerIndex = _controller.index;
    getIt<LeaderboardProvider>().setPrevControllerIndex(_controller.index);
  }

  _setCurrentIndex(int index) {
    // if we're actually changing the index
    if (index != _currentIndex) {
      setState(() {
        // change the index
        _currentIndex = index;
      });
      getIt<LeaderboardProvider>().setCurrentIndex(index);

      // trigger the button animation
      _triggerAnimation();
      // scroll the TabBar to the correct position (if we have a scrollable bar)
      _scrollTo(index);
    }
  }

  _triggerAnimation() {
    // reset the animations so they're ready to go
    _animationControllerOn.reset();
    _animationControllerOff.reset();

    // run the animations!
    _animationControllerOn.forward();
    _animationControllerOff.forward();
  }

  _scrollTo(int index) {
    // get the screen width. This is used to check if we have an element off screen
    double screenWidth = SizeConfig.screenWidth;

    // get the button we want to scroll to
    RenderBox renderBox = _keys[index].currentContext.findRenderObject();
    // get its size
    double size = renderBox.size.width;
    // and position
    double position = renderBox.localToGlobal(Offset.zero).dx;

    // this is how much the button is away from the center of the screen and how much we must scroll to get it into place
    double offset = (position + size / 2) - screenWidth / 2;

    // if the button is to the left of the middle
    if (offset < 0) {
      // get the first button
      renderBox = _keys[0].currentContext.findRenderObject();
      // get the position of the first button of the TabBar
      position = renderBox.localToGlobal(Offset.zero).dx;

      // if the offset pulls the first button away from the left side, we limit that movement so the first button is stuck to the left side
      if (position > offset)
        offset = position - SizeConfig.safeBlockHorizontal * 3;
    } else {
      // if the button is to the right of the middle

      // get the last button
      renderBox = _keys[LeaderboardType.values.length - 1]
          .currentContext
          .findRenderObject();
      // get its position
      position = renderBox.localToGlobal(Offset.zero).dx;

      // and size
      size = renderBox.size.width;

      // if the last button doesn't reach the right side, use it's right side as the limit of the screen for the TabBar
      if (position + size < screenWidth)
        screenWidth = position + size + SizeConfig.safeBlockHorizontal * 3;

      // if the offset pulls the last button away from the right side limit, we reduce that movement so the last button is stuck to the right side limit
      if (position + size - offset < screenWidth) {
        offset =
            position + size - screenWidth + SizeConfig.safeBlockHorizontal * 3;
      }
    }

    // scroll the calculated ammount
    _scrollController.animateTo(
      offset + _scrollController.offset,
      duration: new Duration(milliseconds: 150),
      curve: Curves.easeInOut,
    );
  }

  _getBackgroundColor(int index) {
    if (index == _currentIndex) {
      // if it's active button
      return _colorTweenBackgroundOn.value;
    } else if (index == _prevControllerIndex) {
      // if it's the previous active button
      return _colorTweenBackgroundOff.value;
    } else {
      // if the button is inactive
      return _backgroundOff;
    }
  }

  _getForegroundColor(int index) {
    // the same as the above
    if (index == _currentIndex) {
      return _colorTweenForegroundOn.value;
    } else if (index == _prevControllerIndex) {
      return _colorTweenForegroundOff.value;
    } else {
      return _foregroundOff;
    }
  }
}

class TimeFrameModule extends StatefulWidget {
  final TabController tabController;
  final ScrollController listViewScroller;

  const TimeFrameModule({
    Key key,
    this.tabController,
    this.listViewScroller,
  }) : super(key: key);

  @override
  _TimeFrameModuleState createState() => _TimeFrameModuleState();
}

class _TimeFrameModuleState extends State<TimeFrameModule> {
  @override
  void initState() {
    widget.tabController.animation
      ..addListener(() {
        getIt<LeaderboardProvider>().onTimeFrameChange(
            LeaderboardTimeFrameType.values[widget.tabController.index]);
        if (widget.listViewScroller.hasClients)
          widget.listViewScroller.animateTo(
            widget.listViewScroller.position.minScrollExtent,
            duration: Duration(milliseconds: 250),
            curve: Curves.easeOut,
          );
      });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ValueNotifier _timeFrame = getIt<LeaderboardProvider>().timeFrame;
    return Theme(
      data: ThemeData(
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
        fontFamily: 'Euclid Circular A',
        textTheme: Theme.of(context).textTheme.apply(
              bodyColor: Colors.white,
              displayColor: Colors.white,
              fontFamily: 'Euclid Circular A',
            ),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: TabBar(
          controller: widget.tabController,
          indicator: _TabIndicator(
            color: Colors.white,
            width: 20,
            height: 2.5,
          ),
          labelPadding: EdgeInsets.zero,
          isScrollable: true,
          indicatorPadding: EdgeInsets.zero,
          tabs: [
            AnimatedBuilder(
              animation: Listenable.merge([_timeFrame]),
              builder: (context, child) {
                return TimeFrameItem(
                  key: Key("LeaderboardTimeFrameMonthly"),
                  title: "common.monthly",
                  isActive:
                      _timeFrame.value == LeaderboardTimeFrameType.Monthly,
                  tabValue: LeaderboardTimeFrameType.Monthly,
                  tabController: widget.tabController,
                );
              },
            ),
            AnimatedBuilder(
              animation: Listenable.merge([_timeFrame]),
              builder: (context, child) {
                return TimeFrameItem(
                  key: Key("LeaderboardTimeFrameWeekly"),
                  title: "common.weekly",
                  isActive: _timeFrame.value == LeaderboardTimeFrameType.Weekly,
                  tabValue: LeaderboardTimeFrameType.Weekly,
                  tabController: widget.tabController,
                );
              },
            ),
            AnimatedBuilder(
              animation: Listenable.merge([_timeFrame]),
              builder: (context, child) {
                return TimeFrameItem(
                  key: Key("LeaderboardTimeFrameDaily"),
                  title: "common.daily",
                  isActive: _timeFrame.value == LeaderboardTimeFrameType.Daily,
                  tabValue: LeaderboardTimeFrameType.Daily,
                  tabController: widget.tabController,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class TimeFrameItem extends StatelessWidget {
  final bool isActive;
  final String title;
  final LeaderboardTimeFrameType tabValue;
  final double width;
  final bool isLoading;
  final TabController tabController;

  const TimeFrameItem({
    Key key,
    this.isActive,
    this.title,
    this.tabValue,
    this.width,
    this.isLoading = false,
    this.tabController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color _textColor = isActive ? Colors.white : Colors.white.withOpacity(0.6);

    return AnimatedContainer(
      duration: Duration(milliseconds: 250),
      padding: EdgeInsets.symmetric(
        horizontal: 10,
      ),
      // color: Colors.transparent,
      child: CustomText(
        title,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: _textColor,
          fontSize: 14,
        ),
      ),
    );
  }
}

class _TabIndicator extends Decoration {
  final BoxPainter _painter;

  _TabIndicator({
    @required Color color,
    @required double width,
    @required double height,
  }) : _painter = _TabIndicatorPainter(color, width, height);

  @override
  BoxPainter createBoxPainter([onChanged]) => _painter;
}

class _TabIndicatorPainter extends BoxPainter {
  final Paint _paint;
  final double width;
  final double height;

  _TabIndicatorPainter(Color color, this.width, this.height)
      : _paint = Paint()
          ..color = color
          ..isAntiAlias = true;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration cfg) {
    final Offset circleOffset = offset +
        Offset(cfg.size.width / 2 - (width / 2), cfg.size.height - height);
    canvas.drawRRect(
        RRect.fromRectAndRadius(
            circleOffset & Size(width, height), Radius.circular(50)),
        _paint);
  }
}
