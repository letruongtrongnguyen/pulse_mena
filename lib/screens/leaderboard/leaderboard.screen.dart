import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/leaderboard.provider.dart';
import 'package:flutterbase/screens/leaderboard/leaderboard_app_bar/leaderboard_app_bar.dart';
import 'package:flutterbase/widgets/basic/fade_on_scroll.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_config.dart';
import 'package:flutterbase/widgets/leaderboard/leaderboard_my_rank.widget.dart';

import 'leaderboard_list_module.dart';
import 'top_three_module.dart';

class LeaderboardScreen extends StatefulWidget {
  @override
  _LeaderboardScreenState createState() => _LeaderboardScreenState();
}

class _LeaderboardScreenState extends State<LeaderboardScreen>
    with SingleTickerProviderStateMixin {
  final ScrollController scrollController =
      ScrollController(initialScrollOffset: 0);
  TabController _tabController;

  @override
  void initState() {
    int index = LeaderboardTimeFrameType.values
        .indexOf(getIt<LeaderboardProvider>().timeFrame.value);

    _tabController = new TabController(
        initialIndex: index,
        vsync: this,
        length: LeaderboardTimeFrameType.values.length);

    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      getIt<CommunicateService>().requestHideNavigationBar();

      getIt<LeaderboardProvider>().fetchLeaderboardData();
    });
  }

  @override
  void dispose() {
    getIt<CommunicateService>().requestShowNavigationBar();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ValueNotifier leaderboardDataLoading =
        getIt<LeaderboardProvider>().leaderboardDataLoading;
    ValueNotifier leaderboardData =
        getIt<LeaderboardProvider>().leaderboardData;

    double myRankHeight = 75.0;

    Future<void> _onRefresh() async {
      await getIt<LeaderboardProvider>().fetchLeaderboardData();
    }

    return ValueListenableBuilder(
      valueListenable: getIt<LeaderboardProvider>().currentActiveTab,
      builder: (context, currentActiveTab, child) {
        return Container(
          decoration: BoxDecoration(
            gradient: LeaderboardConfig.itemConfig[currentActiveTab]
                ["gradient"],
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: LeaderboardAppBar(
              tabController: _tabController,
              scrollController: scrollController,
            ),
            body: Stack(
              children: [
                Positioned(
                  bottom: 0,
                  child: BlackBackgroundBouncing(
                    scrollController: scrollController,
                  ),
                ),
                RefreshIndicator(
                  onRefresh: _onRefresh,
                  color: Theme.of(context).iconTheme.color,
                  displacement: 20,
                  child: AnimatedBuilder(
                    animation: Listenable.merge([
                      leaderboardData,
                      leaderboardDataLoading,
                    ]),
                    builder: (context, snapshot) {
                      // double listViewPaddingBottom =
                      //     currentActiveTab != LeaderboardType.TopGiftReceiver
                      //         ? 75 +
                      //             SizeConfig.paddingBottom +
                      //             SizeConfig.safeBlockHorizontal * 1
                      //         : SizeConfig.paddingBottom +
                      //             SizeConfig.safeBlockHorizontal * 1;
                      double listViewPaddingBottom = 75 +
                          SizeConfig.paddingBottom +
                          SizeConfig.safeBlockHorizontal * 1;

                      double remainSpaceHeight = SizeConfig.screenHeight -
                          (kToolbarHeight +
                              SizeConfig.paddingTop +
                              tabBarHeight +
                              timeRangeHeight +
                              SizeConfig.safeBlockHorizontal * 50);

                      double listViewHeight =
                          (leaderboardData.value.length - 3) *
                                  SizeConfig.safeBlockHorizontal *
                                  20 +
                              SizeConfig.safeBlockHorizontal +
                              listViewPaddingBottom;

                      if (leaderboardData.value.length == 0 &&
                          !leaderboardDataLoading.value) {
                        return Positioned.fill(
                          child: Container(
                            color:
                                Theme.of(context).brightness == Brightness.dark
                                    ? Theme.of(context).primaryColor
                                    : Theme.of(context).scaffoldBackgroundColor,
                            padding: EdgeInsets.only(
                              bottom: currentActiveTab !=
                                      LeaderboardType.TopGiftReceiver
                                  ? myRankHeight
                                  : 0,
                            ),
                            child: SingleChildScrollView(
                              physics: AlwaysScrollableScrollPhysics(),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  SizedBox(
                                    height: SizeConfig.safeBlockVertical * 20,
                                  ),
                                  Image.asset(
                                    AssetsPath.fromImagesCharacterAction(
                                      "haki_blink",
                                    ),
                                    fit: BoxFit.fitWidth,
                                    width: SizeConfig.safeBlockHorizontal * 40,
                                  ),
                                  CustomText(
                                    "leaderboard.emptyMessage",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12),
                                    textAlign: TextAlign.center,
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      }

                      return CustomScrollView(
                        physics: AlwaysScrollableScrollPhysics(),
                        controller: scrollController,
                        slivers: [
                          SliverToBoxAdapter(
                            child: FadeOnScrollWidget(
                              scrollController: scrollController,
                              zeroOpacityOffset:
                                  SizeConfig.safeBlockHorizontal * 50,
                              child: TopThreeModule(
                                type: LeaderboardType.TopBuyer,
                              ),
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: Container(
                              color: Colors.black,
                              width: SizeConfig.screenWidth,
                              height: remainSpaceHeight - listViewHeight < 0
                                  ? null
                                  : remainSpaceHeight,
                              child: LeaderboardListModule(
                                padding: EdgeInsetsDirectional.fromSTEB(
                                  SizeConfig.safeBlockHorizontal * 4,
                                  SizeConfig.safeBlockHorizontal,
                                  SizeConfig.safeBlockHorizontal * 4,
                                  listViewPaddingBottom,
                                ),
                                type: currentActiveTab,
                                data: leaderboardData.value,
                                isLoading: leaderboardDataLoading.value,
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
                AnimatedPositioned(
                  duration: Duration(milliseconds: 250),
                  bottom: 0,
                  // bottom: currentActiveTab != LeaderboardType.TopGiftReceiver
                  //     ? 0
                  //     : -(myRankHeight + SizeConfig.paddingBottom),
                  child: LeaderboardMyRankWidget(
                    height: myRankHeight,
                    leaderboardType: currentActiveTab,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class BlackBackgroundBouncing extends StatefulWidget {
  final ScrollController scrollController;

  const BlackBackgroundBouncing({
    Key key,
    this.scrollController,
  }) : super(key: key);

  @override
  _BlackBackgroundBouncingState createState() =>
      _BlackBackgroundBouncingState();
}

class _BlackBackgroundBouncingState extends State<BlackBackgroundBouncing> {
  double _height = 5;

  _scrollListener() {
    if (widget.scrollController.offset > 0 &&
        widget.scrollController.position.outOfRange) {
      setState(() {
        _height = widget.scrollController.offset + 5;
      });
    }
    if (!widget.scrollController.position.outOfRange) {
      setState(() {
        _height = 5;
      });
    }
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    widget.scrollController.addListener(_scrollListener);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.screenWidth,
      height: _height,
      color: Colors.black,
    );
  }
}
