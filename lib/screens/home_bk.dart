import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/screens/room/room.screen.dart';

class HomeScreenBK extends StatefulWidget {
  @override
  _HomeScreenBKState createState() => _HomeScreenBKState();
}

class _HomeScreenBKState extends State<HomeScreenBK> {
  @override
  void initState() {
    // getIt<MeProvider>().fetchUserData();
    // getIt<RoomProvider>().fetchMyRoomData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getIt<CommunicateService>().context = context;
    ValueNotifier meLoading = getIt<MeProvider>().loading;
    ValueNotifier myRoomLoading = getIt<RoomProvider>().myRoomLoading;

    return AnimatedBuilder(
      animation: Listenable.merge([myRoomLoading, meLoading]),
      builder: (ctx, child) {
        if (meLoading.value || myRoomLoading.value)
          return Center(
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Color(0xff00FFE0)),
            ),
          );

        return Container();
      },
    );

    return Scaffold(
      body: Container(
        color: Colors.black,
        padding: EdgeInsets.fromLTRB(
          SizeConfig.paddingLeft,
          SizeConfig.paddingTop,
          SizeConfig.paddingRight,
          SizeConfig.paddingBottom,
        ),
        child: Container(
          child: Column(
            children: [
              // UserWidget(),
              Divider(
                color: Colors.white,
                height: 50,
              ),
              // CoinWidget(),
            ],
          ),
        ),
      ),
    );
  }
}
