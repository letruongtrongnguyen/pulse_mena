import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/widgets/avatar/avatar.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class SystemMessageItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsetsDirectional.only(
            end: SizeConfig.safeBlockHorizontal * 3,
          ),
          child: AvatarWidget(
            avatarUrl:
                "https://s3-alpha-sig.figma.com/img/fb49/b1e2/8eaf82b907fcdd2eca0ee3a402e559a4?Expires=1615161600&Signature=U3XkByNV-csss~98xk~YRZxszuT61TayKMORU0~HMz10ocmv~HN~Qg8sZYAiuiqpa4lRExxtEWiSoe4ZH0ulqpxPXAork2zq364gSPwb4iay7FUynZMBbomn9sdbzsgx~Vm5DZgvt5sPMQ7wSCSYzekAQ6lUpGJBREmkEwXS6wZsWReA1lhPHkvXGiPDrCm7qA3QO~yUxNEqR8vCzP98qPQYtJqpjoSro8wpc20vdBZOUN0pYlPpFEdngcDet2Ky0a-zlOop6nCdyc8~ZdTjJ~Gj6jHpVIvEQ1rGoiXlGTI39u0IVbpJydGAoJDVR~jil5QQgtNUepYNa7QhnKpjQg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
            radius: SizeConfig.safeBlockHorizontal * 6,
            borderWidth: 0,
          ),
        ),
        Container(
          decoration: BoxDecoration(
            color: Color(0xff303030),
            borderRadius: BorderRadius.circular(8),
          ),
          width: SizeConfig.safeBlockHorizontal * 65,
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.safeBlockHorizontal * 5,
            vertical: SizeConfig.safeBlockHorizontal * 3,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  bottom: 15,
                ),
                child: CustomText(
                  "New Version 1.2",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12,
                  ),
                ),
              ),
              CustomText(
                "Dear users, we have updated new features in this version. Now you can: - Send Items to your friends - Play Lucky Haki game while joining room",
                style: TextStyle(
                  fontSize: 12,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
