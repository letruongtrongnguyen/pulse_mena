import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/screens/system_messages/system_message_item.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class SystemMessagesScreen extends StatefulWidget {
  @override
  _SystemMessagesScreenState createState() => _SystemMessagesScreenState();
}

class _SystemMessagesScreenState extends State<SystemMessagesScreen> {
  @override
  void initState() {
    getIt<CommunicateService>().requestHideNavigationBar();
    super.initState();
  }

  @override
  void dispose() {
    getIt<CommunicateService>().requestShowNavigationBar();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          "notification.session.system",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        automaticallyImplyLeading: false,
        centerTitle: true,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: Colors.transparent,
            alignment: AlignmentDirectional.centerStart,
            padding: EdgeInsets.fromLTRB(
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4.5,
              0,
              SizeConfig.safeBlockHorizontal * 4.5,
            ),
            // color: Colors.blueGrey,
            child: SvgPicture.asset(
              AssetsPath.fromImagesSvg("arrow_back_ic"),
            ),
          ),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(
          SizeConfig.safeBlockHorizontal * 4,
          SizeConfig.safeBlockHorizontal * 6,
          SizeConfig.safeBlockHorizontal * 4,
          SizeConfig.safeBlockHorizontal * 6 + SizeConfig.paddingBottom,
        ),
        children: [
          SystemMessageItem(),
        ],
      ),
    );
  }
}
