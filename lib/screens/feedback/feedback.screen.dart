import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/feedback.model.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/@services/services/feedback.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/i18n/i18n.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/widgets/basic/popup/center_popup.widget.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class FeedbackScreen extends StatefulWidget {
  @override
  _FeedbackScreenState createState() => _FeedbackScreenState();
}

class _FeedbackScreenState extends State<FeedbackScreen> {
  ScrollController _scrollController = new ScrollController();
  final GlobalKey<FormState> _formKey = GlobalKey();
  FocusNode _focusNodeOpinion = new FocusNode();
  FocusNode _focusNodeContact = new FocusNode();
  final opinionController = TextEditingController();
  final contactController = TextEditingController();
  String opinion = "";
  String contact = "";

  @override
  void initState() {
    getIt<CommunicateService>().requestHideNavigationBar();
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _scrollController.jumpTo(
        _scrollController.position.maxScrollExtent,
      );
    });
  }

  @override
  void dispose() {
    getIt<CommunicateService>().requestShowNavigationBar();
    super.dispose();
  }

  clearTextInput() {
    opinionController.clear();
    contactController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          "common.feedback",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        automaticallyImplyLeading: false,
        centerTitle: true,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: Colors.transparent,
            alignment: AlignmentDirectional.centerStart,
            padding: EdgeInsetsDirectional.fromSTEB(
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4.5,
              0,
              SizeConfig.safeBlockHorizontal * 4.5,
            ),
            // color: Colors.blueGrey,
            child: SvgPicture.asset(
              AssetsPath.fromImagesSvg("arrow_back_ic"),
              matchTextDirection: true,
            ),
          ),
        ),
      ),
      body: Builder(builder: (context) {
        return GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: ListView(
                  controller: _scrollController,
                  reverse: true,
                  shrinkWrap: true,
                  padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.safeBlockHorizontal * 4,
                    vertical: 10,
                  ),
                  children: [
                    CustomText(
                      "feedback.subtitle",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                              top: 20,
                              bottom: 10,
                            ),
                            child: CustomText(
                              'feedback.inputTitle.opinion',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          TextFormField(
                            controller: opinionController,
                            autofocus: false,
                            focusNode: _focusNodeOpinion,
                            textCapitalization: TextCapitalization.none,
                            textInputAction: TextInputAction.newline,
                            keyboardType: TextInputType.multiline,
                            cursorColor: Color(0xff00FFE0),
                            cursorRadius: Radius.circular(8),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 13,
                            ),
                            minLines: 10,
                            maxLines: 10,
                            validator: (opinion) {
                              if (opinion.isEmpty)
                                return 'Enter your opinion';
                              else
                                return null;
                            },
                            onSaved: (opinion) => opinion = opinion,
                            decoration: InputDecoration(
                              hintText:
                                  I18n.get("feedback.inputPlaceholder.opinion"),
                              contentPadding: EdgeInsets.all(15),
                              border: InputBorder.none,
                              enabledBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.red),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.red),
                              ),
                            ),
                            onFieldSubmitted: (value) {
                              setState(() {
                                opinion = value;
                              });
                            },
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 20,
                              bottom: 10,
                            ),
                            child: CustomText(
                              'feedback.inputTitle.contact',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          TextFormField(
                            controller: contactController,
                            autofocus: false,
                            focusNode: _focusNodeContact,
                            textCapitalization: TextCapitalization.none,
                            cursorColor: Color(0xff00FFE0),
                            cursorRadius: Radius.circular(8),
                            keyboardType: TextInputType.emailAddress,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 13,
                            ),
                            minLines: 2,
                            maxLines: 2,
                            validator: (contact) {
                              Pattern pattern =
                                  r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
                              RegExp regex = new RegExp(pattern);
                              if (!regex.hasMatch(contact))
                                return 'Invalid email';
                              else
                                return null;
                            },
                            onSaved: (contact) => contact = contact,
                            decoration: InputDecoration(
                              hintText:
                                  I18n.get("feedback.inputPlaceholder.contact"),
                              contentPadding: EdgeInsets.all(15),
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.red),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.red),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                            ),
                            onFieldSubmitted: (value) {
                              setState(() {
                                opinion = value;
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        top: 20,
                        bottom: 10,
                      ),
                      child: CustomText(
                        'feedback.content',
                        style: TextStyle(
                          fontSize: 13,
                          color: Color(0xff7F7F7F),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.paddingBottom +
                          SizeConfig.safeBlockHorizontal * 6 +
                          70,
                    )
                  ].reversed.toList(),
                ),
              ),
              Positioned(
                bottom: 0,
                child: Container(
                  padding: EdgeInsets.only(
                    top: 20,
                    bottom: SizeConfig.paddingBottom + 20,
                  ),
                  width: SizeConfig.blockSizeHorizontal * 100,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Color(0xff191919),
                    border: Border(
                      top: BorderSide(
                        width: 1,
                        color: Color(0xff424242),
                      ),
                    ),
                  ),
                  child: ValueListenableBuilder(
                      valueListenable:
                          getIt<GeneralProvider>().feedbackIsSending,
                      builder: (context, isSending, child) {
                        return GestureDetector(
                          onTap: () async {
                            if (isSending) return;

                            if (!_formKey.currentState.validate()) {
                              return _formKey.currentState.save();
                            }

                            final res =
                                await getIt<GeneralProvider>().sendFeedback(
                              data: FeedbackModel(
                                email: contactController.value.text,
                                content: opinionController.value.text,
                              ),
                            );

                            if (res)
                              _openThankYouPopup(context);
                            else {
                              Scaffold.of(context).showSnackBar(
                                SnackBar(
                                  behavior: SnackBarBehavior.floating,
                                  margin: EdgeInsets.fromLTRB(
                                    SizeConfig.safeBlockHorizontal * 4,
                                    0,
                                    SizeConfig.safeBlockHorizontal * 4,
                                    SizeConfig.paddingBottom +
                                        SizeConfig.safeBlockHorizontal * 6 +
                                        60,
                                  ),
                                  content: Text(
                                    'Something error! Please try again.',
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                  backgroundColor: Colors.grey,
                                ),
                              );
                            }
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              vertical: SizeConfig.safeBlockHorizontal * 3,
                              horizontal: SizeConfig.safeBlockHorizontal * 20,
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(60),
                              gradient: LinearGradient(
                                colors: isSending
                                    ? [Colors.grey, Colors.grey]
                                    : [Color(0xff81ffca), Color(0xff2cb7ff)],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                              ),
                            ),
                            child: CustomText(
                              isSending ? "common.sending" : "common.send",
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        );
                      }),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }

  void _openThankYouPopup(BuildContext context) {
    showCenterPopup(
      context: context,
      width: SizeConfig.safeBlockHorizontal * 92,
      height: SizeConfig.safeBlockVertical * 100,
      backgroundColor: Colors.transparent,
      isDismissible: false,
      borderRadius: BorderRadius.circular(20),
      barrierColor: Colors.black.withOpacity(0.9),
      content: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(
              alignment: Alignment.center,
              children: [
                Image.asset(AssetsPath.fromImagesCommon("feedback_thankyou"),
                    height: SizeConfig.safeBlockHorizontal * 50),
                Positioned(
                  bottom: SizeConfig.safeBlockHorizontal * 7,
                  child: CustomText(
                    "feedback.thankYouPopup.title",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 19,
                      shadows: <Shadow>[
                        Shadow(
                          offset: Offset(0, 2),
                          blurRadius: 4.0,
                          color: Colors.black.withOpacity(0.2),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            CustomText(
              "feedback.thankYouPopup.content",
              style: TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: EdgeInsets.only(
                top: SizeConfig.safeBlockVertical * 5,
                bottom: SizeConfig.safeBlockVertical * 20,
              ),
              child: GestureDetector(
                onTap: () {
                  clearTextInput();
                  Navigator.pop(context);
                },
                child: Container(
                  width: SizeConfig.safeBlockHorizontal * 40,
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(
                    vertical: SizeConfig.safeBlockHorizontal * 2,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(60),
                    gradient: LinearGradient(
                      colors: [Color(0xff81ffca), Color(0xff2cb7ff)],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  ),
                  child: CustomText(
                    "common.ok",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
