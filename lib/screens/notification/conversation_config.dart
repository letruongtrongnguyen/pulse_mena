import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/core/utils/assets_path.dart';

enum ConversationType {
  SystemMessage,
  FriendRequest,
  ReceivedGift,
  LuckyHaki,
}

class ConversationConfig {
  static Map<ConversationType, Map<String, dynamic>> conversationConfig() {
    return {
      ConversationType.SystemMessage: {
        "title": "notification.session.system",
        "navigator": "/system-messages-screen",
        "avatarUrl":
            "https://s3-alpha-sig.figma.com/img/fb49/b1e2/8eaf82b907fcdd2eca0ee3a402e559a4?Expires=1615161600&Signature=U3XkByNV-csss~98xk~YRZxszuT61TayKMORU0~HMz10ocmv~HN~Qg8sZYAiuiqpa4lRExxtEWiSoe4ZH0ulqpxPXAork2zq364gSPwb4iay7FUynZMBbomn9sdbzsgx~Vm5DZgvt5sPMQ7wSCSYzekAQ6lUpGJBREmkEwXS6wZsWReA1lhPHkvXGiPDrCm7qA3QO~yUxNEqR8vCzP98qPQYtJqpjoSro8wpc20vdBZOUN0pYlPpFEdngcDet2Ky0a-zlOop6nCdyc8~ZdTjJ~Gj6jHpVIvEQ1rGoiXlGTI39u0IVbpJydGAoJDVR~jil5QQgtNUepYNa7QhnKpjQg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
      },
      ConversationType.FriendRequest: {
        "title": "notification.session.friendRequests",
        "navigator": "/friend-requests-screen",
        "avatarUrl":
            "https://s3-alpha-sig.figma.com/img/fb49/b1e2/8eaf82b907fcdd2eca0ee3a402e559a4?Expires=1615161600&Signature=U3XkByNV-csss~98xk~YRZxszuT61TayKMORU0~HMz10ocmv~HN~Qg8sZYAiuiqpa4lRExxtEWiSoe4ZH0ulqpxPXAork2zq364gSPwb4iay7FUynZMBbomn9sdbzsgx~Vm5DZgvt5sPMQ7wSCSYzekAQ6lUpGJBREmkEwXS6wZsWReA1lhPHkvXGiPDrCm7qA3QO~yUxNEqR8vCzP98qPQYtJqpjoSro8wpc20vdBZOUN0pYlPpFEdngcDet2Ky0a-zlOop6nCdyc8~ZdTjJ~Gj6jHpVIvEQ1rGoiXlGTI39u0IVbpJydGAoJDVR~jil5QQgtNUepYNa7QhnKpjQg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
      },
      ConversationType.ReceivedGift: {
        "title": "notification.session.receivedGifts",
        "navigator": "/received-gift-screen",
        "avatarUrl":
            "https://s3-alpha-sig.figma.com/img/fb49/b1e2/8eaf82b907fcdd2eca0ee3a402e559a4?Expires=1615161600&Signature=U3XkByNV-csss~98xk~YRZxszuT61TayKMORU0~HMz10ocmv~HN~Qg8sZYAiuiqpa4lRExxtEWiSoe4ZH0ulqpxPXAork2zq364gSPwb4iay7FUynZMBbomn9sdbzsgx~Vm5DZgvt5sPMQ7wSCSYzekAQ6lUpGJBREmkEwXS6wZsWReA1lhPHkvXGiPDrCm7qA3QO~yUxNEqR8vCzP98qPQYtJqpjoSro8wpc20vdBZOUN0pYlPpFEdngcDet2Ky0a-zlOop6nCdyc8~ZdTjJ~Gj6jHpVIvEQ1rGoiXlGTI39u0IVbpJydGAoJDVR~jil5QQgtNUepYNa7QhnKpjQg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
      },
      ConversationType.LuckyHaki: {
        "title": "notification.session.luckyHaki",
        "navigator": "/lucky-haki-notification-screen",
        "avatarUrl":
            "https://s3-alpha-sig.figma.com/img/fb49/b1e2/8eaf82b907fcdd2eca0ee3a402e559a4?Expires=1615161600&Signature=U3XkByNV-csss~98xk~YRZxszuT61TayKMORU0~HMz10ocmv~HN~Qg8sZYAiuiqpa4lRExxtEWiSoe4ZH0ulqpxPXAork2zq364gSPwb4iay7FUynZMBbomn9sdbzsgx~Vm5DZgvt5sPMQ7wSCSYzekAQ6lUpGJBREmkEwXS6wZsWReA1lhPHkvXGiPDrCm7qA3QO~yUxNEqR8vCzP98qPQYtJqpjoSro8wpc20vdBZOUN0pYlPpFEdngcDet2Ky0a-zlOop6nCdyc8~ZdTjJ~Gj6jHpVIvEQ1rGoiXlGTI39u0IVbpJydGAoJDVR~jil5QQgtNUepYNa7QhnKpjQg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
      },
    };
  }
}
