import 'package:flutter/material.dart';
import 'package:flutterbase/configs/general.config.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/@services/utils/firebase.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/message/converstion_item.widget.dart';

import 'conversation_config.dart';
import 'notification_list_module.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getIt<CommunicateService>().context = context;

    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        centerTitle: false,
        title: CustomText(
          "notification.title",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xff1c1c1c),
      ),
      body: ListView.builder(
        padding: EdgeInsets.fromLTRB(
          SizeConfig.safeBlockHorizontal * 4,
          SizeConfig.safeBlockHorizontal * 5,
          SizeConfig.safeBlockHorizontal * 4,
          SizeConfig.safeBlockHorizontal * 5 +
              SizeConfig.paddingBottom +
              kBottomNavigationBarHeight,
        ),
        itemCount: ConversationType.values.length,
        itemBuilder: (BuildContext context, int index) {
          if (index == 1) return SizedBox();
          return Padding(
            padding: const EdgeInsets.only(bottom: 25),
            child: GestureDetector(
              onTap: () {
                print(ConversationConfig.conversationConfig()[
                    ConversationType.values[index]]["navigator"]);
                Navigator.of(context).pushNamed(
                    ConversationConfig.conversationConfig()[
                        ConversationType.values[index]]["navigator"]);
              },
              child: ConversationItemWidget(
                isNew: index > 0,
                title: ConversationConfig.conversationConfig()[
                    ConversationType.values[index]]["title"],
                avatarUrl: ConversationConfig.conversationConfig()[
                    ConversationType.values[index]]["avatarUrl"],
              ),
            ),
          );
        },
      ),
      // floatingActionButton: FlatButton(
      //   onPressed: () {
      //     Navigator.of(context).pushNamed("/more-screen");
      //   },
      //   child: Container(
      //     padding: EdgeInsets.all(10),
      //     decoration: BoxDecoration(
      //       shape: BoxShape.circle,
      //       color: Colors.blueAccent,
      //     ),
      //     child: Icon(Icons.navigate_next),
      //   ),
      // ),
    );
  }
}
