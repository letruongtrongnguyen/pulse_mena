import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/notification.model.dart';
import 'package:flutterbase/core/@services/services/notification.service.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/notification/friend_request_notification.widget.dart';
import 'package:flutterbase/widgets/notification/gift_notification.widget.dart';

class NotificationListModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return NotificationService.listener(
      builder: (context, notificationList, isLoaded) {
        if (!isLoaded)
          return Center(
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Color(0xff00FFE0)),
            ),
          );

        if (notificationList.length == 0) {
          return Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(bottom: kToolbarHeight),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image(
                  width: SizeConfig.safeBlockHorizontal * 10,
                  fit: BoxFit.fitWidth,
                  image: AssetImage(
                    AssetsPath.fromImagesCommon("empty_ic"),
                  ),
                ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 2),
                CustomText(
                  "common.noNotification",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          );
        }
        return ListView.builder(
          padding: EdgeInsets.only(
            top: 10,
            bottom: kBottomNavigationBarHeight,
          ),
          itemCount: notificationList.length,
          itemBuilder: (BuildContext ctx, int index) {
            if (notificationList[index].type == NotificationType.FRIEND_REQUEST)
              return Slidable(
                key: Key("notification list ${notificationList[index].key}"),
                actionPane: SlidableDrawerActionPane(),
                actionExtentRatio: 0.17,
                child: FriendRequestNotification(
                    notificationData: notificationList[index]),
                secondaryActions: <Widget>[
                  IconSlideAction(
                    color: Color(0xffA01F1F),
                    iconWidget: SvgPicture.asset(
                      AssetsPath.fromImagesSvg("delete_ic"),
                      width: SizeConfig.safeBlockHorizontal * 5,
                      color: Colors.white,
                    ),
                    onTap: () {
                      notificationList[index].remove();
                    },
                  ),
                ],
              );

            return Slidable(
              key: Key("notification list $index"),
              actionPane: SlidableDrawerActionPane(),
              actionExtentRatio: 0.17,
              child: GiftNotificationWidget(),
              secondaryActions: <Widget>[
                IconSlideAction(
                  color: Color(0xffA01F1F),
                  iconWidget: SvgPicture.asset(
                    AssetsPath.fromImagesSvg("delete_ic"),
                    width: SizeConfig.safeBlockHorizontal * 5,
                    color: Colors.white,
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }
}
