import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/search.provider.dart';
import 'package:flutterbase/screens/search/search_input/search_input.dart';

class SearchAppBar extends StatelessWidget implements PreferredSizeWidget {
  static const appBarSearchInputHeight = 36.0;

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Theme.of(context).appBarTheme.color,
      actions: [
        GestureDetector(
          onTap: () {
            if (getIt<SearchProvider>().isSearchValueEmpty.value) return;

            getIt<SearchProvider>().searchRoomData();
          },
          child: Container(
            color: Colors.transparent,
            alignment: AlignmentDirectional.centerEnd,
            padding: EdgeInsetsDirectional.fromSTEB(
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4.2,
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4.2,
            ),
            // color: Colors.blueGrey,
            child: SvgPicture.asset(
              AssetsPath.fromImagesSvg("search_ic"),
              color: Theme.of(context).appBarTheme.iconTheme.color,

              matchTextDirection: true,
            ),
          ),
        ),
      ],
      titleSpacing: 0,
      leading: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Container(
          color: Colors.transparent,
          alignment: AlignmentDirectional.centerStart,
          padding: EdgeInsetsDirectional.fromSTEB(
            SizeConfig.safeBlockHorizontal * 4,
            SizeConfig.safeBlockHorizontal * 4.5,
            0,
            SizeConfig.safeBlockHorizontal * 4.5,
          ),
          // color: Colors.blueGrey,
          child: SvgPicture.asset(
            AssetsPath.fromImagesSvg("arrow_back_ic"),
            color: Theme.of(context).appBarTheme.iconTheme.color,
            matchTextDirection: true,
          ),
        ),
      ),
      centerTitle: true,
      title: Hero(
        tag: "searchInputContainer",
        child: Material(
          type: MaterialType.transparency,
          child: Container(
            padding: EdgeInsetsDirectional.only(
              start: SizeConfig.safeBlockHorizontal * 4,
            ),
            height: appBarSearchInputHeight,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Color(0xff717171).withOpacity(0.2),
            ),
            child: _buildSearchTextField(context),
          ),
        ),
      ),
    );
  }

  Widget _buildSearchTextField(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: SearchInput(),
        ),
        ClearIcon(),
      ],
    );
  }
}

class ClearIcon extends StatelessWidget {
  final ValueNotifier isSearchValueEmpty =
      getIt<SearchProvider>().isSearchValueEmpty;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: isSearchValueEmpty,
      builder: (ctx, _, child) {
        if (isSearchValueEmpty.value) return SizedBox();

        return GestureDetector(
          onTap: () {
            getIt<SearchProvider>().resetSearchTextValue();
          },
          child: Container(
            color: Colors.transparent,
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.safeBlockHorizontal * 3,
              vertical: SizeConfig.safeBlockHorizontal * 2,
            ),
            child: SvgPicture.asset(
              AssetsPath.fromImagesSvg("clear_filled_ic"),
            ),
          ),
        );
      },
    );
  }
}
