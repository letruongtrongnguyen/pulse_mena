import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/screens/search/search_result_module.dart';
import 'package:flutterbase/widgets/my_scaffold/my_scaffold.widget.dart';

import 'search_app_bar/search_app_bar.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  @override
  void initState() {
    getIt<CommunicateService>().requestHideNavigationBar();
    super.initState();
  }

  @override
  void dispose() {
    getIt<CommunicateService>().requestShowNavigationBar();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // onPanDown: (_) {
      //   FocusScope.of(context).requestFocus(FocusNode());
      // },
      onVerticalDragUpdate: (details) {
        if (details.primaryDelta > 3) {
          FocusScope.of(context).unfocus();
        }
      },
      child: MyScaffold(
        appBar: SearchAppBar(),
        backgroundImagePath: AssetsPath.fromImagesCommon("background"),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        child: SearchResultModule(),
      ),
    );
  }
}
