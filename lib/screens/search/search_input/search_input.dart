import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/i18n/i18n.dart';
import 'package:flutterbase/providers/search.provider.dart';

class SearchInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextEditingController searchEditingController =
        getIt<SearchProvider>().searchEditingController;

    return TextField(
      autofocus: true,
      controller: searchEditingController,
      textCapitalization: TextCapitalization.none,
      cursorColor: Color(0xff00FFE0),
      cursorRadius: Radius.circular(8),
      style: TextStyle(fontSize: 14),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(bottom: 10),
        border: InputBorder.none,
        hintText: I18n.get("search.input"),
        hintStyle: TextStyle(
          color: Color(0xff717171),
          fontSize: 14,
        ),
      ),
    );
  }
}
