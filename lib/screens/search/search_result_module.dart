import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/search.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item_shimmer.dart';

class SearchResultModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ValueNotifier searchData = getIt<SearchProvider>().searchData;
    ValueNotifier loading = getIt<SearchProvider>().loading;
    ValueNotifier isSearchValueEmpty =
        getIt<SearchProvider>().isSearchValueEmpty;
    ValueNotifier prevSearchValue = getIt<SearchProvider>().prevSearchString;

    return AnimatedBuilder(
      animation: Listenable.merge(
          [searchData, loading, isSearchValueEmpty, prevSearchValue]),
      builder: (ctx, child) {
        if (loading.value) {
          return ListView.builder(
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            padding: EdgeInsets.only(
              top: SizeConfig.paddingTop +
                  kToolbarHeight +
                  SizeConfig.safeBlockHorizontal * 2,
              bottom: SizeConfig.safeBlockHorizontal * 2,
            ),
            itemCount: 10,
            itemBuilder: (ctx, index) {
              return Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.safeBlockHorizontal * 4,
                  vertical: SizeConfig.safeBlockHorizontal * 2,
                ),
                child: RoomItemShimmer(),
              );
            },
          );
        }

        if (!isSearchValueEmpty.value && searchData.value.length == 0) {
          return Container(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.only(
              top: SizeConfig.paddingTop +
                  kToolbarHeight +
                  SizeConfig.safeBlockHorizontal * 10,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image(
                  width: SizeConfig.safeBlockHorizontal * 10,
                  fit: BoxFit.fitWidth,
                  image: AssetImage(
                    AssetsPath.fromImagesCommon("empty_ic"),
                  ),
                ),
                SizedBox(height: SizeConfig.safeBlockHorizontal * 2),
                CustomText(
                  "common.noResult",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          );
        }

        if (isSearchValueEmpty.value) return SizedBox();

        return ListView.builder(
          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          addAutomaticKeepAlives: true,
          cacheExtent: 2,
          padding: EdgeInsets.only(
            top: SizeConfig.paddingTop +
                kToolbarHeight +
                SizeConfig.safeBlockHorizontal * 2,
            bottom: SizeConfig.safeBlockHorizontal * 2,
          ),
          itemCount: searchData.value.length + 1,
          itemBuilder: (ctx, index) {
            if (index == 0) {
              return Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.safeBlockHorizontal * 4,
                  vertical: 5,
                ),
                child: Row(
                  children: [
                    CustomText(
                      "${searchData.value.length} ",
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    CustomText(
                      "search.room",
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              );
            }

            index -= 1;

            return Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.safeBlockHorizontal * 4,
                vertical: SizeConfig.safeBlockHorizontal * 2,
              ),
              child: RoomItemWidget(
                roomData: searchData.value[index],
              ),
            );
          },
        );
      },
    );
  }
}
