import 'package:flutter/material.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/screens/discover/discover.screen.dart';
import 'package:flutterbase/screens/feed/feed.screen.dart';
import 'package:flutterbase/screens/more/more.screen.dart';
import 'package:flutterbase/screens/notification/notification.screen.dart';
import 'package:flutterbase/screens/room/room.screen.dart';
import 'package:flutterbase/widgets/bottom_nav_bar/custom_bottom_nav_bar.widget.dart';

class NavigationScreen extends StatefulWidget {
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  PageController pageController = PageController(
    initialPage: 4,
    keepPage: true,
  );

  Widget buildPageView() {
    return PageView(
      controller: pageController,
      physics: new NeverScrollableScrollPhysics(),
      onPageChanged: (index) {
        getIt<GeneralProvider>()
            .onActiveTabChange(MainScreenTabType.values[index]);
      },
      children: <Widget>[
        FeedScreen(),
        RoomScreen(),
        DiscoverScreen(),
        NotificationScreen(),
        MoreScreen(),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildPageView(),
      bottomNavigationBar: CustomBottomNavBar(
        pageController: pageController,
      ),
    );
  }
}
