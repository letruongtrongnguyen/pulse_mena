import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class HakiMailBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    getIt<CommunicateService>().context = context;

    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          "hakiMailBox.title",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xff1c1c1c),
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Padding(
          padding: EdgeInsetsDirectional.fromSTEB(
            SizeConfig.safeBlockHorizontal * 4,
            SizeConfig.safeBlockHorizontal * 4,
            SizeConfig.safeBlockHorizontal * 4,
            0,
          ),
          child: Column(
            children: [
              SizedBox(
                width: SizeConfig.safeBlockHorizontal * 90,
                child: CustomText(
                  "hakiMailBox.comingSoon",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              ),
              SizedBox(height: SizeConfig.safeBlockHorizontal * 20),
              Image.asset(
                AssetsPath.fromImagesCommon("coming_soon"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
