import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/screens/term_policy/term_policy_config.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';

class TermPolicyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          "termPolicy.title",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        centerTitle: true,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: Colors.transparent,
            alignment: AlignmentDirectional.centerStart,
            padding: EdgeInsetsDirectional.fromSTEB(
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4.5,
              0,
              SizeConfig.safeBlockHorizontal * 4.5,
            ),
            // color: Colors.blueGrey,
            child: SvgPicture.asset(
              AssetsPath.fromImagesSvg("arrow_back_ic"),
              matchTextDirection: true,
            ),
          ),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xff1c1c1c),
      ),
      backgroundColor: Colors.black,
      body: ListView.builder(
        padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 4),
        itemCount: TermPolicyConfig.termPolicyItemConfig().length,
        itemBuilder: (BuildContext ctx, int index) {
          return Padding(
            padding: EdgeInsets.only(bottom: 20),
            child: _buildTermPolicyItem(index: index),
          );
        },
      ),
    );
  }

  Widget _buildTermPolicyItem({int index}) {
    final TextStyle titleStyle = TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.bold,
    );
    final TextStyle contentStyle = TextStyle(
      fontSize: 16,
    );

    getType() {
      return TermPolicyType.values[index];
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            CustomText(
              "${index + 1}. ",
              style: titleStyle,
            ),
            CustomText(
              TermPolicyConfig.termPolicyItemConfig()[getType()]["title"],
              style: titleStyle,
            ),
          ],
        ),
        SizedBox(height: 5),
        CustomText(
          TermPolicyConfig.termPolicyItemConfig()[getType()]["content"],
          style: contentStyle,
        ),
      ],
    );
  }
}
