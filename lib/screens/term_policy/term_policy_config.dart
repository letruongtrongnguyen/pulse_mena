import 'package:flutter/material.dart';

enum TermPolicyType {
  Introduction,
  About,
  DataProtection,
  CollectYourData,
  UseYourData,
  ShareYourData,
  InternationalDataTransfers,
  Security,
  Retention,
  YourRights,
  ContactUs,
}

class TermPolicyConfig {
  static Map<TermPolicyType, Map<String, dynamic>> termPolicyItemConfig() {
    return {
      TermPolicyType.Introduction: {
        "title": "termPolicy.sessionTitle.introduction",
        "content": "termPolicy.content.introduction",
      },
      TermPolicyType.About: {
        "title": "termPolicy.sessionTitle.about",
        "content": "termPolicy.content.about",
      },
      TermPolicyType.DataProtection: {
        "title": "termPolicy.sessionTitle.dataProtection",
        "content": "termPolicy.content.dataProtection",
      },
      TermPolicyType.CollectYourData: {
        "title": "termPolicy.sessionTitle.collectYourData",
        "content": "termPolicy.content.collectYourData",
      },
      TermPolicyType.UseYourData: {
        "title": "termPolicy.sessionTitle.useYourData",
        "content": "termPolicy.content.useYourData",
      },
      TermPolicyType.ShareYourData: {
        "title": "termPolicy.sessionTitle.shareYourData",
        "content": "termPolicy.content.shareYourData",
      },
      TermPolicyType.InternationalDataTransfers: {
        "title": "termPolicy.sessionTitle.internationalDataTransfers",
        "content": "termPolicy.content.internationalDataTransfers",
      },
      TermPolicyType.Security: {
        "title": "termPolicy.sessionTitle.security",
        "content": "termPolicy.content.security",
      },
      TermPolicyType.Retention: {
        "title": "termPolicy.sessionTitle.retention",
        "content": "termPolicy.content.retention",
      },
      TermPolicyType.YourRights: {
        "title": "termPolicy.sessionTitle.yourRights",
        "content": "termPolicy.content.yourRights",
      },
      TermPolicyType.ContactUs: {
        "title": "termPolicy.sessionTitle.contactUs",
        "content": "termPolicy.content.contactUs",
      },
    };
  }
}
