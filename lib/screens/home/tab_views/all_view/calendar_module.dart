import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/models/broadcast.model.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:intl/intl.dart';

class CalendarModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ValueNotifier broadcastLoading = getIt<RoomProvider>().broadcastLoading;
    ValueNotifier broadcastData = getIt<RoomProvider>().broadcastData;

    return Container(
        // height: 130,
        width: SizeConfig.safeBlockHorizontal * 92,
        decoration: BoxDecoration(
          color: Theme.of(context).cardTheme.color,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 1),
              blurRadius: 5,
              color: Theme.of(context).cardTheme.shadowColor,
            )
          ],
        ),
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.safeBlockHorizontal * 4,
          vertical: SizeConfig.safeBlockHorizontal * 4,
        ),
        child: AnimatedBuilder(
          animation: Listenable.merge([broadcastLoading, broadcastData]),
          builder: (ctx, child) {
            if (broadcastLoading.value) return SizedBox();

            if (broadcastData.value.length == 0)
              return Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 15,
                ),
                alignment: Alignment.center,
                child: CustomText(
                  "home.calendar.empty",
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
              );

            return ListView.builder(
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              itemCount: broadcastData.value.length,
              itemBuilder: (ctx, index) {
                if (index > 2) return SizedBox();
                return Padding(
                  padding: EdgeInsets.only(top: index > 0 ? 10 : 0),
                  child: _buildCalendarItem(
                    data: broadcastData.value[index],
                  ),
                );
              },
            );
          },
        ));
  }

  Widget _buildCalendarItem({BroadcastModel data}) {
    return GestureDetector(
      onTap: () {
        getIt<CommunicateService>()
            .navigate(route: "/audio-room-screen", args: {
          "user-id": data.broadcasterId,
          "room-id": data.roomId,
        });
      },
      child: Container(
        color: Colors.transparent,
        child: Row(
          children: [
            CustomText(
              DateFormat('HH:mm').format(
                  new DateTime.fromMillisecondsSinceEpoch(
                      data.timestamp * 1000)),
              style: TextStyle(
                color: Color(0xff3EE4FF),
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(width: SizeConfig.safeBlockHorizontal * 5),
            Container(
              constraints: BoxConstraints(
                maxWidth: SizeConfig.safeBlockHorizontal * 65,
              ),
              child: CustomText(
                Formatter.clearZeroWidthSpace(
                  Formatter.capitalize(data.description),
                ),
                // roomData.description,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                softWrap: false,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
