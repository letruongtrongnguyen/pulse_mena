import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/screens/home/tab_views/all_view/calendar_module.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/browse_more/browse_more.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item_shimmer.dart';
import 'package:flutterbase/widgets/room_item/room_square_item.widget.dart';
import 'package:flutterbase/widgets/session_title/session_title.dart';

class AllView extends StatelessWidget {
  final String listKey;
  final ScrollController controller;

  const AllView({
    Key key,
    this.listKey,
    this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ValueNotifier popularRoomData = getIt<RoomProvider>().popularRoomData;
    ValueNotifier popularRoomLoading = getIt<RoomProvider>().popularRoomLoading;
    ValueNotifier newRoomData = getIt<RoomProvider>().newRoomData;
    ValueNotifier newRoomLoading = getIt<RoomProvider>().newRoomLoading;

    Future<void> _onRefresh() async {
      await getIt<RoomProvider>().fetchNewRoomData();
      await getIt<RoomProvider>().fetchPopularRoomData();
    }

    return RefreshIndicator(
      onRefresh: _onRefresh,
      displacement: SizeConfig.paddingTop +
          kToolbarHeight * 2 +
          SizeConfig.safeBlockHorizontal * 5,
      color: Theme.of(context).iconTheme.color,
      child: CustomScrollView(
        key: PageStorageKey(listKey),
        physics: AlwaysScrollableScrollPhysics(),
        controller: controller,
        slivers: [
          SliverToBoxAdapter(
            child: SizedBox(
              height: SizeConfig.paddingTop +
                  kToolbarHeight * 2 +
                  SizeConfig.safeBlockHorizontal * 8,
            ),
          ),
          SliverToBoxAdapter(
            child: Align(
              child: Padding(
                padding: EdgeInsets.only(
                  bottom: 8,
                ),
                child: CustomText(
                  "home.calendar",
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Align(
              child: CustomText(
                "home.topTalents",
                style: Theme.of(context).textTheme.headline2,
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(
                vertical: SizeConfig.safeBlockHorizontal * 6,
                horizontal: SizeConfig.safeBlockHorizontal * 4,
              ),
              child: CalendarModule(),
            ),
          ),
          SliverToBoxAdapter(
            child: Align(
              child: CustomText(
                "home.session.title.mostPopular",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline1,
              ),
            ),
          ),
          AnimatedBuilder(
            animation: Listenable.merge([popularRoomData, popularRoomLoading]),
            builder: (ctx, child) {
              if (popularRoomData.value.length < 3) {
                return SliverToBoxAdapter(
                  child: SizedBox(),
                );
              }

              return SliverToBoxAdapter(
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(
                    SizeConfig.safeBlockHorizontal * 4,
                    SizeConfig.safeBlockHorizontal * 4,
                    SizeConfig.safeBlockHorizontal * 4,
                    SizeConfig.safeBlockHorizontal,
                  ),
                  child: Row(
                    children: [
                      RoomSquareItem(
                        width: SizeConfig.safeBlockHorizontal * 55,
                        height: SizeConfig.safeBlockHorizontal * 55,
                        roomData: popularRoomData?.value[0],
                      ),
                      SizedBox(
                        width: SizeConfig.safeBlockHorizontal * 2,
                      ),
                      Column(
                        children: [
                          RoomSquareItem(
                            width: SizeConfig.safeBlockHorizontal * 35,
                            height: SizeConfig.safeBlockHorizontal * 26.5,
                            roomData: popularRoomData?.value[1],
                          ),
                          SizedBox(
                            height: SizeConfig.safeBlockHorizontal * 2,
                          ),
                          RoomSquareItem(
                            width: SizeConfig.safeBlockHorizontal * 35,
                            height: SizeConfig.safeBlockHorizontal * 26.5,
                            roomData: popularRoomData?.value[2],
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              );
            },
          ),
          AnimatedBuilder(
            animation: Listenable.merge([popularRoomData, popularRoomLoading]),
            builder: (ctx, child) {
              if (popularRoomLoading.value &&
                  popularRoomData.value.length == 0) {
                return SliverToBoxAdapter(
                  child: SizedBox(),
                );
              }

              return SliverList(
                delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                    /// To convert this infinite list to a list with "n" no of items,
                    /// uncomment the following line:
                    /// if (index > n) return null;
                    ///

                    if (index < 3 && popularRoomData.value.length > 2)
                      return SizedBox();

                    return AnimatedOpacity(
                      duration: Duration(milliseconds: 200),
                      opacity: popularRoomLoading.value ? 0.7 : 1.0,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemWidget(
                          roomData: popularRoomData.value[index],
                        ),
                        // child: RoomItemShimmer(),
                      ),
                    );

                    return SizedBox();
                  },
                  childCount: popularRoomData.value.length,
                ),
              );
            },
          ),
          SliverToBoxAdapter(
            child: ValueListenableBuilder(
              valueListenable: popularRoomLoading,
              builder: (ctx, loading, child) {
                if (loading)
                  return Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemShimmer(),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemShimmer(),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemShimmer(),
                      ),
                    ],
                  );

                return SizedBox();
              },
            ),
          ),
          SliverToBoxAdapter(
            child: ValueListenableBuilder(
              valueListenable: popularRoomLoading,
              builder: (ctx, loading, child) {
                if (getIt<RoomProvider>().popularRoomData.value.length /
                            (getIt<RoomProvider>().popularRoomDataCurrentPage *
                                10) <
                        1 &&
                    // getIt<RoomProvider>().popularRoomDataCurrentPage != 1 &&
                    !loading)
                  return Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: 10),
                    child: CustomText(
                      "common.noMoreRoom",
                      style: TextStyle(
                        color: Color(0xffb4b4b4),
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  );

                if (!loading)
                  return Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: SizeConfig.safeBlockHorizontal * 4,
                      horizontal: SizeConfig.safeBlockHorizontal * 4,
                    ),
                    child: Align(
                      alignment: AlignmentDirectional.centerEnd,
                      child: BrowseMoreWidget(
                        onTap: () {
                          getIt<RoomProvider>().fetchPopularRoomData(
                            page: getIt<RoomProvider>()
                                    .popularRoomDataCurrentPage +
                                1,
                          );
                        },
                      ),
                    ),
                  );

                return SizedBox();
              },
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsetsDirectional.fromSTEB(
                SizeConfig.safeBlockHorizontal * 4,
                30,
                SizeConfig.safeBlockHorizontal * 4,
                15,
              ),
              child: SessionTitle(
                title: "home.session.title.newRooms",
              ),
            ),
          ),
          AnimatedBuilder(
            animation: Listenable.merge([newRoomData, newRoomLoading]),
            builder: (ctx, child) {
              if (newRoomLoading.value && newRoomData.value.length == 0) {
                return SliverToBoxAdapter(
                  child: SizedBox(),
                );
              }

              return SliverList(
                delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                    /// To convert this infinite list to a list with "n" no of items,
                    /// uncomment the following line:
                    /// if (index > n) return null;
                    ///
                    // if (recentlyRoomData.value.length == 0) return null;
                    return AnimatedOpacity(
                      duration: Duration(milliseconds: 200),
                      opacity: newRoomLoading.value ? 0.7 : 1.0,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemWidget(
                          roomData: newRoomData.value[index],
                        ),
                        // child: RoomItemShimmer(),
                      ),
                    );
                  },
                  childCount: newRoomData.value.length,
                ),
              );
            },
          ),
          SliverToBoxAdapter(
            child: ValueListenableBuilder(
              valueListenable: newRoomLoading,
              builder: (ctx, loading, child) {
                if (loading)
                  return Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemShimmer(),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemShimmer(),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemShimmer(),
                      ),
                    ],
                  );

                return SizedBox();
              },
            ),
          ),
          SliverToBoxAdapter(
            child: ValueListenableBuilder(
              valueListenable: newRoomLoading,
              builder: (ctx, loading, child) {
                if (getIt<RoomProvider>().newRoomData.value.length /
                            (getIt<RoomProvider>().newRoomDataCurrentPage *
                                10) <
                        1 &&
                    // getIt<RoomProvider>().newRoomDataCurrentPage != 1 &&
                    !loading)
                  return Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: 10),
                    child: CustomText(
                      "common.noMoreRoom",
                      style: TextStyle(
                        color: Color(0xffb4b4b4),
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  );

                if (!loading)
                  return Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: SizeConfig.safeBlockHorizontal * 4,
                      horizontal: SizeConfig.safeBlockHorizontal * 4,
                    ),
                    child: Align(
                      alignment: AlignmentDirectional.centerEnd,
                      child: BrowseMoreWidget(
                        onTap: () {
                          getIt<RoomProvider>().fetchNewRoomData(
                            page: getIt<RoomProvider>().newRoomDataCurrentPage +
                                1,
                          );
                        },
                      ),
                    ),
                  );

                return SizedBox();
              },
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height:
                  SizeConfig.paddingBottom + kBottomNavigationBarHeight + 50,
            ),
          ),
        ],
      ),
    );
  }
}
