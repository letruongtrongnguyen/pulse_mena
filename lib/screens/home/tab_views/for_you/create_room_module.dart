import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item_shimmer.dart';

class CreateRoomModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ValueNotifier myRoomData = getIt<RoomProvider>().myRoomData;
    ValueNotifier myRoomLoading = getIt<RoomProvider>().myRoomLoading;

    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.safeBlockHorizontal * 4,
        vertical: SizeConfig.safeBlockHorizontal * 2,
      ),
      child: AnimatedBuilder(
          animation: Listenable.merge([myRoomData, myRoomLoading]),
          builder: (ctx, child) {
            if (myRoomLoading.value) return RoomItemShimmer();

            if (myRoomData.value == null) return BuildCreate();

            return RoomItemWidget(roomData: myRoomData.value);
          }),
    );
  }
}

class BuildCreate extends StatefulWidget {
  @override
  _BuildCreateState createState() => _BuildCreateState();
}

class _BuildCreateState extends State<BuildCreate>
    with TickerProviderStateMixin {
  Animation _arrowAnimation;
  AnimationController _arrowAnimationController;

  @override
  void initState() {
    super.initState();

    _arrowAnimationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );
    _arrowAnimation =
        Tween(begin: -5.0, end: 5.0).animate(_arrowAnimationController);

    _arrowAnimationController.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        _arrowAnimationController.repeat(reverse: true);
      }
    });

    _arrowAnimationController.forward();
  }

  @override
  void dispose() {
    _arrowAnimationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      textDirection: TextDirection.rtl,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsetsDirectional.only(
                start: SizeConfig.safeBlockHorizontal * 5,
              ),
              child: Image.asset(
                AssetsPath.fromImagesCommon("marvel"),
                matchTextDirection: true,
                width: SizeConfig.safeBlockHorizontal * 70,
                fit: BoxFit.fitWidth,
              ),
            ),
            GestureDetector(
              onTap: () {
                getIt<CommunicateService>().navigate(route: "/create-room");
              },
              child: Stack(
                alignment: Alignment.center,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Image.asset(
                      AssetsPath.fromImagesCommon("create_room_bg"),
                      matchTextDirection: true,
                      width: SizeConfig.safeBlockHorizontal * 92,
                      height: SizeConfig.safeBlockHorizontal * 92 / 3.4,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      CustomText(
                        "home.createRoom",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(width: 20),
                      Image.asset(
                        AssetsPath.fromImagesCommon("create_room_ic"),
                        matchTextDirection: true,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        PositionedDirectional(
          top: 40,
          end: SizeConfig.safeBlockHorizontal * 5,
          child: AnimatedBuilder(
            animation: _arrowAnimationController,
            builder: (context, child) {
              return Transform.translate(
                offset: Offset(0.0, _arrowAnimation.value),
                child: Image.asset(
                  AssetsPath.fromImagesCharacterAction("haki_funny"),
                  width: SizeConfig.safeBlockHorizontal * 30,
                  fit: BoxFit.contain,
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
