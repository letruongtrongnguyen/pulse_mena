import 'package:flutter/material.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/utils/formatter.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/browse_more/browse_more.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item.widget.dart';
import 'package:flutterbase/widgets/room_item/room_item_shimmer.dart';
import 'package:flutterbase/widgets/session_title/session_title.dart';

import 'create_room_module.dart';

class ForYou extends StatelessWidget {
  final String listKey;
  final ScrollController controller;

  const ForYou({
    Key key,
    this.listKey,
    this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ValueNotifier recentlyRoomData = getIt<RoomProvider>().recentlyRoomData;
    ValueNotifier recentlyRoomLoading =
        getIt<RoomProvider>().recentlyRoomLoading;

    Future<void> _onRefresh() async {
      await getIt<RoomProvider>().fetchRecentlyRoomData();
    }

    return RefreshIndicator(
      onRefresh: _onRefresh,
      displacement: SizeConfig.paddingTop +
          kToolbarHeight * 2 +
          SizeConfig.safeBlockHorizontal * 5,
      color: Theme.of(context).iconTheme.color,
      child: CustomScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        key: PageStorageKey(listKey),
        controller: controller,
        slivers: [
          SliverToBoxAdapter(
            child: SizedBox(
              height: SizeConfig.paddingTop +
                  kToolbarHeight * 2 +
                  SizeConfig.safeBlockHorizontal * 8,
            ),
          ),
          SliverToBoxAdapter(
            child: Align(
              child: Padding(
                padding: EdgeInsets.only(
                  bottom: 8,
                ),
                child: CustomText(
                  "home.welcomeBack",
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Align(
              child: ValueListenableBuilder(
                valueListenable: getIt<MeProvider>().data,
                builder: (context, data, child) {
                  return Container(
                    constraints: BoxConstraints(
                      maxWidth: SizeConfig.safeBlockHorizontal * 70,
                    ),
                    child: CustomText(
                      Formatter.clearZeroWidthSpace(
                        Formatter.capitalize(data?.fullName.toString()),
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      softWrap: false,
                      style: Theme.of(context).textTheme.headline2,
                    ),
                  );
                },
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: CreateRoomModule(),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsetsDirectional.fromSTEB(
                SizeConfig.safeBlockHorizontal * 4,
                30,
                SizeConfig.safeBlockHorizontal * 4,
                15,
              ),
              child: SessionTitle(
                title: "home.session.title.recently",
              ),
            ),
          ),
          AnimatedBuilder(
            animation:
                Listenable.merge([recentlyRoomData, recentlyRoomLoading]),
            builder: (ctx, child) {
              return SliverList(
                delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                    return AnimatedOpacity(
                      duration: Duration(milliseconds: 200),
                      opacity: recentlyRoomLoading.value ? 0.7 : 1.0,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemWidget(
                          roomData: recentlyRoomData.value[index],
                        ),
                        // child: RoomItemShimmer(),
                      ),
                    );
                  },
                  childCount: recentlyRoomData.value.length,
                ),
              );
            },
          ),
          SliverToBoxAdapter(
            child: ValueListenableBuilder(
              valueListenable: recentlyRoomLoading,
              builder: (ctx, loading, child) {
                if (loading)
                  return Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemShimmer(),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemShimmer(),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.safeBlockHorizontal * 1.5,
                          horizontal: SizeConfig.safeBlockHorizontal * 4,
                        ),
                        child: RoomItemShimmer(),
                      ),
                    ],
                  );

                return SizedBox();
              },
            ),
          ),
          SliverToBoxAdapter(
            child: ValueListenableBuilder(
              valueListenable: recentlyRoomLoading,
              builder: (ctx, loading, child) {
                if (getIt<RoomProvider>().recentlyRoomData.value.length /
                            (getIt<RoomProvider>().recentlyRoomDataCurrentPage *
                                10) <
                        1 &&
                    // getIt<RoomProvider>().recentlyRoomDataCurrentPage != 0 &&
                    !loading)
                  return Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: 10),
                    child: CustomText(
                      "common.noMoreRoom",
                      style: TextStyle(
                        color: Color(0xffb4b4b4),
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  );

                if (!loading)
                  return Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: SizeConfig.safeBlockHorizontal * 4,
                      horizontal: SizeConfig.safeBlockHorizontal * 4,
                    ),
                    child: Align(
                      alignment: AlignmentDirectional.centerEnd,
                      child: BrowseMoreWidget(
                        onTap: () {
                          getIt<RoomProvider>().fetchRecentlyRoomData(
                              page: getIt<RoomProvider>()
                                      .recentlyRoomDataCurrentPage +
                                  1);
                        },
                      ),
                    ),
                  );

                return SizedBox();
              },
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height:
                  SizeConfig.paddingBottom + kBottomNavigationBarHeight + 50,
            ),
          ),
        ],
      ),
    );
  }
}
