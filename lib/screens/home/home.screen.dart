import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterbase/configs/size.config.dart';
import 'package:flutterbase/core/@services/services/communicate.service.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/i18n/i18n.dart';
import 'package:flutterbase/core/utils/assets_path.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:flutterbase/providers/search.provider.dart';
import 'package:flutterbase/screens/search/search.screen.dart';
import 'package:flutterbase/widgets/basic/text.widget.dart';
import 'package:flutterbase/widgets/my_scaffold/my_scaffold.widget.dart';
import 'package:flutterbase/widgets/others/notification_ic.widget.dart';

import 'tab_views/all_view/all_view.dart';
import 'tab_views/for_you/for_you.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  TabController _tabController;
  ScrollController forYouScrollController = ScrollController();
  ScrollController allScrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2, initialIndex: 0);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getIt<RoomProvider>().fetchBroadcastData();
      getIt<RoomProvider>().fetchRecentlyRoomData();
      getIt<RoomProvider>().fetchNewRoomData();
      getIt<RoomProvider>().fetchPopularRoomData();
      // getIt<RoomProvider>().fetchMyRoomData();
    });

    // getIt<RoomProvider>().listenSocket();
  }

  @override
  void dispose() {
    _tabController.dispose();
    allScrollController.dispose();
    forYouScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    getIt<CommunicateService>().context = context;

    return MyScaffold(
      appBar: AppBar(
        title: Row(
          children: [
            CustomText(
              "home.title",
              style: Theme.of(context).appBarTheme.textTheme.headline1,
            ),
            Spacer(),
            Hero(
              tag: "searchInputContainer",
              child: Material(
                type: MaterialType.transparency,
                child: Container(
                  color: Theme.of(context).primaryColor,
                ),
              ),
            )
          ],
        ),
        // backgroundColor: Color(0xff1c1c1c).withOpacity(0.96),
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.transparent,
        shadowColor: Theme.of(context).brightness == Brightness.light
            ? Theme.of(context).appBarTheme.shadowColor
            : Colors.transparent,
        flexibleSpace: Container(
          width: SizeConfig.safeBlockHorizontal * 100,
          height: SizeConfig.paddingTop + kToolbarHeight * 2,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Theme.of(context).appBarTheme.color,
                Theme.of(context).appBarTheme.color.withOpacity(
                    Theme.of(context).brightness == Brightness.light ? 1 : 0.7),
              ],
            ),
          ),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              // Navigator.of(context).pushNamed("/search-screen");
              getIt<SearchProvider>().searchEditingController.clear();

              Navigator.push(
                context,
                PageRouteBuilder(
                  pageBuilder: (c, a1, a2) => SearchScreen(),
                  // transitionsBuilder: (c, anim, a2, child) =>
                  //     FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 0),
                ),
              );
            },
            child: Container(
              color: Colors.transparent,
              alignment: AlignmentDirectional.centerEnd,
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.safeBlockHorizontal * 2,
                vertical: SizeConfig.safeBlockHorizontal * 4.2,
              ),
              // color: Colors.blueGrey,
              child: SvgPicture.asset(
                AssetsPath.fromImagesSvg("search_ic"),
                color: Theme.of(context).appBarTheme.iconTheme.color,
                matchTextDirection: true,
              ),
            ),
          ),
          NotificationIconWidget(
            padding: EdgeInsetsDirectional.fromSTEB(
              SizeConfig.safeBlockHorizontal * 2,
              SizeConfig.safeBlockHorizontal * 4.2,
              SizeConfig.safeBlockHorizontal * 4,
              SizeConfig.safeBlockHorizontal * 4.2,
            ),
          ),
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(kToolbarHeight),
          child: Theme(
            data: ThemeData(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
            ),
            child: TabBar(
              controller: _tabController,
              labelStyle: TextStyle(
                fontFamily: "Euclid Circular A",
                fontSize: 14 * SizeConfig.screenWidth / 375,
                fontWeight: FontWeight.w500,
              ),
              labelColor: Theme.of(context).accentColor,
              unselectedLabelStyle: TextStyle(
                fontFamily: "Euclid Circular A",
                fontSize: 14 * SizeConfig.screenWidth / 375,
                fontWeight: FontWeight.w300,
                color: Colors.amber,
              ),
              unselectedLabelColor: Color(0xffb4b4b4),

              labelPadding: EdgeInsets.zero,
              isScrollable: true,
              onTap: (index) {
                if (index == 0 && !_tabController.indexIsChanging)
                  forYouScrollController.animateTo(
                    0,
                    duration: Duration(milliseconds: 500),
                    curve: Curves.easeInOut,
                  );
                if (index == 1 && !_tabController.indexIsChanging)
                  allScrollController.animateTo(
                    0,
                    duration: Duration(milliseconds: 500),
                    curve: Curves.easeInOut,
                  );
              },
              // indicatorSize: TabBarIndicatorSize.label,
              indicator: UnderlineTabIndicator(
                insets: EdgeInsets.symmetric(
                  horizontal: SizeConfig.safeBlockHorizontal * 4,
                ),
                borderSide: BorderSide(
                  color: Theme.of(context).accentColor,
                ),
              ),
              indicatorPadding: EdgeInsets.zero,
              tabs: [
                SizedBox(
                  width: SizeConfig.safeBlockHorizontal * 20,
                  child: Tab(
                    text: I18n.get("home.tabbar.title.all"),
                  ),
                ),
                SizedBox(
                  width: SizeConfig.safeBlockHorizontal * 20,
                  child: Tab(
                    text: I18n.get("home.tabbar.title.forYou"),
                  ),
                ),
              ],
            ),
          ),
        ),
        automaticallyImplyLeading: false,
      ),
      extendBodyBehindAppBar: true,
      backgroundImagePath: AssetsPath.fromImagesCommon("background"),
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      child: TabBarView(
        controller: _tabController,
        // physics: NeverScrollableScrollPhysics(),
        children: [
          AllView(
            listKey: "all",
            controller: allScrollController,
          ),
          ForYou(
            listKey: "for_you",
            controller: forYouScrollController,
          ),
        ],
      ),
      // floatingActionButton: FlatButton(
      //   onPressed: () {
      //     Navigator.of(context).pushNamed("/discover-screen");
      //   },
      //   onLongPress: () {
      //     final textDirection = getIt<GeneralProvider>().textDirection.value;
      //     final lang = getIt<GeneralProvider>().lang.value;
      //
      //     getIt<GeneralProvider>().toggleTheme();
      //
      //     // if (textDirection == TextDirection.ltr)
      //     //   getIt<GeneralProvider>().setTextDirection(TextDirection.rtl);
      //     // else
      //     //   getIt<GeneralProvider>().setTextDirection(TextDirection.ltr);
      //     //
      //     // if (lang == "en")
      //     //   getIt<GeneralProvider>().setLanguage("ar");
      //     // else
      //     //   getIt<GeneralProvider>().setLanguage("en");
      //
      //     // AdaptiveTheme.of(context).toggleThemeMode();
      //   },
      //   child: Container(
      //     padding: EdgeInsets.all(10),
      //     decoration: BoxDecoration(
      //       shape: BoxShape.circle,
      //       color: Colors.blueAccent,
      //     ),
      //     child: Icon(Icons.navigate_next),
      //   ),
      // ),
    );
  }
}
